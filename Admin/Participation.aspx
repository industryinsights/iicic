﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site.master" AutoEventWireup="false" CodeFile="Participation.aspx.vb" Inherits="Admin_Participation" %>

<%@ Register Assembly="DevExpress.Web.v18.1, Version=18.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="container-fluid">
        <div class="bg-content">
            <h2 class="text-center page-header">Organization Participation</h2>
            <div class="text-center">
                <table class="table-condensed centered">
                    <tr>
                        <td>
                            <asp:DropDownList ID="ddl_client" runat="server" CssClass="form-control"></asp:DropDownList>
                        </td>
                    </tr>
                </table>

            </div>
            <dx:ASPxGridViewExporter ID="ASPxGridViewExporter1" GridViewID="gv_participation" runat="server"></dx:ASPxGridViewExporter>
            <dx:ASPxGridView ID="gv_participation" runat="server" CssClass="centered" DataSourceID="SqlDataSource1">
                <SettingsPager PageSize="25">
                </SettingsPager>
                <%--<Settings  ShowGroupPanel="True" />--%>
                <%--<SettingsSearchPanel Visible="True" />--%>
                <Settings ShowGroupPanel="True" />
                <SettingsSearchPanel Visible="True" />
            </dx:ASPxGridView>
            <div class="row text-center">
                <asp:Button ID="btn_export" runat="server" Text="Export" CssClass="btn btn-primary" />
            </div>
            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:Website1DatabaseAuthentication %>" SelectCommand=""></asp:SqlDataSource>
        </div>
    </div>
</asp:Content>

