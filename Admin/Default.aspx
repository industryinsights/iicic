﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Default.aspx.vb" Inherits="Admin_Default" %>

<%@ Register assembly="DevExpress.Web.v18.1, Version=18.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web" tagprefix="dx" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <dx:ASPxFileManager ID="ASPxFileManager1" runat="server">
                <settings rootfolder="~/Surveys/Comp/Imports" thumbnailfolder="~/Thumb/" />
                <SettingsFileList View="Details">
                </SettingsFileList>
                <SettingsEditing AllowCopy="True" AllowCreate="True" AllowDelete="True" AllowDownload="True" AllowMove="True" AllowRename="True" />
                <SettingsUpload>
                    <AdvancedModeSettings EnableMultiSelect="True">
                    </AdvancedModeSettings>
                </SettingsUpload>
            </dx:ASPxFileManager>
        </div>
    </form>
</body>
</html>
