﻿
Imports DevExpress.Export
Imports DevExpress.XtraPrinting

Partial Class Admin_Participation
    Inherits System.Web.UI.Page
    Protected Sub Page_Init(sender As Object, e As EventArgs) Handles Me.Init
        Dim cs As String = ConfigurationManager.ConnectionStrings("Website1DatabaseAuthentication").ToString
        SqlDataSource1.ConnectionString = cs
        PopulateClients()
        If ddl_client.Items.Count = 1 Then
            ddl_client.Visible = False
            ddl_client.SelectedIndex = 0
        End If
        SqlDataSource1.SelectCommand = String.Format("spAdminParticipation_{0}", ddl_client.SelectedValue)
        SqlDataSource1.DataBind()
        gv_participation.DataBind()
    End Sub
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load


    End Sub
    Sub PopulateClients()
        Dim items As List(Of ListItem) = PortalHelper.GetParticipationDDLList(Session("UserId"))
        For Each i In items
            ddl_client.Items.Add(New ListItem(i.Text, i.Value))
        Next
    End Sub
    Protected Sub ddl_client_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddl_client.SelectedIndexChanged
        SqlDataSource1.SelectCommand = String.Format("spAdminParticipation_{0}", ddl_client.SelectedValue)
        SqlDataSource1.DataBind()
        gv_participation.DataBind()
    End Sub
    Protected Sub btn_export_Click(sender As Object, e As EventArgs) Handles btn_export.Click
        ASPxGridViewExporter1.WriteXlsxToResponse(New XlsxExportOptionsEx With {.ExportType = ExportType.WYSIWYG})
    End Sub
End Class
