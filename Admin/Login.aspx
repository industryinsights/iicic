﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site.master" AutoEventWireup="false" CodeFile="Login.aspx.vb" Inherits="Admin_Login" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="container-fluid">
        <div class="bg-content">
            <asp:Panel ID="Panel1" runat="server" DefaultButton="btn_login">
                <div class="row">
                    <div class="col-md-12 text-center">

                        <asp:Label ID="lbl_systemMessage" runat="server" CssClass="text-danger text-bold" Font-Size="16pt"></asp:Label>

                    </div>
                </div>
                <div class="row">
                    <div class="col-md-5 col-md-offset-3">
                        <h3>Administrative Log in<span class="sub-text"> </span></h3>
                        <div><span class="sub-text" id="h-login">To protect your data&#39;s confidentiality, your association credentials are not linked to this site.</span></div>
                        <div class="border-gray login-box right">
                            <table class="table">
                                <tr>
                                    <td class="col-lg-1">Email</td>
                                    <td class="col-lg-3">
                                        <asp:TextBox ID="txt_username" runat="server" CssClass="form-control"></asp:TextBox>
                                    </td>
                                    <td class="col-lg-1">
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txt_username" CssClass="text-danger" ErrorMessage="User ID is required." ValidationGroup="login">*</asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="col-lg-1">Password</td>
                                    <td class="col-lg-3">
                                        <asp:TextBox ID="txt_password" runat="server" CssClass="form-control" TextMode="Password"></asp:TextBox>
                                    </td>
                                    <td class="col-lg-1">
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txt_password" CssClass="text-danger" ErrorMessage="Password is required." ValidationGroup="login">*</asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="col-lg-1">

                                        <a href="AccountRecovery.aspx">Forgot password?</a></td>
                                    <td class="text-left" colspan="2">&nbsp;<asp:Button ID="btn_login" runat="server" CssClass="btn btn-default" Text="Sign in" ValidationGroup="login" Width="100px" />

                                    </td>
                                </tr>
                            </table>

                        </div>
                    </div>
                </div>
                <div class="row">
                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="alert-danger" ValidationGroup="login" />
                </div>
                <div class="row">
                    <p class="text-center">
                        <a href="../privacy.html" target="_blank">Terms and Conditions and Privacy Policy</a>
                    </p>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>

