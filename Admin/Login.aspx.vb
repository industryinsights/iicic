﻿
Imports Models

Partial Class Admin_Login
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Session("IsPortalAdmin") = Nothing
            Session("IsClientAdmin") = Nothing
        End If
    End Sub
    Protected Sub btn_login_Click(sender As Object, e As EventArgs) Handles btn_login.Click
        Dim roles As New List(Of Integer)
        Using db As New IICICEntities
            Dim user As New User
            user = (From u In db.Users Where u.UserName = txt_username.Text And u.Password = txt_password.Text).FirstOrDefault

            If user IsNot Nothing Then
                Session("UserId") = user.UserId
                Dim ur = (From r In db.UserRoles Where r.UserId = user.UserId)

                For Each r In ur
                    roles.Add(r.RoleId)
                Next
            End If
        End Using

        If roles.Contains(1) Then
            Session("IsPortalAdmin") = True
        End If
        If roles.Contains(2) Then
            Session("IsClientAdmin") = True
        End If
        If roles.Contains(1) Or roles.Contains(2) Then

            Response.Redirect("participation.aspx")
        Else
            Dim err As New CustomValidator
            err.IsValid = False
            err.ErrorMessage = "Insufficient permissions."
            err.ValidationGroup = "login"
            Page.Validators.Add(err)
        End If
    End Sub

End Class
