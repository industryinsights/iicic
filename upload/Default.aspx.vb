﻿
Partial Class Upload_Default
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Session("UploadOnly") = True
            Response.Redirect("../Account/login.aspx")
        End If
    End Sub
End Class
