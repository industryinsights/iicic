﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site.master" AutoEventWireup="false" CodeFile="ChangePassword.aspx.vb" Inherits="Account_ChangePassword" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="bg-content">
       <h2 class="text-center">Password Reset</h2>
   <div class="text-center"><asp:Label ID="lblInvalid" runat="server" style="color:Red;" visible="false">The link has expired or is invalid, to get a new link please <a href="AccountRecovery.aspx">click here</a>.</asp:Label></div>

      <div align="center"><asp:Label ID="lblSuccess" runat="server" style="color:Green;" visible="false">Password succesfully reset.<br /> <br /><a href="login.aspx">Home</a></asp:Label></div>
<div style="margin-top:15px;margin-left:20px;margin-right:20px;" align="center" id="ResetDiv" runat="server" visible="false">
    
    <p>Please enter a new password.</p>
    <br />
<table class="table-condensed">
<tr><td>New Password:</td><td><asp:TextBox ID="NPwd" TextMode="Password" runat="server" class="loginbox" style="width:150px;" CssClass="form-control"></asp:TextBox></td></tr>
<tr><td>Confirm Password:</td><td><asp:TextBox ID="Cpwd" TextMode="Password" runat="server" class="loginbox" style="width:150px;" CssClass="form-control"></asp:TextBox></td></tr>
<tr><td colspan="2" align="right"><asp:Button CssClass="btn btn-default" Text="Reset" ID="ResetButton" runat="server" style="font-size:12px;"/></td></tr>
</table>
<br />
<div align="center"><asp:Label ID="errorlbl" runat="server" style="color:Red;"></asp:Label></div>
</div>
</div>
</asp:Content>

