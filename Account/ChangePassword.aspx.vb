﻿
Imports System.Data.SqlClient

Partial Class Account_ChangePassword
    Inherits System.Web.UI.Page
    Dim CODE As String = ""
    Dim Userid As Integer = 0

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Request.QueryString("RV") IsNot Nothing Then
            CODE = Request.QueryString("RV").ToString()
            Userid = GetUser(CODE)
        Else
            Response.Redirect("Login.aspx")
        End If

        If Userid = 0 Then
            ResetDiv.Visible = False
            lblInvalid.Visible = True
        Else
            ResetDiv.Visible = True
            lblInvalid.Visible = False

        End If
    End Sub

    Protected Sub ResetButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ResetButton.Click

        If Cpwd.Text.Trim <> "" And NPwd.Text.Trim <> "" Then
            If NPwd.Text.Length >= 5 Then
                If NPwd.Text = Cpwd.Text Then
                    ResetPassword()
                Else
                    errorlbl.Text = "Passwords do not match."
                End If
            Else
                errorlbl.Text = "Password must contain at least 5 characters."
            End If
        Else
            errorlbl.Text = "Password cannot be blank."
        End If

    End Sub

    Function GetUser(ByVal CODE As String) As String
        Dim userid As Integer

        Using connection As New SqlConnection(ConfigurationManager.ConnectionStrings("Website1DatabaseAuthentication").ConnectionString)

            connection.Open()

            Dim command As New SqlCommand
            command.Connection = connection
            command.CommandText = String.Empty
            command.CommandText += " SELECT userid "
            command.CommandText += " FROM Users "
            command.CommandText += " WHERE password =  @TempCode"
            command.Parameters.Add(New SqlParameter("@TempCode", CODE))

            userid = command.ExecuteScalar()

            connection.Close()

        End Using

        Return userid

    End Function
    Protected Sub ResetPassword()

        Using connection As New SqlConnection(ConfigurationManager.ConnectionStrings("Website1DatabaseAuthentication").ConnectionString)
            Dim encryptedPassword = NPwd.Text.Trim
            connection.Open()

            Dim command As New SqlCommand
            command.Connection = connection
            command.CommandText = String.Empty
            command.CommandText += " Update Users SET PASSWORD = @PASSWORD"
            command.CommandText += " WHERE userid =  @IND_ID"
            command.Parameters.Add(New SqlParameter("@IND_ID", Userid))
            command.Parameters.Add(New SqlParameter("@PASSWORD", encryptedPassword))
            command.ExecuteNonQuery()
            connection.Close()

        End Using
        lblSuccess.Visible = True
        ResetDiv.Visible = False
        lblInvalid.Visible = False
    End Sub
End Class
