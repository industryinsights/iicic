﻿
Partial Class Account_Help
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Session("Client") IsNot Nothing Then
            If Session("Client") = "naw" Then
                panel_nawFAQ.Visible = True
                panel_defaultFAQ.Visible = False
            Else
                panel_nawFAQ.Visible = False
                panel_defaultFAQ.Visible = True
            End If
        Else
            panel_nawFAQ.Visible = False
            panel_defaultFAQ.Visible = True
        End If
    End Sub
End Class
