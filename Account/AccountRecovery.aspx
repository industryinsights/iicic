﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site.master" AutoEventWireup="false" CodeFile="AccountRecovery.aspx.vb" Inherits="Account_AccountRecovery" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="bg-content">
        <h2 class="text-uppercase">Password Recovery</h2>
        <p>Please enter the email address associated with your account.</p>
        <table class="table-condensed">
            <tr>
                <td>
                    <asp:TextBox ID="Email" runat="server" class="form-control" CssClass="form-control"></asp:TextBox>
                </td>
                <td class="col-md-4">
                    <asp:Button ID="btn_continue" runat="server" CssClass="btn btn-primary" Text="Continue" />
                </td>
            </tr>
        </table>
        <p>
            <asp:Label ID="lbl_msg" runat="server"></asp:Label></p>
        <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="alert-danger" />
        <p><a href="login.aspx">Return to Login</a></p>

    </div>
</asp:Content>

