﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site.master" AutoEventWireup="false" CodeFile="Login.aspx.vb" Inherits="Login" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="container-fluid">
        <div class="bg-content">
            <asp:Panel ID="Panel1" runat="server" DefaultButton="btn_login">
                <div class="row">
                    <div class="col-md-12 text-center">

                        <asp:Label ID="lbl_systemMessage" runat="server" CssClass="text-danger text-bold" Font-Size="16pt"></asp:Label>

                    </div>
                </div>
                <div class="row">
                    <div class="col-md-5">
                        <h3>Welcome to the Cross-Industry Compensation Portal!</h3>
                        <%--<h4>Deadline to participate:  March 17, 2017</h4>--%>
                        <hr />
                        <p class="visible-lg visible-md">
                            The Cross-Industry Compensation Benchmarking Portal is an invaluable business tool that will present accurate and timely information about your company.  
                            The portal gives respondents a better understanding of how your company's compensation levels and benefits practices compare to other distributors/wholesalers.
                        </p>
                        <p>
                            The Cross-Industry Compensation Benchmarking Study is conducted by Industry Insights, Inc., an objective independent research firm, that specializes in these types of studies.  
                            Industry Insights guarantees confidential information submitted by association members will not be released to any person, company, or 
                            organization for any purposes.  Industry Insights uses all appropriate means of internal controls and security protections to ensure confidentialilty and integrity of the 
                            data provided. 

                        </p>
                        <p class="text-center text-uppercase text-muted">&ndash; Powered BY &ndash;</p>
                        <p class="text-center">
                            <img src="../Content/images/logo-industryinsights.png" />
                        </p>
                    </div>
                    <div class="col-md-1">
                    </div>
                    <div class="col-md-5">
                        <h3>Log in<span class="sub-text"> </span></h3>
                        <div><span class="sub-text" id="h-login">To protect your data&#39;s confidentiality, your association credentials are not linked to this site.</span></div>
                        <div class="border-gray login-box right">
                            <table class="table">
                                <tr>
                                    <td class="col-lg-1">Email</td>
                                    <td class="col-lg-3">
                                        <asp:TextBox ID="txt_username" runat="server" CssClass="form-control"></asp:TextBox>
                                    </td>
                                    <td class="col-lg-1">
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txt_username" CssClass="text-danger" ErrorMessage="User ID is required." ValidationGroup="login">*</asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="col-lg-1">Password</td>
                                    <td class="col-lg-3">
                                        <asp:TextBox ID="txt_password" runat="server" CssClass="form-control" TextMode="Password"></asp:TextBox>
                                    </td>
                                    <td class="col-lg-1">
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txt_password" CssClass="text-danger" ErrorMessage="Password is required." ValidationGroup="login">*</asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="col-lg-1">

                                        <a href="AccountRecovery.aspx">Forgot password?</a></td>
                                    <td class="text-left" colspan="2">&nbsp;<asp:Button ID="btn_login" runat="server" CssClass="btn btn-default" Text="Sign in" ValidationGroup="login" Width="100px" />

                                    </td>
                                </tr>
                            </table>

                        </div>
                        <asp:Panel ID="panel_registration" runat="server">
                            <h4 class="text-isa-blue">
                                <strong>First time participating or accessing site?</strong>
                            </h4>
                            <p class="visible-lg visible-md">
                                If this is your first time accessing the Cross-Industry Compensation portal, please register below. Keep in mind that the credentials for this survey are not matched with your Member site credentials.
                            </p>
                            <p>
                                <strong>&nbsp; </strong>
                                <asp:Button ID="btn_register" runat="server" CssClass="btn btn-primary" PostBackUrl="~/Account/Registration.aspx" TabIndex="99" Text="Register" Width="100px" />
                            </p>
                        </asp:Panel>
                    </div>
                </div>
                <div class="row">
                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="alert-danger" ValidationGroup="login" />
                </div>
                <div class="row">
                    <p class="text-center">
                        <a href="../privacy.html" target="_blank">Terms and Conditions and Privacy Policy</a>
                    </p>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>

