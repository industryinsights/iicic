﻿
Imports Models

Partial Class Account_MyAccount
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not IsPostBack Then
            If Session("LocationID") Is Nothing Then
                Response.Redirect("login.aspx")
            End If
            LoadContactInformation()
            If Session("IsAdmin") IsNot Nothing Then
                If Session("IsAdmin") = False Then
                    txt_address1.Enabled = False
                    txt_address2.Enabled = False
                    txt_city.Enabled = False
                    ddl_state.Enabled = False
                    ddl_country.Enabled = False
                    txt_postalCode.Enabled = False
                End If
            Else
                txt_address1.Enabled = False
                txt_address2.Enabled = False
                txt_city.Enabled = False
                ddl_state.Enabled = False
                ddl_country.Enabled = False
                txt_postalCode.Enabled = False
            End If
        End If

    End Sub
    Sub LoadContactInformation()
        Using db As New IICICEntities
            Dim userId As Integer = Session("UserId")
            Dim orgId As Integer = Session("OrganizationId")
            Dim user As New User
            Dim org As New Organization
            user = (From u In db.Users Where u.UserId = userId).FirstOrDefault
            org = (From o In db.Organizations Where o.OrganizationId = orgId).FirstOrDefault
            If org.Name IsNot Nothing Then
                txt_organization.Text = org.Name
            End If

            If user IsNot Nothing Then
                If user.FirstName IsNot Nothing Then
                    txt_fname.Text = user.FirstName
                End If
                If user.LastName IsNot Nothing Then
                    txt_lname.Text = user.LastName
                End If
                If user.Title IsNot Nothing Then
                    txt_title.Text = user.Title
                End If
                If org.AddressLine1 IsNot Nothing Then
                    txt_address1.Text = org.AddressLine1
                End If
                If org.AddressLine2 IsNot Nothing Then
                    txt_address2.Text = org.AddressLine2
                End If
                If org.City IsNot Nothing Then
                    txt_city.Text = org.City
                End If
                If org.State IsNot Nothing Then
                    ddl_state.SelectedValue = org.State
                End If
                If org.PostalCode IsNot Nothing Then
                    txt_postalCode.Text = org.PostalCode
                End If
                If org.Country IsNot Nothing Then
                    ddl_country.SelectedValue = org.Country
                End If
                If user.Phone IsNot Nothing Then
                    txt_phone.Text = user.Phone
                End If
                If user.Email IsNot Nothing Then
                    txt_email.Text = user.Email
                    txt_confirmEmail.Text = user.Email
                End If
                If user.UserName IsNot Nothing Then
                    txt_username.Text = user.UserName
                    'txt_username.Enabled = False
                End If
                If user.Password IsNot Nothing Then
                    txt_password.Attributes.Add("value", user.Password)
                    txt_confirmPassword.Attributes.Add("value", user.Password)
                End If
            Else
                Dim err As New CustomValidator() With {.IsValid = False, .ErrorMessage = "An unexpected error occured.  Please <a href=""login.aspx"">login</a> and try again."}
                Page.Validators.Add(err)
            End If
        End Using
    End Sub
    Protected Sub btn_update_Click(sender As Object, e As EventArgs) Handles btn_update.Click
        lbl_successfulUpdate.Visible = False
        Using db As New IICICEntities
            Dim userid As Integer = Convert.ToInt16(Session("UserId"))
            Dim orgId As Integer = Convert.ToInt16(Session("OrganizationId"))
            Dim user As New User
            user = (From u In db.Users Where u.UserId <> userid And (u.UserName = txt_email.Text Or u.Email = txt_email.Text)).FirstOrDefault
            If user Is Nothing Then
                user = (From u In db.Users Where u.UserId = userid).FirstOrDefault
                Dim org As New Organization
                org = (From o In db.Organizations Where o.OrganizationId = orgId).FirstOrDefault
                If Not String.IsNullOrEmpty(txt_organization.Text) Then
                    org.Name = txt_organization.Text
                End If
                If Not (String.IsNullOrEmpty(txt_fname.Text) And String.IsNullOrEmpty(txt_lname.Text)) Then
                    user.FullName = String.Format("{0} {1}", txt_fname.Text, txt_lname.Text)
                End If
                If Not String.IsNullOrEmpty(txt_fname.Text) Then
                    user.FirstName = txt_fname.Text
                End If
                If Not String.IsNullOrEmpty(txt_lname.Text) Then
                    user.LastName = txt_lname.Text
                End If
                If Not String.IsNullOrEmpty(txt_title.Text) Then
                    user.Title = txt_title.Text
                End If
                If Not String.IsNullOrEmpty(txt_address1.Text) Then
                    org.AddressLine1 = txt_address1.Text
                End If
                If Not String.IsNullOrEmpty(txt_address2.Text) Then
                    org.AddressLine2 = txt_address2.Text
                End If
                If Not String.IsNullOrEmpty(txt_city.Text) Then
                    org.City = txt_city.Text
                End If
                If Not String.IsNullOrEmpty(ddl_state.Text) Then
                    org.State = ddl_state.SelectedValue
                End If
                If Not String.IsNullOrEmpty(txt_postalCode.Text) Then
                    org.PostalCode = txt_postalCode.Text
                End If
                If Not String.IsNullOrEmpty(ddl_country.SelectedValue) Then
                    org.Country = ddl_country.SelectedValue
                End If
                If Not String.IsNullOrEmpty(txt_phone.Text) Then
                    user.Phone = txt_phone.Text
                End If
                If Not String.IsNullOrEmpty(txt_password.Text) Then
                    If txt_password.Text <> user.Password Then
                        user.Password = txt_password.Text
                    End If
                End If

                user.Email = txt_email.Text
                user.UserName = txt_email.Text
                db.SaveChanges()
                lbl_successfulUpdate.Visible = True
            Else
                Dim err As New CustomValidator() With {.IsValid = False, .ValidationGroup = "myaccount", .ErrorMessage = "User name already exists.  Please try again."}
                Page.Validators.Add(err)
            End If
        End Using
    End Sub
End Class
