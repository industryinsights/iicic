﻿
Imports Models

Partial Class Account_EmailOptOut
    Inherits System.Web.UI.Page

    Protected Sub btn_optOut_Click(sender As Object, e As EventArgs) Handles btn_optOut.Click
        Using db As New IICICEntities
            If Not String.IsNullOrEmpty(txt_email.Text) Then
                Dim optOut As New EmailOptOut
                optOut.ProjectId = 1
                optOut.Email = txt_email.Text
                optOut.OptOutDate = Now
                db.EmailOptOuts.Add(optOut)
                db.SaveChanges()
                lbl_optOut.Visible = True
            End If
        End Using
    End Sub
End Class
