﻿
Imports Models

Partial Class Account_Registration
    Inherits System.Web.UI.Page

    Protected Sub btn_register_Click(sender As Object, e As EventArgs) Handles btn_register.Click
        Using db As New IICICEntities
            Dim user As New User
            user = (From u In db.Users Where u.UserName = txt_username.Text Or u.Email = txt_email.Text).FirstOrDefault
            If user IsNot Nothing Then
                Dim err As New CustomValidator With {.IsValid = False, .ValidationGroup = "registration", .ErrorMessage = "User name or email is already in use.  Please use <a href=""accountrecovery.aspx"">Account Recovery</a> to recovery your account."}
                Page.Validators.Add(err)
            Else
                Dim org As New Organization With {.Name = txt_organization.Text, .IsActive = True}

                Dim dSet As New DataSet
                db.Organizations.Add(org)
                db.SaveChanges()
                dSet.OrganizationId = org.OrganizationId
                dSet.Name = "Default Location"
                dSet.CreateDate = Now
                db.DataSets.Add(dSet)
                db.SaveChanges()
                user = New User
                user.UserName = txt_email.Text
                user.Password = txt_password.Text
                user.OrganizationId = org.OrganizationId
                If Request.QueryString("name") IsNot Nothing Then
                    user.Notes = Request.QueryString("name")
                End If
                If Not (String.IsNullOrEmpty(txt_fname.Text) And String.IsNullOrEmpty(txt_lname.Text)) Then
                    user.FullName = String.Format("{0} {1}", txt_fname.Text, txt_lname.Text)
                End If
                If Not String.IsNullOrEmpty(txt_fname.Text) Then
                    user.FirstName = txt_fname.Text
                End If
                If Not String.IsNullOrEmpty(txt_lname.Text) Then
                    user.LastName = txt_lname.Text
                End If
                If Not String.IsNullOrEmpty(txt_title.Text) Then
                    user.Title = txt_title.Text
                End If
                If Not String.IsNullOrEmpty(txt_address1.Text) Then
                    org.AddressLine1 = txt_address1.Text
                End If
                If Not String.IsNullOrEmpty(txt_address2.Text) Then
                    org.AddressLine2 = txt_address2.Text
                End If
                If Not String.IsNullOrEmpty(txt_city.Text) Then
                    org.City = txt_city.Text
                End If
                If Not String.IsNullOrEmpty(ddl_state.Text) Then
                    org.State = ddl_state.SelectedValue
                End If
                If Not String.IsNullOrEmpty(txt_postalCode.Text) Then
                    org.PostalCode = txt_postalCode.Text
                End If
                If Not String.IsNullOrEmpty(ddl_country.SelectedValue) Then
                    org.Country = ddl_country.SelectedValue
                End If
                If Not String.IsNullOrEmpty(txt_phone.Text) Then
                    user.Phone = txt_phone.Text
                End If

                user.Email = txt_email.Text
                user.IsActive = True
                user.CreateDate = Now
                db.Users.Add(user)
                db.SaveChanges()
                Dim ud As New UserDataSet With {.LocationID = dSet.LocationID, .UserId = user.UserId}
                db.UserDataSets.Add(ud)
                db.SaveChanges()
                Dim role As New UserRole With {.RoleId = 3, .UserId = user.UserId}
                db.UserRoles.Add(role)
                db.SaveChanges()
                role = New UserRole With {.RoleId = 4, .UserId = user.UserId}
                db.UserRoles.Add(role)
                db.SaveChanges()
                role = New UserRole With {.RoleId = 6, .UserId = user.UserId}
                db.UserRoles.Add(role)
                db.SaveChanges()
                Dim ul As New UserLog With {.UserId = user.UserId, .LoginDate = Now, .IP = Request.UserHostAddress}
                If Session("Client") IsNot Nothing Then
                    ul.Site = Session("Client")
                End If
                db.UserLogs.Add(ul)
                db.SaveChanges()
                Dim userRoles As New List(Of Integer)
                userRoles.Add(3)
                userRoles.Add(4)
                userRoles.Add(6)
                Session("UserId") = user.UserId
                Session("LocationID") = dSet.LocationID
                Session("OrganizationId") = user.OrganizationId
                Session("IsAdmin") = True
                Session("UserRoles") = userRoles
                FormsAuthentication.SetAuthCookie(txt_email.Text, True)
                FormsAuthentication.RedirectFromLoginPage(txt_email.Text, True)
            End If
        End Using
    End Sub
End Class
