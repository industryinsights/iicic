﻿
Imports System.Data
Imports DevExpress.Web
Imports Models

Partial Class Account_LocationAssignment
    Inherits System.Web.UI.Page
    Private ds As System.Data.DataSet = Nothing
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim cs As String = ConfigurationManager.ConnectionStrings("Website1DatabaseAuthentication").ToString
        SqlDataSource1.ConnectionString = cs
        Dim LocationID As Integer
        Dim userID As Integer = Session("UserId")
        Dim roles As List(Of Integer) = Session("UserRoles")
        If Not IsPostBack Then
            'Prevent others from accessing datasets they do not own
            If Session("Datasets") IsNot Nothing Then
                If Request.QueryString("loc") IsNot Nothing Then
                    LocationID = Convert.ToInt16(Request.QueryString("loc").ToString)
                    'Dim datasets As List(Of Integer) = Session("Datasets")
                    'If Not datasets.Contains(LocationID) Then
                    '    Response.Redirect("login.aspx")
                    'End If
                Else
                    Response.Redirect("login.aspx")
                End If
            Else
                Response.Redirect("login.aspx")
            End If

            Using db As New IICICEntities
                Dim dSet As New Models.DataSet
                dSet = (From d In db.DataSets Where d.LocationID = LocationID).FirstOrDefault

                If dSet IsNot Nothing Then
                    If dSet.DSAdminUserId = userID Or roles.Contains(3) Then
                        lbl_location.Text = dSet.Name

                        If (Not IsPostBack) OrElse (Session("Dataset") Is Nothing) Then
                            Dim args As New DataSourceSelectArguments
                            ds = New System.Data.DataSet

                            Dim view As DataView = DirectCast(SqlDataSource1.Select(args), DataView)
                            Dim masterTable As DataTable = view.ToTable()
                            masterTable.PrimaryKey = New DataColumn() {masterTable.Columns("UserId")}
                            Dim detailTable As New DataTable()
                            detailTable = masterTable
                            detailTable.PrimaryKey = New DataColumn() {detailTable.Columns("UserId")}
                            detailTable.Columns.Add("MasterID", GetType(Integer))
                            Dim index As Integer = 0
                            ds.Tables.Add(masterTable)
                            ds.Tables.Remove(detailTable)
                            ds.Tables.Add(detailTable)
                            Session("Dataset") = ds
                        Else
                            ds = DirectCast(Session("Dataset"), System.Data.DataSet)
                        End If
                        ASPxGridView1.DataSource = ds.Tables(0)
                        ASPxGridView1.DataBind()
                    Else
                        Response.Redirect("organizationadmin.aspx")
                    End If
                End If
            End Using


        End If
    End Sub
#Region "Gridview Routines"
    Protected Sub ASPxGridView2_BeforePerformDataSelect(ByVal sender As Object, ByVal e As EventArgs)
        ds = DirectCast(Session("Dataset"), System.Data.DataSet)
        Dim detailTable As DataTable = ds.Tables(1)
        Dim dv As New DataView(detailTable)
        Dim detailGridView As ASPxGridView = DirectCast(sender, ASPxGridView)
        dv.RowFilter = "MasterID = " & detailGridView.GetMasterRowKeyValue()
        detailGridView.DataSource = dv
    End Sub
    Protected Sub ASPxGridView1_RowUpdating(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataUpdatingEventArgs)
        ds = DirectCast(Session("Dataset"), System.Data.DataSet)
        Dim gridView As ASPxGridView = DirectCast(sender, ASPxGridView)
        Dim dataTable As DataTable = If(gridView.GetMasterRowKeyValue() IsNot Nothing, ds.Tables(1), ds.Tables(0))
        Dim row As DataRow = dataTable.Rows.Find(e.Keys(0))
        Dim enumerator As IDictionaryEnumerator = e.NewValues.GetEnumerator()
        enumerator.Reset()
        Using db As New IICICEntities
            Dim user As New User
            Dim userId As Integer = e.Keys(0)
            user = (From u In db.Users Where u.UserId = userId).FirstOrDefault
            Dim roles = (From r In db.UserRoles Where r.UserId = user.UserId)
            Dim userRoles As New List(Of Integer)
            For Each r As UserRole In roles
                userRoles.Add(r.RoleId)
            Next
            Dim LocationID As Integer = Convert.ToInt16(Request.QueryString("loc").ToString)
            Do While enumerator.MoveNext()
                If enumerator.Value = Nothing Then
                    row(enumerator.Key.ToString()) = DBNull.Value
                Else
                    row(enumerator.Key.ToString()) = enumerator.Value
                End If
                If enumerator.Key.ToString = "Location Member" Then
                    If enumerator.Value = True Then
                        Dim newDS As New UserDataSet() With {.UserId = user.UserId, .LocationID = LocationID}
                        db.UserDataSets.Add(newDS)
                        db.SaveChanges()
                    ElseIf enumerator.Value = False Then
                        Dim removeDS As UserDataSet = (From d In db.UserDataSets Where d.UserId = user.UserId And d.LocationID = LocationID).FirstOrDefault
                        If removeDS IsNot Nothing Then
                            db.UserDataSets.Remove(removeDS)
                        End If
                    End If
                End If
            Loop
            db.SaveChanges()
            Dim dSet As New Models.DataSet
            dSet = (From d In db.DataSets Where d.LocationID = LocationID).FirstOrDefault

            If dSet IsNot Nothing Then

                lbl_location.Text = dSet.Name

                If (Not IsPostBack) OrElse (Session("Dataset") Is Nothing) Then
                    Dim args As New DataSourceSelectArguments
                    ds = New System.Data.DataSet

                    Dim view As DataView = DirectCast(SqlDataSource1.Select(args), DataView)
                    Dim masterTable As DataTable = view.ToTable()
                    masterTable.PrimaryKey = New DataColumn() {masterTable.Columns("UserId")}
                    Dim detailTable As New DataTable()
                    detailTable = masterTable
                    detailTable.PrimaryKey = New DataColumn() {detailTable.Columns("UserId")}
                    detailTable.Columns.Add("MasterID", GetType(Integer))
                    Dim index As Integer = 0
                    ds.Tables.Add(masterTable)
                    ds.Tables.Remove(detailTable)
                    ds.Tables.Add(detailTable)
                    Session("Dataset") = ds
                Else
                    ds = DirectCast(Session("Dataset"), System.Data.DataSet)
                End If
                ASPxGridView1.DataSource = ds.Tables(0)
                ASPxGridView1.DataBind()
            End If
        End Using
        gridView.CancelEdit()
        e.Cancel = True

    End Sub
#End Region
End Class
