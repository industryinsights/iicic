﻿
Imports System.Data.SqlClient
Imports System.Net.Mail

Partial Class Account_AccountRecovery
    Inherits System.Web.UI.Page
    Protected Function MailAddressListContainsMailAddress(ByVal mailAddressList As List(Of MailAddress), ByVal mailAddress As MailAddress) As Boolean

        Dim result As Boolean = False

        For Each item In mailAddressList
            result = result OrElse (item.Address = mailAddress.Address AndAlso item.DisplayName = mailAddress.DisplayName)
        Next

        Return result

    End Function

    Protected Sub SendEmail(ByVal mailAddress As MailAddress)

        Dim CODE As String = ""
        Using connection As New SqlConnection(ConfigurationManager.ConnectionStrings("Website1DatabaseAuthentication").ConnectionString)

            connection.Open()

            Dim command As New SqlCommand() With {.Connection = connection, .CommandText = String.Empty}
            command.CommandText += " UPDATE Users SET password= NEWID() WHERE Username = @Username  "
            command.Parameters.Add(New SqlParameter("@Username", Email.Text.Trim().ToString()))
            command.ExecuteNonQuery()
            command.CommandText = "SELECT password FROM Users WHERE Username = @Username"
            CODE = command.ExecuteScalar()


            connection.Close()

        End Using

        Dim client As New SmtpClient()
        Dim message As New MailMessage
        message.From = New MailAddress(message.From.Address, "Mike Ferris")
        message.IsBodyHtml = True
        message.To.Add(mailAddress.Address)
        ' message.To.Add("jbeebe@industryinsights.com")
        message.Subject = "Cross-Industry Compensation Password Reset"
        message.Body = GetEmailHtml(mailAddress, CODE)
        client.Send(message)

        lbl_msg.Text = "<font color=""green""> The email was successfully sent and should arrive momentarily.</font>"

    End Sub


    Private Function GetEmailHtml(ByVal mailAddress As MailAddress, ByVal CODE As String) As String
        Dim url As String = "https://compensationbenchmarking.com/Account/ChangePassword.aspx?RV="
        If Session("Client") IsNot Nothing Then
            url = String.Format("https://www.compensationbenchmarking.com/Account/ChangePassword.aspx?c={0}&RV=", Session("Client"))
        End If
        Dim html As New StringBuilder()
        html.Append("<html>")
        html.Append("<head>")
        html.Append("<title></title>")
        html.Append("<style type=""text/css"">")
        html.Append("html, body")
        html.Append("{")
        html.Append("margin: 0;")
        html.Append("font-family: Arial;")
        html.Append("font-size: 10pt;")
        html.Append("}")
        html.Append("")
        html.Append("</style>")
        html.Append("</head>")
        html.Append("<body>")
        html.Append("<p>You are receiving this email because you have requested a password reset.</p>")
        html.AppendFormat("<p>To reset your password please visit the following link:  <a href=""{0}{1}"">{0}{1}</a>.</p>", url, CODE)
        html.Append("<p>We appreciate your participation! Please let me know if you have any questions.</p>")
        html.Append("<p>Thanks,</p>")
        html.Append("<p style=""color:#7F7F7F"">Mike Ferris<br/>Industry Insights, Inc.<br/>614.389.2100 ext. 241<br/><a href=""mailto:comp@industryinsights.com"">comp@industryinsights.com</a></p>")
        html.Append("</body>")
        html.Append("</html>")
        Return html.ToString()

    End Function


    Function EmailAddressChecker(ByVal emailAddress As String) As Boolean
        Dim regExPattern As String = "^([0-9a-zA-Z]([-\.\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,9})$"
        Dim emailAddressMatch As Match = Regex.Match(emailAddress, regExPattern)
        If emailAddressMatch.Success Then
            Return True
        Else
            Return False
        End If
    End Function


    Protected Sub btn_continue_Click(sender As Object, e As EventArgs) Handles btn_continue.Click
        If Not (String.IsNullOrEmpty(Email.Text)) Then

            Dim Match As Boolean = False
            Dim Active As Boolean = True
            Dim MailEmail As String = ""

            Using connection As New SqlConnection(ConfigurationManager.ConnectionStrings("Website1DatabaseAuthentication").ConnectionString)

                connection.Open()

                Dim command As New SqlCommand() With {.Connection = connection, .CommandText = String.Empty}
                command.CommandText += " SELECT email"
                command.CommandText += " FROM Users "
                command.CommandText += " WHERE username =  @Email"
                command.Parameters.Add(New SqlParameter("@Email", Email.Text.Trim().ToString()))
                Dim reader As SqlDataReader = command.ExecuteReader()
                reader.Read()
                Match = reader.HasRows
                reader.Close()
                command.CommandText = "SELECT email FROM Users WHERE username = @Email"
                MailEmail = command.ExecuteScalar()

                connection.Close()

            End Using

            If Not Match Then
                Dim err As New CustomValidator
                err.IsValid = False
                err.ErrorMessage = "No match found for this username."
                Page.Validators.Add(err)
            ElseIf Match Then
                Dim lines As String() = MailEmail.Split(New String() {vbCrLf, vbCr, vbLf}, StringSplitOptions.None)
                Dim mailAddressList As New List(Of MailAddress)
                For Each line As String In lines

                    If Not String.IsNullOrEmpty(line) Then

                        Dim cells As String() = line.Split(vbTab)
                        ' Dim name As String = cells(0).Trim()
                        Dim email As String = cells(0).Trim()

                        Try
                            Dim mailAddress As New MailAddress(email)

                            If Not mailAddressList.Contains(mailAddress) Then
                                mailAddressList.Add(mailAddress)
                            End If
                        Catch ex As Exception
                            Response.Write(String.Format("<html><head></head><body>{0}<br/>{1}<br/><br/></body></html>", If(email, String.Empty), ex.ToString()))
                            Response.End()
                            Exit Sub
                        End Try
                    End If

                Next

                For Each item As MailAddress In mailAddressList
                    Try
                        SendEmail(item)
                    Catch ex As Exception
                        Response.Write(String.Format("<html><head></head><body>{0}<br/>{1}<br/><br/>{2}</body></html>", If(item.DisplayName, String.Empty), If(item.Address, String.Empty), ex.ToString()))
                        Response.End()
                        Exit Sub
                    End Try
                Next
            End If
        Else
            Dim err2 As New CustomValidator
            err2.IsValid = False
            err2.ErrorMessage = "Please input the username for which you need to recover the password."
            Page.Validators.Add(err2)
        End If
    End Sub
End Class
