﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site.master" AutoEventWireup="false" CodeFile="LocationAssignment.aspx.vb" Inherits="Account_LocationAssignment" %>

<%@ Register Assembly="DevExpress.Web.v18.1, Version=18.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>



<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="bg-content">
        <div class="container-fluid">
            <div class="container-fluid">
                <div class="row text-left">
                    <ol class="breadcrumb">
                        <li><a href="../Surveys/Comp/TableOfContents.aspx">Home</a></li>
                        <li><a href="OrganizationAdmin.aspx">Organization Admin</a></li>
                        <li class="active">Location</li>
                    </ol>
                </div>
                <div class="row">
                    <h3>Location: 
                        <em>
                            <asp:Label ID="lbl_location" runat="server" Text=""></asp:Label><asp:HiddenField ID="HiddenField1" runat="server" />
                        </em>
                    </h3>
                    <hr />
                </div>
                <div class="row">
                    <dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False"
                        OnRowUpdating="ASPxGridView1_RowUpdating" KeyFieldName="UserId" Theme="BlackGlass" Caption="Location Members">
                        <SettingsAdaptivity AdaptivityMode="HideDataCells">
                        </SettingsAdaptivity>
                        <SettingsEditing Mode="Batch">
                            <BatchEditSettings AllowValidationOnEndEdit="False" />
                        </SettingsEditing>
                        <SettingsCommandButton>
                            <ShowAdaptiveDetailButton ButtonType="Image"></ShowAdaptiveDetailButton>

                            <HideAdaptiveDetailButton ButtonType="Image"></HideAdaptiveDetailButton>
                        </SettingsCommandButton>
                        <Columns>
                            <dx:GridViewDataTextColumn FieldName="UserId" Visible="False" VisibleIndex="1" ShowInCustomizationForm="True">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="OrganizationId" Visible="False" VisibleIndex="2" ShowInCustomizationForm="True">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="Location ID" FieldName="LocationID" Visible="False" VisibleIndex="3" ShowInCustomizationForm="True">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="Location" FieldName="Name" Visible="False" VisibleIndex="4" ShowInCustomizationForm="True">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Full Name" VisibleIndex="6" ShowInCustomizationForm="True">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Email" VisibleIndex="5" ShowInCustomizationForm="True">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataCheckColumn FieldName="Location Member" VisibleIndex="7" Caption="Location Member?">
                                <EditFormSettings Visible="True" />
                            </dx:GridViewDataCheckColumn>
                        </Columns>
                    </dx:ASPxGridView>
                    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="Data Source=66.11.14.181;Initial Catalog=IICIC;User ID=II_USER;Password=IIReport1394*nf;MultipleActiveResultSets=True;Application Name=EntityFramework" ProviderName="System.Data.SqlClient" SelectCommand="SELECT * FROM [LocationManagement] WHERE (([OrganizationId] = @OrganizationId) AND ([LocationID] = @LocationID)) ORDER BY [Email]">
                        <SelectParameters>
                            <asp:SessionParameter Name="OrganizationId" SessionField="OrganizationId" Type="Int64" />
                            <asp:QueryStringParameter Name="LocationID" QueryStringField="loc" Type="Int64" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                </div>
                <div class="row"><a href="organizationadmin.aspx">Return to Organization Admin</a></div>
            </div>
        </div>
    </div>
</asp:Content>

