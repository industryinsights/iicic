﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Survey.master" AutoEventWireup="false" CodeFile="OrganizationAdmin.aspx.vb" Inherits="Account_OrganizationAdmin" %>

<%@ Register Assembly="DevExpress.Web.v18.1, Version=18.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script type="text/javascript">
        function OnNewClick(s, e) {
            grid.AddNewRow();
        }
        function OnEditClick(s, e) {
            var index = grid.GetFocusedRowIndex();
            grid.StartEditRow(index);
        }

        function OnSaveClick(s, e) {
            grid.UpdateEdit();
        }

        function OnCancelClick(s, e) {
            grid.CancelEdit();
        }

        function OnDeleteClick(s, e) {
            var index = grid.GetFocusedRowIndex();
            grid.DeleteRow(index);
        }
    </script>

    <div class="bg-content">
        <div class="container-fluid">
            <div class="container-fluid">
                <div class="row text-left">
                    <ol class="breadcrumb">
                        <li><a href="../Surveys/Comp/TableOfContents.aspx">Home</a></li>
                        <li class="active">Organization Administration</li>
                    </ol>
                </div>
                <asp:Panel ID="panel_admin" runat="server">
                    <div class="collapse">
                        <h3 class="page-header">Location Management</h3>
                        <hr />
                        <p>
                            This section allows the organization’s administrator to add multiple users to this account and to add additional company locations, if applicable.
                        </p>
                        <p>
                            Below are the locations that are currently listed for your company (if you have only one location, it will list “Default Location”). To add a new location, click the &quot;Add&quot; link. Once a location is created, you can assign users to that location.
                        </p>
                        <p>
                            Note: All users will be added to your default location. Once additional locations are added, you can assign users to that location.
                        </p>
                        <dx:ASPxGridView ID="gv_locations" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource2" KeyFieldName="LocationID" Width="90%" Caption="Locations"
                            OnCommandButtonInitialize="gv_locations_CommandButtonInitialize" CssClass="table table-hover">
                            <SettingsBehavior ConfirmDelete="True" />
                            <SettingsText ConfirmDelete="Are you sure you want to remove this location?  Any survey associated with this location will be lost." />
                            <SettingsCommandButton>
                                <ShowAdaptiveDetailButton ButtonType="Image"></ShowAdaptiveDetailButton>

                                <HideAdaptiveDetailButton ButtonType="Image"></HideAdaptiveDetailButton>
                                <NewButton Text="Add">
                                </NewButton>
                            </SettingsCommandButton>
                            <Columns>
                                <dx:GridViewCommandColumn ShowDeleteButton="True" ShowEditButton="True" ShowNewButtonInHeader="True" VisibleIndex="0">
                                </dx:GridViewCommandColumn>
                                <dx:GridViewDataTextColumn FieldName="LocationID" ReadOnly="True" Visible="False" VisibleIndex="1">
                                    <EditFormSettings Visible="False" />
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="OrganizationId" Visible="False" VisibleIndex="2">
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="Name" VisibleIndex="3" Caption="Location">
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataDateColumn FieldName="CreateDate" Visible="False" VisibleIndex="5">
                                </dx:GridViewDataDateColumn>
                                <dx:GridViewDataTextColumn VisibleIndex="4">
                                    <EditFormSettings Visible="False" />
                                    <DataItemTemplate>
                                        <a id="clickElement" target="_parent" href="LocationAssignment.aspx?loc=<%# Container.KeyValue%>">Assign Users</a>
                                    </DataItemTemplate>
                                </dx:GridViewDataTextColumn>
                            </Columns>
                        </dx:ASPxGridView>
                        <asp:SqlDataSource ID="SqlDataSource2" runat="server"
                            ConnectionString="Data Source=65.61.38.111;Initial Catalog=IICIC;User ID=u1002154_admin;Password=AlphaTest!;MultipleActiveResultSets=True;Application Name=EntityFramework"
                            ProviderName="System.Data.SqlClient"
                            SelectCommand="SELECT * FROM [DataSets] WHERE ([OrganizationId] = @OrganizationId)"
                            ConflictDetection="CompareAllValues" DeleteCommand="DELETE FROM [DataSets] WHERE [LocationID] = @original_LocationID AND [OrganizationId] = @original_OrganizationId AND (([Name] = @original_Name) OR ([Name] IS NULL AND @original_Name IS NULL)) AND (([CreateDate] = @original_CreateDate) OR ([CreateDate] IS NULL AND @original_CreateDate IS NULL))" InsertCommand="INSERT INTO [DataSets] ([OrganizationId], [Name], [CreateDate]) VALUES (@OrganizationId, @Name, @CreateDate)" OldValuesParameterFormatString="original_{0}" UpdateCommand="UPDATE [DataSets] SET [OrganizationId] = @OrganizationId, [Name] = @Name, [CreateDate] = @CreateDate WHERE [LocationID] = @original_LocationID AND [OrganizationId] = @original_OrganizationId AND (([Name] = @original_Name) OR ([Name] IS NULL AND @original_Name IS NULL)) AND (([CreateDate] = @original_CreateDate) OR ([CreateDate] IS NULL AND @original_CreateDate IS NULL))">
                            <DeleteParameters>
                                <asp:Parameter Name="original_LocationID" Type="Int64" />
                                <asp:Parameter Name="original_OrganizationId" Type="Int64" />
                                <asp:Parameter Name="original_Name" Type="String" />
                                <asp:Parameter Name="original_CreateDate" Type="DateTime" />
                            </DeleteParameters>
                            <InsertParameters>
                                <asp:SessionParameter Name="OrganizationId" SessionField="OrganizationId" Type="Int64" />
                                <asp:Parameter Name="Name" Type="String" />
                                <asp:Parameter Name="CreateDate" Type="DateTime" />
                            </InsertParameters>
                            <SelectParameters>
                                <asp:SessionParameter Name="OrganizationId" SessionField="OrganizationId" Type="Int64" />
                            </SelectParameters>
                            <UpdateParameters>
                                <asp:Parameter Name="OrganizationId" Type="Int64" />
                                <asp:Parameter Name="Name" Type="String" />
                                <asp:Parameter Name="CreateDate" Type="DateTime" />
                                <asp:Parameter Name="original_LocationID" Type="Int64" />
                                <asp:Parameter Name="original_OrganizationId" Type="Int64" />
                                <asp:Parameter Name="original_Name" Type="String" />
                                <asp:Parameter Name="original_CreateDate" Type="DateTime" />
                            </UpdateParameters>
                        </asp:SqlDataSource>



                    </div>
                    <div>
                        <h3 class="page-header">User Management</h3>
                        <hr />
                        <p>
                            Your company&#39;s current authorized users are listed below. To create a new user, click the &quot;Add&quot; link. 
                                        <br />
                            User-specific credentials will be emailed to each user when his/her account is created.
                        </p>
                        <dx:ASPxGridView ID="gv_users" runat="server" AutoGenerateColumns="False" EnableTheming="True" KeyFieldName="UserId" Width="100%"
                            ClientInstanceName="gv_users" OnRowUpdating="gv_Users_RowUpdating" OnRowDeleting="gv_Users_RowDeleting" OnRowInserting="gv_Users_RowInserting"
                            OnRowValidating="gv_Users_RowValidating" OnCommandButtonInitialize="gv_users_CommandButtonInitialize" CssClass="table table-hover">
                            <SettingsAdaptivity AdaptivityMode="HideDataCells">
                            </SettingsAdaptivity>
                            <SettingsBehavior ConfirmDelete="True" />
                            <SettingsCommandButton>
                                <ShowAdaptiveDetailButton ButtonType="Image"></ShowAdaptiveDetailButton>

                                <HideAdaptiveDetailButton ButtonType="Image"></HideAdaptiveDetailButton>
                                <NewButton Text="Add" ButtonType="Button" RenderMode="Button">
                                    <Styles>
                                        <Style CssClass="btn btn-primary btn-sm">
                                        </Style>
                                    </Styles>
                                </NewButton>
                            </SettingsCommandButton>
                            <SettingsText ConfirmDelete="Are you sure you want to remove this account?" />
                            <Columns>
                                <dx:GridViewCommandColumn ShowDeleteButton="True" ShowEditButton="True" ShowNewButtonInHeader="True" VisibleIndex="0" ShowNewButton="False">
                                </dx:GridViewCommandColumn>
                                <dx:GridViewDataTextColumn FieldName="UserId" ReadOnly="True" Visible="True" VisibleIndex="1" ShowInCustomizationForm="False">
                                    <SettingsHeaderFilter>
                                        <DateRangePickerSettings EditFormatString=""></DateRangePickerSettings>
                                    </SettingsHeaderFilter>

                                    <EditFormSettings Visible="False" />
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="OrganizationId" Visible="False" VisibleIndex="2">
                                    <SettingsHeaderFilter>
                                        <DateRangePickerSettings EditFormatString=""></DateRangePickerSettings>
                                    </SettingsHeaderFilter>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="Full Name" VisibleIndex="3">
                                    <SettingsHeaderFilter>
                                        <DateRangePickerSettings EditFormatString=""></DateRangePickerSettings>
                                    </SettingsHeaderFilter>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="Email" VisibleIndex="7">
                                    <SettingsHeaderFilter>
                                        <DateRangePickerSettings EditFormatString=""></DateRangePickerSettings>
                                    </SettingsHeaderFilter>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataCheckColumn FieldName="Organization Admin" VisibleIndex="12" PropertiesCheckEdit-ValueType="System.Int32">
                                    <PropertiesCheckEdit ValueChecked="1" ValueUnchecked="0" ValueGrayed="0" DisplayTextChecked="Yes" DisplayTextUnchecked="No" NullDisplayText="No" UseDisplayImages="False"></PropertiesCheckEdit>
                                </dx:GridViewDataCheckColumn>
                            </Columns>
                        </dx:ASPxGridView>
                        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="Data Source=66.11.14.181;Initial Catalog=IICIC;User ID=II_USER;Password=IIReport1394*nf;MultipleActiveResultSets=True;Application Name=EntityFramework" ProviderName="System.Data.SqlClient" SelectCommand="SELECT * FROM [AdminManageUsers] WHERE ([OrganizationId] = @OrganizationId)">
                            <SelectParameters>
                                <asp:SessionParameter Name="OrganizationId" SessionField="OrganizationId" Type="Int64" />
                            </SelectParameters>
                        </asp:SqlDataSource>
                    </div>
                </asp:Panel>
                <asp:Label ID="lbl_noAccess" runat="server" CssClass="text-danger" Text="You do not have the required permissions to view this section of the site." Visible="false"></asp:Label>
            </div>
        </div>
    </div>
</asp:Content>

