﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site.master" AutoEventWireup="false" CodeFile="EmailOptOut.aspx.vb" Inherits="Account_EmailOptOut" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="bg-content">
        <div class="container-fluid">
            <div class="row">
                Please enter your email and click the button below to no longer receive emails about this survey.
                <br />
                <br />
            </div>
            <div class="row">
                <div class="col-md-1">Email:  </div>
                <div class="col-md-3">
                    <asp:TextBox ID="txt_email" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
                <asp:Button ID="btn_optOut" runat="server" Text="Opt Out" CssClass="btn btn-default" />

            </div>
            <div class="row">
                <p>
                    <asp:Label ID="lbl_optOut" runat="server" CssClass="text-success" Text="You will no longer receive emails about this survey." Visible="False"></asp:Label>
                </p>
            </div>
        </div>
    </div>
</asp:Content>

