﻿
Imports Models

Partial Class Login
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not IsPostBack Then
            'Clear Session Variables

            Session("UserId") = Nothing
            Session("LocationID") = Nothing 'Instead of IICode per SH
            Session("OrganizationId") = Nothing
            Session("IsAdmin") = Nothing
            'Using db As New IICICEntities
            '    Dim project As New Project
            '    project = (From p In db.Projects Where p.ProjectId = 1185).FirstOrDefault
            '    If project IsNot Nothing Then
            '        If project.MaintenanceModeIsActive Then
            '            btn_login.Enabled = False
            '            lbl_systemMessage.Text = project.MaintenanceMessage
            '        End If
            '        If project.SurveyIsActive Then
            '            panel_registration.Visible = True
            '        Else
            '            panel_registration.Visible = False
            '        End If
            '    End If
            'End Using
        End If
    End Sub
    Protected Sub btn_login_Click(sender As Object, e As EventArgs) Handles btn_login.Click
        Using db As New IICICEntities
            Dim user As New User
            user = (From u In db.Users Where u.UserName = txt_username.Text And u.Password = txt_password.Text).FirstOrDefault
            If user IsNot Nothing Then
                If user.IsActive Then
                    Dim uLog As New UserLog With {.UserId = user.UserId, .LoginDate = Now, .IP = Request.UserHostAddress}
                    If Session("Client") IsNot Nothing Then
                        uLog.Site = Session("Client")
                    End If
                    db.UserLogs.Add(uLog)
                    db.SaveChanges()
                    Dim userRoles As New List(Of Integer)
                    Dim roles = (From ur In db.UserRoles Where ur.UserId = user.UserId)
                    Dim ds As New DataSet
                    Dim datasets = (From d In db.DataSets Where d.OrganizationId = user.OrganizationId)
                    Dim datasetList As New List(Of Integer)
                    For Each d As DataSet In datasets
                        datasetList.Add(d.LocationID)
                    Next
                    For Each r As UserRole In roles
                        userRoles.Add(r.RoleId)
                    Next
                    Dim dSet As New DataSet
                    dSet = (From d In db.DataSets Where d.OrganizationId = user.OrganizationId).FirstOrDefault
                    Session("UserId") = user.UserId
                    Session("LocationID") = dSet.LocationID
                    Session("OrganizationId") = user.OrganizationId
                    If userRoles.Contains(3) Then
                        Session("IsAdmin") = True
                    Else
                        Session("IsAdmin") = False
                    End If

                    Session("UserRoles") = userRoles
                    Session("Datasets") = datasetList

                    If Session("UploadOnly") IsNot Nothing Then
                        If Session("UploadOnly") = True Then
                            FormsAuthentication.SetAuthCookie(txt_username.Text, True)
                            Response.Redirect("../Surveys/Comp/OfflineSurvey.aspx")
                        Else
                            FormsAuthentication.RedirectFromLoginPage(txt_username.Text, True)
                        End If
                    Else
                        FormsAuthentication.RedirectFromLoginPage(txt_username.Text, True)
                    End If

                Else
                    Dim err As New CustomValidator() With {.IsValid = False, .ErrorMessage = "Your account has been locked.  Please contact Industry Insights.", .ValidationGroup = "login"}
                    Page.Validators.Add(err)
                End If
            Else
                Dim err As New CustomValidator() With {.IsValid = False, .ErrorMessage = "Invalid login.  Please try again or contact Industry Insights.", .ValidationGroup = "login"}
                Page.Validators.Add(err)
            End If
        End Using
    End Sub
End Class
