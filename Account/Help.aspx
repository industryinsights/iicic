﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site.master" AutoEventWireup="false" CodeFile="Help.aspx.vb" Inherits="Account_Help" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="bg-content">
        <h2 class="page-header">Cross-Industry Compensation Benchmarking Portal</h2>
        <p>
            The Cross-Industry Compensation Benchmarking Portal is an invaluable business tool that will present accurate and timely information about your company. The portal gives respondents a better understanding of how your company&#39;s compensation levels and benefits practices compare to other distributors/wholesalers.&nbsp; 
        </p>
        <h3 class="helpHeader">Industry Insights, Inc.:</h3>
        <p>
            The Cross-Industry Compensation Benchmarking Study is conducted by <a href="http://www.industryinsights.com">Industry Insights, Inc.</a> an objective independent research firm, that specializes in these types of studies. Industry Insights has more than 35 years of experience in conducting confidential, compensation and financial/operational benchmarking studies of this nature.&nbsp; All information submitted to Industry Insights (in any format) is securely maintained and held strictly confidential. No individual company data will ever be divulged beyond Industry Insights.
        </p>
        <p>
            If you have any questions or issues, please contact:
        </p>
        <table class="table-condensed centered">
            <tr>
                <td colspan="3" class="text-bold">Industry Insights:</td>
            </tr>
            <tr>
                <td>Mike Ferris<br />
                    <a href="mailto:comp@industryinsights.com">comp@industryinsights.com</a><br />
                    614.389.2100 x241</td>
                <td>&nbsp;&nbsp;&nbsp;OR &nbsp;
                </td>
                <td>Greg Manns, CPA, CGMA, CITP<br />
                    <a href="mailto:gmanns@industryinsights.com">gmanns@industryinsights.com</a><br />
                    614.389.2100 x108
                </td>
            </tr>
        </table>
        <h3 class="helpHeader">Participation Formats:</h3>
        <p>
            Our goal is to provide you with multiple ways to participate, so you can find a format that works best for you. We will accept surveys from: the online questionnaire; Excel/PDF files uploaded to the site; Excel/PDF files emailed to Industry Insights (<a href="mailto:comp@industryinsights.com">comp@industryinsights.com</a>); and printed surveys mailed or faxed to Industry Insights.
        </p>
        <h3 class="helpHeader">Online Questionnaire:</h3>
        <p>
            It is estimated that the survey will take 1 to 2&nbsp; hours to complete, so you are able to complete the online surveys in more than one sitting using password-enabled save and return functionality. As you navigate the forms, your responses will be saved each time you select one of the navigation buttons at the top or bottom of each page. When you have completed a given page, please check the box labeled "This section is complete." Your progress can be monitored by returning to the "Home" page and reviewing 
                    the status of your sections in the survey table.&nbsp; Once all sections of your form are &quot;Complete,&quot; the &quot;Submit Survey&quot; button will become active.
        </p>
        <h3 class="helpHeader">Navigating the Online Questionnaire:</h3>
        <p>
            You can access each survey section by clicking the appropriate link on the Table of Contents listed on the "Home" page. Navigation buttons are located at the bottom of each page to guide you from section to section. You can also use the menu&nbsp; at the top of the page to transition from one page to another.
        </p>
        <h3 class="helpHeader">Saving Data</h3>
        <p>
            Please note that your full survey will be saved each time you click "Save" or a navigation button. However, your results for any given section will not be compiled for the final report unless you indicate that the section is complete by checking the box at the bottom of each section labeled "This section is complete.&quot;
        </p>

        <h3 class="helpHeader">Excel File</h3>
        <p>
            An Excel version of the questionnaire is available for those who prefer to use this format. The link to the download/upload page is listed on the &quot;Home&quot; page. Simply click the appropriate link to download the file to your computer. Once you complete and save the survey, you can upload the file to this portal, or you can send the file to Industry Insights at 6235 Emerald Parkway, Dublin, OH 43016, Fax it to (614) 389-3816, or email it to&nbsp;<a href="mailto:comp@industryinsights.com">comp@industryinsights.com</a>. <em>(Note – uploading the file through the portal is the most secure method.  If you do chose to email the file, we recommend that you password protect the file.)</em>
        </p>
        <h3 class="helpHeader">Adding Users to Your Account</h3>
        <p>
            When you register an account, by default your account is an administrator account.&nbsp; As an administrator, you may add or remove users to your account.&nbsp; To add a user, on the home page, click on the &quot;Add Users&quot; button.&nbsp; Enter the email address and name for the user you&#39;d like to add and indicate if this user should be an administrator (an account may have multiple administrators). Administrators have the ability to add/remove users from the account.&nbsp; When you click the &quot;Add User&quot; button, an account for the user will be created, and an email with unique account credentials will be sent informing the user that he/she has been added to your account.&nbsp;
        </p>
        <p>
            If you&#39;d like to remove users on your account, click on the &quot;Organization&quot; link in the top menu.&nbsp; This allows you to view the full list of authorized users on your account and allows you to add, edit, and/or delete users from this page.
        </p>

        <h2 class="page-header">FAQs</h2>
        <p class="helpHeader text-bold">
            Q: What is the <em>Cross-Industry Compensation Benchmarking Portal</em>?
        </p>
        <p>
            A: The Cross-Industry Compensation Benchmarking Portal (<a href="http://www.compensationbenchmarking.com">www.compensationbenchmarking.com</a>) contains the survey and reporting tools for the Cross-industry Compensation and Benefits Survey conducted by Industry Insights, Inc.&nbsp; The project is sponsored by over 20 wholesale-distributor associations.&nbsp; The portal is a one-stop location for an organization to benchmark their compensation levels and benefit practices.
        </p>
        <p class="helpHeader text-bold">
            Q: Once I enter my data, is it secure?
        </p>
        <p>
            A: It sure is. For all of its online survey research, Industry Insights utilizes a secure server, 
                    which is housed in highly secure data centers that meet critical standards that are supported by third‐party SSAE18/SOC attestation reports.&nbsp; Additionally, all data transmitted between the server and site users are protected by SSL data encryption.
        </p>
        <p class="helpHeader text-bold">
            Q: What is the minimum number of companies that need to submit before it will be included in the report results? 
        </p>
        <p>
            A: There is a minimum of 5 participants required before any data point will be reported for both the mean and median values.  A minimum of 7 respondents is needed for any quartile information to be reported.
        </p>
        <p class="helpHeader text-bold">
            Q: What do I receive for participating?
        </p>
        <p>
            A: <strong>Survey participants</strong> will receive the following benefits:
        </p>
        <asp:Panel ID="panel_defaultFAQ" runat="server">
            <ul>
                <li>A PDF based <strong>Association Specific Report</strong> will include several data aggregations at the cross-industry level (i.e., revenue size, industry type, geographic region) as well as an aggregation for the other association constituents that participated in the study.</li>
                <li><strong>Excel Data Tables</strong> will provide a user with the cross-industry data aggregates that will appear in the association-specific report as well as additional aggregates that go beyond the level of detail possible for a printer-friendly report.</li>
                <li>A PDF based <strong>Company-specific Compensation Report (CCR)</strong> is a confidential, individualized report of a participant’s own compensation and benefit information displayed alongside the appropriate comparatives (i.e., association constituent’s data, cross-industry revenue size, cross-industry region, etc.).&nbsp; This report provides participants with a useful managerial tool for quickly identifying compensation levels that may be too high or too low as well as the gauging the strength of their benefit packages. </li>
                <li>Your association may have optionally provided access to Industry Insights&#39; <strong>ASearchable Results Application</strong> which allows users to aggregate the data set based on multiple filters of their choosing.&nbsp; For example, users can aggregate their data by multiple levels (e.g., respondents by a specific geographic region with a certain revenue size and in a specific industry type), whereas the PDF reports and Excel data tables must stay more general, usually employing just single-level aggregations.</li>
            </ul>
        </asp:Panel>
        <asp:Panel ID="panel_nawFAQ" runat="server" Visible="false">
            <ul>
                <li>A PDF based <strong>Association Specific Report</strong> will include several data aggregations at the cross-industry level (i.e., revenue size, industry type, geographic region) as well as an aggregation for the other association constituents that participated in the study.</li>
                <li><strong>Excel Data Tables</strong> will provide a user with the cross-industry data aggregates that will appear in the association-specific report as well as additional aggregates that go beyond the level of detail possible for a printer-friendly report.</li>
                <li>A PDF based <strong>Company-specific Compensation Report (CCR)</strong> is a confidential, individualized report of a participant’s own compensation and benefit information displayed alongside the appropriate comparatives (i.e., association constituent’s data, cross-industry revenue size, cross-industry region, etc.).&nbsp; This report provides participants with a useful managerial tool for quickly identifying compensation levels that may be too high or too low as well as the gauging the strength of their benefit packages. </li>
                <li><strong>A Searchable Results Application</strong> which allows users to aggregate the data set based on multiple filters of their choosing.&nbsp; For example, users can aggregate their data by multiple levels (e.g., respondents by a specific geographic region with a certain revenue size and in a specific industry type), whereas the PDF reports and Excel data tables must stay more general, usually employing just single-level aggregations.</li>
                <li><strong>Executive Compensation Report</strong>:  This report will deeply examine the compensation statistics and patterns of executives’ pay and benefit plans.</li>
                <li><strong>Interactive Infographic</strong>:  This interactive tool allows you to create an especially attractive display of the study’s most interesting results for your own reporting use.</li>
                <li><strong>Executive Compensation Calculator</strong>:  This tool is custom built for NAW participants and provides an estimated total compensation figure and expected “pay band” for users, based on selected options (e.g., years of experience, sales volume, education background, etc.)</li>
            </ul>
        </asp:Panel>
    </div>
</asp:Content>

