﻿
Imports System.Data
Imports System.Net.Mail
Imports DevExpress.Web
Imports Models

Partial Class Account_OrganizationAdmin
    Inherits System.Web.UI.Page
    Private ds As System.Data.DataSet = Nothing
    Protected Sub Page_Init(sender As Object, e As EventArgs) Handles Me.Init
        Dim cs As String = ConfigurationManager.ConnectionStrings("Website1DatabaseAuthentication").ToString
        SqlDataSource1.ConnectionString = cs
        SqlDataSource2.ConnectionString = cs
    End Sub
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim userId As Integer = 0
        If Session("UserId") Is Nothing Then
            Response.Redirect("login.aspx")
        Else
            userId = Session("UserId")
        End If
        Using db As New IICICEntities
            Dim userRole As New UserRole
            userRole = (From ur In db.UserRoles Where ur.UserId = userId And ur.RoleId = 3).FirstOrDefault

            If userRole Is Nothing Then
                panel_admin.Visible = False
                lbl_noAccess.Visible = True
            Else
                If Not IsPostBack Then
                    Dim orgId As Integer = Session("OrganizationId")
                End If
                If (Not IsPostBack) OrElse (Session("Dataset") Is Nothing) Then
                    Dim args As New DataSourceSelectArguments
                    ds = New System.Data.DataSet()

                    Dim view As DataView = DirectCast(SqlDataSource1.Select(args), DataView)
                    Dim masterTable As DataTable = view.ToTable()
                    masterTable.PrimaryKey = New DataColumn() {masterTable.Columns("UserId")}
                    Dim detailTable As New DataTable()
                    detailTable = masterTable
                    detailTable.PrimaryKey = New DataColumn() {detailTable.Columns("UserId")}
                    detailTable.Columns.Add("MasterID", GetType(Integer))
                    Dim index As Integer = 0
                    ds.Tables.Add(masterTable)
                    ds.Tables.Remove(detailTable)
                    ds.Tables.Add(detailTable)
                    Session("Dataset") = ds
                Else
                    ds = DirectCast(Session("Dataset"), System.Data.DataSet)
                End If
                gv_users.DataSource = ds.Tables(0)
                gv_users.DataBind()
            End If
        End Using

    End Sub

#Region "Gridview Routines"

    Protected Sub gv_Users_RowUpdating(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataUpdatingEventArgs)
        ds = DirectCast(Session("Dataset"), System.Data.DataSet)
        Dim gridView As ASPxGridView = DirectCast(sender, ASPxGridView)
        Dim dataTable As DataTable = If(gridView.GetMasterRowKeyValue() IsNot Nothing, ds.Tables(1), ds.Tables(0))
        Dim row As DataRow = dataTable.Rows.Find(e.Keys(0))
        Dim enumerator As IDictionaryEnumerator = e.NewValues.GetEnumerator()
        enumerator.Reset()
        Using db As New IICICEntities
            Dim user As New User
            Dim userId As Integer = e.Keys(0)
            user = (From u In db.Users Where u.UserId = userId).FirstOrDefault
            Dim roles = (From r In db.UserRoles Where r.UserId = user.UserId)
            Dim userRoles As New List(Of Integer)
            For Each r As UserRole In roles
                userRoles.Add(r.RoleId)
            Next
            Do While enumerator.MoveNext()
                If enumerator.Value = Nothing Then
                    row(enumerator.Key.ToString()) = DBNull.Value
                Else
                    row(enumerator.Key.ToString()) = enumerator.Value
                End If
                If enumerator.Key.ToString = "User Id" Then
                    'Dim userId As Integer = enumerator.Value
                    'user = (From u In db.Users Where u.UserId = userId).FirstOrDefault
                    'Dim roles = (From r In db.UserRoles Where r.UserId = user.UserId)
                    'Dim userRoles As New List(Of Integer)
                    'For Each r As UserRole In roles
                    '    userRoles.Add(r.RoleId)
                    'Next
                End If
                If enumerator.Key.ToString = "Email" Then
                    user.Email = enumerator.Value
                ElseIf enumerator.Key.ToString = "Full Name" Then
                    user.FullName = enumerator.Value
                ElseIf enumerator.Key.ToString = "Organization Admin" Then
                    If enumerator.Value = True And Not userRoles.Contains(3) Then
                        Dim newUserRole As New UserRole() With {.RoleId = 3, .UserId = user.UserId}
                        db.UserRoles.Add(newUserRole)
                    ElseIf enumerator.Value = False Then
                        Dim removeUserRole As UserRole = (From u In db.UserRoles Where u.UserId = user.UserId And u.RoleId = 3).FirstOrDefault
                        If removeUserRole IsNot Nothing Then
                            db.UserRoles.Remove(removeUserRole)
                        End If
                    End If
                ElseIf enumerator.Key.ToString = "Survey Access" Then
                    If enumerator.Value = True Then
                        Dim ur As New UserRole() With {.UserId = user.UserId, .RoleId = 4}
                        db.UserRoles.Add(ur)
                    ElseIf enumerator.Value = False Then
                        Dim removeUserRole As UserRole = (From u In db.UserRoles Where u.UserId = user.UserId And u.RoleId = 4).FirstOrDefault
                        If removeUserRole IsNot Nothing Then
                            db.UserRoles.Remove(removeUserRole)
                        End If
                    End If
                ElseIf enumerator.Key.ToString = "Reporting Access" Then
                    If enumerator.Value = True Then
                        Dim ur As New UserRole() With {.UserId = user.UserId, .RoleId = 6}
                        db.UserRoles.Add(ur)
                    ElseIf enumerator.Value = False Then
                        Dim removeUserRole As UserRole = (From u In db.UserRoles Where u.UserId = user.UserId And u.RoleId = 6).FirstOrDefault
                        If removeUserRole IsNot Nothing Then
                            db.UserRoles.Remove(removeUserRole)
                        End If
                    End If
                End If
            Loop
            db.SaveChanges()
        End Using
        gridView.CancelEdit()
        e.Cancel = True
    End Sub
    Protected Sub gv_Users_RowInserting(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataInsertingEventArgs)
        ds = DirectCast(Session("Dataset"), System.Data.DataSet)
        Dim gridView As ASPxGridView = DirectCast(sender, ASPxGridView)
        Dim dataTable As DataTable = If(gridView.GetMasterRowKeyValue() IsNot Nothing, ds.Tables(1), ds.Tables(0))
        Dim row As DataRow = dataTable.NewRow()
        e.NewValues("UserId") = GetNewId()
        Dim enumerator As IDictionaryEnumerator = e.NewValues.GetEnumerator()
        enumerator.Reset()
        Using db As New IICICEntities
            Dim orgId As Integer = Session("OrganizationId")
            Dim LocationID As Integer = 0
            Dim ds As New Models.DataSet
            ds = (From d In db.DataSets Where d.OrganizationId = orgId Order By LocationID).FirstOrDefault
            LocationID = ds.LocationID
            Dim user As New User With {.OrganizationId = orgId, .CreateDate = Now, .IsActive = True, .Password = GeneratePassword()}
            db.Users.Add(user)
            db.SaveChanges()
            Dim ud As New UserDataSet With {.UserId = user.UserId, .LocationID = LocationID}
            db.UserDataSets.Add(ud)
            db.SaveChanges()
            Do While enumerator.MoveNext()
                If enumerator.Key.ToString() <> "Count" Then
                    If enumerator.Value = Nothing Then
                        row(enumerator.Key.ToString()) = DBNull.Value
                    Else
                        row(enumerator.Key.ToString()) = enumerator.Value
                    End If
                    If enumerator.Key.ToString = "Email" Then
                        user.Email = enumerator.Value
                        user.UserName = enumerator.Value
                    ElseIf enumerator.Key.ToString = "Full Name" Then
                        user.FullName = enumerator.Value
                    ElseIf enumerator.Key.ToString = "Title" Then
                        user.Title = enumerator.Value
                    ElseIf enumerator.Key.ToString = "Address" Then
                        user.AddressLine1 = enumerator.Value
                    ElseIf enumerator.Key.ToString = "City" Then
                        user.City = enumerator.Value
                    ElseIf enumerator.Key.ToString = "State" Then
                        user.State = enumerator.Value
                    ElseIf enumerator.Key.ToString = "Zip Code" Then
                        user.ZipCode = enumerator.Value
                    ElseIf enumerator.Key.ToString = "Phone" Then
                        user.Phone = enumerator.Value
                        'ElseIf enumerator.Key.ToString = "Email Opt Out" Then
                        '    user.EmailOptOut = enumerator.Value
                    ElseIf enumerator.Key.ToString = "Is Active" Then
                        user.IsActive = enumerator.Value
                    ElseIf enumerator.Key.ToString = "Organization Admin" Then
                        If enumerator.Value = True Then
                            Dim ur As New UserRole() With {.UserId = user.UserId, .RoleId = 3}
                            db.UserRoles.Add(ur)
                        ElseIf enumerator.Value = False Then
                            Dim removeUserRole As UserRole = (From u In db.UserRoles Where u.UserId = user.UserId And u.RoleId = 3).FirstOrDefault
                            If removeUserRole IsNot Nothing Then
                                db.UserRoles.Remove(removeUserRole)
                            End If
                        End If
                    ElseIf enumerator.Key.ToString = "Survey Access" Then
                        If enumerator.Value = True Then
                            Dim ur As New UserRole() With {.UserId = user.UserId, .RoleId = 4}
                            db.UserRoles.Add(ur)
                        ElseIf enumerator.Value = False Then
                            Dim removeUserRole As UserRole = (From u In db.UserRoles Where u.UserId = user.UserId And u.RoleId = 4).FirstOrDefault
                            If removeUserRole IsNot Nothing Then
                                db.UserRoles.Remove(removeUserRole)
                            End If
                        End If
                    ElseIf enumerator.Key.ToString = "Reporting Access" Then
                        If enumerator.Value = True Then
                            Dim ur As New UserRole() With {.UserId = user.UserId, .RoleId = 6}
                            db.UserRoles.Add(ur)
                        ElseIf enumerator.Value = False Then
                            Dim removeUserRole As UserRole = (From u In db.UserRoles Where u.UserId = user.UserId And u.RoleId = 6).FirstOrDefault
                            If removeUserRole IsNot Nothing Then
                                db.UserRoles.Remove(removeUserRole)
                            End If
                        End If
                    End If
                End If
            Loop
            db.SaveChanges()
            SendEmail(user.UserName, user.Password)
        End Using
        gridView.CancelEdit()
        e.Cancel = True
        dataTable.Rows.Add(row)
    End Sub
    Protected Sub gv_Users_RowValidating(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataValidationEventArgs)
        For Each column As GridViewColumn In gv_users.Columns
            Dim dataColumn As GridViewDataColumn = TryCast(column, GridViewDataColumn)
            If dataColumn Is Nothing Then
                Continue For
            End If
            If e.NewValues(dataColumn.FieldName) IsNot Nothing Then
                If dataColumn.FieldName = "Email" Then
                    If e.NewValues("Email") Is Nothing Then
                        e.Errors(dataColumn) = "Email is required."
                    Else
                        If e.IsNewRow = True Then

                            Using db As New IICICEntities
                                Dim user As New User
                                Dim temp As String = e.NewValues("Email")
                                user = (From u In db.Users Where u.Email = temp Or u.UserName = temp).FirstOrDefault
                                If user IsNot Nothing Then
                                    e.Errors(dataColumn) = "Account already exists"
                                End If
                            End Using
                        End If
                    End If
                End If
            End If
        Next column
        If e.Errors.Count > 0 Then
            e.RowError = "One or more required fields are missing or the account already exists."
        End If

    End Sub

    Protected Sub gv_Users_RowDeleting(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataDeletingEventArgs)
        If gv_users.VisibleRowCount <> 1 Then
            Dim i As Integer = gv_users.FindVisibleIndexByKeyValue(e.Keys(gv_users.KeyFieldName))
            Using db As New IICICEntities
                Dim user As New User
                Dim id As Integer = e.Keys(gv_users.KeyFieldName)
                user = (From u In db.Users Where u.UserId = id).FirstOrDefault
                db.Users.Remove(user)
                db.SaveChanges()
            End Using
            Dim c As Control = gv_users.FindDetailRowTemplateControl(i, "gv_users2")
            e.Cancel = True
            ds = DirectCast(Session("Dataset"), System.Data.DataSet)
            ds.Tables(0).Rows.Remove(ds.Tables(0).Rows.Find(e.Keys(gv_users.KeyFieldName)))
        Else
            e.Cancel = True
        End If
    End Sub
    Private Function GetNewId() As Integer
        ds = DirectCast(Session("Dataset"), System.Data.DataSet)
        Dim table As DataTable = ds.Tables(0)
        If table.Rows.Count = 0 Then
            Return 0
        End If
        Dim max As Integer = Convert.ToInt32(table.Rows(0)("UserId"))
        For i As Integer = 1 To table.Rows.Count - 1
            If Convert.ToInt32(table.Rows(i)("UserId")) > max Then
                max = Convert.ToInt32(table.Rows(i)("UserId"))
            End If
        Next i
        Return max + 1
    End Function
    Sub gv_locations_CommandButtonInitialize(ByVal sender As Object, ByVal e As ASPxGridViewCommandButtonEventArgs)
        If e.VisibleIndex = -1 Then
            Return
        End If

        Select Case e.ButtonType
            Case ColumnCommandButtonType.Edit

            Case ColumnCommandButtonType.Delete
                If gv_locations.VisibleRowCount = 1 Then
                    e.Enabled = False
                End If
        End Select
    End Sub
    Sub gv_users_CommandButtonInitialize(ByVal sender As Object, ByVal e As ASPxGridViewCommandButtonEventArgs)
        If e.VisibleIndex = -1 Then
            Return
        End If

        Select Case e.ButtonType
            Case ColumnCommandButtonType.Edit

            Case ColumnCommandButtonType.Delete
                If gv_users.VisibleRowCount = 1 Then
                    e.Enabled = False
                End If
        End Select
    End Sub
    Function GeneratePassword() As String
        Dim s As String = "ACDEFGHJKLMNPQRSTUVWXYZabxdefghijkmnpqrstuvwxyz2345679!"
        Dim r As New Random
        Dim sb As New StringBuilder
        For i As Integer = 1 To 8
            Dim idx As Integer = r.Next(0, 35)
            sb.Append(s.Substring(idx, 1))
        Next
        Return sb.ToString
    End Function
    Sub SendEmail(ByVal username As String, ByVal password As String)
        Using client As New SmtpClient
            Dim message As New MailMessage
            Dim address As Object = Nothing
            address = New MailAddress(username)
            message.From = New MailAddress(message.From.Address, "Mike Ferris")
            message.IsBodyHtml = True
            message.To.Add(address)
            message.Subject = "Welcome to the Cross-Industry Compensation Portal!"
            message.Body = GetEmailHtml(username, password)
            client.Send(message)
        End Using
    End Sub

    Private Function GetEmailHtml(ByVal username As String, ByVal password As String) As String
        Dim url As String = "https://compensationbenchmarking.com"
        If Session("Client") IsNot Nothing Then
            url = String.Format("https://compensationbenchmarking.com/{0}", Session("Client"))
        End If
        Dim html As New StringBuilder()
        html.Append("<html>")
        html.Append("<head>")
        html.Append("<title></title>")
        html.Append("<style type=""text/css"">")
        html.Append("html, body")
        html.Append("{")
        html.Append("margin: 0;")
        html.Append("font-family: Arial;")
        html.Append("font-size: 10pt;")
        html.Append("}")
        html.Append("")
        html.Append("</style>")
        html.Append("</head>")
        html.Append("<body>")
        html.Append("<p>You have been given access to the Cross-Industry Compensation Portal by your Organization.</p>")
        html.Append("<p>Your login credentials are:</p>")
        html.AppendFormat("<p>Username:  {0}</p>", username)
        html.AppendFormat("<p>Password:  {0}</p>", password)

        html.AppendFormat("<p>To login, please visit the <a href=""{0}"">Cross-Industry Compensation Portal Login</a>.</p>", url)

        html.Append("<p>If you have any questions or believe that you have received this email in error, please contact me by using the information below.</p>")
        html.Append("<p>Thanks,</p>")
        html.Append("<p style=""color:#7F7F7F"">Mike Ferris<br/>Industry Insights, Inc.<br/>614.389.2100 ext. 241<br/><a href=""mailto:comp@industryinsights.com"">comp@industryinsights.com</a></p>")
        html.Append("</body>")
        html.Append("</html>")
        Return html.ToString()

    End Function
#End Region

End Class
