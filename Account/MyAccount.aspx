﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Survey.master" AutoEventWireup="false" CodeFile="MyAccount.aspx.vb" Inherits="Account_MyAccount" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script type="text/javascript">
        $(document).ready(function () {
            $('[data-toggle="tooltip"]').tooltip();
            $('#ContentPlaceHolder1_txt_email').on('change', function () {
                $('#ContentPlaceHolder1_txt_username').val($('#ContentPlaceHolder1_txt_email').val());
            });
        });
    </script>
    <div class="bg-content">
        <div class="container-fluid">
            <div class="row text-left">
                <ol class="breadcrumb">
                    <%--Temp change--%>
                    <%--<li><a href="../Surveys/Comp/TOC.aspx">Home</a></li>--%>
                    <li><a href="../Surveys/Comp/TableOfContents.aspx">Home</a></li>
                    <li class="active">My Account</li>
                </ol>
            </div>
            <div class="row">
                <h2 class="text-left">My Account</h2>
                <hr />
                <p class="text-center"><span class="text-danger">*</span> indicates required field</p>
            </div>
            <div class="row">
                <div class="col-md-12 text-center">
                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="alert-danger" ValidationGroup="myaccount" />
                </div>
            </div>
            <div class="row text-center">
                <asp:Label ID="lbl_successfulUpdate" runat="server" CssClass="text-success" Text="Update was successful!" Visible="False"></asp:Label>
            </div>
            <div class="row">
                <div class="col-md-12 text-center">
                    <table class="table-condensed table-registration table-hover centered">

                        <tr>
                            <td class="td-bg-primary text-left text-bold" colspan="3">Contact Information</td>
                        </tr>
                        <tr>
                            <td class="col-md-1 text-left">
                                <span class="text-danger">*</span><asp:RequiredFieldValidator ID="RequiredFieldValidator22" runat="server" CssClass="text-danger" ErrorMessage="First Name is required." ControlToValidate="txt_fname" ValidationGroup="myaccount">*</asp:RequiredFieldValidator>
                            </td>
                            <td class="col-md-2">First Name:</td>
                            <td class="col-md-4 text-center">
                                <asp:TextBox ID="txt_fname" runat="server" CssClass="form-control"></asp:TextBox>
                            </td>
                        </tr>

                        <tr>
                            <td class="col-md-1 text-left">
                                <span class="text-danger">*</span><asp:RequiredFieldValidator ID="RequiredFieldValidator21" runat="server" CssClass="text-danger" ErrorMessage="Last Name is required." ControlToValidate="txt_lname" ValidationGroup="myaccount">*</asp:RequiredFieldValidator>
                            </td>
                            <td class="col-md-2">Last Name:</td>
                            <td class="col-md-4 text-center">
                                <asp:TextBox ID="txt_lname" runat="server" CssClass="form-control"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="col-md-1 text-left">&nbsp;</td>
                            <td class="col-md-2">Title:</td>
                            <td class="col-md-4 text-center">
                                <asp:TextBox ID="txt_title" runat="server" CssClass="form-control"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="col-md-1 text-left">
                                <span class="text-danger">*</span><asp:RequiredFieldValidator ID="RequiredFieldValidator15" runat="server" CssClass="text-danger" ErrorMessage="Email is required." ControlToValidate="txt_email" ValidationGroup="myaccount">*</asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" CssClass="text-danger" ErrorMessage="Invalid email format." ControlToValidate="txt_email" ValidationExpression="^([0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,9})$" ValidationGroup="myaccount">*</asp:RegularExpressionValidator>
                            </td>
                            <td class="col-md-2">Email:</td>
                            <td class="col-md-4 text-center">
                                <asp:TextBox ID="txt_email" runat="server" CssClass="form-control"></asp:TextBox>
                            </td>
                        </tr>

                        <tr>
                            <td class="col-md-1 text-left">
                                <span class="text-danger">*</span><asp:RequiredFieldValidator ID="RequiredFieldValidator16" runat="server" CssClass="text-danger" ErrorMessage="Confirm Email is required." ControlToValidate="txt_confirmEmail" ValidationGroup="myaccount">*</asp:RequiredFieldValidator>
                                <asp:CompareValidator ID="CompareValidator1" runat="server" CssClass="text-danger" ErrorMessage="Emails do not match." ControlToCompare="txt_email" ControlToValidate="txt_confirmEmail" ValidationGroup="myaccount">*</asp:CompareValidator>
                            </td>
                            <td class="col-md-2">Confirm Email:</td>
                            <td class="col-md-4 text-center">
                                <asp:TextBox ID="txt_confirmEmail" runat="server" CssClass="form-control"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="col-md-1 text-left">&nbsp;</td>
                            <td class="col-md-2">Phone:</td>
                            <td class="col-md-4  text-center">
                                <asp:TextBox ID="txt_phone" runat="server" CssClass="form-control"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="td-bg-primary text-left text-bold" colspan="3">Organization Information</td>
                        </tr>
                        <tr>
                            <td class="col-md-1 text-left">
                                <span class="text-danger">*</span><asp:RequiredFieldValidator ID="RequiredFieldValidator20" runat="server" CssClass="text-danger" ErrorMessage="Organization name is required." ControlToValidate="txt_organization" ValidationGroup="myaccount">*</asp:RequiredFieldValidator>
                            </td>
                            <td class="col-md-2">Organization Name:</td>
                            <td class="col-md-4  text-center">
                                <asp:TextBox ID="txt_organization" runat="server" CssClass="form-control"></asp:TextBox>
                            </td>
                        </tr>

                        <tr>
                            <td class="col-md-1 text-left">&nbsp;</td>
                            <td class="col-md-2">Address Line 1:</td>
                            <td class="col-md-4  text-center">
                                <asp:TextBox ID="txt_address1" runat="server" CssClass="form-control"></asp:TextBox>
                            </td>
                        </tr>

                        <tr>
                            <td class="col-md-1 text-left">&nbsp;</td>
                            <td class="col-md-2">Address Line 2:</td>
                            <td class="col-md-4  text-center">
                                <asp:TextBox ID="txt_address2" runat="server" CssClass="form-control"></asp:TextBox>
                            </td>
                        </tr>

                        <tr>
                            <td class="col-md-1 text-left">&nbsp;</td>
                            <td class="col-md-2">City:</td>
                            <td class="col-md-4  text-center">
                                <asp:TextBox ID="txt_city" runat="server" CssClass="form-control"></asp:TextBox>
                            </td>
                        </tr>

                        <tr>
                            <td class="col-md-1 text-left">
                                <span class="text-danger">*</span><asp:RequiredFieldValidator ID="RequiredFieldValidator17" runat="server" CssClass="text-danger" ErrorMessage="State is required." ControlToValidate="ddl_state" ValidationGroup="myaccount">*</asp:RequiredFieldValidator>
                            </td>
                            <td class="col-md-2">State:</td>
                            <td class="col-md-4  text-center">
                                <asp:DropDownList ID="ddl_state" runat="server" CssClass="form-control">
                                    <asp:ListItem Value="">Select state</asp:ListItem>
                                    <asp:ListItem Value="1">Alabama</asp:ListItem>
                                    <asp:ListItem Value="2">Alaska</asp:ListItem>
                                    <asp:ListItem Value="63">American Samoa</asp:ListItem>
                                    <asp:ListItem Value="3">Arizona</asp:ListItem>
                                    <asp:ListItem Value="4">Arkansas</asp:ListItem>
                                    <asp:ListItem Value="5">California</asp:ListItem>
                                    <asp:ListItem Value="6">Colorado</asp:ListItem>
                                    <asp:ListItem Value="7">Connecticut</asp:ListItem>
                                    <asp:ListItem Value="8">Delaware</asp:ListItem>
                                    <asp:ListItem Value="9">District of Columbia</asp:ListItem>
                                    <asp:ListItem Value="10">Florida</asp:ListItem>
                                    <asp:ListItem Value="11">Georgia</asp:ListItem>
                                    <asp:ListItem Value="64">Guam</asp:ListItem>
                                    <asp:ListItem Value="12">Hawaii</asp:ListItem>
                                    <asp:ListItem Value="13">Idaho</asp:ListItem>
                                    <asp:ListItem Value="14">Illinois</asp:ListItem>
                                    <asp:ListItem Value="15">Indiana</asp:ListItem>
                                    <asp:ListItem Value="16">Iowa</asp:ListItem>
                                    <asp:ListItem Value="17">Kansas</asp:ListItem>
                                    <asp:ListItem Value="18">Kentucky</asp:ListItem>
                                    <asp:ListItem Value="19">Louisiana</asp:ListItem>
                                    <asp:ListItem Value="20">Maine</asp:ListItem>
                                    <asp:ListItem Value="21">Maryland</asp:ListItem>
                                    <asp:ListItem Value="22">Massachusetts</asp:ListItem>
                                    <asp:ListItem Value="23">Michigan</asp:ListItem>
                                    <asp:ListItem Value="24">Minnesota</asp:ListItem>
                                    <asp:ListItem Value="25">Mississippi</asp:ListItem>
                                    <asp:ListItem Value="26">Missouri</asp:ListItem>
                                    <asp:ListItem Value="27">Montana</asp:ListItem>
                                    <asp:ListItem Value="28">Nebraska</asp:ListItem>
                                    <asp:ListItem Value="29">Nevada</asp:ListItem>
                                    <asp:ListItem Value="30">New Hampshire</asp:ListItem>
                                    <asp:ListItem Value="31">New Jersey</asp:ListItem>
                                    <asp:ListItem Value="32">New Mexico</asp:ListItem>
                                    <asp:ListItem Value="33">New York</asp:ListItem>
                                    <asp:ListItem Value="34">North Carolina</asp:ListItem>
                                    <asp:ListItem Value="35">North Dakota</asp:ListItem>
                                    <asp:ListItem Value="65">Northern Mariana Islands</asp:ListItem>
                                    <asp:ListItem Value="36">Ohio</asp:ListItem>
                                    <asp:ListItem Value="37">Oklahoma</asp:ListItem>
                                    <asp:ListItem Value="38">Oregon</asp:ListItem>
                                    <asp:ListItem Value="39">Pennsylvania</asp:ListItem>
                                    <asp:ListItem Value="62">Puerto Rico</asp:ListItem>
                                    <asp:ListItem Value="40">Rhode Island</asp:ListItem>
                                    <asp:ListItem Value="41">South Carolina</asp:ListItem>
                                    <asp:ListItem Value="42">South Dakota</asp:ListItem>
                                    <asp:ListItem Value="43">Tennessee</asp:ListItem>
                                    <asp:ListItem Value="44">Texas</asp:ListItem>
                                    <asp:ListItem Value="45">Utah</asp:ListItem>
                                    <asp:ListItem Value="46">Vermont</asp:ListItem>
                                    <asp:ListItem Value="61">Virgin Islands (US)</asp:ListItem>
                                    <asp:ListItem Value="47">Virginia</asp:ListItem>
                                    <asp:ListItem Value="48">Washington</asp:ListItem>
                                    <asp:ListItem Value="49">West Virginia</asp:ListItem>
                                    <asp:ListItem Value="50">Wisconsin</asp:ListItem>
                                    <asp:ListItem Value="51">Wyoming</asp:ListItem>
                                    <asp:ListItem Value="52">Alberta</asp:ListItem>
                                    <asp:ListItem Value="53">British Columbia</asp:ListItem>
                                    <asp:ListItem Value="54">Manitoba</asp:ListItem>
                                    <asp:ListItem Value="55">New Brunswick</asp:ListItem>
                                    <asp:ListItem Value="56">Nova Scotia</asp:ListItem>
                                    <asp:ListItem Value="57">Ontario</asp:ListItem>
                                    <asp:ListItem Value="58">Prince Edward Islands</asp:ListItem>
                                    <asp:ListItem Value="59">Quebec</asp:ListItem>
                                    <asp:ListItem Value="60">Saskechewan</asp:ListItem>
                                    <asp:ListItem Value="71">Other</asp:ListItem>

                                </asp:DropDownList>
                            </td>
                        </tr>

                        <tr>
                            <td class="col-md-1 text-left">&nbsp;</td>
                            <td class="col-md-2">Country:</td>
                            <td class="col-md-4  text-center">
                                <asp:DropDownList ID="ddl_country" runat="server" CssClass="form-control">
                                    <asp:ListItem Value="">Select country</asp:ListItem>
                                    <asp:ListItem Value="1"> Afghanistan</asp:ListItem>
                                    <asp:ListItem Value="2"> Albania</asp:ListItem>
                                    <asp:ListItem Value="3"> Algeria</asp:ListItem>
                                    <asp:ListItem Value="4"> American Samoa</asp:ListItem>
                                    <asp:ListItem Value="5"> Andorra</asp:ListItem>
                                    <asp:ListItem Value="6"> Angola</asp:ListItem>
                                    <asp:ListItem Value="7"> Anguilla</asp:ListItem>
                                    <asp:ListItem Value="8"> Antarctica</asp:ListItem>
                                    <asp:ListItem Value="9"> Antigua And Barbuda</asp:ListItem>
                                    <asp:ListItem Value="10"> Argentina</asp:ListItem>
                                    <asp:ListItem Value="11"> Armenia</asp:ListItem>
                                    <asp:ListItem Value="12"> Aruba</asp:ListItem>
                                    <asp:ListItem Value="13"> Australia</asp:ListItem>
                                    <asp:ListItem Value="14"> Austria</asp:ListItem>
                                    <asp:ListItem Value="15"> Azerbaijan</asp:ListItem>
                                    <asp:ListItem Value="16"> Bahamas</asp:ListItem>
                                    <asp:ListItem Value="17"> Bahrain</asp:ListItem>
                                    <asp:ListItem Value="18"> Bangladesh</asp:ListItem>
                                    <asp:ListItem Value="19"> Barbados</asp:ListItem>
                                    <asp:ListItem Value="20"> Belarus</asp:ListItem>
                                    <asp:ListItem Value="21"> Belgium</asp:ListItem>
                                    <asp:ListItem Value="22"> Belize</asp:ListItem>
                                    <asp:ListItem Value="23"> Benin</asp:ListItem>
                                    <asp:ListItem Value="24"> Bermuda</asp:ListItem>
                                    <asp:ListItem Value="25"> Bhutan</asp:ListItem>
                                    <asp:ListItem Value="26"> Bolivia</asp:ListItem>
                                    <asp:ListItem Value="27"> Bosnia And Herzegovina</asp:ListItem>
                                    <asp:ListItem Value="28"> Botswana</asp:ListItem>
                                    <asp:ListItem Value="29"> Bouvet Island</asp:ListItem>
                                    <asp:ListItem Value="30"> Brazil</asp:ListItem>
                                    <asp:ListItem Value="31"> British Indian Ocean Territory</asp:ListItem>
                                    <asp:ListItem Value="32"> Brunei Darussalam</asp:ListItem>
                                    <asp:ListItem Value="33"> Bulgaria</asp:ListItem>
                                    <asp:ListItem Value="34"> Burkina Faso</asp:ListItem>
                                    <asp:ListItem Value="35"> Burundi</asp:ListItem>
                                    <asp:ListItem Value="36"> Cambodia</asp:ListItem>
                                    <asp:ListItem Value="37"> Cameroon</asp:ListItem>
                                    <asp:ListItem Value="38"> Canada</asp:ListItem>
                                    <asp:ListItem Value="39"> Cape Verde</asp:ListItem>
                                    <asp:ListItem Value="40"> Cayman Islands</asp:ListItem>
                                    <asp:ListItem Value="41"> Central African Republic</asp:ListItem>
                                    <asp:ListItem Value="42"> Chad</asp:ListItem>
                                    <asp:ListItem Value="43"> Chile</asp:ListItem>
                                    <asp:ListItem Value="44"> China</asp:ListItem>
                                    <asp:ListItem Value="45"> Christmas Island</asp:ListItem>
                                    <asp:ListItem Value="46"> Cocos (Keeling) Islands</asp:ListItem>
                                    <asp:ListItem Value="47"> Colombia</asp:ListItem>
                                    <asp:ListItem Value="48"> Comoros</asp:ListItem>
                                    <asp:ListItem Value="49"> Congo</asp:ListItem>
                                    <asp:ListItem Value="50"> Cook Islands</asp:ListItem>
                                    <asp:ListItem Value="51"> Costa Rica</asp:ListItem>
                                    <asp:ListItem Value="52"> Cote D&#39;Ivoire</asp:ListItem>
                                    <asp:ListItem Value="53"> Croatia (Local Name: Hrvatska)</asp:ListItem>
                                    <asp:ListItem Value="54"> Cuba</asp:ListItem>
                                    <asp:ListItem Value="55"> Cyprus</asp:ListItem>
                                    <asp:ListItem Value="56"> Czech Republic</asp:ListItem>
                                    <asp:ListItem Value="57"> Denmark</asp:ListItem>
                                    <asp:ListItem Value="58"> Djibouti</asp:ListItem>
                                    <asp:ListItem Value="59"> Dominica</asp:ListItem>
                                    <asp:ListItem Value="60"> Dominican Republic</asp:ListItem>
                                    <asp:ListItem Value="61"> Ecuador</asp:ListItem>
                                    <asp:ListItem Value="62"> Egypt</asp:ListItem>
                                    <asp:ListItem Value="63"> El Salvador</asp:ListItem>
                                    <asp:ListItem Value="64"> Equatorial Guinea</asp:ListItem>
                                    <asp:ListItem Value="65"> Eritrea</asp:ListItem>
                                    <asp:ListItem Value="66"> Estonia</asp:ListItem>
                                    <asp:ListItem Value="67"> Ethiopia</asp:ListItem>
                                    <asp:ListItem Value="68"> Falkland Islands (Malvinas)</asp:ListItem>
                                    <asp:ListItem Value="69"> Faroe Islands</asp:ListItem>
                                    <asp:ListItem Value="70"> Fiji</asp:ListItem>
                                    <asp:ListItem Value="71"> Finland</asp:ListItem>
                                    <asp:ListItem Value="72"> France</asp:ListItem>
                                    <asp:ListItem Value="73"> France, Metropolitan</asp:ListItem>
                                    <asp:ListItem Value="74">French Guiana</asp:ListItem>
                                    <asp:ListItem Value="75">French Polynesia</asp:ListItem>
                                    <asp:ListItem Value="76">French Southern Territories</asp:ListItem>
                                    <asp:ListItem Value="77">Gabon</asp:ListItem>
                                    <asp:ListItem Value="78">Gambia</asp:ListItem>
                                    <asp:ListItem Value="79">Georgia</asp:ListItem>
                                    <asp:ListItem Value="80">Germany</asp:ListItem>
                                    <asp:ListItem Value="81">Ghana</asp:ListItem>
                                    <asp:ListItem Value="82">Gibraltar</asp:ListItem>
                                    <asp:ListItem Value="83">Greece</asp:ListItem>
                                    <asp:ListItem Value="84">Greenland</asp:ListItem>
                                    <asp:ListItem Value="85">Grenada</asp:ListItem>
                                    <asp:ListItem Value="86">Guadeloupe</asp:ListItem>
                                    <asp:ListItem Value="87">Guam</asp:ListItem>
                                    <asp:ListItem Value="88">Guatemala</asp:ListItem>
                                    <asp:ListItem Value="89">Guinea</asp:ListItem>
                                    <asp:ListItem Value="90">Guinea-Bissau</asp:ListItem>
                                    <asp:ListItem Value="91">Guyana</asp:ListItem>
                                    <asp:ListItem Value="92">Haiti</asp:ListItem>
                                    <asp:ListItem Value="93">Heard Island &amp; Mcdonald Islands</asp:ListItem>
                                    <asp:ListItem Value="94">Honduras</asp:ListItem>
                                    <asp:ListItem Value="95">Hong Kong</asp:ListItem>
                                    <asp:ListItem Value="96">Hungary</asp:ListItem>
                                    <asp:ListItem Value="97">Iceland</asp:ListItem>
                                    <asp:ListItem Value="98">India</asp:ListItem>
                                    <asp:ListItem Value="99">Indonesia</asp:ListItem>
                                    <asp:ListItem Value="100">Iran, Islamic Republic Of</asp:ListItem>
                                    <asp:ListItem Value="101">Iraq</asp:ListItem>
                                    <asp:ListItem Value="102">Ireland</asp:ListItem>
                                    <asp:ListItem Value="103">Israel</asp:ListItem>
                                    <asp:ListItem Value="104">Italy</asp:ListItem>
                                    <asp:ListItem Value="105">Jamaica</asp:ListItem>
                                    <asp:ListItem Value="106">Japan</asp:ListItem>
                                    <asp:ListItem Value="107">Jordan</asp:ListItem>
                                    <asp:ListItem Value="108">Kazakhstan</asp:ListItem>
                                    <asp:ListItem Value="109">Kenya</asp:ListItem>
                                    <asp:ListItem Value="110">Kiribati</asp:ListItem>
                                    <asp:ListItem Value="111">Korea, Democratic People&#39;S Republic Of</asp:ListItem>
                                    <asp:ListItem Value="112">Korea, Republic Of</asp:ListItem>
                                    <asp:ListItem Value="113">Kuwait</asp:ListItem>
                                    <asp:ListItem Value="114">Kyrgyzstan</asp:ListItem>
                                    <asp:ListItem Value="115">Lao People&#39;S Democratic Republic</asp:ListItem>
                                    <asp:ListItem Value="116">Latvia</asp:ListItem>
                                    <asp:ListItem Value="117">Lebanon</asp:ListItem>
                                    <asp:ListItem Value="118">Lesotho</asp:ListItem>
                                    <asp:ListItem Value="119">Liberia</asp:ListItem>
                                    <asp:ListItem Value="120">Libyan Arab Jamahiriya</asp:ListItem>
                                    <asp:ListItem Value="121">Liechtenstein</asp:ListItem>
                                    <asp:ListItem Value="122">Lithuania</asp:ListItem>
                                    <asp:ListItem Value="123">Luxembourg</asp:ListItem>
                                    <asp:ListItem Value="124">Macau</asp:ListItem>
                                    <asp:ListItem Value="125">Macedonia, The Former Yugoslav Republic Of</asp:ListItem>
                                    <asp:ListItem Value="126">Madagascar</asp:ListItem>
                                    <asp:ListItem Value="127">Malawi</asp:ListItem>
                                    <asp:ListItem Value="128">Malaysia</asp:ListItem>
                                    <asp:ListItem Value="129">Maldives</asp:ListItem>
                                    <asp:ListItem Value="130">Mali</asp:ListItem>
                                    <asp:ListItem Value="131">Malta</asp:ListItem>
                                    <asp:ListItem Value="132">Marshall Islands</asp:ListItem>
                                    <asp:ListItem Value="133">Martinique</asp:ListItem>
                                    <asp:ListItem Value="134">Mauritania</asp:ListItem>
                                    <asp:ListItem Value="135">Mauritius</asp:ListItem>
                                    <asp:ListItem Value="136">Mayotte</asp:ListItem>
                                    <asp:ListItem Value="137">Mexico</asp:ListItem>
                                    <asp:ListItem Value="138">Micronesia, Federated States Of</asp:ListItem>
                                    <asp:ListItem Value="139">Moldova, Republic Of</asp:ListItem>
                                    <asp:ListItem Value="140">Monaco</asp:ListItem>
                                    <asp:ListItem Value="141">Mongolia</asp:ListItem>
                                    <asp:ListItem Value="142">Montserrat</asp:ListItem>
                                    <asp:ListItem Value="143">Morocco</asp:ListItem>
                                    <asp:ListItem Value="144">Mozambique</asp:ListItem>
                                    <asp:ListItem Value="145">Myanmar</asp:ListItem>
                                    <asp:ListItem Value="146">Namibia</asp:ListItem>
                                    <asp:ListItem Value="147">Nauru</asp:ListItem>
                                    <asp:ListItem Value="148">Nepal</asp:ListItem>
                                    <asp:ListItem Value="149">Netherlands</asp:ListItem>
                                    <asp:ListItem Value="150">Netherlands Antilles</asp:ListItem>
                                    <asp:ListItem Value="151">New Caledonia</asp:ListItem>
                                    <asp:ListItem Value="152">New Zealand</asp:ListItem>
                                    <asp:ListItem Value="153">Nicaragua</asp:ListItem>
                                    <asp:ListItem Value="154">Niger</asp:ListItem>
                                    <asp:ListItem Value="155">Nigeria</asp:ListItem>
                                    <asp:ListItem Value="156">Niue</asp:ListItem>
                                    <asp:ListItem Value="157">Norfolk Island</asp:ListItem>
                                    <asp:ListItem Value="158">Northern Mariana Islands</asp:ListItem>
                                    <asp:ListItem Value="159">Norway</asp:ListItem>
                                    <asp:ListItem Value="160">Oman</asp:ListItem>
                                    <asp:ListItem Value="161">Pakistan</asp:ListItem>
                                    <asp:ListItem Value="162">Palau</asp:ListItem>
                                    <asp:ListItem Value="163">Panama</asp:ListItem>
                                    <asp:ListItem Value="164">Papua New Guinea</asp:ListItem>
                                    <asp:ListItem Value="165">Paraguay</asp:ListItem>
                                    <asp:ListItem Value="166">Peru</asp:ListItem>
                                    <asp:ListItem Value="167">Philippines</asp:ListItem>
                                    <asp:ListItem Value="168">Pitcairn</asp:ListItem>
                                    <asp:ListItem Value="169">Poland</asp:ListItem>
                                    <asp:ListItem Value="170">Portugal</asp:ListItem>
                                    <asp:ListItem Value="171">Puerto Rico</asp:ListItem>
                                    <asp:ListItem Value="172">Qatar</asp:ListItem>
                                    <asp:ListItem Value="173">Reunion</asp:ListItem>
                                    <asp:ListItem Value="174">Romania</asp:ListItem>
                                    <asp:ListItem Value="175">Russian Federation</asp:ListItem>
                                    <asp:ListItem Value="176">Rwanda</asp:ListItem>
                                    <asp:ListItem Value="177">Saint Helena</asp:ListItem>
                                    <asp:ListItem Value="178">Saint Kitts And Nevis</asp:ListItem>
                                    <asp:ListItem Value="179">Saint Lucia</asp:ListItem>
                                    <asp:ListItem Value="180">Saint Pierre And Miquelon</asp:ListItem>
                                    <asp:ListItem Value="181">Saint Vincent And The Grenadines</asp:ListItem>
                                    <asp:ListItem Value="182">Samoa</asp:ListItem>
                                    <asp:ListItem Value="183">San Marino</asp:ListItem>
                                    <asp:ListItem Value="184">Sao Tome And Principe</asp:ListItem>
                                    <asp:ListItem Value="185">Saudi Arabia</asp:ListItem>
                                    <asp:ListItem Value="186">Senegal</asp:ListItem>
                                    <asp:ListItem Value="187">Seychelles</asp:ListItem>
                                    <asp:ListItem Value="188">Sierra Leone</asp:ListItem>
                                    <asp:ListItem Value="189">Singapore</asp:ListItem>
                                    <asp:ListItem Value="190">Slovakia (Slovak Republic)</asp:ListItem>
                                    <asp:ListItem Value="191">Slovenia</asp:ListItem>
                                    <asp:ListItem Value="192">Solomon Islands</asp:ListItem>
                                    <asp:ListItem Value="193">Somalia</asp:ListItem>
                                    <asp:ListItem Value="194">South Africa</asp:ListItem>
                                    <asp:ListItem Value="195">Spain</asp:ListItem>
                                    <asp:ListItem Value="196">Sri Lanka</asp:ListItem>
                                    <asp:ListItem Value="197">Sudan</asp:ListItem>
                                    <asp:ListItem Value="198">Suriname</asp:ListItem>
                                    <asp:ListItem Value="199">Svalbard And Jan Mayen Islands</asp:ListItem>
                                    <asp:ListItem Value="200">Swaziland</asp:ListItem>
                                    <asp:ListItem Value="201">Sweden</asp:ListItem>
                                    <asp:ListItem Value="202">Switzerland</asp:ListItem>
                                    <asp:ListItem Value="203">Syrian Arab Republic</asp:ListItem>
                                    <asp:ListItem Value="204">Taiwan, Province Of China</asp:ListItem>
                                    <asp:ListItem Value="205">Tajikistan</asp:ListItem>
                                    <asp:ListItem Value="206">Tanzania, United Republic Of</asp:ListItem>
                                    <asp:ListItem Value="207">Thailand</asp:ListItem>
                                    <asp:ListItem Value="208">Togo</asp:ListItem>
                                    <asp:ListItem Value="209">Tokelau</asp:ListItem>
                                    <asp:ListItem Value="210">Tonga</asp:ListItem>
                                    <asp:ListItem Value="211">Trinidad And Tobago</asp:ListItem>
                                    <asp:ListItem Value="212">Tunisia</asp:ListItem>
                                    <asp:ListItem Value="213">Turkey</asp:ListItem>
                                    <asp:ListItem Value="214">Turkmenistan</asp:ListItem>
                                    <asp:ListItem Value="215">Turks And Caicos Islands</asp:ListItem>
                                    <asp:ListItem Value="216">Tuvalu</asp:ListItem>
                                    <asp:ListItem Value="217">Uganda</asp:ListItem>
                                    <asp:ListItem Value="218">Ukraine</asp:ListItem>
                                    <asp:ListItem Value="219">United Arab Emirates</asp:ListItem>
                                    <asp:ListItem Value="220">United Kingdom</asp:ListItem>
                                    <asp:ListItem Value="221">United States</asp:ListItem>
                                    <asp:ListItem Value="222">United States Minor Outlying Islands</asp:ListItem>
                                    <asp:ListItem Value="223">Uruguay</asp:ListItem>
                                    <asp:ListItem Value="224">Uzbekistan</asp:ListItem>
                                    <asp:ListItem Value="225">Vanuatu</asp:ListItem>
                                    <asp:ListItem Value="226">Vatican City State (Holy See)</asp:ListItem>
                                    <asp:ListItem Value="227">Venezuela</asp:ListItem>
                                    <asp:ListItem Value="228">Viet Nam</asp:ListItem>
                                    <asp:ListItem Value="229">Virgin Islands (British)</asp:ListItem>
                                    <asp:ListItem Value="230">Virgin Islands (U.S.)</asp:ListItem>
                                    <asp:ListItem Value="231">Wallis And Futuna Islands</asp:ListItem>
                                    <asp:ListItem Value="232">Western Sahara</asp:ListItem>
                                    <asp:ListItem Value="233">Yemen</asp:ListItem>
                                    <asp:ListItem Value="234">Yugoslavia</asp:ListItem>
                                    <asp:ListItem Value="235">Zaire</asp:ListItem>
                                    <asp:ListItem Value="236">Zambia</asp:ListItem>
                                    <asp:ListItem Value="237">Zimbabwe</asp:ListItem>

                                </asp:DropDownList>
                            </td>
                        </tr>

                        <tr>
                            <td class="col-md-1 text-left">&nbsp;</td>
                            <td class="col-md-2">Postal Code:</td>
                            <td class="col-md-4  text-center">
                                <asp:TextBox ID="txt_postalCode" runat="server" CssClass="form-control"></asp:TextBox>
                            </td>
                        </tr>


                        <tr>
                            <td class="td-bg-primary text-left text-bold" colspan="3">Credentials</td>
                        </tr>
                        <tr>
                            <td class="col-md-1 text-left">
                                <span class="text-danger">*</span></td>
                            <td class="col-md-2">User Name:</td>
                            <td class="col-md-4 text-center">
                                <asp:TextBox ID="txt_username" runat="server" CssClass="form-control disabled" Enabled="false"></asp:TextBox>
                            </td>
                        </tr>

                        <tr>
                            <td class="col-md-1 text-left">
                                <span class="text-danger">*</span><asp:RequiredFieldValidator ID="RequiredFieldValidator18" runat="server" CssClass="text-danger" ErrorMessage="Password is required." ControlToValidate="txt_password" ValidationGroup="myaccount">*</asp:RequiredFieldValidator>
                            </td>
                            <td class="col-md-2">Password:</td>
                            <td class="col-md-4 text-center">
                                <asp:TextBox ID="txt_password" runat="server" CssClass="form-control" TextMode="Password"></asp:TextBox>
                            </td>
                        </tr>

                        <tr>
                            <td class="col-md-1 text-left">
                                <span class="text-danger">*</span><asp:RequiredFieldValidator ID="RequiredFieldValidator19" runat="server" CssClass="text-danger" ErrorMessage="Confirm Password is required." ControlToValidate="txt_confirmPassword" ValidationGroup="myaccount">*</asp:RequiredFieldValidator>
                                <asp:CompareValidator ID="CompareValidator2" runat="server" CssClass="text-danger" ErrorMessage="Passwords do not match." ControlToCompare="txt_password" ControlToValidate="txt_confirmPassword" ValidationGroup="myaccount">*</asp:CompareValidator>
                            </td>
                            <td class="col-md-2">Confirm Password:</td>
                            <td class="col-md-4 text-center">
                                <asp:TextBox ID="txt_confirmPassword" runat="server" CssClass="form-control" TextMode="Password"></asp:TextBox>
                            </td>
                        </tr>

                    </table>


                    &nbsp;<hr />
                    <asp:Button ID="btn_update" runat="server" Text="Update" CssClass="btn btn-primary btn-sm" ValidationGroup="myaccount" />
                </div>

            </div>
        </div>
    </div>
</asp:Content>

