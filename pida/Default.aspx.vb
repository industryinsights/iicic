﻿
Partial Class _Default
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Dim client As String = Request.CurrentExecutionFilePath
            Dim parts = Request.CurrentExecutionFilePath.Split("/"c)
            client = parts(1)

            If Not String.IsNullOrEmpty(client) Then
                Session("Client") = client.ToLower
            Else
                Session("Client") = Nothing
            End If
            Response.Redirect("../Account/Login.aspx")
        End If
    End Sub
End Class
