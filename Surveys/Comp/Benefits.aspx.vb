﻿
Imports Models

Partial Class Surveys_Benefits
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load



        If Session("LocationID") Is Nothing Then
            If Not String.IsNullOrEmpty(txt_LocationID.Text) Then
                Session("LocationID") = Convert.ToInt32(txt_LocationID.Text)
            Else
                Response.Redirect("../../account/login.aspx")
            End If
        End If
        If Session("UserId") Is Nothing Then
            If Not String.IsNullOrEmpty(txt_userid.Text) Then
                Session("UserId") = Convert.ToInt32(txt_userid.Text)
            Else
                Response.Redirect("../../account/login.aspx")
            End If
        End If
        If Not IsPostBack Then
            Dim LocationID As Integer = Session("LocationID")
            txt_LocationID.Text = LocationID
            Dim userid As Integer = Session("UserId")
            txt_userid.Text = userid
            If Session("Client") IsNot Nothing And Not String.IsNullOrEmpty(Session("Client")) Then
                lbl_deadline.Text = SurveyHelper.ReturnDeadline(Session("Client").ToString)
            Else
                lbl_deadline.Text = "Friday, April 13, 2018"
            End If
            ddl_location.Items.Clear()
            Dim locations As List(Of ListItem) = SurveyHelper.ReturnLocationList(Session("OrganizationId"))
            For Each l In locations
                ddl_location.Items.Add(l)
            Next
            ddl_location.SelectedValue = Session("LocationID")
            tbl_location.Visible = False
            LoadSurvey()
        End If
    End Sub
    Sub LoadSurvey()
        Using db As New IICICEntities
            Dim LocationID As Integer = txt_LocationID.Text
            Dim survey As New CompSurvey
            survey = (From s In db.CompSurveys Where s.LocationID = LocationID And s.SurveyYear = Now.Year).FirstOrDefault
            If survey IsNot Nothing Then
                lbl_lastSave.Text = survey.ModifiedDate

                '__Custom Show/hide based on assocation____________________________\/

                NAWtbl.Visible = False 'default

                If survey.s1914 IsNot Nothing Then
                    'show NAW sections
                    If survey.s1914 = True Then NAWtbl.Visible = True
                End If
                '__Custom Show/hide based on assocation____________________________/\


                '---PASTE LOAD STATEMENTS---
                If survey.s284 IsNot Nothing Then
                    txt_s284.Text = String.Format(survey.s284, "N0")
                End If
                If survey.s287 IsNot Nothing Then
                    txt_s287.Text = String.Format(survey.s287, "N0")
                End If
                If survey.s285 IsNot Nothing Then
                    txt_s285.Text = String.Format(survey.s285, "N0")
                End If
                If survey.s288 IsNot Nothing Then
                    txt_s288.Text = String.Format(survey.s288, "N0")
                End If
                If survey.s286 IsNot Nothing Then
                    txt_s286.Text = String.Format(survey.s286, "N0")
                End If
                If survey.s289 IsNot Nothing Then
                    txt_s289.Text = String.Format(survey.s289, "N0")
                End If
                If Not String.IsNullOrEmpty(survey.s302oth) Then
                    txt_s302oth.Text = survey.s302oth
                End If
                If survey.s652 IsNot Nothing Then
                    txt_s652.Text = String.Format(survey.s652, "N0")
                End If
                If survey.s654 IsNot Nothing Then
                    txt_s654.Text = String.Format(survey.s654, "N0")
                End If
                If survey.s653 IsNot Nothing Then
                    txt_s653.Text = String.Format(survey.s653, "N0")
                End If
                If survey.s655 IsNot Nothing Then
                    txt_s655.Text = String.Format(survey.s655, "N0")
                End If
                If survey.s656 IsNot Nothing Then
                    txt_s656.Text = String.Format(survey.s656, "N0")
                End If
                If survey.s666 IsNot Nothing Then
                    txt_s666.Text = String.Format(survey.s666, "N0")
                End If
                If survey.s676 IsNot Nothing Then
                    txt_s676.Text = String.Format(survey.s676, "N0")
                End If
                If survey.s657 IsNot Nothing Then
                    txt_s657.Text = String.Format(survey.s657, "N0")
                End If
                If survey.s667 IsNot Nothing Then
                    txt_s667.Text = String.Format(survey.s667, "N0")
                End If
                If survey.s677 IsNot Nothing Then
                    txt_s677.Text = String.Format(survey.s677, "N0")
                End If
                If survey.s686 IsNot Nothing Then
                    txt_s686.Text = String.Format(survey.s686, "N0")
                End If
                If survey.s658 IsNot Nothing Then
                    txt_s658.Text = String.Format(survey.s658, "N0")
                End If
                If survey.s668 IsNot Nothing Then
                    txt_s668.Text = String.Format(survey.s668, "N0")
                End If
                If survey.s678 IsNot Nothing Then
                    txt_s678.Text = String.Format(survey.s678, "N0")
                End If
                If survey.s659 IsNot Nothing Then
                    txt_s659.Text = String.Format(survey.s659, "N0")
                End If
                If survey.s669 IsNot Nothing Then
                    txt_s669.Text = String.Format(survey.s669, "N0")
                End If
                If survey.s679 IsNot Nothing Then
                    txt_s679.Text = String.Format(survey.s679, "N0")
                End If
                If survey.s687 IsNot Nothing Then
                    txt_s687.Text = String.Format(survey.s687, "N0")
                End If
                If survey.s660 IsNot Nothing Then
                    txt_s660.Text = String.Format(survey.s660, "N0")
                End If
                If survey.s670 IsNot Nothing Then
                    txt_s670.Text = String.Format(survey.s670, "N0")
                End If
                If survey.s680 IsNot Nothing Then
                    txt_s680.Text = String.Format(survey.s680, "N0")
                End If
                If survey.s661 IsNot Nothing Then
                    txt_s661.Text = String.Format(survey.s661, "N0")
                End If
                If survey.s671 IsNot Nothing Then
                    txt_s671.Text = String.Format(survey.s671, "N0")
                End If
                If survey.s681 IsNot Nothing Then
                    txt_s681.Text = String.Format(survey.s681, "N0")
                End If
                If survey.s688 IsNot Nothing Then
                    txt_s688.Text = String.Format(survey.s688, "N0")
                End If
                If survey.s662 IsNot Nothing Then
                    txt_s662.Text = String.Format(survey.s662, "N0")
                End If
                If survey.s672 IsNot Nothing Then
                    txt_s672.Text = String.Format(survey.s672, "N0")
                End If
                If survey.s682 IsNot Nothing Then
                    txt_s682.Text = String.Format(survey.s682, "N0")
                End If
                If survey.s663 IsNot Nothing Then
                    txt_s663.Text = String.Format(survey.s663, "N0")
                End If
                If survey.s673 IsNot Nothing Then
                    txt_s673.Text = String.Format(survey.s673, "N0")
                End If
                If survey.s683 IsNot Nothing Then
                    txt_s683.Text = String.Format(survey.s683, "N0")
                End If
                If survey.s689 IsNot Nothing Then
                    txt_s689.Text = String.Format(survey.s689, "N0")
                End If
                If survey.s664 IsNot Nothing Then
                    txt_s664.Text = String.Format(survey.s664, "N0")
                End If
                If survey.s674 IsNot Nothing Then
                    txt_s674.Text = String.Format(survey.s674, "N0")
                End If
                If survey.s684 IsNot Nothing Then
                    txt_s684.Text = String.Format(survey.s684, "N0")
                End If
                If survey.s665 IsNot Nothing Then
                    txt_s665.Text = String.Format(survey.s665, "N0")
                End If
                If survey.s675 IsNot Nothing Then
                    txt_s675.Text = String.Format(survey.s675, "N0")
                End If
                If survey.s685 IsNot Nothing Then
                    txt_s685.Text = String.Format(survey.s685, "N0")
                End If
                If survey.s690 IsNot Nothing Then
                    txt_s690.Text = String.Format(survey.s690, "N0")
                End If
                If survey.s556 IsNot Nothing Then
                    txt_s556.Text = String.Format(survey.s556, "N0")
                End If
                If Not String.IsNullOrEmpty(survey.s333oth) Then
                    txt_s333oth.Text = survey.s333oth
                End If
                If survey.s338 IsNot Nothing Then
                    txt_s338.Text = String.Format(survey.s338, "N0")
                End If
                If survey.s340 IsNot Nothing Then
                    txt_s340.Text = String.Format(survey.s340, "N0")
                End If
                If Not String.IsNullOrEmpty(survey.s341oth) Then
                    txt_s341oth.Text = survey.s341oth
                End If
                If survey.s342 IsNot Nothing Then
                    txt_s342.Text = String.Format(survey.s342, "N0")
                End If
                If survey.s557 IsNot Nothing Then
                    txt_s557.Text = String.Format(survey.s557, "N0")
                End If
                If Not String.IsNullOrEmpty(survey.s323oth) Then
                    txt_s323oth.Text = survey.s323oth
                End If
                If survey.s344 IsNot Nothing Then
                    txt_s344.Text = String.Format(survey.s344, "N0")
                End If
                If survey.s345 IsNot Nothing Then
                    txt_s345.Text = String.Format(survey.s345, "N0")
                End If
                If survey.s347 IsNot Nothing Then
                    txt_s347.Text = String.Format(survey.s347, "N0")
                End If
                If survey.s349 IsNot Nothing Then
                    txt_s349.Text = String.Format(survey.s349, "N0")
                End If
                If survey.s353 IsNot Nothing Then
                    txt_s353.Text = String.Format(survey.s353, "N0")
                End If
                If survey.s350 IsNot Nothing Then
                    txt_s350.Text = String.Format(survey.s350, "N0")
                End If
                If survey.s354 IsNot Nothing Then
                    txt_s354.Text = String.Format(survey.s354, "N0")
                End If
                If survey.s351 IsNot Nothing Then
                    txt_s351.Text = String.Format(survey.s351, "N0")
                End If
                If survey.s355 IsNot Nothing Then
                    txt_s355.Text = String.Format(survey.s355, "N0")
                End If
                If survey.s352 IsNot Nothing Then
                    txt_s352.Text = String.Format(survey.s352, "N0")
                End If
                If survey.s356 IsNot Nothing Then
                    txt_s356.Text = String.Format(survey.s356, "N0")
                End If
                If survey.s358 IsNot Nothing Then
                    txt_s358.Text = String.Format(survey.s358, "N0")
                End If
                If survey.s360 IsNot Nothing Then
                    txt_s360.Text = String.Format(survey.s360, "N0")
                End If
                If survey.s361 IsNot Nothing Then
                    txt_s361.Text = String.Format(survey.s361, "N0")
                End If
                If survey.s362 IsNot Nothing Then
                    txt_s362.Text = String.Format(survey.s362, "N0")
                End If
                If Not String.IsNullOrEmpty(survey.s367oth) Then
                    txt_s367oth.Text = survey.s367oth
                End If
                If survey.s369 IsNot Nothing Then
                    txt_s369.Text = String.Format(survey.s369, "N0")
                End If

                If survey.s290 IsNot Nothing Then
                    cb_s290.Checked = survey.s290
                End If
                If survey.s294 IsNot Nothing Then
                    cb_s294.Checked = survey.s294
                End If
                If survey.s298 IsNot Nothing Then
                    cb_s298.Checked = survey.s298
                End If
                If survey.s291 IsNot Nothing Then
                    cb_s291.Checked = survey.s291
                End If
                If survey.s295 IsNot Nothing Then
                    cb_s295.Checked = survey.s295
                End If
                If survey.s299 IsNot Nothing Then
                    cb_s299.Checked = survey.s299
                End If
                If survey.s292 IsNot Nothing Then
                    cb_s292.Checked = survey.s292
                End If
                If survey.s296 IsNot Nothing Then
                    cb_s296.Checked = survey.s296
                End If
                If survey.s300 IsNot Nothing Then
                    cb_s300.Checked = survey.s300
                End If
                If survey.s293 IsNot Nothing Then
                    cb_s293.Checked = survey.s293
                End If
                If survey.s297 IsNot Nothing Then
                    cb_s297.Checked = survey.s297
                End If
                If survey.s301 IsNot Nothing Then
                    cb_s301.Checked = survey.s301
                End If
                If survey.s302 IsNot Nothing Then
                    cb_s302.Checked = survey.s302
                End If
                If survey.s327 IsNot Nothing Then
                    cb_s327.Checked = survey.s327
                End If
                If survey.s331 IsNot Nothing Then
                    cb_s331.Checked = survey.s331
                End If
                If survey.s328 IsNot Nothing Then
                    cb_s328.Checked = survey.s328
                End If
                If survey.s332 IsNot Nothing Then
                    cb_s332.Checked = survey.s332
                End If
                If survey.s329 IsNot Nothing Then
                    cb_s329.Checked = survey.s329
                End If
                If survey.s330 IsNot Nothing Then
                    cb_s330.Checked = survey.s330
                End If
                If survey.s333 IsNot Nothing Then
                    cb_s333.Checked = survey.s333
                End If
                If survey.s334 IsNot Nothing Then
                    cb_s334.Checked = survey.s334
                End If
                If survey.s337 IsNot Nothing Then
                    cb_s337.Checked = survey.s337
                End If
                If survey.s339 IsNot Nothing Then
                    cb_s339.Checked = survey.s339
                End If
                If survey.s341 IsNot Nothing Then
                    cb_s341.Checked = survey.s341
                End If
                If survey.s303 IsNot Nothing Then
                    cb_s303.Checked = survey.s303
                End If
                If survey.s304 IsNot Nothing Then
                    cb_s304.Checked = survey.s304
                End If
                If survey.s311 IsNot Nothing Then
                    cb_s311.Checked = survey.s311
                End If
                If survey.s318 IsNot Nothing Then
                    cb_s318.Checked = survey.s318
                End If
                If survey.s305 IsNot Nothing Then
                    cb_s305.Checked = survey.s305
                End If
                If survey.s312 IsNot Nothing Then
                    cb_s312.Checked = survey.s312
                End If
                If survey.s319 IsNot Nothing Then
                    cb_s319.Checked = survey.s319
                End If
                If survey.s306 IsNot Nothing Then
                    cb_s306.Checked = survey.s306
                End If
                If survey.s313 IsNot Nothing Then
                    cb_s313.Checked = survey.s313
                End If
                If survey.s320 IsNot Nothing Then
                    cb_s320.Checked = survey.s320
                End If
                If survey.s307 IsNot Nothing Then
                    cb_s307.Checked = survey.s307
                End If
                If survey.s314 IsNot Nothing Then
                    cb_s314.Checked = survey.s314
                End If
                If survey.s321 IsNot Nothing Then
                    cb_s321.Checked = survey.s321
                End If
                If survey.s308 IsNot Nothing Then
                    cb_s308.Checked = survey.s308
                End If
                If survey.s315 IsNot Nothing Then
                    cb_s315.Checked = survey.s315
                End If
                If survey.s322 IsNot Nothing Then
                    cb_s322.Checked = survey.s322
                End If
                If survey.s574 IsNot Nothing Then
                    cb_s574.Checked = survey.s574
                End If
                If survey.s576 IsNot Nothing Then
                    cb_s576.Checked = survey.s576
                End If
                If survey.s578 IsNot Nothing Then
                    cb_s578.Checked = survey.s578
                End If
                If survey.s575 IsNot Nothing Then
                    cb_s575.Checked = survey.s575
                End If
                If survey.s577 IsNot Nothing Then
                    cb_s577.Checked = survey.s577
                End If
                If survey.s579 IsNot Nothing Then
                    cb_s579.Checked = survey.s579
                End If
                If survey.s309 IsNot Nothing Then
                    cb_s309.Checked = survey.s309
                End If
                If survey.s316 IsNot Nothing Then
                    cb_s316.Checked = survey.s316
                End If
                If survey.s323 IsNot Nothing Then
                    cb_s323.Checked = survey.s323
                End If
                If survey.s310 IsNot Nothing Then
                    cb_s310.Checked = survey.s310
                End If
                If survey.s317 IsNot Nothing Then
                    cb_s317.Checked = survey.s317
                End If
                If survey.s363 IsNot Nothing Then
                    cb_s363.Checked = survey.s363
                End If
                If survey.s366 IsNot Nothing Then
                    cb_s366.Checked = survey.s366
                End If
                If survey.s364 IsNot Nothing Then
                    cb_s364.Checked = survey.s364
                End If
                If survey.s367 IsNot Nothing Then
                    cb_s367.Checked = survey.s367
                End If
                If survey.s365 IsNot Nothing Then
                    cb_s365.Checked = survey.s365
                End If
                If survey.s368 IsNot Nothing Then
                    cb_s368.Checked = survey.s368
                End If
                If survey.s558 IsNot Nothing Then
                    cb_s558.Checked = survey.s558
                End If
                If survey.section5Complete IsNot Nothing Then
                    cb_section5Complete.Checked = survey.section5Complete
                End If

                If survey.s278 IsNot Nothing Then
                    If survey.s278 = 1 Then rb_s278_1.Checked = True
                End If
                If survey.s278 IsNot Nothing Then
                    If survey.s278 = 2 Then rb_s278_2.Checked = True
                End If
                If survey.s281 IsNot Nothing Then
                    If survey.s281 = 1 Then rb_s281_1.Checked = True
                End If
                If survey.s281 IsNot Nothing Then
                    If survey.s281 = 2 Then rb_s281_2.Checked = True
                End If
                If survey.s279 IsNot Nothing Then
                    If survey.s279 = 1 Then rb_s279_1.Checked = True
                End If
                If survey.s279 IsNot Nothing Then
                    If survey.s279 = 2 Then rb_s279_2.Checked = True
                End If
                If survey.s282 IsNot Nothing Then
                    If survey.s282 = 1 Then rb_s282_1.Checked = True
                End If
                If survey.s282 IsNot Nothing Then
                    If survey.s282 = 2 Then rb_s282_2.Checked = True
                End If
                If survey.s280 IsNot Nothing Then
                    If survey.s280 = 1 Then rb_s280_1.Checked = True
                End If
                If survey.s280 IsNot Nothing Then
                    If survey.s280 = 2 Then rb_s280_2.Checked = True
                End If
                If survey.s283 IsNot Nothing Then
                    If survey.s283 = 1 Then rb_s283_1.Checked = True
                End If
                If survey.s283 IsNot Nothing Then
                    If survey.s283 = 2 Then rb_s283_2.Checked = True
                End If
                If survey.s651 IsNot Nothing Then
                    If survey.s651 = 1 Then rb_s651_1.Checked = True
                End If
                If survey.s651 IsNot Nothing Then
                    If survey.s651 = 2 Then rb_s651_2.Checked = True
                End If
                If survey.s324 IsNot Nothing Then
                    If survey.s324 = 1 Then rb_s324_1.Checked = True
                End If
                If survey.s324 IsNot Nothing Then
                    If survey.s324 = 2 Then rb_s324_2.Checked = True
                End If
                If survey.s326 IsNot Nothing Then
                    If survey.s326 = 1 Then rb_s326_1.Checked = True
                End If
                If survey.s326 IsNot Nothing Then
                    If survey.s326 = 2 Then rb_s326_2.Checked = True
                End If
                If survey.s335 IsNot Nothing Then
                    If survey.s335 = 1 Then rb_s335_1.Checked = True
                End If
                If survey.s335 IsNot Nothing Then
                    If survey.s335 = 2 Then rb_s335_2.Checked = True
                End If
                If survey.s336 IsNot Nothing Then
                    If survey.s336 = 1 Then rb_s336_1.Checked = True
                End If
                If survey.s336 IsNot Nothing Then
                    If survey.s336 = 2 Then rb_s336_2.Checked = True
                End If
                If survey.s343 IsNot Nothing Then
                    If survey.s343 = 1 Then rb_s343_1.Checked = True
                End If
                If survey.s343 IsNot Nothing Then
                    If survey.s343 = 2 Then rb_s343_2.Checked = True
                End If
                If survey.s346 IsNot Nothing Then
                    If survey.s346 = 1 Then rb_s346_1.Checked = True
                End If
                If survey.s346 IsNot Nothing Then
                    If survey.s346 = 2 Then rb_s346_2.Checked = True
                End If
                If survey.s348 IsNot Nothing Then
                    If survey.s348 = 1 Then rb_s348_1.Checked = True
                End If
                If survey.s348 IsNot Nothing Then
                    If survey.s348 = 2 Then rb_s348_2.Checked = True
                End If
                If survey.s357 IsNot Nothing Then
                    If survey.s357 = 1 Then rb_s357_1.Checked = True
                End If
                If survey.s357 IsNot Nothing Then
                    If survey.s357 = 2 Then rb_s357_2.Checked = True
                End If
                If survey.s359 IsNot Nothing Then
                    If survey.s359 = 1 Then rb_s359_1.Checked = True
                End If
                If survey.s359 IsNot Nothing Then
                    If survey.s359 = 2 Then rb_s359_2.Checked = True
                End If




            End If
        End Using
    End Sub
    Sub SaveSurvey(ByVal IsSubmit As Boolean)
        Using db As New Models.IICICEntities
            Dim existingSurvey As Boolean = False
            Dim LocationID As Integer = txt_LocationID.Text
            Dim userid As Integer = Convert.ToInt16(Session("UserId"))
            Dim survey As New CompSurvey
            survey = (From s In db.CompSurveys Where s.LocationID = LocationID And s.SurveyYear = Now.Year).FirstOrDefault
            If survey Is Nothing Then
                survey = New CompSurvey
                db.CompSurveys.Add(survey)
                db.SaveChanges()
            End If
            survey.LocationID = LocationID
            survey.LastModUserId = userid
            survey.ModifiedDate = Now
            survey.SurveyYear = Now.Year
            If IsSubmit Then
                survey.SubmitDate = Now
            End If

            '---PASTE SAVE STATEMENTS---
            If Not String.IsNullOrEmpty(txt_s284.Text) Then
                survey.s284 = txt_s284.Text.Replace(",", String.Empty)
            Else
                survey.s284 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s287.Text) Then
                survey.s287 = txt_s287.Text.Replace(",", String.Empty)
            Else
                survey.s287 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s285.Text) Then
                survey.s285 = txt_s285.Text.Replace(",", String.Empty)
            Else
                survey.s285 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s288.Text) Then
                survey.s288 = txt_s288.Text.Replace(",", String.Empty)
            Else
                survey.s288 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s286.Text) Then
                survey.s286 = txt_s286.Text.Replace(",", String.Empty)
            Else
                survey.s286 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s289.Text) Then
                survey.s289 = txt_s289.Text.Replace(",", String.Empty)
            Else
                survey.s289 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s302oth.Text) Then
                survey.s302oth = txt_s302oth.Text.Replace(",", String.Empty)
            Else
                survey.s302oth = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s652.Text) Then
                survey.s652 = txt_s652.Text.Replace(",", String.Empty)
            Else
                survey.s652 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s654.Text) Then
                survey.s654 = txt_s654.Text.Replace(",", String.Empty)
            Else
                survey.s654 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s653.Text) Then
                survey.s653 = txt_s653.Text.Replace(",", String.Empty)
            Else
                survey.s653 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s655.Text) Then
                survey.s655 = txt_s655.Text.Replace(",", String.Empty)
            Else
                survey.s655 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s656.Text) Then
                survey.s656 = txt_s656.Text.Replace(",", String.Empty)
            Else
                survey.s656 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s666.Text) Then
                survey.s666 = txt_s666.Text.Replace(",", String.Empty)
            Else
                survey.s666 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s676.Text) Then
                survey.s676 = txt_s676.Text.Replace(",", String.Empty)
            Else
                survey.s676 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s657.Text) Then
                survey.s657 = txt_s657.Text.Replace(",", String.Empty)
            Else
                survey.s657 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s667.Text) Then
                survey.s667 = txt_s667.Text.Replace(",", String.Empty)
            Else
                survey.s667 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s677.Text) Then
                survey.s677 = txt_s677.Text.Replace(",", String.Empty)
            Else
                survey.s677 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s686.Text) Then
                survey.s686 = txt_s686.Text.Replace(",", String.Empty)
            Else
                survey.s686 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s658.Text) Then
                survey.s658 = txt_s658.Text.Replace(",", String.Empty)
            Else
                survey.s658 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s668.Text) Then
                survey.s668 = txt_s668.Text.Replace(",", String.Empty)
            Else
                survey.s668 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s678.Text) Then
                survey.s678 = txt_s678.Text.Replace(",", String.Empty)
            Else
                survey.s678 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s659.Text) Then
                survey.s659 = txt_s659.Text.Replace(",", String.Empty)
            Else
                survey.s659 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s669.Text) Then
                survey.s669 = txt_s669.Text.Replace(",", String.Empty)
            Else
                survey.s669 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s679.Text) Then
                survey.s679 = txt_s679.Text.Replace(",", String.Empty)
            Else
                survey.s679 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s687.Text) Then
                survey.s687 = txt_s687.Text.Replace(",", String.Empty)
            Else
                survey.s687 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s660.Text) Then
                survey.s660 = txt_s660.Text.Replace(",", String.Empty)
            Else
                survey.s660 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s670.Text) Then
                survey.s670 = txt_s670.Text.Replace(",", String.Empty)
            Else
                survey.s670 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s680.Text) Then
                survey.s680 = txt_s680.Text.Replace(",", String.Empty)
            Else
                survey.s680 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s661.Text) Then
                survey.s661 = txt_s661.Text.Replace(",", String.Empty)
            Else
                survey.s661 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s671.Text) Then
                survey.s671 = txt_s671.Text.Replace(",", String.Empty)
            Else
                survey.s671 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s681.Text) Then
                survey.s681 = txt_s681.Text.Replace(",", String.Empty)
            Else
                survey.s681 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s688.Text) Then
                survey.s688 = txt_s688.Text.Replace(",", String.Empty)
            Else
                survey.s688 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s662.Text) Then
                survey.s662 = txt_s662.Text.Replace(",", String.Empty)
            Else
                survey.s662 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s672.Text) Then
                survey.s672 = txt_s672.Text.Replace(",", String.Empty)
            Else
                survey.s672 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s682.Text) Then
                survey.s682 = txt_s682.Text.Replace(",", String.Empty)
            Else
                survey.s682 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s663.Text) Then
                survey.s663 = txt_s663.Text.Replace(",", String.Empty)
            Else
                survey.s663 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s673.Text) Then
                survey.s673 = txt_s673.Text.Replace(",", String.Empty)
            Else
                survey.s673 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s683.Text) Then
                survey.s683 = txt_s683.Text.Replace(",", String.Empty)
            Else
                survey.s683 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s689.Text) Then
                survey.s689 = txt_s689.Text.Replace(",", String.Empty)
            Else
                survey.s689 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s664.Text) Then
                survey.s664 = txt_s664.Text.Replace(",", String.Empty)
            Else
                survey.s664 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s674.Text) Then
                survey.s674 = txt_s674.Text.Replace(",", String.Empty)
            Else
                survey.s674 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s684.Text) Then
                survey.s684 = txt_s684.Text.Replace(",", String.Empty)
            Else
                survey.s684 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s665.Text) Then
                survey.s665 = txt_s665.Text.Replace(",", String.Empty)
            Else
                survey.s665 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s675.Text) Then
                survey.s675 = txt_s675.Text.Replace(",", String.Empty)
            Else
                survey.s675 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s685.Text) Then
                survey.s685 = txt_s685.Text.Replace(",", String.Empty)
            Else
                survey.s685 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s690.Text) Then
                survey.s690 = txt_s690.Text.Replace(",", String.Empty)
            Else
                survey.s690 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s556.Text) Then
                survey.s556 = txt_s556.Text.Replace(",", String.Empty)
            Else
                survey.s556 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s333oth.Text) Then
                survey.s333oth = txt_s333oth.Text.Replace(",", String.Empty)
            Else
                survey.s333oth = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s338.Text) Then
                survey.s338 = txt_s338.Text.Replace(",", String.Empty)
            Else
                survey.s338 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s340.Text) Then
                survey.s340 = txt_s340.Text.Replace(",", String.Empty)
            Else
                survey.s340 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s341oth.Text) Then
                survey.s341oth = txt_s341oth.Text.Replace(",", String.Empty)
            Else
                survey.s341oth = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s342.Text) Then
                survey.s342 = txt_s342.Text.Replace(",", String.Empty)
            Else
                survey.s342 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s557.Text) Then
                survey.s557 = txt_s557.Text.Replace(",", String.Empty)
            Else
                survey.s557 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s323oth.Text) Then
                survey.s323oth = txt_s323oth.Text.Replace(",", String.Empty)
            Else
                survey.s323oth = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s344.Text) Then
                survey.s344 = txt_s344.Text.Replace(",", String.Empty)
            Else
                survey.s344 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s345.Text) Then
                survey.s345 = txt_s345.Text.Replace(",", String.Empty)
            Else
                survey.s345 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s347.Text) Then
                survey.s347 = txt_s347.Text.Replace(",", String.Empty)
            Else
                survey.s347 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s349.Text) Then
                survey.s349 = txt_s349.Text.Replace(",", String.Empty)
            Else
                survey.s349 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s353.Text) Then
                survey.s353 = txt_s353.Text.Replace(",", String.Empty)
            Else
                survey.s353 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s350.Text) Then
                survey.s350 = txt_s350.Text.Replace(",", String.Empty)
            Else
                survey.s350 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s354.Text) Then
                survey.s354 = txt_s354.Text.Replace(",", String.Empty)
            Else
                survey.s354 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s351.Text) Then
                survey.s351 = txt_s351.Text.Replace(",", String.Empty)
            Else
                survey.s351 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s355.Text) Then
                survey.s355 = txt_s355.Text.Replace(",", String.Empty)
            Else
                survey.s355 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s352.Text) Then
                survey.s352 = txt_s352.Text.Replace(",", String.Empty)
            Else
                survey.s352 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s356.Text) Then
                survey.s356 = txt_s356.Text.Replace(",", String.Empty)
            Else
                survey.s356 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s358.Text) Then
                survey.s358 = txt_s358.Text.Replace(",", String.Empty)
            Else
                survey.s358 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s360.Text) Then
                survey.s360 = txt_s360.Text.Replace(",", String.Empty)
            Else
                survey.s360 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s361.Text) Then
                survey.s361 = txt_s361.Text.Replace(",", String.Empty)
            Else
                survey.s361 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s362.Text) Then
                survey.s362 = txt_s362.Text.Replace(",", String.Empty)
            Else
                survey.s362 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s367oth.Text) Then
                survey.s367oth = txt_s367oth.Text.Replace(",", String.Empty)
            Else
                survey.s367oth = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s369.Text) Then
                survey.s369 = txt_s369.Text.Replace(",", String.Empty)
            Else
                survey.s369 = Nothing
            End If

            survey.s290 = cb_s290.Checked
            survey.s294 = cb_s294.Checked
            survey.s298 = cb_s298.Checked
            survey.s291 = cb_s291.Checked
            survey.s295 = cb_s295.Checked
            survey.s299 = cb_s299.Checked
            survey.s292 = cb_s292.Checked
            survey.s296 = cb_s296.Checked
            survey.s300 = cb_s300.Checked
            survey.s293 = cb_s293.Checked
            survey.s297 = cb_s297.Checked
            survey.s301 = cb_s301.Checked
            survey.s302 = cb_s302.Checked
            survey.s327 = cb_s327.Checked
            survey.s331 = cb_s331.Checked
            survey.s328 = cb_s328.Checked
            survey.s332 = cb_s332.Checked
            survey.s329 = cb_s329.Checked
            survey.s330 = cb_s330.Checked
            survey.s333 = cb_s333.Checked
            survey.s334 = cb_s334.Checked
            survey.s337 = cb_s337.Checked
            survey.s339 = cb_s339.Checked
            survey.s341 = cb_s341.Checked
            survey.s303 = cb_s303.Checked
            survey.s304 = cb_s304.Checked
            survey.s311 = cb_s311.Checked
            survey.s318 = cb_s318.Checked
            survey.s305 = cb_s305.Checked
            survey.s312 = cb_s312.Checked
            survey.s319 = cb_s319.Checked
            survey.s306 = cb_s306.Checked
            survey.s313 = cb_s313.Checked
            survey.s320 = cb_s320.Checked
            survey.s307 = cb_s307.Checked
            survey.s314 = cb_s314.Checked
            survey.s321 = cb_s321.Checked
            survey.s308 = cb_s308.Checked
            survey.s315 = cb_s315.Checked
            survey.s322 = cb_s322.Checked
            survey.s574 = cb_s574.Checked
            survey.s576 = cb_s576.Checked
            survey.s578 = cb_s578.Checked
            survey.s575 = cb_s575.Checked
            survey.s577 = cb_s577.Checked
            survey.s579 = cb_s579.Checked
            survey.s309 = cb_s309.Checked
            survey.s316 = cb_s316.Checked
            survey.s323 = cb_s323.Checked
            survey.s310 = cb_s310.Checked
            survey.s317 = cb_s317.Checked
            survey.s363 = cb_s363.Checked
            survey.s366 = cb_s366.Checked
            survey.s364 = cb_s364.Checked
            survey.s367 = cb_s367.Checked
            survey.s365 = cb_s365.Checked
            survey.s368 = cb_s368.Checked
            survey.s558 = cb_s558.Checked
            survey.section5Complete = cb_section5Complete.Checked

            If rb_s278_1.Checked Then survey.s278 = 1
            If rb_s278_2.Checked Then survey.s278 = 2
            If rb_s281_1.Checked Then survey.s281 = 1
            If rb_s281_2.Checked Then survey.s281 = 2
            If rb_s279_1.Checked Then survey.s279 = 1
            If rb_s279_2.Checked Then survey.s279 = 2
            If rb_s282_1.Checked Then survey.s282 = 1
            If rb_s282_2.Checked Then survey.s282 = 2
            If rb_s280_1.Checked Then survey.s280 = 1
            If rb_s280_2.Checked Then survey.s280 = 2
            If rb_s283_1.Checked Then survey.s283 = 1
            If rb_s283_2.Checked Then survey.s283 = 2
            If rb_s651_1.Checked Then survey.s651 = 1
            If rb_s651_2.Checked Then survey.s651 = 2
            If rb_s324_1.Checked Then survey.s324 = 1
            If rb_s324_2.Checked Then survey.s324 = 2
            If rb_s326_1.Checked Then survey.s326 = 1
            If rb_s326_2.Checked Then survey.s326 = 2
            If rb_s335_1.Checked Then survey.s335 = 1
            If rb_s335_2.Checked Then survey.s335 = 2
            If rb_s336_1.Checked Then survey.s336 = 1
            If rb_s336_2.Checked Then survey.s336 = 2
            If rb_s343_1.Checked Then survey.s343 = 1
            If rb_s343_2.Checked Then survey.s343 = 2
            If rb_s346_1.Checked Then survey.s346 = 1
            If rb_s346_2.Checked Then survey.s346 = 2
            If rb_s348_1.Checked Then survey.s348 = 1
            If rb_s348_2.Checked Then survey.s348 = 2
            If rb_s357_1.Checked Then survey.s357 = 1
            If rb_s357_2.Checked Then survey.s357 = 2
            If rb_s359_1.Checked Then survey.s359 = 1
            If rb_s359_2.Checked Then survey.s359 = 2



            survey.IsAuditComplete = False
            db.SaveChanges()
            lbl_lastSave.Text = survey.ModifiedDate
        End Using
    End Sub
    Protected Sub btn_previous_Click(sender As Object, e As EventArgs) Handles btn_previous.Click
        SaveSurvey(False)
        Response.Redirect("CompByJob.aspx")
    End Sub
    Protected Sub btn_save_Click(sender As Object, e As EventArgs) Handles btn_save.Click
        SaveSurvey(False)
    End Sub
    Protected Sub btn_next_Click(sender As Object, e As EventArgs) Handles btn_next.Click
        SaveSurvey(False)
        Response.Redirect("Sales.aspx")
    End Sub
    Protected Sub ddl_location_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddl_location.SelectedIndexChanged
        SaveSurvey(False)
        Session("LocationID") = ddl_location.SelectedValue
        txt_LocationID.Text = ddl_location.SelectedValue
        Response.Redirect("Benefits.aspx")
    End Sub
    Protected Sub lb_toc_bot_Click(sender As Object, e As EventArgs) Handles lb_toc_bot.Click
        SaveSurvey(False)
        Response.Redirect("TableOfContents.aspx")
    End Sub
End Class
