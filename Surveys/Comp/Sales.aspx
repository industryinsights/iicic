﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Survey.master"  MaintainScrollPositionOnPostback="true" AutoEventWireup="false" CodeFile="Sales.aspx.vb" Inherits="Surveys_Sales" %>

<%@ Register Src="~/Controls/SurveyNavigation.ascx" TagPrefix="uc1" TagName="SurveyNavigation" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script type="text/javascript">
        $(document).ready(function () {
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
    <script src="../../Scripts/autoNumeric/autoNumeric-min.js"></script>
    <script src="../../Scripts/Sales.js?v=1"></script>
    <asp:TextBox ID="txt_LocationID" runat="server" Visible="false"></asp:TextBox>
    <asp:TextBox ID="txt_userid" runat="server" Visible="false"></asp:TextBox>
    <div class="bg-content">
        <div class="row">
            <table class="table-condensed centered" runat="server" id="tbl_location">
                <tr>
                    <td>
                        <asp:DropDownList ID="ddl_location" runat="server" CssClass="form-control" AutoPostBack="true"></asp:DropDownList>
                    </td>
                    <td><span class="glyphicon glyphicon-info-sign icon-info" data-placement="left" data-toggle="tooltip" title="This box is disabled by default.  If you are submitting for multiple locations, please add the locations on the Organization Admin page and you'll be able to cycle through them here."></span></td>
                </tr>
            </table>

        </div>

        <div class="">
            <h3 class="text-center">Deadline: 
                <asp:Label ID="lbl_deadline" runat="server" Text=""></asp:Label></h3>
        </div>
        <div class="">
            <span class="col-md-12 text-center text-muted"><em>Last updated: 
            <asp:Label ID="lbl_lastSave" runat="server" Text=""></asp:Label></em></span>
        </div>

        <h2 class="page-header">Salespeople and Sales Incentives</h2>




        <table class="table-condensed table-hover centered">

            <tr class="tr-question">
                <td>1.</td>
                <td>Please indicate the percentage of sales from:</td>
                <td class="text-center">&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td colspan="2">
                    <table class="table-condensed">
                        <tr>
                            <td class="text-right">Outside salespeople</td>
                            <td>
                                <span class="input-group">
                                    <asp:TextBox ID="txt_s375" runat="server" CssClass="form-control text-right"></asp:TextBox>
                                    <span class="input-group-addon">%</span>
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right">Inside salespeople</td>
                            <td>
                                <span class="input-group">
                                    <asp:TextBox ID="txt_s377" runat="server" CssClass="form-control text-right"></asp:TextBox>
                                    <span class="input-group-addon">%</span>
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table>
                                    <tr>
                                        <td>Other</td>
                                        <td>
                                            <asp:TextBox ID="txt_s378oth" runat="server" CssClass="form-control"></asp:TextBox></td>
                                    </tr>
                                </table>
                            </td>
                            <td>
                                <span class="input-group">
                                    <asp:TextBox ID="txt_s378" runat="server" CssClass="form-control text-right"></asp:TextBox>
                                    <span class="input-group-addon">%</span>
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right">Total</td>
                            <td class="text-center">100%</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td colspan="2">&nbsp;</td>
            </tr>

            <tr class="tr-question">
                <td>2.</td>
                <td colspan="2">Do you have formal hiring processes in place for salespeople?</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td colspan="2">
                    <asp:RadioButton ID="rb_s379_1" runat="server" GroupName="s379" Text="Yes" />
                    <asp:RadioButton ID="rb_s379_2" runat="server" GroupName="s379" Text="No" />
                </td>
            </tr>

            <tr>
                <td>&nbsp;</td>
                <td colspan="2">&nbsp;</td>
            </tr>

            <tr class="tr-question">
                <td>3.</td>
                <td colspan="2">Do you conduct aptitude/skill tests for new salesforce candidates?</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td colspan="2">
                    <asp:RadioButton ID="rb_s380_1" runat="server" GroupName="s380" Text="Yes" />
                    <asp:RadioButton ID="rb_s380_2" runat="server" GroupName="s380" Text="No" />
                </td>
            </tr>

            <tr>
                <td>&nbsp;</td>
                <td colspan="2">&nbsp;</td>
            </tr>

            <tr class="tr-question">
                <td>4.</td>
                <td>What capping mechanisms, if any, did you use to limit the amount of incentive pay earned for the year? (check all that apply)</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td colspan="2">
                    <table class="table-condensed">
                        <tr>
                            <td class="col-md-4">
                                <asp:CheckBox ID="cb_s381" runat="server" Text="No cap and no decelerator" CssClass="checkbox-inline" />
                            </td>
                            <td class="col-md-4">
                                <asp:CheckBox ID="cb_s384" runat="server" Text="“Decelerator” that slows earning increases" CssClass="checkbox-inline" />
                            </td>
                        </tr>
                        <tr>
                            <td class="col-md-4">
                                <asp:CheckBox ID="cb_s382" runat="server" Text="Absolute cap on incentive earnings" CssClass="checkbox-inline" />
                            </td>
                            <td class="col-md-4">

                                <table>
                                    <tr>
                                        <td>
                                            <asp:CheckBox ID="cb_s385" runat="server" Text="Other" CssClass="checkbox-inline" /></td>
                                        <td>
                                            <asp:TextBox ID="txt_s385oth" runat="server" CssClass="form-control text-left"></asp:TextBox></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td class="col-md-4">
                                <asp:CheckBox ID="cb_s383" runat="server" Text="&quot;Per deal&quot; cap applied to each sale separately" CssClass="checkbox-inline" />
                            </td>
                            <td class="col-md-4">&nbsp;</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td colspan="2">&nbsp;</td>
            </tr>

            <tr class="tr-question">
                <td>5.</td>
                <td>When are commissions typically paid?</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td colspan="2">
                    <table class="table-condensed">
                        <tr>
                            <td class="text-nowrap">
                                <asp:RadioButton CssClass="radio-inline" ID="rb_s386_1" runat="server" GroupName="s386" Text="When the order is booked" />
                            </td>
                            <td class="text-nowrap">
                                <asp:RadioButton CssClass="radio-inline" ID="rb_s386_4" runat="server" GroupName="s386" Text="When an order is paid" />
                            </td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td class="text-nowrap">
                                <asp:RadioButton CssClass="radio-inline" ID="rb_s386_2" runat="server" GroupName="s386" Text="When the order is invoiced" />
                            </td>
                            <td class="text-nowrap">
                                <asp:RadioButton CssClass="radio-inline" ID="rb_s386_5" runat="server" GroupName="s386" Text="When the revenue is recognized" />
                            </td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td class="text-nowrap">
                                <asp:RadioButton CssClass="radio-inline" ID="rb_s386_3" runat="server" GroupName="s386" Text="When the order is shipped" />
                            </td>
                            <td class="text-nowrap">
                                <table>
                                    <tr>
                                        <td>
                                            <asp:RadioButton CssClass="radio-inline" ID="rb_s386_6" runat="server" GroupName="s386" Text="Other" /></td>
                                        <td>
                                            <asp:TextBox ID="txt_s386oth" runat="server" CssClass="form-control text-left"></asp:TextBox></td>
                                    </tr>
                                </table>

                            </td>
                            <td>&nbsp;</td>
                        </tr>

                    </table>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td colspan="2">&nbsp;</td>
            </tr>

            <tr class="tr-question">
                <td>6.</td>
                <td>What methods are commonly used to determine commissions for outside salespeople? (check all that apply)</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td colspan="2">
                    <table class="table-condensed">
                        <tr>
                            <td class="col-md-4">
                                <asp:CheckBox ID="cb_s559" runat="server" Text="Sales goals" CssClass="checkbox-inline" />
                            </td>
                            <td class="col-md-4">
                                <asp:CheckBox ID="cb_s562" runat="server" Text="New service sales" CssClass="checkbox-inline" />
                            </td>
                        </tr>
                        <tr>
                            <td class="col-md-4">
                                <asp:CheckBox ID="cb_s560" runat="server" Text="Profit goals" CssClass="checkbox-inline" />
                            </td>
                            <td class="col-md-4">
                                <table>
                                    <tr>
                                        <td>
                                            <asp:CheckBox ID="cb_s563" runat="server" Text="Other" CssClass="checkbox-inline" /></td>
                                        <td>
                                            <asp:TextBox ID="txt_s563oth" runat="server" CssClass="form-control text-left"></asp:TextBox></td>
                                    </tr>
                                </table>

                            </td>
                        </tr>
                        <tr>
                            <td class="col-md-4">
                                <asp:CheckBox ID="cb_s561" runat="server" Text="New product sales" CssClass="checkbox-inline" />
                            </td>
                            <td class="col-md-4">&nbsp;</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td colspan="2">&nbsp;</td>
            </tr>

            <tr class="tr-question">
                <td>7.</td>
                <td>Most common method used to cover travel and entertainment expenses</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td colspan="2">
                    <table class="table-condensed">
                        <tr>
                            <td class="col-md-4 text-nowrap">
                                <asp:RadioButton CssClass="radio-inline" ID="rb_s387_1" runat="server" GroupName="s387" Text="All expenses reimbursed" />
                            </td>
                            <td class="col-md-4 text-nowrap">
                                <asp:RadioButton CssClass="radio-inline" ID="rb_s387_4" runat="server" GroupName="s387" Text="Per diem payment" />
                            </td>
                        </tr>
                        <tr>
                            <td class="col-md-4 text-nowrap">
                                <asp:RadioButton CssClass="radio-inline" ID="rb_s387_2" runat="server" GroupName="s387" Text="Compensation covers expenses" />
                            </td>
                            <td class="col-md-4 text-nowrap">
                                <asp:RadioButton CssClass="radio-inline" ID="rb_s387_5" runat="server" GroupName="s387" Text="Fixed monthly expense allowance" />
                            </td>
                        </tr>
                        <tr>
                            <td class="col-md-4 text-nowrap">
                                <asp:RadioButton CssClass="radio-inline" ID="rb_s387_3" runat="server" GroupName="s387" Text="Travel expenses only reimbursed" />
                            </td>
                            <td class="col-md-4 text-nowrap">
                                <table>
                                    <tr>
                                        <td>
                                            <asp:RadioButton CssClass="radio-inline" ID="rb_s387_6" runat="server" GroupName="s387" Text="Other" /></td>
                                        <td>
                                            <asp:TextBox ID="txt_s387oth" runat="server" CssClass="form-control text-left"></asp:TextBox></td>
                                    </tr>
                                </table>

                            </td>

                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td colspan="2">&nbsp;</td>
            </tr>

            <tr class="tr-question">
                <td>8.</td>
                <td>Outside salespeople are eligible for commissions on which of the following? (select all that apply)</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td colspan="2">
                    <table class="table-condensed">
                        <tr>
                            <td class="col-md-4 text-nowrap">
                                <asp:CheckBox ID="cb_s564" runat="server" Text="Prompt payment or other billing discounts" CssClass="checkbox-inline" />
                            </td>
                            <td class="col-md-4 text-nowrap">
                                <asp:CheckBox ID="cb_s567" runat="server" Text="Accessories & tooling" CssClass="checkbox-inline" />
                            </td>
                        </tr>
                        <tr>
                            <td class="col-md-4 text-nowrap">
                                <asp:CheckBox ID="cb_s565" runat="server" Text="Service" CssClass="checkbox-inline" />
                            </td>
                            <td class="col-md-4 text-nowrap">
                                <asp:CheckBox ID="cb_s568" runat="server" Text="Finance plans" CssClass="checkbox-inline" />
                            </td>
                        </tr>
                        <tr>
                            <td class="col-md-4 text-nowrap">
                                <asp:CheckBox ID="cb_s566" runat="server" Text="Repair parts" CssClass="checkbox-inline" />
                            </td>
                            <td class="col-md-4 text-nowrap">
                                <asp:CheckBox ID="cb_s569" runat="server" Text="Collection of cancellation charges" CssClass="checkbox-inline" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td colspan="2">&nbsp;</td>
            </tr>

            <tr class="tr-question">
                <td>9.</td>
                <td>Most common method used to provide salespeople with automobiles:</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td colspan="2">
                    <table class="table-condensed">
                        <tr>
                            <td class="col-md-3">
                                <asp:RadioButton CssClass="radio-inline" ID="rb_s570_1" runat="server" GroupName="s570" Text="None" />
                            </td>
                            <td class="col-md-3">
                                <asp:RadioButton CssClass="radio-inline" ID="rb_s570_3" runat="server" GroupName="s570" Text="Company-owned" />
                            </td>
                        </tr>
                        <tr>
                            <td class="col-md-3  text-nowrap">
                                <asp:RadioButton CssClass="radio-inline" ID="rb_s570_2" runat="server" GroupName="s570" Text="Company-leased" />
                            </td>
                            <td class="text-nowrap">
                                <asp:RadioButton CssClass="radio-inline" ID="rb_s570_4" runat="server" GroupName="s570" Text="Employee-owned reimbursement" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td colspan="2">&nbsp;</td>
            </tr>


            <tr class="tr-question">
                <td>10.</td>
                <td>If the method is employee-owned reimbursement, what basis is most commonly used?</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td colspan="2">
                    <table class="table-condensed">
                        <tr>
                            <td class="col-md-3 text-nowrap">
                                <asp:RadioButton CssClass="radio-inline" ID="rb_s571_1" runat="server" GroupName="s571" Text="Mileage" />
                            </td>
                            <td class="col-md-3 text-nowrap">
                                <asp:RadioButton CssClass="radio-inline" ID="rb_s571_3" runat="server" GroupName="s571" Text="Monthly allowance" />
                            </td>
                        </tr>
                        <tr>
                            <td class="col-md-3 text-nowrap">
                                <asp:RadioButton CssClass="radio-inline" ID="rb_s571_2" runat="server" GroupName="s571" Text="Gas & oil" />
                            </td>
                            <td class="col-md-3 text-nowrap">
                                <table>
                                    <tr>
                                        <td>
                                            <asp:RadioButton CssClass="radio-inline" ID="rb_s571_4" runat="server" GroupName="s571" Text="Other" /></td>
                                        <td>
                                            <asp:TextBox ID="txt_s571oth" runat="server" CssClass="form-control"></asp:TextBox></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td colspan="2">&nbsp;</td>
            </tr>

            <tr class="tr-question">
                <td>11.</td>
                <td>Most common method used to provide salespeople with cell phones:</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td colspan="2">
                    <table class="table-condensed">
                        <tr>
                            <td class="col-md-3 text-nowrap">
                                <asp:RadioButton CssClass="radio-inline" ID="rb_s572_1" runat="server" GroupName="s572" Text="None" />
                            </td>
                            <td class="col-md-3 text-nowrap">
                                <asp:RadioButton CssClass="radio-inline" ID="rb_s572_3" runat="server" GroupName="s572" Text="Monthly phone allowance" />
                            </td>
                        </tr>
                        <tr>
                            <td class="col-md-3 text-nowrap">
                                <asp:RadioButton CssClass="radio-inline" ID="rb_s572_2" runat="server" GroupName="s572" Text="Company-provided phone" />
                            </td>
                            <td class="col-md-3 text-nowrap">
                                <table>
                                    <tr>
                                        <td>
                                            <asp:RadioButton CssClass="radio-inline" ID="rb_s572_4" runat="server" GroupName="s572" Text="Other" /></td>
                                        <td>
                                            <asp:TextBox ID="txt_s572oth" runat="server" CssClass="form-control"></asp:TextBox></td>
                                    </tr>
                                </table>
                            </td>

                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td colspan="2">&nbsp;</td>
            </tr>


        </table>

        <div id="MHEDAtbl" runat="server">
            <h3 class="h3-header">Compensation by Product Line</h3>
            <p>
                Report compensation for only ONE TYPICAL FULL-TIME employee in each position.  Leave an item blank if it does not apply. Do not include fringe benefits.  Report annualized W-2 (T-4) wages prior to employee deductions. If a position is filled with part-timers only, report the compensation one employee would receive if they were full-time.

            </p>
            <%--<table class="table table-condensed table-hover table-survey centered" id="MHEDAtbl" runat="server">--%>
            <table class="table table-condensed table-hover table-survey centered">

                <thead>
                    <tr class="td-bg-primary">
                        <th>&nbsp;</th>
                        <th class="text-center">Lift Truck Equipment Salesperson</th>
                        <th class="text-center">General Line Equipment Salesperson</th>
                        <th class="text-center">Product Sales Support</th>
                        <th class="text-center">Engineered Products Salesperson</th>
                        <th class="text-center">Inside Salesperson</th>
                    </tr>
                </thead>

                <tbody>
                    <tr class="question">
                        <td class="col-md-2">Years with the firm in this position</td>
                        <td class="col-md-2">
                            <span class="input-group">
                                <span class="input-group-addon">$</span>
                                <asp:TextBox ID="txt_s915" runat="server" CssClass="form-control text-right"></asp:TextBox>
                            </span>
                        </td>
                        <td class="col-md-2">
                            <span class="input-group">
                                <span class="input-group-addon">$</span>
                                <asp:TextBox ID="txt_s925" runat="server" CssClass="form-control text-right"></asp:TextBox>
                            </span>
                        </td>
                        <td class="col-md-2">
                            <span class="input-group">
                                <span class="input-group-addon">$</span>
                                <asp:TextBox ID="txt_s935" runat="server" CssClass="form-control text-right"></asp:TextBox>
                            </span>
                        </td>
                        <td class="col-md-2">
                            <span class="input-group">
                                <span class="input-group-addon">$</span>
                                <asp:TextBox ID="txt_s945" runat="server" CssClass="form-control text-right"></asp:TextBox>
                            </span>
                        </td>
                        <td class="col-md-2">
                            <span class="input-group">
                                <span class="input-group-addon">$</span>
                                <asp:TextBox ID="txt_s955" runat="server" CssClass="form-control text-right"></asp:TextBox>
                            </span>
                        </td>
                    </tr>
                    <tr class="question">
                        <td class="col-md-2"></td>
                        <td class="col-md-2">&nbsp;</td>
                        <td class="col-md-2">&nbsp;</td>
                        <td class="col-md-2">&nbsp;</td>
                        <td class="col-md-2">&nbsp;</td>
                        <td class="col-md-2">&nbsp;</td>
                    </tr>
                    <tr class="question">
                        <td class="col-md-2">Either base salary (no draw)</td>
                        <td class="col-md-2">
                            <span class="input-group">
                                <span class="input-group-addon">$</span>
                                <asp:TextBox ID="txt_s916" runat="server" CssClass="form-control text-right"></asp:TextBox>
                            </span>
                        </td>
                        <td class="col-md-2">
                            <span class="input-group">
                                <span class="input-group-addon">$</span>
                                <asp:TextBox ID="txt_s926" runat="server" CssClass="form-control text-right"></asp:TextBox>
                            </span>
                        </td>
                        <td class="col-md-2">
                            <span class="input-group">
                                <span class="input-group-addon">$</span>
                                <asp:TextBox ID="txt_s936" runat="server" CssClass="form-control text-right"></asp:TextBox>
                            </span>
                        </td>
                        <td class="col-md-2">
                            <span class="input-group">
                                <span class="input-group-addon">$</span>
                                <asp:TextBox ID="txt_s946" runat="server" CssClass="form-control text-right"></asp:TextBox>
                            </span>
                        </td>
                        <td class="col-md-2">
                            <span class="input-group">
                                <span class="input-group-addon">$</span>
                                <asp:TextBox ID="txt_s956" runat="server" CssClass="form-control text-right"></asp:TextBox>
                            </span>
                        </td>
                    </tr>
                    <tr class="question">
                        <td class="col-md-2">OR draw only (no base salary)</td>
                        <td class="col-md-2">
                            <span class="input-group">
                                <span class="input-group-addon">$</span>
                                <asp:TextBox ID="txt_s917" runat="server" CssClass="form-control text-right"></asp:TextBox>
                            </span>
                        </td>
                        <td class="col-md-2">
                            <span class="input-group">
                                <span class="input-group-addon">$</span>
                                <asp:TextBox ID="txt_s927" runat="server" CssClass="form-control text-right"></asp:TextBox>
                            </span>
                        </td>
                        <td class="col-md-2">
                            <span class="input-group">
                                <span class="input-group-addon">$</span>
                                <asp:TextBox ID="txt_s937" runat="server" CssClass="form-control text-right"></asp:TextBox>
                            </span>
                        </td>
                        <td class="col-md-2">
                            <span class="input-group">
                                <span class="input-group-addon">$</span>
                                <asp:TextBox ID="txt_s947" runat="server" CssClass="form-control text-right"></asp:TextBox>
                            </span>
                        </td>
                        <td class="col-md-2">
                            <span class="input-group">
                                <span class="input-group-addon">$</span>
                                <asp:TextBox ID="txt_s957" runat="server" CssClass="form-control text-right"></asp:TextBox>
                            </span>
                        </td>
                    </tr>
                    <tr class="question">
                        <td class="col-md-2">Commissions on billings</td>
                        <td class="col-md-2">
                            <span class="input-group">
                                <span class="input-group-addon">$</span>
                                <asp:TextBox ID="txt_s918" runat="server" CssClass="form-control text-right"></asp:TextBox>
                            </span>
                        </td>
                        <td class="col-md-2">
                            <span class="input-group">
                                <span class="input-group-addon">$</span>
                                <asp:TextBox ID="txt_s928" runat="server" CssClass="form-control text-right"></asp:TextBox>
                            </span>
                        </td>
                        <td class="col-md-2">
                            <span class="input-group">
                                <span class="input-group-addon">$</span>
                                <asp:TextBox ID="txt_s938" runat="server" CssClass="form-control text-right"></asp:TextBox>
                            </span>
                        </td>
                        <td class="col-md-2">
                            <span class="input-group">
                                <span class="input-group-addon">$</span>
                                <asp:TextBox ID="txt_s948" runat="server" CssClass="form-control text-right"></asp:TextBox>
                            </span>
                        </td>
                        <td class="col-md-2">
                            <span class="input-group">
                                <span class="input-group-addon">$</span>
                                <asp:TextBox ID="txt_s958" runat="server" CssClass="form-control text-right"></asp:TextBox>
                            </span>
                        </td>
                    </tr>
                    <tr class="question">
                        <td class="col-md-2">Commissions on gross profit</td>
                        <td class="col-md-2">
                            <span class="input-group">
                                <span class="input-group-addon">$</span>
                                <asp:TextBox ID="txt_s919" runat="server" CssClass="form-control text-right"></asp:TextBox>
                            </span>
                        </td>
                        <td class="col-md-2">
                            <span class="input-group">
                                <span class="input-group-addon">$</span>
                                <asp:TextBox ID="txt_s929" runat="server" CssClass="form-control text-right"></asp:TextBox>
                            </span>
                        </td>
                        <td class="col-md-2">
                            <span class="input-group">
                                <span class="input-group-addon">$</span>
                                <asp:TextBox ID="txt_s939" runat="server" CssClass="form-control text-right"></asp:TextBox>
                            </span>
                        </td>
                        <td class="col-md-2">
                            <span class="input-group">
                                <span class="input-group-addon">$</span>
                                <asp:TextBox ID="txt_s949" runat="server" CssClass="form-control text-right"></asp:TextBox>
                            </span>
                        </td>
                        <td class="col-md-2">
                            <span class="input-group">
                                <span class="input-group-addon">$</span>
                                <asp:TextBox ID="txt_s959" runat="server" CssClass="form-control text-right"></asp:TextBox>
                            </span>
                        </td>
                    </tr>
                    <tr class="question">
                        <td class="col-md-2">Other Bonuses</td>
                        <td class="col-md-2">
                            <span class="input-group">
                                <span class="input-group-addon">$</span>
                                <asp:TextBox ID="txt_s920" runat="server" CssClass="form-control text-right"></asp:TextBox>
                            </span>
                        </td>
                        <td class="col-md-2">
                            <span class="input-group">
                                <span class="input-group-addon">$</span>
                                <asp:TextBox ID="txt_s930" runat="server" CssClass="form-control text-right"></asp:TextBox>
                            </span>
                        </td>
                        <td class="col-md-2">
                            <span class="input-group">
                                <span class="input-group-addon">$</span>
                                <asp:TextBox ID="txt_s940" runat="server" CssClass="form-control text-right"></asp:TextBox>
                            </span>
                        </td>
                        <td class="col-md-2">
                            <span class="input-group">
                                <span class="input-group-addon">$</span>
                                <asp:TextBox ID="txt_s950" runat="server" CssClass="form-control text-right"></asp:TextBox>
                            </span>
                        </td>
                        <td class="col-md-2">
                            <span class="input-group">
                                <span class="input-group-addon">$</span>
                                <asp:TextBox ID="txt_s960" runat="server" CssClass="form-control text-right"></asp:TextBox>
                            </span>
                        </td>
                    </tr>
                    <tr class="question">
                        <td class="col-md-2">Total cash compensation</td>
                        <td class="col-md-2 text-right SumTruck" id="SumTruck">$,$$$</td>
                        <td class="col-md-2 text-right SumLine" id="SumLine">$,$$$</td>
                        <td class="col-md-2 text-right SumSalesSupp" id="SumSalesSupp">$,$$$</td>
                        <td class="col-md-2 text-right SumProduct" id="SumProduct">$,$$$</td>
                        <td class="col-md-2 text-right SumInsideSales" id="SumInsideSales">$,$$$</td>
                    </tr>
                    <tr class="question">
                        <td class="col-md-2">Total annual sales per salesperson</td>
                        <td class="col-md-2">
                            <span class="input-group">
                                <span class="input-group-addon">$</span>
                                <asp:TextBox ID="txt_s921" runat="server" CssClass="form-control text-right"></asp:TextBox>
                            </span>
                        </td>
                        <td class="col-md-2">
                            <span class="input-group">
                                <span class="input-group-addon">$</span>
                                <asp:TextBox ID="txt_s931" runat="server" CssClass="form-control text-right"></asp:TextBox>
                            </span>
                        </td>
                        <td class="col-md-2">
                            <span class="input-group">
                                <span class="input-group-addon">$</span>
                                <asp:TextBox ID="txt_s941" runat="server" CssClass="form-control text-right"></asp:TextBox>
                            </span>
                        </td>
                        <td class="col-md-2">
                            <span class="input-group">
                                <span class="input-group-addon">$</span>
                                <asp:TextBox ID="txt_s951" runat="server" CssClass="form-control text-right"></asp:TextBox>
                            </span>
                        </td>
                        <td class="col-md-2">
                            <span class="input-group">
                                <span class="input-group-addon">$</span>
                                <asp:TextBox ID="txt_s961" runat="server" CssClass="form-control text-right"></asp:TextBox>
                            </span>
                        </td>
                    </tr>
                    <tr class="question">
                        <td class="col-md-2">Average order size</td>
                        <td class="col-md-2">
                            <span class="input-group">
                                <span class="input-group-addon">$</span>
                                <asp:TextBox ID="txt_s922" runat="server" CssClass="form-control text-right"></asp:TextBox>
                            </span>
                        </td>
                        <td class="col-md-2">
                            <span class="input-group">
                                <span class="input-group-addon">$</span>
                                <asp:TextBox ID="txt_s932" runat="server" CssClass="form-control text-right"></asp:TextBox>
                            </span>
                        </td>
                        <td class="col-md-2">
                            <span class="input-group">
                                <span class="input-group-addon">$</span>
                                <asp:TextBox ID="txt_s942" runat="server" CssClass="form-control text-right"></asp:TextBox>
                            </span>
                        </td>
                        <td class="col-md-2">
                            <span class="input-group">
                                <span class="input-group-addon">$</span>
                                <asp:TextBox ID="txt_s952" runat="server" CssClass="form-control text-right"></asp:TextBox>
                            </span>
                        </td>
                        <td class="col-md-2">
                            <span class="input-group">
                                <span class="input-group-addon">$</span>
                                <asp:TextBox ID="txt_s962" runat="server" CssClass="form-control text-right"></asp:TextBox>
                            </span>
                        </td>
                    </tr>
                    <tr class="question">
                        <td class="col-md-2">Average gross margin % on sales</td>
                        <td class="col-md-2">
                            <span class="input-group">
                                <span class="input-group-addon">%</span>
                                <asp:TextBox ID="txt_s923" runat="server" CssClass="form-control text-right"></asp:TextBox>

                            </span>
                        </td>
                        <td class="col-md-2">
                            <span class="input-group">
                                <span class="input-group-addon">%</span><asp:TextBox ID="txt_s933" runat="server" CssClass="form-control text-right"></asp:TextBox>

                            </span>
                        </td>
                        <td class="col-md-2">
                            <span class="input-group">
                                <span class="input-group-addon">%</span><asp:TextBox ID="txt_s943" runat="server" CssClass="form-control text-right"></asp:TextBox>

                            </span>
                        </td>
                        <td class="col-md-2">
                            <span class="input-group">
                                <span class="input-group-addon">%</span><asp:TextBox ID="txt_s953" runat="server" CssClass="form-control text-right"></asp:TextBox>

                            </span>
                        </td>
                        <td class="col-md-2">
                            <span class="input-group">
                                <span class="input-group-addon">%</span><asp:TextBox ID="txt_s963" runat="server" CssClass="form-control text-right"></asp:TextBox>

                            </span>
                        </td>
                    </tr>
                    <tr class="question">
                        <td class="col-md-2">Average number of sales calls per week</td>
                        <td class="col-md-2">
                            <span class="input-group">
                                <span class="input-group-addon">#</span>
                                <asp:TextBox ID="txt_s924" runat="server" CssClass="form-control text-right"></asp:TextBox>
                            </span>
                        </td>
                        <td class="col-md-2">
                            <span class="input-group">
                                <span class="input-group-addon">#</span>
                                <asp:TextBox ID="txt_s934" runat="server" CssClass="form-control text-right"></asp:TextBox>
                            </span>
                        </td>
                        <td class="col-md-2">
                            <span class="input-group">
                                <span class="input-group-addon">#</span>
                                <asp:TextBox ID="txt_s944" runat="server" CssClass="form-control text-right"></asp:TextBox>
                            </span>
                        </td>
                        <td class="col-md-2">
                            <span class="input-group">
                                <span class="input-group-addon">#</span>
                                <asp:TextBox ID="txt_s954" runat="server" CssClass="form-control text-right"></asp:TextBox>
                            </span>
                        </td>
                        <td class="col-md-2">
                            <span class="input-group">
                                <span class="input-group-addon">#</span>
                                <asp:TextBox ID="txt_s964" runat="server" CssClass="form-control text-right"></asp:TextBox>
                            </span>
                        </td>
                    </tr>

                </tbody>
            </table>
        </div>





        <table class="table-condensed centered">
            <tr>
                <td class="text-center" colspan="3">
                    <asp:CheckBox ID="cb_section6Complete" runat="server" Text="This section is complete." />
                </td>
            </tr>
            <tr>
                <td class="text-center">
                    <asp:Button ID="btn_previous" runat="server" CssClass="btn btn-primary" Text="Previous Section" Width="141px" />
                </td>
                <td class="text-center">
                    <asp:Button ID="btn_save" runat="server" CssClass="btn btn-primary" Text="Save" Width="141px" />
                </td>
                <td class="text-center">
                    <asp:Button ID="btn_next" runat="server" CssClass="btn btn-primary" Text="Next Section" Width="141px" />
                </td>
            </tr>
            <tr>
                <td class="text-center">&nbsp;</td>
                <td class="text-center">
                    <asp:LinkButton ID="lb_toc_bot" runat="server" CssClass="text-bold">Home</asp:LinkButton>
                </td>
                <td class="text-center">&nbsp;</td>
            </tr>
        </table>
    </div>
</asp:Content>

