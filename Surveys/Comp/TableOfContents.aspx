﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Survey.master" AutoEventWireup="false" CodeFile="TableOfContents.aspx.vb" Inherits="Surveys_Financial_TableOfContents" %>

<%@ Register Src="~/Controls/SurveyNavigation.ascx" TagPrefix="uc1" TagName="SurveyNavigation" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script type="text/javascript">
        $(document).ready(function () {
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
    <div class="bg-content">
        <h3 class="text-bold">Welcome to the Cross-Industry Compensation Portal</h3>


        <table class="table-condensed centered" id="tbl_location" runat="server">
            <tr>
                <td>
                    <asp:DropDownList ID="ddl_location" runat="server" CssClass="form-control" AutoPostBack="true"></asp:DropDownList>
                </td>
                <td><span class="glyphicon glyphicon-info-sign icon-info" data-placement="left" data-toggle="tooltip" title="This box is disabled by default.  If you are submitting for multiple locations, please add the locations on the Organization Admin page and you'll be able to cycle through them here."></span></td>
            </tr>
        </table>
        <div class="col-md-12">
            <p>
                This year&#39;s questionnaire includes 7 main sections that cover your company as a whole. You may complete the sections in any order.
            </p>
            <p>
                Note:  All information in this site will be securely stored and confidentially maintained by Industry Insights.  Your data will never be available to anyone outside <a href="http://www.industryinsights.com/" target="_blank">Industry Insights</a>.
            </p>
            <p>
                <asp:Button ID="btn_start" runat="server" CssClass="btn btn-success" Text="Begin Survey" PostBackUrl="~/Surveys/Comp/Profile.aspx" Width="185px" />
                <asp:Label ID="lbl_surveyClosed" runat="server" CssClass="text-bold text-danger" Text="The survey is closed." Visible="False"></asp:Label>
            </p>
        </div>
        <div class="row">
            <div class="col-md-7 text-left">
                <table class="table-condensed table-hover">
                    <tr>
                        <td class="td-bg-primary">Online Survey</td>
                        <td class="td-bg-primary col-md-3">Status</td>
                    </tr>
                    <tr>
                        <td>
                            <asp:LinkButton ID="lb_Profile" runat="server" PostBackUrl="~/Surveys/Comp/Profile.aspx">General Information</asp:LinkButton></td>
                        <td><em class="small">
                            <asp:Label ID="lbl_statusProfile" runat="server" Text="Not started"></asp:Label></em></td>
                    </tr>

                    <tr>
                        <td>
                            <asp:LinkButton ID="lb_Recruiting" runat="server" PostBackUrl="~/Surveys/Comp/Recruiting.aspx">Recruiting and Retention</asp:LinkButton></td>
                        <td><em class="small">
                            <asp:Label ID="lbl_statusRecruiting" runat="server" Text="Not started"></asp:Label></em></td>
                    </tr>

                    <tr>
                        <td>
                            <asp:LinkButton ID="lb_Compensation" runat="server" PostBackUrl="~/Surveys/Comp/Compensation.aspx">  Executive Compensation and Benefits</asp:LinkButton></td>
                        <td><em class="small">
                            <asp:Label ID="lbl_statusCompensation" runat="server" Text="Not started"></asp:Label></em></td>
                    </tr>

                    <tr>
                        <td>
                            <asp:LinkButton ID="lb_CompByJob" runat="server" PostBackUrl="~/Surveys/Comp/CompByJob.aspx">Employee Compensation</asp:LinkButton></td>
                        <td><em class="small">
                            <asp:Label ID="lbl_statusCompByJob" runat="server" Text="Not started"></asp:Label></em></td>
                    </tr>

                    <tr>
                        <td>
                            <asp:LinkButton ID="lb_Benefits" runat="server" PostBackUrl="~/Surveys/Comp/Benefits.aspx">Healthcare and Benefits</asp:LinkButton></td>
                        <td><em class="small">
                            <asp:Label ID="lbl_statusBenefits" runat="server" Text="Not started"></asp:Label></em></td>
                    </tr>

                    <tr>
                        <td>
                            <asp:LinkButton ID="lb_Sales" runat="server" PostBackUrl="~/Surveys/Comp/Sales.aspx">Salespeople and Sales Practices</asp:LinkButton></td>
                        <td><em class="small">
                            <asp:Label ID="lbl_statusSales" runat="server" Text="Not started"></asp:Label></em></td>
                    </tr>

                    <tr>
                        <td>
                            <asp:LinkButton ID="lb_BranchCompBen" runat="server" PostBackUrl="~/Surveys/Comp/BranchCompBen.aspx">Location Specific Information</asp:LinkButton></td>
                        <td><em class="small">
                            <asp:Label ID="lbl_statusBranchCompBen" runat="server" Text="Not started"></asp:Label></em></td>
                    </tr>

                    <tr>
                        <td class="text-center">
                            <asp:Button ID="btn_submit" runat="server" CssClass="btn btn-lg btn-default disabled" Text="Submit Survey" Enabled="False" Width="185px" />
                            <br />
                            The submit button will become active once all sections of the survey are marked complete.</td>
                        <td>
                            <em>
                                <asp:Label ID="lbl_submit" runat="server" CssClass="text-muted" Text="Not complete"></asp:Label>
                                <br />
                                <asp:Label ID="lbl_deadline" runat="server" CssClass="text-muted small"></asp:Label></em>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="col-md-5 text-left">
                <table class="table-condensed">
                    <tr>
                        <td class="td-bg-primary" colspan="2">Other Survey Options</td>
                    </tr>
                    <tr>
                        <td class="col-md-1">
                            <a id="imgbtn_excel" runat="server" href="#" target="_blank">
                                <img src="../../Content/images/icon-excel.png" /></a></td>
                        <td>
                            <a href="#" id="lb_excel" runat="server" target="_blank">Download Excel File</a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a id="imgbtn_pdf" runat="server" href="../../Resources/II Cross Industry Compensation Survey.pdf" target="_blank">
                                <img src="../../Content/images/icon-pdf.png" /></a>
                        </td>
                        <td>
                            <a id="lb_pdf" runat="server" href="../../Resources/II Cross Industry Compensation Survey.pdf" target="_blank">Download PDF</a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:ImageButton ID="imgbtn_upload" runat="server" ImageUrl="~/Content/images/icon-upload.png" PostBackUrl="~/Surveys/Comp/OfflineSurvey.aspx" />
                        </td>
                        <td>
                            <asp:Button ID="btn_upload" runat="server" CssClass="btn btn-success" Text="Upload Excel/PDF" Width="185px" PostBackUrl="~/Surveys/Comp/OfflineSurvey.aspx" />
                        </td>
                    </tr>
                    <tr id="tr_addUsers" runat="server">
                        <td>
                            <img alt="" src="../../Content/images/icon-adduser.png" /></td>
                        <td>
                            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#userModal" style="width: 185px;">Add Users</button>
                        </td>
                    </tr>
                </table>
                <table class="table-condensed">
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                </table>
                <asp:Label runat="server" ID="lblAuthUser" Text="" Visible="False" CssClass="alert-success"></asp:Label>
            </div>
        </div>
        <!-- Modal -->
        <div id="userModal" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title"><strong>Account Management – Add Authorized User</strong></h4>
                    </div>
                    <div class="modal-body">
                        <p>Please enter a valid email and check the levels of access you wish to grant the user.</p>
                        <p>An account will be created, and an email will be sent informing the user that he/she has been added to your account.</p>

                        <div class="form-group">
                            <label for="txt_authEmail">Email address:</label>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txt_authEmail" CssClass="text-danger" ErrorMessage="Email is required." ValidationGroup="AddAuthorized">*</asp:RequiredFieldValidator>
                            <asp:TextBox type="email" CssClass="form-control" ID="txt_authEmail" runat="server"></asp:TextBox>
                        </div>
                        <div class="form-group">
                            <label for="txt_authName">
                                Name:<asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txt_authName" CssClass="text-danger" ErrorMessage="Name is required." ValidationGroup="AddAuthorized">*</asp:RequiredFieldValidator>
                            </label>
                            &nbsp;<asp:TextBox type="text" CssClass="form-control" ID="txt_authName" runat="server"></asp:TextBox>
                        </div>

                        <p>Should this user be an organization administrator?</p>
                        <div class="form-group">
                            <asp:CheckBox CssClass="checkbox-inline" ID="cb_admin" runat="server" Text="This user is an organization administrator." />
                        </div>

                        <div class="form-group">
                            <asp:Button ID="btn_AddUser" runat="server" CssClass="btn btn-success" Text="Add User" Visible="true" ValidationGroup="AddAuthorized" />
                        </div>

                        <div class="row">
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="alert-danger" ValidationGroup="AddAuthorized" />
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div>
    </div>
</asp:Content>

