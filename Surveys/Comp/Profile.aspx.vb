﻿
Imports Models

Partial Class Surveys_Profile
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load



        If Session("LocationID") Is Nothing Then
            If Not String.IsNullOrEmpty(txt_LocationID.Text) Then
                Session("LocationID") = Convert.ToInt32(txt_LocationID.Text)
            Else
                Response.Redirect("../../account/login.aspx")
            End If
        End If
        If Session("UserId") Is Nothing Then
            If Not String.IsNullOrEmpty(txt_userid.Text) Then
                Session("UserId") = Convert.ToInt32(txt_userid.Text)
            Else
                Response.Redirect("../../account/login.aspx")
            End If
        End If
        If Not IsPostBack Then
            Dim LocationID As Integer = Session("LocationID")
            txt_LocationID.Text = LocationID
            Dim userid As Integer = Session("UserId")
            txt_userid.Text = userid
            If Session("Client") IsNot Nothing And Not String.IsNullOrEmpty(Session("Client")) Then
                lbl_deadline.Text = SurveyHelper.ReturnDeadline(Session("Client").ToString)
            Else
                lbl_deadline.Text = "Friday, April 13, 2018"
            End If

            btn_previous.Text = "Home"
            ddl_location.Items.Clear()
            Dim locations As List(Of ListItem) = SurveyHelper.ReturnLocationList(Session("OrganizationId"))
            For Each l In locations
                ddl_location.Items.Add(l)
            Next
            ddl_location.SelectedValue = Session("LocationID")
            tbl_location.Visible = False
            LoadSurvey()
        End If
    End Sub
    Sub LoadSurvey()
        Using db As New IICICEntities
            Dim LocationID As Integer = txt_LocationID.Text
            Dim survey As New CompSurvey
            survey = (From s In db.CompSurveys Where s.LocationID = LocationID And s.SurveyYear = Now.Year).FirstOrDefault
            If survey IsNot Nothing Then

                lbl_lastSave.Text = survey.ModifiedDate

                '__Custom Show/hide based on assocation____________________________\/
                'DONE VIA JAVASCRIPT FOR THIS PAGE (since this is where the options are selected)
                'MHEDAtbl.Visible = False 'default

                'If survey.s1910 IsNot Nothing Then
                '    'show NAW sections
                '    If survey.s1910 = True Then MHEDAtbl.Visible = True
                'End If
                '__Custom Show/hide based on assocation____________________________/\

                '---PASTE LOAD STATEMENTS---


                If Not String.IsNullOrEmpty(survey.s700oth) Then
                    txt_s700oth.Text = survey.s700oth
                End If
                If Not String.IsNullOrEmpty(survey.s701oth) Then
                    txt_s701oth.Text = survey.s701oth
                End If
                If Not String.IsNullOrEmpty(survey.s703oth) Then
                    txt_s703oth.Text = survey.s703oth
                End If
                If survey.s1 IsNot Nothing Then
                    txt_s1.Text = survey.s1
                End If
                If Not String.IsNullOrEmpty(survey.s3oth) Then
                    txt_s3oth.Text = survey.s3oth
                End If
                If survey.s5 IsNot Nothing Then
                    txt_s5.Text = String.Format(survey.s5, "N0")
                End If
                If survey.s522 IsNot Nothing Then
                    txt_s522.Text = String.Format(survey.s522, "N0")
                End If
                If survey.s6 IsNot Nothing Then
                    txt_s6.Text = String.Format(survey.s6, "N0")
                End If
                If survey.s7 IsNot Nothing Then
                    txt_s7.Text = String.Format(survey.s7, "N0")
                End If
                If survey.s8 IsNot Nothing Then
                    txt_s8.Text = String.Format(survey.s8, "N0")
                End If
                If survey.s10 IsNot Nothing Then
                    txt_s10.Text = String.Format(survey.s10, "N0")
                End If
                If Not String.IsNullOrEmpty(survey.s34oth) Then
                    txt_s34oth.Text = survey.s34oth
                End If

                If survey.s1900 IsNot Nothing Then
                    cb_s1900.Checked = survey.s1900
                End If
                If survey.s1901 IsNot Nothing Then
                    cb_s1901.Checked = survey.s1901
                End If
                If survey.s1902 IsNot Nothing Then
                    cb_s1902.Checked = survey.s1902
                End If
                If survey.s1903 IsNot Nothing Then
                    cb_s1903.Checked = survey.s1903
                End If
                If survey.s1904 IsNot Nothing Then
                    cb_s1904.Checked = survey.s1904
                End If
                If survey.s1905 IsNot Nothing Then
                    cb_s1905.Checked = survey.s1905
                End If
                If survey.s1906 IsNot Nothing Then
                    cb_s1906.Checked = survey.s1906
                End If
                If survey.s1907 IsNot Nothing Then
                    cb_s1907.Checked = survey.s1907
                End If
                If survey.s1908 IsNot Nothing Then
                    cb_s1908.Checked = survey.s1908
                End If
                If survey.s1909 IsNot Nothing Then
                    cb_s1909.Checked = survey.s1909
                End If
                If survey.s1910 IsNot Nothing Then
                    cb_s1910.Checked = survey.s1910
                End If
                If survey.s1911 IsNot Nothing Then
                    cb_s1911.Checked = survey.s1911
                End If
                If survey.s1912 IsNot Nothing Then
                    cb_s1912.Checked = survey.s1912
                End If
                If survey.s1913 IsNot Nothing Then
                    cb_s1913.Checked = survey.s1913
                End If
                If survey.s1914 IsNot Nothing Then
                    cb_s1914.Checked = survey.s1914
                End If
                If survey.s1915 IsNot Nothing Then
                    cb_s1915.Checked = survey.s1915
                End If
                If survey.s1916 IsNot Nothing Then
                    cb_s1916.Checked = survey.s1916
                End If
                If survey.s1917 IsNot Nothing Then
                    cb_s1917.Checked = survey.s1917
                End If
                If survey.s1918 IsNot Nothing Then
                    cb_s1918.Checked = survey.s1918
                End If
                If survey.s1919 IsNot Nothing Then
                    cb_s1919.Checked = survey.s1919
                End If
                If survey.s1920 IsNot Nothing Then
                    cb_s1920.Checked = survey.s1920
                End If
                If survey.s1921 IsNot Nothing Then
                    cb_s1921.Checked = survey.s1921
                End If
                If survey.s1922 IsNot Nothing Then
                    cb_s1922.Checked = survey.s1922
                End If
                If survey.s1923 IsNot Nothing Then
                    cb_s1923.Checked = survey.s1923
                End If
                If survey.s1924 IsNot Nothing Then
                    cb_s1924.Checked = survey.s1924
                End If
                If survey.s1925 IsNot Nothing Then
                    cb_s1925.Checked = survey.s1925
                End If
                If survey.s1926 IsNot Nothing Then
                    cb_s1926.Checked = survey.s1926
                End If
                If survey.s1927 IsNot Nothing Then
                    cb_s1927.Checked = survey.s1927
                End If
                If survey.s1928 IsNot Nothing Then
                    cb_s1928.Checked = survey.s1928
                End If
                If survey.s1929 IsNot Nothing Then
                    cb_s1929.Checked = survey.s1929
                End If
                If survey.s1930 IsNot Nothing Then
                    cb_s1930.Checked = survey.s1930
                End If
                If survey.s1931 IsNot Nothing Then
                    cb_s1931.Checked = survey.s1931
                End If
                If survey.s1932 IsNot Nothing Then
                    cb_s1932.Checked = survey.s1932
                End If
                If survey.s1933 IsNot Nothing Then
                    cb_s1933.Checked = survey.s1933
                End If
                If survey.s1934 IsNot Nothing Then
                    cb_s1934.Checked = survey.s1934
                End If
                If survey.s1935 IsNot Nothing Then
                    cb_s1935.Checked = survey.s1935
                End If
                If survey.s1936 IsNot Nothing Then
                    cb_s1936.Checked = survey.s1936
                End If
                If survey.s1937 IsNot Nothing Then
                    cb_s1937.Checked = survey.s1937
                End If
                If survey.s1938 IsNot Nothing Then
                    cb_s1938.Checked = survey.s1938
                End If
                If survey.s1939 IsNot Nothing Then
                    cb_s1939.Checked = survey.s1939
                End If
                If survey.s1940 IsNot Nothing Then
                    cb_s1940.Checked = survey.s1940
                End If
                If survey.s1941 IsNot Nothing Then
                    cb_s1941.Checked = survey.s1941
                End If
                If survey.s1942 IsNot Nothing Then
                    cb_s1942.Checked = survey.s1942
                End If
                If survey.s1943 IsNot Nothing Then
                    cb_s1943.Checked = survey.s1943
                End If
                If survey.s1944 IsNot Nothing Then
                    cb_s1944.Checked = survey.s1944
                End If
                If survey.s1945 IsNot Nothing Then
                    cb_s1945.Checked = survey.s1945
                End If
                If survey.s1946 IsNot Nothing Then
                    cb_s1946.Checked = survey.s1946
                End If
                If survey.s1947 IsNot Nothing Then
                    cb_s1947.Checked = survey.s1947
                End If
                If survey.s1948 IsNot Nothing Then
                    cb_s1948.Checked = survey.s1948
                End If
                If survey.s1949 IsNot Nothing Then
                    cb_s1949.Checked = survey.s1949
                End If
                If survey.s1950 IsNot Nothing Then
                    cb_s1950.Checked = survey.s1950
                End If
                If survey.s12 IsNot Nothing Then
                    cb_s12.Checked = survey.s12
                End If
                If survey.s20 IsNot Nothing Then
                    cb_s20.Checked = survey.s20
                End If
                If survey.s28 IsNot Nothing Then
                    cb_s28.Checked = survey.s28
                End If
                If survey.s13 IsNot Nothing Then
                    cb_s13.Checked = survey.s13
                End If
                If survey.s21 IsNot Nothing Then
                    cb_s21.Checked = survey.s21
                End If
                If survey.s29 IsNot Nothing Then
                    cb_s29.Checked = survey.s29
                End If
                If survey.s14 IsNot Nothing Then
                    cb_s14.Checked = survey.s14
                End If
                If survey.s22 IsNot Nothing Then
                    cb_s22.Checked = survey.s22
                End If
                If survey.s30 IsNot Nothing Then
                    cb_s30.Checked = survey.s30
                End If
                If survey.s15 IsNot Nothing Then
                    cb_s15.Checked = survey.s15
                End If
                If survey.s23 IsNot Nothing Then
                    cb_s23.Checked = survey.s23
                End If
                If survey.s31 IsNot Nothing Then
                    cb_s31.Checked = survey.s31
                End If
                If survey.s16 IsNot Nothing Then
                    cb_s16.Checked = survey.s16
                End If
                If survey.s24 IsNot Nothing Then
                    cb_s24.Checked = survey.s24
                End If
                If survey.s32 IsNot Nothing Then
                    cb_s32.Checked = survey.s32
                End If
                If survey.s17 IsNot Nothing Then
                    cb_s17.Checked = survey.s17
                End If
                If survey.s25 IsNot Nothing Then
                    cb_s25.Checked = survey.s25
                End If
                If survey.s33 IsNot Nothing Then
                    cb_s33.Checked = survey.s33
                End If
                If survey.s18 IsNot Nothing Then
                    cb_s18.Checked = survey.s18
                End If
                If survey.s26 IsNot Nothing Then
                    cb_s26.Checked = survey.s26
                End If
                If survey.s34 IsNot Nothing Then
                    cb_s34.Checked = survey.s34
                End If
                If survey.s19 IsNot Nothing Then
                    cb_s19.Checked = survey.s19
                End If
                If survey.s27 IsNot Nothing Then
                    cb_s27.Checked = survey.s27
                End If
                If survey.s35 IsNot Nothing Then
                    cb_s35.Checked = survey.s35
                End If
                If survey.section1Complete IsNot Nothing Then
                    cb_section1Complete.Checked = survey.section1Complete
                End If

                If survey.s700 IsNot Nothing Then
                    If survey.s700 = 1 Then rb_s700_1.Checked = True
                End If
                If survey.s700 IsNot Nothing Then
                    If survey.s700 = 2 Then rb_s700_2.Checked = True
                End If
                If survey.s700 IsNot Nothing Then
                    If survey.s700 = 3 Then rb_s700_3.Checked = True
                End If
                If survey.s700 IsNot Nothing Then
                    If survey.s700 = 4 Then rb_s700_4.Checked = True
                End If
                If survey.s701 IsNot Nothing Then
                    If survey.s701 = 1 Then rb_s701_1.Checked = True
                End If
                If survey.s701 IsNot Nothing Then
                    If survey.s701 = 2 Then rb_s701_2.Checked = True
                End If
                If survey.s701 IsNot Nothing Then
                    If survey.s701 = 3 Then rb_s701_3.Checked = True
                End If
                If survey.s701 IsNot Nothing Then
                    If survey.s701 = 4 Then rb_s701_4.Checked = True
                End If
                If survey.s701 IsNot Nothing Then
                    If survey.s701 = 5 Then rb_s701_5.Checked = True
                End If
                If survey.s701 IsNot Nothing Then
                    If survey.s701 = 6 Then rb_s701_6.Checked = True
                End If
                If survey.s702 IsNot Nothing Then
                    If survey.s702 = 1 Then rb_s702_1.Checked = True
                End If
                If survey.s702 IsNot Nothing Then
                    If survey.s702 = 2 Then rb_s702_2.Checked = True
                End If
                If survey.s702 IsNot Nothing Then
                    If survey.s702 = 3 Then rb_s702_3.Checked = True
                End If
                If survey.s703 IsNot Nothing Then
                    If survey.s703 = 1 Then rb_s703_1.Checked = True
                End If
                If survey.s703 IsNot Nothing Then
                    If survey.s703 = 2 Then rb_s703_2.Checked = True
                End If
                If survey.s703 IsNot Nothing Then
                    If survey.s703 = 3 Then rb_s703_3.Checked = True
                End If
                If survey.s703 IsNot Nothing Then
                    If survey.s703 = 4 Then rb_s703_4.Checked = True
                End If
                If survey.s704 IsNot Nothing Then
                    If survey.s704 = 1 Then rb_s704_1.Checked = True
                End If
                If survey.s704 IsNot Nothing Then
                    If survey.s704 = 2 Then rb_s704_2.Checked = True
                End If
                If survey.s704 IsNot Nothing Then
                    If survey.s704 = 3 Then rb_s704_3.Checked = True
                End If
                If survey.s704 IsNot Nothing Then
                    If survey.s704 = 4 Then rb_s704_4.Checked = True
                End If
                If survey.s705 IsNot Nothing Then
                    If survey.s705 = 1 Then rb_s705_1.Checked = True
                End If
                If survey.s705 IsNot Nothing Then
                    If survey.s705 = 2 Then rb_s705_2.Checked = True
                End If
                If survey.s705 IsNot Nothing Then
                    If survey.s705 = 3 Then rb_s705_3.Checked = True
                End If
                If survey.s705 IsNot Nothing Then
                    If survey.s705 = 4 Then rb_s705_4.Checked = True
                End If
                If survey.s705 IsNot Nothing Then
                    If survey.s705 = 5 Then rb_s705_5.Checked = True
                End If
                If survey.s705 IsNot Nothing Then
                    If survey.s705 = 6 Then rb_s705_6.Checked = True
                End If
                If survey.s705 IsNot Nothing Then
                    If survey.s705 = 7 Then rb_s705_7.Checked = True
                End If
                If survey.s706 IsNot Nothing Then
                    If survey.s706 = 1 Then rb_s706_1.Checked = True
                End If
                If survey.s706 IsNot Nothing Then
                    If survey.s706 = 2 Then rb_s706_2.Checked = True
                End If
                If survey.s706 IsNot Nothing Then
                    If survey.s706 = 3 Then rb_s706_3.Checked = True
                End If
                If survey.s706 IsNot Nothing Then
                    If survey.s706 = 4 Then rb_s706_4.Checked = True
                End If
                If survey.s706 IsNot Nothing Then
                    If survey.s706 = 5 Then rb_s706_5.Checked = True
                End If
                If survey.s707 IsNot Nothing Then
                    If survey.s707 = 1 Then rb_s707_1.Checked = True
                End If
                If survey.s707 IsNot Nothing Then
                    If survey.s707 = 2 Then rb_s707_2.Checked = True
                End If
                If survey.s708 IsNot Nothing Then
                    If survey.s708 = 1 Then rb_s708_1.Checked = True
                End If
                If survey.s708 IsNot Nothing Then
                    If survey.s708 = 2 Then rb_s708_2.Checked = True
                End If
                If survey.s708 IsNot Nothing Then
                    If survey.s708 = 3 Then rb_s708_3.Checked = True
                End If
                If survey.s2 IsNot Nothing Then
                    If survey.s2 = 1 Then rb_s2_1.Checked = True
                End If
                If survey.s2 IsNot Nothing Then
                    If survey.s2 = 2 Then rb_s2_2.Checked = True
                End If
                If survey.s3 IsNot Nothing Then
                    If survey.s3 = 1 Then rb_s3_1.Checked = True
                End If
                If survey.s3 IsNot Nothing Then
                    If survey.s3 = 2 Then rb_s3_2.Checked = True
                End If
                If survey.s3 IsNot Nothing Then
                    If survey.s3 = 3 Then rb_s3_3.Checked = True
                End If
                If survey.s3 IsNot Nothing Then
                    If survey.s3 = 4 Then rb_s3_4.Checked = True
                End If
                If survey.s4 IsNot Nothing Then
                    If survey.s4 = 1 Then rb_s4_1.Checked = True
                End If
                If survey.s4 IsNot Nothing Then
                    If survey.s4 = 2 Then rb_s4_2.Checked = True
                End If
                If survey.s4 IsNot Nothing Then
                    If survey.s4 = 3 Then rb_s4_3.Checked = True
                End If
                If survey.s4 IsNot Nothing Then
                    If survey.s4 = 4 Then rb_s4_4.Checked = True
                End If
                If survey.s9 IsNot Nothing Then
                    If survey.s9 = 1 Then rb_s9_1.Checked = True
                End If
                If survey.s9 IsNot Nothing Then
                    If survey.s9 = 2 Then rb_s9_2.Checked = True
                End If
                If survey.s9 IsNot Nothing Then
                    If survey.s9 = 3 Then rb_s9_3.Checked = True
                End If
                If survey.s11 IsNot Nothing Then
                    If survey.s11 = 1 Then rb_s11_1.Checked = True
                End If
                If survey.s11 IsNot Nothing Then
                    If survey.s11 = 2 Then rb_s11_2.Checked = True
                End If
                If survey.s11 IsNot Nothing Then
                    If survey.s11 = 3 Then rb_s11_3.Checked = True
                End If
            Else
                PreLoadAssoc()
            End If
        End Using
    End Sub
    Sub SaveSurvey(ByVal IsSubmit As Boolean)
        Using db As New Models.IICICEntities
            Dim existingSurvey As Boolean = False
            Dim LocationID As Integer = txt_LocationID.Text
            Dim userid As Integer = Convert.ToInt16(Session("UserId"))
            Dim survey As New CompSurvey
            survey = (From s In db.CompSurveys Where s.LocationID = LocationID And s.SurveyYear = Now.Year).FirstOrDefault
            If survey Is Nothing Then
                survey = New CompSurvey
                db.CompSurveys.Add(survey)
                db.SaveChanges()
            End If
            survey.LocationID = LocationID
            survey.LastModUserId = userid
            survey.ModifiedDate = Now
            survey.SurveyYear = Now.Year
            If IsSubmit Then
                survey.SubmitDate = Now
            End If

            '---PASTE SAVE STATEMENTS---

            If Not String.IsNullOrEmpty(txt_s700oth.Text) Then
                survey.s700oth = txt_s700oth.Text.Replace(",", String.Empty)
            Else
                survey.s700oth = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s701oth.Text) Then
                survey.s701oth = txt_s701oth.Text.Replace(",", String.Empty)
            Else
                survey.s701oth = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s703oth.Text) Then
                survey.s703oth = txt_s703oth.Text.Replace(",", String.Empty)
            Else
                survey.s703oth = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s1.Text) Then
                survey.s1 = txt_s1.Text
            Else
                survey.s1 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s3oth.Text) Then
                survey.s3oth = txt_s3oth.Text.Replace(",", String.Empty)
            Else
                survey.s3oth = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s5.Text) Then
                survey.s5 = txt_s5.Text.Replace(",", String.Empty)
            Else
                survey.s5 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s522.Text) Then
                survey.s522 = txt_s522.Text.Replace(",", String.Empty)
            Else
                survey.s522 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s6.Text) Then
                survey.s6 = txt_s6.Text.Replace(",", String.Empty)
            Else
                survey.s6 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s7.Text) Then
                survey.s7 = txt_s7.Text.Replace(",", String.Empty)
            Else
                survey.s7 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s8.Text) Then
                survey.s8 = txt_s8.Text.Replace(",", String.Empty)
            Else
                survey.s8 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s10.Text) Then
                survey.s10 = txt_s10.Text.Replace(",", String.Empty)
            Else
                survey.s10 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s34oth.Text) Then
                survey.s34oth = txt_s34oth.Text.Replace(",", String.Empty)
            Else
                survey.s34oth = Nothing
            End If

            survey.s1900 = cb_s1900.Checked
            survey.s1901 = cb_s1901.Checked
            survey.s1902 = cb_s1902.Checked
            survey.s1903 = cb_s1903.Checked
            survey.s1904 = cb_s1904.Checked
            survey.s1905 = cb_s1905.Checked
            survey.s1906 = cb_s1906.Checked
            survey.s1907 = cb_s1907.Checked
            survey.s1908 = cb_s1908.Checked
            survey.s1909 = cb_s1909.Checked
            survey.s1910 = cb_s1910.Checked
            survey.s1911 = cb_s1911.Checked
            survey.s1912 = cb_s1912.Checked
            survey.s1913 = cb_s1913.Checked
            survey.s1914 = cb_s1914.Checked
            survey.s1915 = cb_s1915.Checked
            survey.s1916 = cb_s1916.Checked
            survey.s1917 = cb_s1917.Checked
            survey.s1918 = cb_s1918.Checked
            survey.s1919 = cb_s1919.Checked
            survey.s1920 = cb_s1920.Checked
            survey.s1921 = cb_s1921.Checked
            survey.s1922 = cb_s1922.Checked
            survey.s1923 = cb_s1923.Checked
            survey.s1924 = cb_s1924.Checked
            survey.s1925 = cb_s1925.Checked
            survey.s1926 = cb_s1926.Checked
            survey.s1927 = cb_s1927.Checked
            survey.s1928 = cb_s1928.Checked
            survey.s1929 = cb_s1929.Checked
            survey.s1930 = cb_s1930.Checked
            survey.s1931 = cb_s1931.Checked
            survey.s1932 = cb_s1932.Checked
            survey.s1933 = cb_s1933.Checked
            survey.s1934 = cb_s1934.Checked
            survey.s1935 = cb_s1935.Checked
            survey.s1936 = cb_s1936.Checked
            survey.s1937 = cb_s1937.Checked
            survey.s1938 = cb_s1938.Checked
            survey.s1939 = cb_s1939.Checked
            survey.s1940 = cb_s1940.Checked
            survey.s1941 = cb_s1941.Checked
            survey.s1942 = cb_s1942.Checked
            survey.s1943 = cb_s1943.Checked
            survey.s1944 = cb_s1944.Checked
            survey.s1945 = cb_s1945.Checked
            survey.s1946 = cb_s1946.Checked
            survey.s1947 = cb_s1947.Checked
            survey.s1948 = cb_s1948.Checked
            survey.s1949 = cb_s1949.Checked
            survey.s1950 = cb_s1950.Checked
            survey.s12 = cb_s12.Checked
            survey.s20 = cb_s20.Checked
            survey.s28 = cb_s28.Checked
            survey.s13 = cb_s13.Checked
            survey.s21 = cb_s21.Checked
            survey.s29 = cb_s29.Checked
            survey.s14 = cb_s14.Checked
            survey.s22 = cb_s22.Checked
            survey.s30 = cb_s30.Checked
            survey.s15 = cb_s15.Checked
            survey.s23 = cb_s23.Checked
            survey.s31 = cb_s31.Checked
            survey.s16 = cb_s16.Checked
            survey.s24 = cb_s24.Checked
            survey.s32 = cb_s32.Checked
            survey.s17 = cb_s17.Checked
            survey.s25 = cb_s25.Checked
            survey.s33 = cb_s33.Checked
            survey.s18 = cb_s18.Checked
            survey.s26 = cb_s26.Checked
            survey.s34 = cb_s34.Checked
            survey.s19 = cb_s19.Checked
            survey.s27 = cb_s27.Checked
            survey.s35 = cb_s35.Checked
            survey.section1Complete = cb_section1Complete.Checked

            If rb_s700_1.Checked Then survey.s700 = 1
            If rb_s700_2.Checked Then survey.s700 = 2
            If rb_s700_3.Checked Then survey.s700 = 3
            If rb_s700_4.Checked Then survey.s700 = 4
            If rb_s701_1.Checked Then survey.s701 = 1
            If rb_s701_2.Checked Then survey.s701 = 2
            If rb_s701_3.Checked Then survey.s701 = 3
            If rb_s701_4.Checked Then survey.s701 = 4
            If rb_s701_5.Checked Then survey.s701 = 5
            If rb_s701_6.Checked Then survey.s701 = 6
            If rb_s702_1.Checked Then survey.s702 = 1
            If rb_s702_2.Checked Then survey.s702 = 2
            If rb_s702_3.Checked Then survey.s702 = 3
            If rb_s703_1.Checked Then survey.s703 = 1
            If rb_s703_2.Checked Then survey.s703 = 2
            If rb_s703_3.Checked Then survey.s703 = 3
            If rb_s703_4.Checked Then survey.s703 = 4
            If rb_s704_1.Checked Then survey.s704 = 1
            If rb_s704_2.Checked Then survey.s704 = 2
            If rb_s704_3.Checked Then survey.s704 = 3
            If rb_s704_4.Checked Then survey.s704 = 4
            If rb_s705_1.Checked Then survey.s705 = 1
            If rb_s705_2.Checked Then survey.s705 = 2
            If rb_s705_3.Checked Then survey.s705 = 3
            If rb_s705_4.Checked Then survey.s705 = 4
            If rb_s705_5.Checked Then survey.s705 = 5
            If rb_s705_6.Checked Then survey.s705 = 6
            If rb_s705_7.Checked Then survey.s705 = 7
            If rb_s706_1.Checked Then survey.s706 = 1
            If rb_s706_2.Checked Then survey.s706 = 2
            If rb_s706_3.Checked Then survey.s706 = 3
            If rb_s706_4.Checked Then survey.s706 = 4
            If rb_s706_5.Checked Then survey.s706 = 5
            If rb_s707_1.Checked Then survey.s707 = 1
            If rb_s707_2.Checked Then survey.s707 = 2
            If rb_s708_1.Checked Then survey.s708 = 1
            If rb_s708_2.Checked Then survey.s708 = 2
            If rb_s708_3.Checked Then survey.s708 = 3
            If rb_s2_1.Checked Then survey.s2 = 1
            If rb_s2_2.Checked Then survey.s2 = 2
            If rb_s3_1.Checked Then survey.s3 = 1
            If rb_s3_2.Checked Then survey.s3 = 2
            If rb_s3_3.Checked Then survey.s3 = 3
            If rb_s3_4.Checked Then survey.s3 = 4
            If rb_s4_1.Checked Then survey.s4 = 1
            If rb_s4_2.Checked Then survey.s4 = 2
            If rb_s4_3.Checked Then survey.s4 = 3
            If rb_s4_4.Checked Then survey.s4 = 4
            If rb_s9_1.Checked Then survey.s9 = 1
            If rb_s9_2.Checked Then survey.s9 = 2
            If rb_s9_3.Checked Then survey.s9 = 3
            If rb_s11_1.Checked Then survey.s11 = 1
            If rb_s11_2.Checked Then survey.s11 = 2
            If rb_s11_3.Checked Then survey.s11 = 3






            survey.IsAuditComplete = False
            db.SaveChanges()
            lbl_lastSave.Text = survey.ModifiedDate
        End Using
    End Sub

    Sub PreLoadAssoc()
        Dim client As String = String.Empty
        If Session("Client") IsNot Nothing Then
            client = Session("Client")
            If Not String.IsNullOrEmpty(client) Then
                Select Case client
                    Case "naed"
                        cb_s1911.Checked = True
                    Case "ifda"
                        cb_s1908.Checked = True
                    Case "aed"
                        cb_s1900.Checked = True
                    Case "asa"
                        cb_s1901.Checked = True
                    Case "fpda"
                        cb_s1904.Checked = True
                    Case "nahad"
                        cb_s1913.Checked = True
                    Case "wffsa"
                        cb_s1922.Checked = True
                    Case "nbmda"
                        cb_s1916.Checked = True
                    Case "hardi"
                        cb_s1905.Checked = True
                    Case "ctda"
                        cb_s1903.Checked = True
                    Case "mheda"
                        cb_s1910.Checked = True
                    Case "pei"
                        cb_s1919.Checked = True
                    Case "stafda"
                        cb_s1921.Checked = True
                    Case "cda"
                        cb_s1902.Checked = True
                    Case "iapd"
                        cb_s1907.Checked = True
                    Case "npta"
                        cb_s1918.Checked = True
                    Case "nawla"
                        cb_s1915.Checked = True
                    Case "naw"
                        cb_s1914.Checked = True
                    Case "isa"
                        cb_s1909.Checked = True
                    Case "hda"
                        cb_s1906.Checked = True
                    Case "pida"
                        cb_s1920.Checked = True
                    Case "nbwa"
                        cb_s1917.Checked = True
                End Select
            End If
        End If
    End Sub
    Protected Sub btn_previous_Click(sender As Object, e As EventArgs) Handles btn_previous.Click
        SaveSurvey(False)
        Response.Redirect("TableOfContents.aspx")
    End Sub
    Protected Sub btn_save_Click(sender As Object, e As EventArgs) Handles btn_save.Click
        SaveSurvey(False)
    End Sub
    Protected Sub btn_next_Click(sender As Object, e As EventArgs) Handles btn_next.Click
        SaveSurvey(False)
        Response.Redirect("Recruiting.aspx")
    End Sub
    Protected Sub ddl_location_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddl_location.SelectedIndexChanged
        SaveSurvey(False)
        Session("LocationID") = ddl_location.SelectedValue
        txt_LocationID.Text = ddl_location.SelectedValue
        Response.Redirect("Profile.aspx")
    End Sub
End Class
