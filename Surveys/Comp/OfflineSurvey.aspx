﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Survey.master" AutoEventWireup="false" CodeFile="OfflineSurvey.aspx.vb" Inherits="Surveys_Financial_OfflineSurvey" %>

<%@ Register Assembly="DevExpress.Web.v18.1, Version=18.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script type="text/javascript">
        function OnFileUploadComplete(s, e) {
            $('#ContentPlaceHolder1_lbl_uploadSuccess').show();
        }
    </script>
    <div class="bg-content">
        <div class="container">
            <div class="row">
                <h3>Upload</h3>
                <hr class="hr-dark" />
                <p>Once you have completed your survey, select your file and click the "Upload" link.</p>

                <div class="">
                    <dx:ASPxUploadControl ID="ASPxUploadControl1" runat="server" ShowUploadButton="True" OnFileUploadComplete="Upload_FileUploadComplete" NullText="Select a file." ShowProgressPanel="True">
                        <ValidationSettings AllowedFileExtensions=".pdf,.xlsx,.xls">
                        </ValidationSettings>
                        <FileSystemSettings UploadFolder="" />
                        <ClientSideEvents FileUploadComplete="OnFileUploadComplete" />
                    </dx:ASPxUploadControl>

                    <p>
                        <asp:Label ID="lbl_uploadSuccess" runat="server" CssClass="text-success collapse" Text="Your file has been uploaded."></asp:Label>
                    </p>


                    <p>&nbsp;</p>
                    <p><!--Summary of uploads load here-->
                        <asp:table ID="SummaryTBL" runat="server" CssClass="table-bordered table-condensed" Visible="false">
                            <asp:TableHeaderRow>
                                <asp:TableHeaderCell CssClass="text-left header" ColumnSpan="2">Previously Uploaded Files... <a href="OfflineSurvey.aspx"><span class="glyphicon glyphicon-refresh"></span><small>Refresh List</small></a></asp:TableHeaderCell>
                            </asp:TableHeaderRow>
                            <asp:TableHeaderRow CssClass="bg-primary">
                                <asp:TableHeaderCell CssClass="text-center header">Upload Date</asp:TableHeaderCell>
                                <asp:TableHeaderCell CssClass="text-center header">File Name</asp:TableHeaderCell>
                            </asp:TableHeaderRow>
                        </asp:table>
                    </p>

                </div>
            </div>
        </div>
    </div>
</asp:Content>

