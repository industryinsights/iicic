﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Survey.master"  MaintainScrollPositionOnPostback="true" AutoEventWireup="false" CodeFile="Recruiting.aspx.vb" Inherits="Surveys_Recruiting" %>

<%@ Register Src="~/Controls/SurveyNavigation.ascx" TagPrefix="uc1" TagName="SurveyNavigation" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script type="text/javascript">
        $(document).ready(function () {
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
    <script src="../../Scripts/autoNumeric/autoNumeric-min.js"></script>
    <script src="../../Scripts/Recruiting.js?v=1"></script>
    <asp:TextBox ID="txt_LocationID" runat="server" Visible="false"></asp:TextBox>
    <asp:TextBox ID="txt_userid" runat="server" Visible="false"></asp:TextBox>
    <div class="bg-content">
        <div class="row">
            <table class="table-condensed centered" runat="server" id="tbl_location">
                <tr>
                    <td>
                        <asp:DropDownList ID="ddl_location" runat="server" CssClass="form-control" AutoPostBack="true"></asp:DropDownList>
                    </td>
                    <td><span class="glyphicon glyphicon-info-sign icon-info" data-placement="left" data-toggle="tooltip" title="This box is disabled by default.  If you are submitting for multiple locations, please add the locations on the Organization Admin page and you'll be able to cycle through them here."></span></td>
                </tr>
            </table>

        </div>

        <div class="">
            <h3 class="text-center">Deadline: 
                <asp:Label ID="lbl_deadline" runat="server" Text=""></asp:Label></h3>
        </div>
        <div class="">
            <span class="col-md-12 text-center text-muted"><em>Last updated: 
            <asp:Label ID="lbl_lastSave" runat="server" Text=""></asp:Label></em></span>
        </div>

        <h2 class="page-header">Recruiting & Retention</h2>

        <p>&nbsp;</p>
        <div class="row tr-question">
            <div class="col-md-12">1. Employee turnover rates</div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <table class="table-condensed table-inner-borders">
                    <tr>
                        <td>&nbsp;</td>
                        <td class="text-center"><strong>2017 Total<br />
                            Turnover Rate*</strong></td>
                        <td class="text-center"><strong>2017<br />
                            Quit Rate**</strong></td>
                    </tr>
                    <tr>
                        <td class="text-right">Executive/senior management positions</td>
                        <td>
                            <span class="input-group">
                                <asp:TextBox ID="txt_s36" runat="server" CssClass="form-control text-right"></asp:TextBox>
                                <span class="input-group-addon">%</span>
                            </span>
                        </td>
                        <td>
                            <span class="input-group">
                                <asp:TextBox ID="txt_s39" runat="server" CssClass="form-control text-right"></asp:TextBox>
                                <span class="input-group-addon">%</span>
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td class="text-right">Management positions</td>
                        <td>
                            <span class="input-group">
                                <asp:TextBox ID="txt_s37" runat="server" CssClass="form-control text-right"></asp:TextBox>
                                <span class="input-group-addon">%</span>
                            </span>
                        </td>
                        <td>
                            <span class="input-group">
                                <asp:TextBox ID="txt_s40" runat="server" CssClass="form-control text-right"></asp:TextBox>
                                <span class="input-group-addon">%</span>
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td class="text-right">Non-management positions</td>
                        <td>
                            <span class="input-group">
                                <asp:TextBox ID="txt_s38" runat="server" CssClass="form-control text-right"></asp:TextBox>
                                <span class="input-group-addon">%</span>
                            </span>
                        </td>
                        <td>
                            <span class="input-group">
                                <asp:TextBox ID="txt_s41" runat="server" CssClass="form-control text-right"></asp:TextBox>
                                <span class="input-group-addon">%</span>
                            </span>
                        </td>
                    </tr>
                </table>
                <p>
                    <span class="sub-text">*Total # of separations in 2017 &divide; average # of employees for 2017</span>
                    <br />
                    <span class="sub-text">** Total # of quits (voluntary separations) in 2017 &divide; average # of employees for 2017</span>
                </p>
                <p>
                    (Total separations includes quits, layoffs and discharges, and other separations.)</p>
            </div>
        </div>
        <hr />

        <div class="row tr-question">
            <div class="col-md-12">2. Average time to fill these positions (in days):</div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <table class="table-condensed table-inner-borders">
                    <tr>
                        <td class="text-right">Executive/senior management positions</td>
                        <td>
                            <span class="input-group">
                                <asp:TextBox ID="txt_s42" runat="server" CssClass="form-control text-right"></asp:TextBox>
                                <span class="input-group-addon">days</span>
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td class="text-right">Management positions</td>
                        <td>
                            <span class="input-group">
                                <asp:TextBox ID="txt_s43" runat="server" CssClass="form-control text-right"></asp:TextBox>
                                <span class="input-group-addon">days</span>
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td class="text-right">Non-management positions</td>
                        <td>
                            <span class="input-group">
                                <asp:TextBox ID="txt_s44" runat="server" CssClass="form-control text-right"></asp:TextBox>
                                <span class="input-group-addon">days</span>
                            </span>
                        </td>
                    </tr>
                </table>

            </div>
        </div>
        <hr />

        <div class="row tr-question">
            <div class="col-md-12">3. What benefits/compensation is your company using to attract and retain employees? (Check all that apply)</div>
        </div>
        <div class="row col-md-12">
            <table class="table-condensed">
                <tr>
                    <td>
                        <asp:CheckBox ID="cb_s52" runat="server" Text="Retention bonus:" CssClass="checkbox-inline" />
                    </td>
                    <td>
                        <table class="table-condensed">
                            <tr>
                                <td>Retention bonus as an average percentage of the base salary?&nbsp; </td>
                                <td>
                                    <span class="input-group">
                                        <asp:TextBox ID="txt_s53" runat="server" CssClass="form-control text-right"></asp:TextBox>
                                        <span class="input-group-addon">%</span> </span>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:CheckBox ID="cb_s54" runat="server" Text="Signing bonus:" CssClass="checkbox-inline" />
                    </td>
                    <td>
                        <table class="table-condensed">
                            <tr>
                                <td>Signing bonus as an average percentage of the base salary?</td>
                                <td><span class="input-group">
                                    <asp:TextBox ID="txt_s55" runat="server" CssClass="form-control text-right"></asp:TextBox>
                                    <span class="input-group-addon">%</span>
                                </span>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>


                <tr>
                    <td colspan="2"></td>
                </tr>
                <tr>
                    <td class=" col-md-3">
                        <asp:CheckBox ID="cb_s45" runat="server" Text="Above market salaries" CssClass="checkbox-inline" />
                    </td>
                    <td class=" ">
                        <asp:CheckBox ID="cb_s46" runat="server" Text="Added medical benefits" CssClass="checkbox-inline" />
                    </td>
                </tr>
                <tr>
                    <td class=" ">
                        <asp:CheckBox ID="cb_s47" runat="server" Text="Added vacation days/PTO" CssClass="checkbox-inline" />
                    </td>
                    <td class=" ">
                        <asp:CheckBox ID="cb_s524" runat="server" Text="Childcare services/reimbursement" CssClass="checkbox-inline" />
                    </td>
                </tr>
                <tr>
                    <td class=" ">
                        <asp:CheckBox ID="cb_s48" runat="server" Text="Flexible work schedule" CssClass="checkbox-inline" />
                    </td>
                    <td class=" ">
                        <asp:CheckBox ID="cb_s56" runat="server" Text="Stock options" CssClass="checkbox-inline" />
                    </td>
                </tr>
                <tr>
                    <td class=" ">
                        <asp:CheckBox ID="cb_s523" runat="server" Text="Telecommuting" CssClass="checkbox-inline" />
                    </td>
                    <td class=" ">
                        <asp:CheckBox ID="cb_s51" runat="server" Text="Profit sharing" CssClass="checkbox-inline" />

                    </td>
                </tr>
                <tr>
                    <td class=" ">
                        <asp:CheckBox ID="cb_s49" runat="server" Text="Incentive compensation" CssClass="checkbox-inline" />
                    </td>
                    <td class=" ">
                        <table>
                            <tr>
                                <td>
                                    <asp:CheckBox ID="cb_s57" runat="server" Text="Other" CssClass="checkbox-inline" /></td>
                                <td>
                                    <asp:TextBox ID="txt_s57oth" runat="server" CssClass="form-control"></asp:TextBox></td>
                            </tr>
                        </table>

                    </td>
                </tr>
                <tr>
                    <td class=" ">
                        <asp:CheckBox ID="cb_s50" runat="server" Text="Mentoring program" CssClass="checkbox-inline" />
                    </td>
                    <td class=" ">
                        <asp:CheckBox ID="cb_s58" runat="server" Text="We are doing nothing to attract or retain employees" CssClass="checkbox-inline" />
                    </td>
                </tr>
            </table>
            <br />
        </div>
        <div class="row">
            <hr />
        </div>
        
        <div class="row tr-question">
            <div class="col-md-12">3b. If telecommuting is available, do you have a formal telecommuting (work from home) policy?</div>
        </div>
        <div class="row">
            <div class="col-md-8">
                <asp:RadioButton CssClass="radio-inline" ID="rb_s525_1" runat="server" GroupName="s525" Text="Yes" />
            </div>
        </div>
        <div class="row">
            <div class="col-md-8">
                <asp:RadioButton CssClass="radio-inline" ID="rb_s525_2" runat="server" GroupName="s525" Text="No" />
            </div>
        </div>
        <hr />



        <div class="row tr-question">
            <div class="col-md-12">4. Base salary adjustments made in 2017 and anticipated for 2018:</div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <table class="table-condensed table-inner-borders">
                    <tr>
                        <td>&nbsp;</td>
                        <td class="text-center"><strong>2017</strong></td>
                        <td class="text-center"><strong>Anticipated 2018</strong></td>
                    </tr>
                    <tr>
                        <td class="text-right">Executive employees</td>
                        <td>
                            <span class="input-group"><span class="input-group">
                                <asp:TextBox ID="txt_s59" runat="server" CssClass="form-control text-right"></asp:TextBox>
                                <span class="input-group-addon">%</span>
                            </span></span>
                        </td>
                        <td>
                            <span class="input-group"><span class="input-group">
                                <asp:TextBox ID="txt_s63" runat="server" CssClass="form-control text-right"></asp:TextBox>
                                <span class="input-group-addon">%</span>
                            </span></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="text-right">Full-time employees (manager)</td>
                        <td>
                            <span class="input-group"><span class="input-group">
                                <asp:TextBox ID="txt_s60" runat="server" CssClass="form-control text-right"></asp:TextBox>
                                <span class="input-group-addon">%</span>
                            </span></span>
                        </td>
                        <td>
                            <span class="input-group"><span class="input-group">
                                <asp:TextBox ID="txt_s64" runat="server" CssClass="form-control text-right"></asp:TextBox>
                                <span class="input-group-addon">%</span>
                            </span></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="text-right">Full-time employees (non-manager)</td>
                        <td>
                            <span class="input-group"><span class="input-group">
                                <asp:TextBox ID="txt_s61" runat="server" CssClass="form-control text-right"></asp:TextBox>
                                <span class="input-group-addon">%</span>
                            </span></span>
                        </td>
                        <td>
                            <span class="input-group"><span class="input-group">
                                <asp:TextBox ID="txt_s65" runat="server" CssClass="form-control text-right"></asp:TextBox>
                                <span class="input-group-addon">%</span>
                            </span></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="text-right">Part-time employees</td>
                        <td>
                            <span class="input-group"><span class="input-group">
                                <asp:TextBox ID="txt_s62" runat="server" CssClass="form-control text-right"></asp:TextBox>
                                <span class="input-group-addon">%</span>
                            </span></span>
                        </td>
                        <td>
                            <span class="input-group"><span class="input-group">
                                <asp:TextBox ID="txt_s66" runat="server" CssClass="form-control text-right"></asp:TextBox>
                                <span class="input-group-addon">%</span>
                            </span></span>
                        </td>
                    </tr>
                </table>

            </div>
        </div>
        <hr />

        <div class="row tr-question">
            <div class="col-md-12">5. Frequency of salary review and adjustments? (Check all that apply)</div>
        </div>
        <div class="row">
            <div class="col-md-8">
                <asp:CheckBox CssClass="checkbox-inline" ID="cb_s526" runat="server" Text="Annual" />
            </div>
        </div>
        <div class="row">
            <div class="col-md-8">
                <asp:CheckBox CssClass="checkbox-inline" ID="cb_s527" runat="server" Text="Semiannual" />
            </div>
        </div>
        <div class="row">
            <div class="col-md-8">
                <asp:CheckBox CssClass="checkbox-inline" ID="cb_s528" runat="server" Text="Quarterly" />
            </div>
        </div>
        <div class="row">
            <div class="col-md-8">
                <table>
                    <tr>
                        <td>
                            <asp:CheckBox CssClass="checkbox-inline" ID="cb_s529" runat="server" Text="Other" /></td>
                        <td>
                            <asp:TextBox ID="txt_s529oth" runat="server" CssClass="form-control"></asp:TextBox></td>
                    </tr>
                </table>
            </div>
        </div>
        <hr />


        <table class="table-condensed centered">
            <tr>
                <td class="text-center" colspan="3">
                    <asp:CheckBox ID="cb_section2Complete" runat="server" Text="This section is complete." />
                </td>
            </tr>
            <tr>
                <td class="text-center">
                    <asp:Button ID="btn_previous" runat="server" CssClass="btn btn-primary" Text="Previous Section" Width="141px" />
                </td>
                <td class="text-center">
                    <asp:Button ID="btn_save" runat="server" CssClass="btn btn-primary" Text="Save" Width="141px" />
                </td>
                <td class="text-center">
                    <asp:Button ID="btn_next" runat="server" CssClass="btn btn-primary" Text="Next Section" Width="141px" />
                </td>
            </tr>
            <tr>
                <td class="text-center">&nbsp;</td>
                <td class="text-center">
                    <asp:LinkButton ID="lb_toc_bot" runat="server" CssClass="text-bold">Home</asp:LinkButton>
                </td>
                <td class="text-center">&nbsp;</td>
            </tr>
        </table>
    </div>
</asp:Content>

