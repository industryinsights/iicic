﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Survey.master"  MaintainScrollPositionOnPostback="true" AutoEventWireup="false" CodeFile="Compensation.aspx.vb" Inherits="Surveys_Compensation" %>

<%@ Register Src="~/Controls/SurveyNavigation.ascx" TagPrefix="uc1" TagName="SurveyNavigation" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script type="text/javascript">
        $(document).ready(function () {
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
    <script src="../../Scripts/autoNumeric/autoNumeric-min.js"></script>
    <script src="../../Scripts/Compensation.js?v=1"></script>
    <asp:TextBox ID="txt_LocationID" runat="server" Visible="false"></asp:TextBox>
    <asp:TextBox ID="txt_userid" runat="server" Visible="false"></asp:TextBox>
    <div class="bg-content">
        <div class="row">
            <table class="table-condensed centered" id="tbl_location" runat="server">
                <tr>
                    <td>
                        <asp:DropDownList ID="ddl_location" runat="server" CssClass="form-control" AutoPostBack="true"></asp:DropDownList>
                    </td>
                    <td><span class="glyphicon glyphicon-info-sign icon-info" data-placement="left" data-toggle="tooltip" title="This box is disabled by default.  If you are submitting for multiple locations, please add the locations on the Organization Admin page and you'll be able to cycle through them here."></span></td>
                </tr>
            </table>

        </div>
        <div class="">
            <h3 class="text-center">Deadline: 
                <asp:Label ID="lbl_deadline" runat="server" Text=""></asp:Label></h3>
        </div>
        <div class="">
            <span class="col-md-12 text-center text-muted"><em>Last updated: 
            <asp:Label ID="lbl_lastSave" runat="server" Text=""></asp:Label></em></span>
        </div>


        <h2 class="page-header">Executive Compensation and Benefits</h2>

        <h3 class="h3-header">2017 Executive Compensation</h3>
        <ul class="sub-text">
            <li>Report each top executive in the most appropriate position.  If an executive has multiple roles, use the single most applicable position.  Do not report individuals employed less than one year.</li>
            <li>Please focus on the job function rather than the title level; the top executive in your company may have a different title, such as Vice President or Director. Refer to the enclosed position description as a guide for employee functions and classifications.&nbsp; If you do not employ a person for the particular position listed, please leave blank and skip to the next position. NOTE: If the same person fulfills more than one of the following functions, place the person under their primary function and leave secondary function(s) blank.  Please enter all dollar compensation figures as full figures, rather than abbreviations (e.g., “85,000” not “85K”)</li>
            <li>Canadian firms, please report in Canadian Dollars.</li>
        </ul>


        <table class="table-condensed">
            <tr>
                <td><a href="../../Resources/JobDescriptions.pdf" target="_blank">
                    <img src="../../Content/images/icon-pdf.png" /></a></td>
                <td><a href="../../Resources/JobDescriptions.pdf" target="_blank">Job Descriptions</a></td>
            </tr>
        </table>


        <table class="table table-condensed table-hover table-survey centered">
            <thead>
                <tr class="td-bg-primary">
                    <th>&nbsp;</th>
                    <th class="text-center">Chief Executive Officer/President</th>
                    <th class="text-center">Chief Operating Officer/Executive Vice President/General Manager</th>
                    <th class="text-center">Chief Financial Officer/Top Financial Officer</th>
                    <th class="text-center">Chief Marketing Officer/Top Sales or Marketing Officer</th>
                    <th class="text-center">Chief Technology Officer/Chief Information Officer</th>
                </tr>
            </thead>

            <tbody>

                <tr>
                    <td class="col-md-2">Years in industry</td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">#</span>
                            <asp:TextBox ID="txt_s67" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">#</span>
                            <asp:TextBox ID="txt_s90" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">#</span>
                            <asp:TextBox ID="txt_s113" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">#</span>
                            <asp:TextBox ID="txt_s136" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">#</span>
                            <asp:TextBox ID="txt_s159" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                </tr>
                <tr>
                    <td class="col-md-2">Years with the company</td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">#</span>
                            <asp:TextBox ID="txt_s68" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">#</span>
                            <asp:TextBox ID="txt_s91" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">#</span>
                            <asp:TextBox ID="txt_s114" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">#</span>
                            <asp:TextBox ID="txt_s137" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">#</span>
                            <asp:TextBox ID="txt_s160" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                </tr>
                <tr>
                    <td class="col-md-2">Highest level of education </td>
                    <td class="col-md-2">
                        <asp:DropDownList ID="drop_s69" runat="server" CssClass="form-control" AutoPostBack="False">
                            <asp:ListItem Value="">Select one</asp:ListItem>
                            <asp:ListItem Value="1">High school</asp:ListItem>
                            <asp:ListItem Value="2">Some college</asp:ListItem>
                            <asp:ListItem Value="3">Bachelor’s degree</asp:ListItem>
                            <asp:ListItem Value="4">Master’s degree</asp:ListItem>
                            <asp:ListItem Value="5">Ph.D</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td class="col-md-2">
                        <asp:DropDownList ID="drop_s92" runat="server" CssClass="form-control" AutoPostBack="False">
                            <asp:ListItem Value="">Select one</asp:ListItem>
                            <asp:ListItem Value="1">High school</asp:ListItem>
                            <asp:ListItem Value="2">Some college</asp:ListItem>
                            <asp:ListItem Value="3">Bachelor’s degree</asp:ListItem>
                            <asp:ListItem Value="4">Master’s degree</asp:ListItem>
                            <asp:ListItem Value="5">Ph.D</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td class="col-md-2">
                        <asp:DropDownList ID="drop_s115" runat="server" CssClass="form-control" AutoPostBack="False">
                            <asp:ListItem Value="">Select one</asp:ListItem>
                            <asp:ListItem Value="1">High school</asp:ListItem>
                            <asp:ListItem Value="2">Some college</asp:ListItem>
                            <asp:ListItem Value="3">Bachelor’s degree</asp:ListItem>
                            <asp:ListItem Value="4">Master’s degree</asp:ListItem>
                            <asp:ListItem Value="5">Ph.D</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td class="col-md-2">
                        <asp:DropDownList ID="drop_s138" runat="server" CssClass="form-control" AutoPostBack="False">
                            <asp:ListItem Value="">Select one</asp:ListItem>
                            <asp:ListItem Value="1">High school</asp:ListItem>
                            <asp:ListItem Value="2">Some college</asp:ListItem>
                            <asp:ListItem Value="3">Bachelor’s degree</asp:ListItem>
                            <asp:ListItem Value="4">Master’s degree</asp:ListItem>
                            <asp:ListItem Value="5">Ph.D</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td class="col-md-2">
                        <asp:DropDownList ID="drop_s161" runat="server" CssClass="form-control" AutoPostBack="False">
                            <asp:ListItem Value="">Select one</asp:ListItem>
                            <asp:ListItem Value="1">High school</asp:ListItem>
                            <asp:ListItem Value="2">Some college</asp:ListItem>
                            <asp:ListItem Value="3">Bachelor’s degree</asp:ListItem>
                            <asp:ListItem Value="4">Master’s degree</asp:ListItem>
                            <asp:ListItem Value="5">Ph.D</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td class="col-md-2">Percent of equity owned</td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">%</span>
                            <asp:TextBox ID="txt_s70" runat="server" CssClass="form-control text-right"></asp:TextBox>

                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">%</span>
                            <asp:TextBox ID="txt_s93" runat="server" CssClass="form-control text-right"></asp:TextBox>

                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">%</span>
                            <asp:TextBox ID="txt_s116" runat="server" CssClass="form-control text-right"></asp:TextBox>

                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">%</span>
                            <asp:TextBox ID="txt_s139" runat="server" CssClass="form-control text-right"></asp:TextBox>

                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">%</span>
                            <asp:TextBox ID="txt_s162" runat="server" CssClass="form-control text-right"></asp:TextBox>

                        </span>
                    </td>
                </tr>
                <tr>
                    <td class="col-md-2">Base salary</td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s71" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s94" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s117" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s140" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s163" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                </tr>
                <tr>
                    <td class="col-md-2">Bonus/incentives/commissions <small>(enter 0 if none)</small></td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s72" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s95" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s118" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s141" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s164" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                </tr>
                <tr>
                    <td class="col-md-2">Total Compensation</td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s73" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s96" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s119" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s142" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s165" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                </tr>
                <tr>
                    <td class="col-md-2">Change in total compensation <small>(2016 to 2017)</small></td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">%</span><asp:TextBox ID="txt_s74" runat="server" CssClass="form-control text-right"></asp:TextBox>

                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">%</span><asp:TextBox ID="txt_s97" runat="server" CssClass="form-control text-right"></asp:TextBox>

                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">%</span><asp:TextBox ID="txt_s120" runat="server" CssClass="form-control text-right"></asp:TextBox>

                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">%</span><asp:TextBox ID="txt_s143" runat="server" CssClass="form-control text-right"></asp:TextBox>

                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">%</span><asp:TextBox ID="txt_s166" runat="server" CssClass="form-control text-right"></asp:TextBox>

                        </span>
                    </td>
                </tr>
            </tbody>
        </table>

        <h3 class="h3-header">
            Executive Benefits/Perquisites in Addition to Standard Company Offerings (Please check all benefits/perquisites that apply to each position)
        </h3>

        <table class="table table-condensed table-hover table-survey centered">
            <thead>
                <tr class="td-bg-primary">
                    <th>&nbsp;</th>
                    <th class="text-center">CEO</th>
                    <th class="text-center">COO</th>
                    <th class="text-center">CFO</th>
                    <th class="text-center">CMO</th>
                    <th class="text-center">CTO</th>
                </tr>
            </thead>

            <tbody>
                <tr class="warning">
                    <td class="col-md-2">Not eligible for additional benefits/perquisites</td>
                    <td class="col-md-2 text-center" style="vertical-align: text-top;">
                        <asp:CheckBox ID="cb_s75" runat="server" Text="" CssClass="checkbox-inline" />
                    </td>
                    <td class="col-md-2 text-center" style="vertical-align: text-top;">
                        <asp:CheckBox ID="cb_s98" runat="server" Text="" CssClass="checkbox-inline" />
                    </td>
                    <td class="col-md-2 text-center" style="vertical-align: text-top;">
                        <asp:CheckBox ID="cb_s121" runat="server" Text="" CssClass="checkbox-inline" />
                    </td>
                    <td class="col-md-2 text-center" style="vertical-align: text-top;">
                        <asp:CheckBox ID="cb_s144" runat="server" Text="" CssClass="checkbox-inline" />
                    </td>
                    <td class="col-md-2 text-center">
                        <asp:CheckBox ID="cb_s167" runat="server" Text="" CssClass="checkbox-inline" />
                    </td>
                </tr>
                <tr>
                    <td class="col-md-2">Annual physical exam</td>
                    <td class="col-md-2 text-center" style="vertical-align: text-top;">
                        <asp:CheckBox ID="cb_s530" runat="server" Text="" CssClass="checkbox-inline" />
                    </td>
                    <td class="col-md-2 text-center" style="vertical-align: text-top;">
                        <asp:CheckBox ID="cb_s531" runat="server" Text="" CssClass="checkbox-inline" />
                    </td>
                    <td class="col-md-2 text-center" style="vertical-align: text-top;">
                        <asp:CheckBox ID="cb_s532" runat="server" Text="" CssClass="checkbox-inline" />
                    </td>
                    <td class="col-md-2 text-center" style="vertical-align: text-top;">
                        <asp:CheckBox ID="cb_s533" runat="server" Text="" CssClass="checkbox-inline" />
                    </td>
                    <td class="col-md-2 text-center">
                        <asp:CheckBox ID="cb_s534" runat="server" Text="" CssClass="checkbox-inline" />
                    </td>
                </tr>
                <tr>
                    <td class="col-md-2">Cell phone</td>
                    <td class="col-md-2 text-center" style="vertical-align: text-top;">
                        <asp:CheckBox ID="cb_s76" runat="server" Text="" CssClass="checkbox-inline" />
                    </td>
                    <td class="col-md-2 text-center" style="vertical-align: text-top;">
                        <asp:CheckBox ID="cb_s99" runat="server" Text="" CssClass="checkbox-inline" />
                    </td>
                    <td class="col-md-2 text-center" style="vertical-align: text-top;">
                        <asp:CheckBox ID="cb_s122" runat="server" Text="" CssClass="checkbox-inline" />
                    </td>
                    <td class="col-md-2 text-center" style="vertical-align: text-top;">
                        <asp:CheckBox ID="cb_s145" runat="server" Text="" CssClass="checkbox-inline" />
                    </td>
                    <td class="col-md-2 text-center">
                        <asp:CheckBox ID="cb_s168" runat="server" Text="" CssClass="checkbox-inline" />
                    </td>
                </tr>
                <tr>
                    <td class="col-md-2">Club memberships</td>
                    <td class="col-md-2 text-center" style="vertical-align: text-top;">
                        <asp:CheckBox ID="cb_s77" runat="server" Text="" CssClass="checkbox-inline" />
                    </td>
                    <td class="col-md-2 text-center" style="vertical-align: text-top;">
                        <asp:CheckBox ID="cb_s100" runat="server" Text="" CssClass="checkbox-inline" />
                    </td>
                    <td class="col-md-2 text-center" style="vertical-align: text-top;">
                        <asp:CheckBox ID="cb_s123" runat="server" Text="" CssClass="checkbox-inline" />
                    </td>
                    <td class="col-md-2 text-center" style="vertical-align: text-top;">
                        <asp:CheckBox ID="cb_s146" runat="server" Text="" CssClass="checkbox-inline" />
                    </td>
                    <td class="col-md-2 text-center">
                        <asp:CheckBox ID="cb_s169" runat="server" Text="" CssClass="checkbox-inline" />
                    </td>
                </tr>
                <tr>
                    <td class="col-md-2">Company car & expenses</td>
                    <td class="col-md-2 text-center" style="vertical-align: text-top;">
                        <asp:CheckBox ID="cb_s78" runat="server" Text="" CssClass="checkbox-inline" />
                    </td>
                    <td class="col-md-2 text-center" style="vertical-align: text-top;">
                        <asp:CheckBox ID="cb_s101" runat="server" Text="" CssClass="checkbox-inline" />
                    </td>
                    <td class="col-md-2 text-center" style="vertical-align: text-top;">
                        <asp:CheckBox ID="cb_s124" runat="server" Text="" CssClass="checkbox-inline" />
                    </td>
                    <td class="col-md-2 text-center" style="vertical-align: text-top;">
                        <asp:CheckBox ID="cb_s147" runat="server" Text="" CssClass="checkbox-inline" />
                    </td>
                    <td class="col-md-2 text-center">
                        <asp:CheckBox ID="cb_s170" runat="server" Text="" CssClass="checkbox-inline" />
                    </td>
                </tr>
                <tr>
                    <td class="col-md-2">Deferred compensation</td>
                    <td class="col-md-2 text-center" style="vertical-align: text-top;">
                        <asp:CheckBox ID="cb_s79" runat="server" Text="" CssClass="checkbox-inline" />
                    </td>
                    <td class="col-md-2 text-center" style="vertical-align: text-top;">
                        <asp:CheckBox ID="cb_s102" runat="server" Text="" CssClass="checkbox-inline" />
                    </td>
                    <td class="col-md-2 text-center" style="vertical-align: text-top;">
                        <asp:CheckBox ID="cb_s125" runat="server" Text="" CssClass="checkbox-inline" />
                    </td>
                    <td class="col-md-2 text-center" style="vertical-align: text-top;">
                        <asp:CheckBox ID="cb_s148" runat="server" Text="" CssClass="checkbox-inline" />
                    </td>
                    <td class="col-md-2 text-center">
                        <asp:CheckBox ID="cb_s171" runat="server" Text="" CssClass="checkbox-inline" />
                    </td>
                </tr>
                <tr>
                    <td class="col-md-2">First class air travel</td>
                    <td class="col-md-2 text-center" style="vertical-align: text-top;">
                        <asp:CheckBox ID="cb_s80" runat="server" Text="" CssClass="checkbox-inline" />
                    </td>
                    <td class="col-md-2 text-center" style="vertical-align: text-top;">
                        <asp:CheckBox ID="cb_s103" runat="server" Text="" CssClass="checkbox-inline" />
                    </td>
                    <td class="col-md-2 text-center" style="vertical-align: text-top;">
                        <asp:CheckBox ID="cb_s126" runat="server" Text="" CssClass="checkbox-inline" />
                    </td>
                    <td class="col-md-2 text-center" style="vertical-align: text-top;">
                        <asp:CheckBox ID="cb_s149" runat="server" Text="" CssClass="checkbox-inline" />
                    </td>
                    <td class="col-md-2 text-center">
                        <asp:CheckBox ID="cb_s172" runat="server" Text="" CssClass="checkbox-inline" />
                    </td>
                </tr>
                <tr>
                    <td class="col-md-2">Low or no-interest loans</td>
                    <td class="col-md-2 text-center" style="vertical-align: text-top;">
                        <asp:CheckBox ID="cb_s81" runat="server" Text="" CssClass="checkbox-inline" />
                    </td>
                    <td class="col-md-2 text-center" style="vertical-align: text-top;">
                        <asp:CheckBox ID="cb_s104" runat="server" Text="" CssClass="checkbox-inline" />
                    </td>
                    <td class="col-md-2 text-center" style="vertical-align: text-top;">
                        <asp:CheckBox ID="cb_s127" runat="server" Text="" CssClass="checkbox-inline" />
                    </td>
                    <td class="col-md-2 text-center" style="vertical-align: text-top;">
                        <asp:CheckBox ID="cb_s150" runat="server" Text="" CssClass="checkbox-inline" />
                    </td>
                    <td class="col-md-2 text-center">
                        <asp:CheckBox ID="cb_s173" runat="server" Text="" CssClass="checkbox-inline" />
                    </td>
                </tr>
                <tr>
                    <td class="col-md-2">Matching gifts/charity</td>
                    <td class="col-md-2 text-center" style="vertical-align: text-top;">
                        <asp:CheckBox ID="cb_s82" runat="server" Text="" CssClass="checkbox-inline" />
                    </td>
                    <td class="col-md-2 text-center" style="vertical-align: text-top;">
                        <asp:CheckBox ID="cb_s105" runat="server" Text="" CssClass="checkbox-inline" />
                    </td>
                    <td class="col-md-2 text-center" style="vertical-align: text-top;">
                        <asp:CheckBox ID="cb_s128" runat="server" Text="" CssClass="checkbox-inline" />
                    </td>
                    <td class="col-md-2 text-center" style="vertical-align: text-top;">
                        <asp:CheckBox ID="cb_s151" runat="server" Text="" CssClass="checkbox-inline" />
                    </td>
                    <td class="col-md-2 text-center">
                        <asp:CheckBox ID="cb_s174" runat="server" Text="" CssClass="checkbox-inline" />
                    </td>
                </tr>
                <tr>
                    <td class="col-md-2">Personal financial planning</td>
                    <td class="col-md-2 text-center" style="vertical-align: text-top;">
                        <asp:CheckBox ID="cb_s83" runat="server" Text="" CssClass="checkbox-inline" />
                    </td>
                    <td class="col-md-2 text-center" style="vertical-align: text-top;">
                        <asp:CheckBox ID="cb_s106" runat="server" Text="" CssClass="checkbox-inline" />
                    </td>
                    <td class="col-md-2 text-center" style="vertical-align: text-top;">
                        <asp:CheckBox ID="cb_s129" runat="server" Text="" CssClass="checkbox-inline" />
                    </td>
                    <td class="col-md-2 text-center" style="vertical-align: text-top;">
                        <asp:CheckBox ID="cb_s152" runat="server" Text="" CssClass="checkbox-inline" />
                    </td>
                    <td class="col-md-2 text-center">
                        <asp:CheckBox ID="cb_s175" runat="server" Text="" CssClass="checkbox-inline" />
                    </td>
                </tr>
                <tr>
                    <td class="col-md-2">Tax Return preparation</td>
                    <td class="col-md-2 text-center" style="vertical-align: text-top;">
                        <asp:CheckBox ID="cb_s535" runat="server" Text="" CssClass="checkbox-inline" />
                    </td>
                    <td class="col-md-2 text-center" style="vertical-align: text-top;">
                        <asp:CheckBox ID="cb_s536" runat="server" Text="" CssClass="checkbox-inline" />
                    </td>
                    <td class="col-md-2 text-center" style="vertical-align: text-top;">
                        <asp:CheckBox ID="cb_s537" runat="server" Text="" CssClass="checkbox-inline" />
                    </td>
                    <td class="col-md-2 text-center" style="vertical-align: text-top;">
                        <asp:CheckBox ID="cb_s538" runat="server" Text="" CssClass="checkbox-inline" />
                    </td>
                    <td class="col-md-2 text-center">
                        <asp:CheckBox ID="cb_s539" runat="server" Text="" CssClass="checkbox-inline" />
                    </td>
                </tr>
                <tr>
                    <td class="col-md-2">Personal legal services</td>
                    <td class="col-md-2 text-center" style="vertical-align: text-top;">
                        <asp:CheckBox ID="cb_s84" runat="server" Text="" CssClass="checkbox-inline" />
                    </td>
                    <td class="col-md-2 text-center" style="vertical-align: text-top;">
                        <asp:CheckBox ID="cb_s107" runat="server" Text="" CssClass="checkbox-inline" />
                    </td>
                    <td class="col-md-2 text-center" style="vertical-align: text-top;">
                        <asp:CheckBox ID="cb_s130" runat="server" Text="" CssClass="checkbox-inline" />
                    </td>
                    <td class="col-md-2 text-center" style="vertical-align: text-top;">
                        <asp:CheckBox ID="cb_s153" runat="server" Text="" CssClass="checkbox-inline" />
                    </td>
                    <td class="col-md-2 text-center">
                        <asp:CheckBox ID="cb_s176" runat="server" Text="" CssClass="checkbox-inline" />
                    </td>
                </tr>
                <tr>
                    <td class="col-md-2">Stock options</td>
                    <td class="col-md-2 text-center" style="vertical-align: text-top;">
                        <asp:CheckBox ID="cb_s85" runat="server" Text="" CssClass="checkbox-inline" />
                    </td>
                    <td class="col-md-2 text-center" style="vertical-align: text-top;">
                        <asp:CheckBox ID="cb_s108" runat="server" Text="" CssClass="checkbox-inline" />
                    </td>
                    <td class="col-md-2 text-center" style="vertical-align: text-top;">
                        <asp:CheckBox ID="cb_s131" runat="server" Text="" CssClass="checkbox-inline" />
                    </td>
                    <td class="col-md-2 text-center" style="vertical-align: text-top;">
                        <asp:CheckBox ID="cb_s154" runat="server" Text="" CssClass="checkbox-inline" />
                    </td>
                    <td class="col-md-2 text-center">
                        <asp:CheckBox ID="cb_s177" runat="server" Text="" CssClass="checkbox-inline" />
                    </td>
                </tr>
                <tr>
                    <td class="col-md-2">Supplemental life insurance*</td>
                    <td class="col-md-2 text-center" style="vertical-align: text-top;">
                        <asp:CheckBox ID="cb_s86" runat="server" Text="" CssClass="checkbox-inline" />
                    </td>
                    <td class="col-md-2 text-center" style="vertical-align: text-top;">
                        <asp:CheckBox ID="cb_s109" runat="server" Text="" CssClass="checkbox-inline" />
                    </td>
                    <td class="col-md-2 text-center" style="vertical-align: text-top;">
                        <asp:CheckBox ID="cb_s132" runat="server" Text="" CssClass="checkbox-inline" />
                    </td>
                    <td class="col-md-2 text-center" style="vertical-align: text-top;">
                        <asp:CheckBox ID="cb_s155" runat="server" Text="" CssClass="checkbox-inline" />
                    </td>
                    <td class="col-md-2 text-center">
                        <asp:CheckBox ID="cb_s178" runat="server" Text="" CssClass="checkbox-inline" />
                    </td>
                </tr>
                <tr>
                    <td class="col-md-2">Supplemental medical insurance*</td>
                    <td class="col-md-2 text-center" style="vertical-align: text-top;">
                        <asp:CheckBox ID="cb_s87" runat="server" Text="" CssClass="checkbox-inline" />
                    </td>
                    <td class="col-md-2 text-center" style="vertical-align: text-top;">
                        <asp:CheckBox ID="cb_s110" runat="server" Text="" CssClass="checkbox-inline" />
                    </td>
                    <td class="col-md-2 text-center" style="vertical-align: text-top;">
                        <asp:CheckBox ID="cb_s133" runat="server" Text="" CssClass="checkbox-inline" />
                    </td>
                    <td class="col-md-2 text-center" style="vertical-align: text-top;">
                        <asp:CheckBox ID="cb_s156" runat="server" Text="" CssClass="checkbox-inline" />
                    </td>
                    <td class="col-md-2 text-center">
                        <asp:CheckBox ID="cb_s179" runat="server" Text="" CssClass="checkbox-inline" />
                    </td>
                </tr>
                <tr>
                    <td class="col-md-2">Supplemental retirement plans*</td>
                    <td class="col-md-2 text-center" style="vertical-align: text-top;">
                        <asp:CheckBox ID="cb_s88" runat="server" Text="" CssClass="checkbox-inline" />
                    </td>
                    <td class="col-md-2 text-center" style="vertical-align: text-top;">
                        <asp:CheckBox ID="cb_s111" runat="server" Text="" CssClass="checkbox-inline" />
                    </td>
                    <td class="col-md-2 text-center" style="vertical-align: text-top;">
                        <asp:CheckBox ID="cb_s134" runat="server" Text="" CssClass="checkbox-inline" />
                    </td>
                    <td class="col-md-2 text-center" style="vertical-align: text-top;">
                        <asp:CheckBox ID="cb_s157" runat="server" Text="" CssClass="checkbox-inline" />
                    </td>
                    <td class="col-md-2 text-center">
                        <asp:CheckBox ID="cb_s180" runat="server" Text="" CssClass="checkbox-inline" />
                    </td>
                </tr>
                <tr>
                    <td class="col-md-2">Use of corporate aircraft</td>
                    <td class="col-md-2 text-center" style="vertical-align: text-top;">
                        <asp:CheckBox ID="cb_s89" runat="server" Text="" CssClass="checkbox-inline" />
                    </td>
                    <td class="col-md-2 text-center" style="vertical-align: text-top;">
                        <asp:CheckBox ID="cb_s112" runat="server" Text="" CssClass="checkbox-inline" />
                    </td>
                    <td class="col-md-2 text-center" style="vertical-align: text-top;">
                        <asp:CheckBox ID="cb_s135" runat="server" Text="" CssClass="checkbox-inline" />
                    </td>
                    <td class="col-md-2 text-center" style="vertical-align: text-top;">
                        <asp:CheckBox ID="cb_s158" runat="server" Text="" CssClass="checkbox-inline" />
                    </td>
                    <td class="col-md-2 text-center">
                        <asp:CheckBox ID="cb_s181" runat="server" Text="" CssClass="checkbox-inline" />
                    </td>
                </tr>
                <tr>
                    <td colspan="6">
                        <p class="small">*Beyond customary company-wide benefits</p>
                    </td>
                </tr>
            </tbody>
        </table>
        <p>&nbsp;</p>


        <div id="NAWdiv" runat="server">
            <table class="table table-condensed table-hover table-survey centered">
                <thead>
                    <tr class="td-bg-primary">
                        <th class="col-md-8">&nbsp;</th>
                        <th class="text-center col-md-2">Chief Human Resources Officer</th>
                        <th class="text-center col-md-2">Chief Legal Officer</th>
                    </tr>
                </thead>

                <tbody>
                    <tr>
                        <td class="col-md-2">Years in industry</td>
                        <td class="col-md-2">
                            <span class="input-group">
                                <span class="input-group-addon">#</span>
                                <asp:TextBox ID="txt_s601" runat="server" CssClass="form-control text-right"></asp:TextBox>
                            </span>
                        </td>
                        <td class="col-md-2">
                            <span class="input-group">
                                <span class="input-group-addon">#</span>
                                <asp:TextBox ID="txt_s626" runat="server" CssClass="form-control text-right"></asp:TextBox>
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td class="col-md-2">Years with the company</td>
                        <td class="col-md-2">
                            <span class="input-group">
                                <span class="input-group-addon">#</span>
                                <asp:TextBox ID="txt_s602" runat="server" CssClass="form-control text-right"></asp:TextBox>
                            </span>
                        </td>
                        <td class="col-md-2">
                            <span class="input-group">
                                <span class="input-group-addon">#</span>
                                <asp:TextBox ID="txt_s627" runat="server" CssClass="form-control text-right"></asp:TextBox>
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td class="col-md-2">Highest level of education </td>
                        <td class="col-md-2">
                            <asp:DropDownList ID="drop_s603" runat="server" CssClass="form-control" AutoPostBack="False">
                                <asp:ListItem Value="">Select one</asp:ListItem>
                                <asp:ListItem Value="1">High school</asp:ListItem>
                                <asp:ListItem Value="2">Some college</asp:ListItem>
                                <asp:ListItem Value="3">Bachelor’s degree</asp:ListItem>
                                <asp:ListItem Value="4">Master’s degree</asp:ListItem>
                                <asp:ListItem Value="5">Ph.D</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td class="col-md-2">
                            <asp:DropDownList ID="drop_s628" runat="server" CssClass="form-control" AutoPostBack="False">
                                <asp:ListItem Value="">Select one</asp:ListItem>
                                <asp:ListItem Value="1">High school</asp:ListItem>
                                <asp:ListItem Value="2">Some college</asp:ListItem>
                                <asp:ListItem Value="3">Bachelor’s degree</asp:ListItem>
                                <asp:ListItem Value="4">Master’s degree</asp:ListItem>
                                <asp:ListItem Value="5">Ph.D</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="col-md-2">Percent of equity owned</td>
                        <td class="col-md-2">
                            <span class="input-group">
                                <span class="input-group-addon">%</span><asp:TextBox ID="txt_s604" runat="server" CssClass="form-control text-right"></asp:TextBox>

                            </span>
                        </td>
                        <td class="col-md-2">
                            <span class="input-group">
                                <span class="input-group-addon">%</span><asp:TextBox ID="txt_s629" runat="server" CssClass="form-control text-right"></asp:TextBox>

                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td class="col-md-2">Base salary</td>
                        <td class="col-md-2">
                            <span class="input-group">
                                <span class="input-group-addon">$</span>
                                <asp:TextBox ID="txt_s605" runat="server" CssClass="form-control text-right"></asp:TextBox>
                            </span>
                        </td>
                        <td class="col-md-2">
                            <span class="input-group">
                                <span class="input-group-addon">$</span>
                                <asp:TextBox ID="txt_s630" runat="server" CssClass="form-control text-right"></asp:TextBox>
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td class="col-md-2">Bonus/incentives/commissions <small>(enter 0 if none)</small></td>
                        <td class="col-md-2">
                            <span class="input-group">
                                <span class="input-group-addon">$</span>
                                <asp:TextBox ID="txt_s606" runat="server" CssClass="form-control text-right"></asp:TextBox>
                            </span>
                        </td>
                        <td class="col-md-2">
                            <span class="input-group">
                                <span class="input-group-addon">$</span>
                                <asp:TextBox ID="txt_s631" runat="server" CssClass="form-control text-right"></asp:TextBox>
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td class="col-md-2">Total Compensation</td>
                        <td class="col-md-2">
                            <span class="input-group">
                                <span class="input-group-addon">$</span>
                                <asp:TextBox ID="txt_s607" runat="server" CssClass="form-control text-right"></asp:TextBox>
                            </span>
                        </td>
                        <td class="col-md-2">
                            <span class="input-group">
                                <span class="input-group-addon">$</span>
                                <asp:TextBox ID="txt_s632" runat="server" CssClass="form-control text-right"></asp:TextBox>
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td class="col-md-2">Change in total compensation <small>(2016 to 2017)</small></td>
                        <td class="col-md-2">
                            <span class="input-group">
                                <span class="input-group-addon">%</span><asp:TextBox ID="txt_s608" runat="server" CssClass="form-control text-right"></asp:TextBox>

                            </span>
                        </td>
                        <td class="col-md-2">
                            <span class="input-group">
                                <span class="input-group-addon">%</span><asp:TextBox ID="txt_s633" runat="server" CssClass="form-control text-right"></asp:TextBox>

                            </span>
                        </td>
                    </tr>
                </tbody>
            </table>
            <h3 class="h3-header">
                Executive Benefits/Perquisites in Addition to Standard Company Offerings (Please check all benefits/perquisites that apply to each position)
            </h3>
            <table class="table table-condensed table-hover table-survey centered">
                <thead>
                    <tr class="td-bg-primary">
                        <th class="col-md-8">&nbsp;</th>
                        <th class="text-center">CHRO</th>
                        <th class="text-center">CLO</th>
                    </tr>
                </thead>

                <tbody>
                    <tr class="warning">
                        <td class="col-md-2">Not eligible for additional benefits/perquisites</td>
                        <td class="col-md-2 text-center" style="vertical-align: text-top;">
                            <asp:CheckBox ID="cb_s609" runat="server" Text="" CssClass="checkbox-inline" />
                        </td>
                        <td class="col-md-2 text-center" style="vertical-align: text-top;">
                            <asp:CheckBox ID="cb_s634" runat="server" Text="" CssClass="checkbox-inline" />
                        </td>
                    </tr>
                    <tr>
                        <td class="col-md-2">Annual physical exam</td>
                        <td class="col-md-2 text-center" style="vertical-align: text-top;">
                            <asp:CheckBox ID="cb_s610" runat="server" Text="" CssClass="checkbox-inline" />
                        </td>
                        <td class="col-md-2 text-center" style="vertical-align: text-top;">
                            <asp:CheckBox ID="cb_s635" runat="server" Text="" CssClass="checkbox-inline" />
                        </td>
                    </tr>
                    <tr>
                        <td class="col-md-2">Cell phone</td>
                        <td class="col-md-2 text-center" style="vertical-align: text-top;">
                            <asp:CheckBox ID="cb_s611" runat="server" Text="" CssClass="checkbox-inline" />
                        </td>
                        <td class="col-md-2 text-center" style="vertical-align: text-top;">
                            <asp:CheckBox ID="cb_s636" runat="server" Text="" CssClass="checkbox-inline" />
                        </td>
                    </tr>
                    <tr>
                        <td class="col-md-2">Club memberships</td>
                        <td class="col-md-2 text-center" style="vertical-align: text-top;">
                            <asp:CheckBox ID="cb_s612" runat="server" Text="" CssClass="checkbox-inline" />
                        </td>
                        <td class="col-md-2 text-center" style="vertical-align: text-top;">
                            <asp:CheckBox ID="cb_s637" runat="server" Text="" CssClass="checkbox-inline" />
                        </td>
                    </tr>
                    <tr>
                        <td class="col-md-2">Company car & expenses</td>
                        <td class="col-md-2 text-center" style="vertical-align: text-top;">
                            <asp:CheckBox ID="cb_s613" runat="server" Text="" CssClass="checkbox-inline" />
                        </td>
                        <td class="col-md-2 text-center" style="vertical-align: text-top;">
                            <asp:CheckBox ID="cb_s638" runat="server" Text="" CssClass="checkbox-inline" />
                        </td>
                    </tr>
                    <tr>
                        <td class="col-md-2">Deferred compensation</td>
                        <td class="col-md-2 text-center" style="vertical-align: text-top;">
                            <asp:CheckBox ID="cb_s614" runat="server" Text="" CssClass="checkbox-inline" />
                        </td>
                        <td class="col-md-2 text-center" style="vertical-align: text-top;">
                            <asp:CheckBox ID="cb_s639" runat="server" Text="" CssClass="checkbox-inline" />
                        </td>
                    </tr>
                    <tr>
                        <td class="col-md-2">First class air travel</td>
                        <td class="col-md-2 text-center" style="vertical-align: text-top;">
                            <asp:CheckBox ID="cb_s615" runat="server" Text="" CssClass="checkbox-inline" />
                        </td>
                        <td class="col-md-2 text-center" style="vertical-align: text-top;">
                            <asp:CheckBox ID="cb_s640" runat="server" Text="" CssClass="checkbox-inline" />
                        </td>
                    </tr>
                    <tr>
                        <td class="col-md-2">Low or no-interest loans</td>
                        <td class="col-md-2 text-center" style="vertical-align: text-top;">
                            <asp:CheckBox ID="cb_s616" runat="server" Text="" CssClass="checkbox-inline" />
                        </td>
                        <td class="col-md-2 text-center" style="vertical-align: text-top;">
                            <asp:CheckBox ID="cb_s641" runat="server" Text="" CssClass="checkbox-inline" />
                        </td>
                    </tr>
                    <tr>
                        <td class="col-md-2">Matching gifts/charity</td>
                        <td class="col-md-2 text-center" style="vertical-align: text-top;">
                            <asp:CheckBox ID="cb_s617" runat="server" Text="" CssClass="checkbox-inline" />
                        </td>
                        <td class="col-md-2 text-center" style="vertical-align: text-top;">
                            <asp:CheckBox ID="cb_s642" runat="server" Text="" CssClass="checkbox-inline" />
                        </td>
                    </tr>
                    <tr>
                        <td class="col-md-2">Personal financial planning</td>
                        <td class="col-md-2 text-center" style="vertical-align: text-top;">
                            <asp:CheckBox ID="cb_s618" runat="server" Text="" CssClass="checkbox-inline" />
                        </td>
                        <td class="col-md-2 text-center" style="vertical-align: text-top;">
                            <asp:CheckBox ID="cb_s643" runat="server" Text="" CssClass="checkbox-inline" />
                        </td>
                    </tr>
                    <tr>
                        <td class="col-md-2">Tax Return preparation</td>
                        <td class="col-md-2 text-center" style="vertical-align: text-top;">
                            <asp:CheckBox ID="cb_s619" runat="server" Text="" CssClass="checkbox-inline" />
                        </td>
                        <td class="col-md-2 text-center" style="vertical-align: text-top;">
                            <asp:CheckBox ID="cb_s644" runat="server" Text="" CssClass="checkbox-inline" />
                        </td>
                    </tr>
                    <tr>
                        <td class="col-md-2">Personal legal services</td>
                        <td class="col-md-2 text-center" style="vertical-align: text-top;">
                            <asp:CheckBox ID="cb_s620" runat="server" Text="" CssClass="checkbox-inline" />
                        </td>
                        <td class="col-md-2 text-center" style="vertical-align: text-top;">
                            <asp:CheckBox ID="cb_s645" runat="server" Text="" CssClass="checkbox-inline" />
                        </td>
                    </tr>
                    <tr>
                        <td class="col-md-2">Stock options</td>
                        <td class="col-md-2 text-center" style="vertical-align: text-top;">
                            <asp:CheckBox ID="cb_s621" runat="server" Text="" CssClass="checkbox-inline" />
                        </td>
                        <td class="col-md-2 text-center" style="vertical-align: text-top;">
                            <asp:CheckBox ID="cb_s646" runat="server" Text="" CssClass="checkbox-inline" />
                        </td>
                    </tr>
                    <tr>
                        <td class="col-md-2">Supplemental life insurance*</td>
                        <td class="col-md-2 text-center" style="vertical-align: text-top;">
                            <asp:CheckBox ID="cb_s622" runat="server" Text="" CssClass="checkbox-inline" />
                        </td>
                        <td class="col-md-2 text-center" style="vertical-align: text-top;">
                            <asp:CheckBox ID="cb_s647" runat="server" Text="" CssClass="checkbox-inline" />
                        </td>
                    </tr>
                    <tr>
                        <td class="col-md-2">Supplemental medical insurance*</td>
                        <td class="col-md-2 text-center" style="vertical-align: text-top;">
                            <asp:CheckBox ID="cb_s623" runat="server" Text="" CssClass="checkbox-inline" />
                        </td>
                        <td class="col-md-2 text-center" style="vertical-align: text-top;">
                            <asp:CheckBox ID="cb_s648" runat="server" Text="" CssClass="checkbox-inline" />
                        </td>
                    </tr>
                    <tr>
                        <td class="col-md-2">Supplemental retirement plans*</td>
                        <td class="col-md-2 text-center" style="vertical-align: text-top;">
                            <asp:CheckBox ID="cb_s624" runat="server" Text="" CssClass="checkbox-inline" />
                        </td>
                        <td class="col-md-2 text-center" style="vertical-align: text-top;">
                            <asp:CheckBox ID="cb_s649" runat="server" Text="" CssClass="checkbox-inline" />
                        </td>
                    </tr>
                    <tr>
                        <td class="col-md-2">Use of corporate aircraft</td>
                        <td class="col-md-2 text-center" style="vertical-align: text-top;">
                            <asp:CheckBox ID="cb_s625" runat="server" Text="" CssClass="checkbox-inline" />
                        </td>
                        <td class="col-md-2 text-center" style="vertical-align: text-top;">
                            <asp:CheckBox ID="cb_s650" runat="server" Text="" CssClass="checkbox-inline" />
                        </td>
                    </tr>
                    <tr>
                        <td class="col-md-2">* Beyond customary company-wide benefits.</td>
                        <td class="col-md-2 text-center" style="vertical-align: text-top;">&nbsp;</td>
                        <td class="col-md-2 text-center" style="vertical-align: text-top;">&nbsp;</td>
                    </tr>
                </tbody>
            </table>
        </div>

        <table class="table-condensed">
            <tr class="tr-question">
                <td>&nbsp;</td>
                <td colspan="3">Is an executive bonus program used?</td>
            </tr>
            <tr>
                <td></td>
                <td colspan="3">
                    <table class="table-condensed">
                        <tr>
                            <td>
                                <asp:RadioButton ID="rb_s540_1" runat="server" GroupName="s540" Text="Yes" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:RadioButton ID="rb_s540_2" runat="server" GroupName="s540" Text="No" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr class="tr-question">
                <td colspan="4">&nbsp;</td>
            </tr>
            <tr class="tr-question">
                <td>&nbsp;</td>
                <td colspan="3">If yes, which of the following are used as a basis to allocate executive bonuses? (check all that apply)</td>
            </tr>
            <tr>
                <td></td>
                <td colspan="3">
                    <table class="table-condensed">
                        <tr>
                            <td>
                                <asp:CheckBox ID="cb_s541" runat="server" Text="Discretionary" CssClass="checkbox-inline" />
                            </td>
                            <td>
                                <asp:CheckBox ID="cb_s543" runat="server" Text="Achievement of sales or operating goals" CssClass="checkbox-inline" />
                            </td>
                            <td>
                                <asp:CheckBox ID="cb_s545" runat="server" Text="Achievement of profit goals" CssClass="checkbox-inline" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:CheckBox ID="cb_s542" runat="server" Text="Percentage of sales" CssClass="checkbox-inline" />
                            </td>
                            <td>
                                <asp:CheckBox ID="cb_s544" runat="server" Text="Percentage of profit" CssClass="checkbox-inline" />
                            </td>
                            <td>
                                <asp:CheckBox ID="cb_s546" runat="server" Text="Return on equity/assets/sales" CssClass="checkbox-inline" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <hr />
        <p>&nbsp;</p>

        <table class="table-condensed centered">
            <tr>
                <td class="text-center" colspan="3">
                    <asp:CheckBox ID="cb_section3Complete" runat="server" Text="This section is complete." />
                </td>
            </tr>
            <tr>
                <td class="text-center">
                    <asp:Button ID="btn_previous" runat="server" CssClass="btn btn-primary" Text="Previous Section" Width="141px" />
                </td>
                <td class="text-center">
                    <asp:Button ID="btn_save" runat="server" CssClass="btn btn-primary" Text="Save" Width="141px" />
                </td>
                <td class="text-center">
                    <asp:Button ID="btn_next" runat="server" CssClass="btn btn-primary" Text="Next Section" Width="141px" />
                </td>
            </tr>
            <tr>
                <td class="text-center">&nbsp;</td>
                <td class="text-center">
                    <asp:LinkButton ID="lb_toc_bot" runat="server" CssClass="text-bold">Home</asp:LinkButton>
                </td>
                <td class="text-center">&nbsp;</td>
            </tr>
        </table>
    </div>
</asp:Content>

