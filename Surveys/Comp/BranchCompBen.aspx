﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Survey.master" AutoEventWireup="false"  MaintainScrollPositionOnPostback="true" CodeFile="BranchCompBen.aspx.vb" Inherits="Surveys_BranchCompBen" %>

<%@ Register Src="~/Controls/SurveyNavigation.ascx" TagPrefix="uc1" TagName="SurveyNavigation" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script type="text/javascript">
        $(document).ready(function () {
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
    <script src="../../Scripts/autoNumeric/autoNumeric-min.js"></script>
    <script src="../../Scripts/BranchCompBen.js?v=1"></script>
        <asp:TextBox ID="txt_LocationID" runat="server" Visible="false"></asp:TextBox>
    <asp:TextBox ID="txt_userid" runat="server" Visible="false"></asp:TextBox>
    <div class="bg-content">
                <div class="row">
            <table class="table-condensed centered" runat="server" id="tbl_location">
                <tr>
                    <td>
                        <asp:DropDownList ID="ddl_location" runat="server" CssClass="form-control" AutoPostBack="true"></asp:DropDownList>
                    </td>
                    <td>
                        <button type="button" class="btn btn-primary btn-md" data-toggle="modal" data-target="#userModal">Add Location</button>
                    </td>
                </tr>
            </table>
                    
        </div>
            
        <div class=""><h3 class="text-center">Deadline:  <asp:Label ID="lbl_deadline" runat="server" Text=""></asp:Label></h3></div>
        <div class=""><span class="col-md-12 text-center text-muted"><em>Last updated:  <asp:Label ID="lbl_lastSave" runat="server" Text=""></asp:Label></em></span></div>

        <h2 class="page-header">Branch/Location-Specific Compensation &amp; Benefits</h2>

        <p><strong>Complete this section, even if your company has only 1 location (the headquarters)</strong><br />Complete the column for the primary location then a column for each additional location.</p>

        <p><strong>Annual Compensation</strong> -- Report compensation for <strong>only one typical, full-time employee</strong> in each position. Estimate if necessary. Exclude fringe benefits. Report <strong>annual W-2 (T-4) wages</strong> prior to employee deductions. If a position is filled with part-time workers, report what one employee would receive if they were full-time.</p>

        <p>Please start by entering information for a single location.  When complete, you can add another facility and the form will expand as needed.</p>


<h3 class="h3-header">Location Information</h3>
        <div class="row">
            <div class="col-md-6">
                <table class="table table-condensed">
                    <tbody>
                        <tr>
                            <td class="col-md-2 text-left">Zip/Postal code</td>
                            <td class="col-md-2 text-center">
                                <span class="input-group">
                                    <asp:TextBox ID="txt_s388oth" runat="server" CssClass="form-control text-right"></asp:TextBox>
                                    
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td class="col-md-2 text-left">Location name (or identifier)</td>
                            <td class="col-md-2 text-center">
                                <span class="input-group">
                                    <asp:TextBox ID="txt_s389oth" runat="server" CssClass="form-control text-right"></asp:TextBox>
                                    
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td class="col-md-2 text-left">2017 Sales volume/shipment volume</td>
                            <td class="col-md-2 text-center">
                                <span class="input-group">
                                    <span class="input-group-addon">$</span>
                                    <asp:TextBox ID="txt_s390" runat="server" CssClass="form-control text-right"></asp:TextBox>
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td class="col-md-2 text-left">Number of employees at location (FTEs)</td>
                            <td class="col-md-2 text-center">
                                <span class="input-group">
                                    <span class="input-group-addon">#</span>
                                    <asp:TextBox ID="txt_s391" runat="server" CssClass="form-control text-right"></asp:TextBox>
                                </span>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <table class="table table-condensed">
                    <tbody>
                        <tr class="td-bg-secondary">
                            <td class="col-md-2">Branch Manager</td>
                            <td class="col-md-2 text-center">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td class="col-md-2">Years in industry</td>
                            <td class="col-md-2 text-center">
                                <span class="input-group">
                                    <span class="input-group-addon">#</span>
                                    <asp:TextBox ID="txt_s392" runat="server" CssClass="form-control text-right"></asp:TextBox>
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td class="col-md-2 text-left">Years in position</td>
                            <td class="col-md-2 text-center">
                                <span class="input-group">
                                    <span class="input-group-addon">#</span>
                                    <asp:TextBox ID="txt_s393" runat="server" CssClass="form-control text-right"></asp:TextBox>
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td class="col-md-2 text-left">Base salary</td>
                            <td class="col-md-2 text-center">
                                <span class="input-group">
                                    <span class="input-group-addon">$</span>
                                    <asp:TextBox ID="txt_s394" runat="server" CssClass="form-control text-right"></asp:TextBox>
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td class="col-md-2 text-left">Commissions, incentive pay, and bonus</td>
                            <td class="col-md-2 text-center">
                                <span class="input-group">
                                    <span class="input-group-addon">$</span>
                                    <asp:TextBox ID="txt_s395" runat="server" CssClass="form-control text-right"></asp:TextBox>
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td class="col-md-2 text-left"><strong>Eligible for bonus/incentives</strong></td>
                            <td class="col-md-2 text-left">
                <asp:RadioButton CssClass="radio-inline" ID="rb_s396_1" runat="server" GroupName="s396" Text="Yes" /><br />
                <asp:RadioButton CssClass="radio-inline" ID="rb_s396_2" runat="server" GroupName="s396" Text="No" />
                            </td>
                        </tr>
                        <tr>
                            <td class="col-md-2 text-left"><strong>Basis for determining bonus/incentive: (check all that apply)</strong></td>
                            <td class="col-md-2 text-left">
                <asp:CheckBox CssClass="checkbox-inline" ID="cb_s397" runat="server" Text="Discretionary" />
                                <br />
                <asp:CheckBox CssClass="checkbox-inline" ID="cb_s398" runat="server" Text="Sales" />
                                <br />
                <asp:CheckBox CssClass="checkbox-inline" ID="cb_s573" runat="server" Text="Gross Margins" />
                                <br />
                <asp:CheckBox CssClass="checkbox-inline" ID="cb_s399" runat="server" Text="Profitability" />
                                <br />
                <asp:CheckBox CssClass="checkbox-inline" ID="cb_s400" runat="server" Text="Return on capital" />
                                <br />
                <table>
                                <tr>
                                    <td><asp:CheckBox CssClass="checkbox-inline" ID="cb_s401" runat="server" Text="Other" /></td>
                                    <td><asp:TextBox ID="txt_s401oth" runat="server" CssClass="form-control"></asp:TextBox></td>
                                </tr>
                            </table>
                
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>


       
        <div class="row">
            <div class="col-md-7">
                <table class="table table-condensed">
                    <thead>
                        <tr>
                            <td colspan="3" class="text-left td-bg-secondary">Salespeople</td>

                        </tr>
                    </thead>

                    <tbody>
                        <tr>
                            <td></td>
                            <td class="col-md-2 text-center">
                                <strong>Outside Sales</strong></td>
                            <td class="col-md-2 text-center">
                                <strong>Inside Sales</strong></td>
                        </tr>
                        <tr class="tr-question">
                            <td class="col-md-3 text-left">Entry-Level (less than 2 years of experience)</td>
                            <td class="col-md-2 text-center">
                                &nbsp;</td>
                            <td class="col-md-2 text-center">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td class="col-md-3 text-left">Average years in position?</td>
                            <td class="col-md-2 text-center">
                                <span class="input-group">
                                    <span class="input-group-addon">#</span>
                                    <asp:TextBox ID="txt_s402" runat="server" CssClass="form-control text-right"></asp:TextBox>
                                </span>
                            </td>
                            <td class="col-md-2 text-center">
                                <span class="input-group">
                                    <span class="input-group-addon">#</span>
                                    <asp:TextBox ID="txt_s502" runat="server" CssClass="form-control text-right"></asp:TextBox>
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td class="col-md-3 text-left">Number of full-time employees </td>
                            <td class="col-md-2 text-center">
                                <span class="input-group">
                                    <span class="input-group-addon">#</span>
                                    <asp:TextBox ID="txt_s403" runat="server" CssClass="form-control text-right"></asp:TextBox>
                                </span>
                            </td>
                            <td class="col-md-2 text-center">
                                <span class="input-group">
                                    <span class="input-group-addon">#</span>
                                    <asp:TextBox ID="txt_s503" runat="server" CssClass="form-control text-right"></asp:TextBox>
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td class="col-md-3 text-left">Base salary</td>
                            <td class="col-md-2 text-center">
                                <span class="input-group">
                                    <span class="input-group-addon">$</span>
                                    <asp:TextBox ID="txt_s404" runat="server" CssClass="form-control text-right"></asp:TextBox>
                                </span>
                            </td>
                            <td class="col-md-2 text-center">
                                <span class="input-group">
                                    <span class="input-group-addon">$</span>
                                    <asp:TextBox ID="txt_s504" runat="server" CssClass="form-control text-right"></asp:TextBox>
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td class="col-md-3 text-left">Commissions</td>
                            <td class="col-md-2 text-center">
                                <span class="input-group">
                                    <span class="input-group-addon">$</span>
                                    <asp:TextBox ID="txt_s405" runat="server" CssClass="form-control text-right"></asp:TextBox>
                                </span>
                            </td>
                            <td class="col-md-2 text-center">
                                <span class="input-group">
                                    <span class="input-group-addon">$</span>
                                    <asp:TextBox ID="txt_s505" runat="server" CssClass="form-control text-right"></asp:TextBox>
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td class="col-md-3 text-left">Bonus and all other cash compensation</td>
                            <td class="col-md-2 text-center">
                                <span class="input-group">
                                    <span class="input-group-addon">$</span>
                                    <asp:TextBox ID="txt_s691" runat="server" CssClass="form-control text-right"></asp:TextBox>
                                </span>
                            </td>
                            <td class="col-md-2 text-center">
                                <span class="input-group">
                                    <span class="input-group-addon">$</span>
                                    <asp:TextBox ID="txt_s692" runat="server" CssClass="form-control text-right"></asp:TextBox>
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td class="col-md-3 text-left tr-question">Mid-Level (2 to 10 years of experience)</td>
                            <td class="col-md-2 text-center">
                                &nbsp;</td>
                            <td class="col-md-2 text-center">
                                &nbsp;</td>
                        </tr>
                                                <tr>
                            <td class="col-md-3 text-left">Average years in position?</td>
                            <td class="col-md-2 text-center">
                                <span class="input-group">
                                    <span class="input-group-addon">#</span>
                                    <asp:TextBox ID="txt_s406" runat="server" CssClass="form-control text-right"></asp:TextBox>
                                </span>
                            </td>
                            <td class="col-md-2 text-center">
                                <span class="input-group">
                                    <span class="input-group-addon">#</span>
                                    <asp:TextBox ID="txt_s506" runat="server" CssClass="form-control text-right"></asp:TextBox>
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td class="col-md-3 text-left">Number of full-time employees </td>
                            <td class="col-md-2 text-center">
                                <span class="input-group">
                                    <span class="input-group-addon">#</span>
                                    <asp:TextBox ID="txt_s407" runat="server" CssClass="form-control text-right"></asp:TextBox>
                                </span>
                            </td>
                            <td class="col-md-2 text-center">
                                <span class="input-group">
                                    <span class="input-group-addon">#</span>
                                    <asp:TextBox ID="txt_s507" runat="server" CssClass="form-control text-right"></asp:TextBox>
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td class="col-md-3 text-left">Base salary</td>
                            <td class="col-md-2 text-center">
                                <span class="input-group">
                                    <span class="input-group-addon">$</span>
                                    <asp:TextBox ID="txt_s408" runat="server" CssClass="form-control text-right"></asp:TextBox>
                                </span>
                            </td>
                            <td class="col-md-2 text-center">
                                <span class="input-group">
                                    <span class="input-group-addon">$</span>
                                    <asp:TextBox ID="txt_s508" runat="server" CssClass="form-control text-right"></asp:TextBox>
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td class="col-md-3 text-left">Commissions</td>
                            <td class="col-md-2 text-center">
                                <span class="input-group">
                                    <span class="input-group-addon">$</span>
                                    <asp:TextBox ID="txt_s409" runat="server" CssClass="form-control text-right"></asp:TextBox>
                                </span>
                            </td>
                            <td class="col-md-2 text-center">
                                <span class="input-group">
                                    <span class="input-group-addon">$</span>
                                    <asp:TextBox ID="txt_s509" runat="server" CssClass="form-control text-right"></asp:TextBox>
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td class="col-md-3 text-left">Bonus and all other cash compensation</td>
                            <td class="col-md-2 text-center">
                                <span class="input-group">
                                    <span class="input-group-addon">$</span>
                                    <asp:TextBox ID="txt_s693" runat="server" CssClass="form-control text-right"></asp:TextBox>
                                </span>
                            </td>
                            <td class="col-md-2 text-center">
                                <span class="input-group">
                                    <span class="input-group-addon">$</span>
                                    <asp:TextBox ID="txt_s694" runat="server" CssClass="form-control text-right"></asp:TextBox>
                                </span>
                            </td>
                        </tr>
                        <tr class="tr-question">
                            <td class="col-md-3 text-left">Senior-Level (More than 10 years of experience)</td>
                            <td class="col-md-2 text-center">
                                &nbsp;</td>
                            <td class="col-md-2 text-center">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td class="col-md-3 text-left">Average years in position?</td>
                            <td class="col-md-2 text-center">
                                <span class="input-group">
                                    <span class="input-group-addon">#</span>
                                    <asp:TextBox ID="txt_s410" runat="server" CssClass="form-control text-right"></asp:TextBox>
                                </span>
                            </td>
                            <td class="col-md-2 text-center">
                                <span class="input-group">
                                    <span class="input-group-addon">#</span>
                                    <asp:TextBox ID="txt_s510" runat="server" CssClass="form-control text-right"></asp:TextBox>
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td class="col-md-3 text-left">Number of full-time employees </td>
                            <td class="col-md-2 text-center">
                                <span class="input-group">
                                    <span class="input-group-addon">#</span>
                                    <asp:TextBox ID="txt_s411" runat="server" CssClass="form-control text-right"></asp:TextBox>
                                </span>
                            </td>
                            <td class="col-md-2 text-center">
                                <span class="input-group">
                                    <span class="input-group-addon">#</span>
                                    <asp:TextBox ID="txt_s511" runat="server" CssClass="form-control text-right"></asp:TextBox>
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td class="col-md-3 text-left">Base salary</td>
                            <td class="col-md-2 text-center">
                                <span class="input-group">
                                    <span class="input-group-addon">$</span>
                                    <asp:TextBox ID="txt_s412" runat="server" CssClass="form-control text-right"></asp:TextBox>
                                </span>
                            </td>
                            <td class="col-md-2 text-center">
                                <span class="input-group">
                                    <span class="input-group-addon">$</span>
                                    <asp:TextBox ID="txt_s512" runat="server" CssClass="form-control text-right"></asp:TextBox>
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td class="col-md-3 text-left">Commissions</td>
                            <td class="col-md-2 text-center">
                                <span class="input-group">
                                    <span class="input-group-addon">$</span>
                                    <asp:TextBox ID="txt_s413" runat="server" CssClass="form-control text-right"></asp:TextBox>
                                </span>
                            </td>
                            <td class="col-md-2 text-center">
                                <span class="input-group">
                                    <span class="input-group-addon">$</span>
                                    <asp:TextBox ID="txt_s513" runat="server" CssClass="form-control text-right"></asp:TextBox>
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td class="col-md-3 text-left">Bonus and all other cash compensation</td>
                            <td class="col-md-2 text-center">
                                <span class="input-group">
                                    <span class="input-group-addon">$</span>
                                    <asp:TextBox ID="txt_s695" runat="server" CssClass="form-control text-right"></asp:TextBox>
                                </span>
                            </td>
                            <td class="col-md-2 text-center">
                                <span class="input-group">
                                    <span class="input-group-addon">$</span>
                                    <asp:TextBox ID="txt_s696" runat="server" CssClass="form-control text-right"></asp:TextBox>
                                </span>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        


        <div class="row">
            <div class="col-md-7">
                <table class="table table-condensed">
                    <thead>
                        <tr class="td-bg-secondary">
                            <td colspan="1" class="text-left ">Sales Practices </td>
                            <td colspan="1" class="text-center">&nbsp;</td>
                            <td colspan="1" class="text-center">&nbsp;</td>
                        </tr>
                        <tr>
                            <th colspan="1" class="text-center">&nbsp;</th>
                            <th colspan="1" class="text-center">Outside Sales</th>
                            <th colspan="1" class="text-center">Inside Sales</th>
                        </tr>
                    </thead>

                    <tbody>
                        <tr>
                            <td class="col-md-3 text-left">Does your salesforce have quotas?</td>
                            <td class="col-md-2 text-center">
                                <asp:RadioButton CssClass="radio-inline" ID="rb_s414_1" runat="server" GroupName="s414" Text="Yes" />
                                <asp:RadioButton CssClass="radio-inline" ID="rb_s414_2" runat="server" GroupName="s414" Text="No" />
                            </td>
                            <td class="col-md-2 text-center">
                                <asp:RadioButton CssClass="radio-inline" ID="rb_s514_1" runat="server" GroupName="s514" Text="Yes" />
                                <asp:RadioButton CssClass="radio-inline" ID="rb_s514_2" runat="server" GroupName="s514" Text="No" />
                            </td>
                        </tr>
                        <tr>
                            <td class="col-md-3 text-left" style="padding-left:30px;">If yes, at what operational level are quotas set?  <em>(check all that apply)</em></td>
                            <td class="col-md-2 text-left">
                                <asp:CheckBox CssClass="checkbox-inline" ID="cb_s415" runat="server" Text="Company" /><br />
                                <asp:CheckBox CssClass="checkbox-inline" ID="cb_s416" runat="server" Text="Location" /><br />
                                <asp:CheckBox CssClass="checkbox-inline" ID="cb_s417" runat="server" Text="Department" /><br />
                                <asp:CheckBox CssClass="checkbox-inline" ID="cb_s418" runat="server" Text="Team" /><br />
                                <asp:CheckBox CssClass="checkbox-inline" ID="cb_s419" runat="server" Text="Individual" />
                            </td>
                            <td class="col-md-2 text-left">
                                <asp:CheckBox CssClass="checkbox-inline" ID="cb_s515" runat="server" Text="Company" /><br />
                                <asp:CheckBox CssClass="checkbox-inline" ID="cb_s516" runat="server" Text="Location" /><br />
                                <asp:CheckBox CssClass="checkbox-inline" ID="cb_s517" runat="server" Text="Department" /><br />
                                <asp:CheckBox CssClass="checkbox-inline" ID="cb_s518" runat="server" Text="Team" /><br />
                                <asp:CheckBox CssClass="checkbox-inline" ID="cb_s519" runat="server" Text="Individual" />
                            </td>
                        </tr>
                        <tr><td colspan="3">&nbsp;</td></tr>
                        <tr>
                            <td class="col-md-3 text-left">Please indicate the most common compensation method for your salespeople:</td>
                            <td class="col-md-2 text-center">
                                <asp:DropDownList ID="drop_s420" runat="server" CssClass="form-control">
                                    <asp:ListItem Value="999" Text="Select one"></asp:ListItem>
                                    <asp:ListItem Value="1" Text="Salary only"></asp:ListItem>
                                    <asp:ListItem Value="2" Text="Salary plus bonus or contest awards"></asp:ListItem>
                                    <asp:ListItem Value="3" Text="Salary plus commission"></asp:ListItem>
                                    <asp:ListItem Value="4" Text="Salary, commission & bonus or contest awards"></asp:ListItem>
                                    <asp:ListItem Value="5" Text="Commission only"></asp:ListItem>
                                    <asp:ListItem Value="6" Text="Commission and bonus"></asp:ListItem>
                                    <asp:ListItem Value="7" Text="Draw and commission"></asp:ListItem>
                                    <asp:ListItem Value="8" Text="Hourly wage"></asp:ListItem>
                                    <asp:ListItem Value="9" Text="Hourly wage plus commission"></asp:ListItem>
                                    <asp:ListItem Value="10" Text="Hourly wage plus bonus or contest awards"></asp:ListItem>
                                    <asp:ListItem Value="11" Text="Other"></asp:ListItem>
                                </asp:DropDownList>
                                <br />
                                If Other, please specify:
                                <asp:TextBox ID="txt_s420oth" runat="server" CssClass="form-control"></asp:TextBox>
                            </td>
                            <td class="col-md-2 text-center">
                                <asp:DropDownList ID="drop_s520" runat="server" CssClass="form-control">
                                    <asp:ListItem Value="999" Text="Select one"></asp:ListItem>
                                    <asp:ListItem Value="1" Text="Salary only"></asp:ListItem>
                                    <asp:ListItem Value="2" Text="Salary plus bonus or contest awards"></asp:ListItem>
                                    <asp:ListItem Value="3" Text="Salary plus commission"></asp:ListItem>
                                    <asp:ListItem Value="4" Text="Salary, commission & bonus or contest awards"></asp:ListItem>
                                    <asp:ListItem Value="5" Text="Commission only"></asp:ListItem>
                                    <asp:ListItem Value="6" Text="Commission and bonus"></asp:ListItem>
                                    <asp:ListItem Value="7" Text="Draw and commission"></asp:ListItem>
                                    <asp:ListItem Value="8" Text="Hourly wage"></asp:ListItem>
                                    <asp:ListItem Value="9" Text="Hourly wage plus commission"></asp:ListItem>
                                    <asp:ListItem Value="10" Text="Hourly wage plus bonus or contest awards"></asp:ListItem>
                                    <asp:ListItem Value="11" Text="Other"></asp:ListItem>
                                </asp:DropDownList>
                                <br />
                                If Other, please specify:
                                <asp:TextBox ID="txt_s520oth" runat="server" CssClass="form-control"></asp:TextBox>
                            </td>
                        </tr>
                        <tr><td colspan="3">&nbsp;</td></tr>
                        <tr>
                            <td class="col-md-3 text-left">What is the most common method used to compensate salespeople?</td>
                            <td class="col-md-2 text-center">
                                <asp:DropDownList ID="drop_s421" runat="server" CssClass="form-control">
                                    <asp:ListItem Value="999" Text="Select one"></asp:ListItem>
                                    <asp:ListItem Value="1" Text="Flat rate based on dollar amount of sales "></asp:ListItem>
                                    <asp:ListItem Value="2" Text="Variable rate based on dollar amount of sales"></asp:ListItem>
                                    <asp:ListItem Value="3" Text="Flat rate based on gross profit on the sale"></asp:ListItem>
                                    <asp:ListItem Value="4" Text="Variable rate based on gross profit on the sale"></asp:ListItem>
                                    <asp:ListItem Value="5" Text="Varies by product sold"></asp:ListItem>
                                    <asp:ListItem Value="6" Text="Other"></asp:ListItem>
                                </asp:DropDownList>
                                <br />
                                If Other, please specify:
                                <asp:TextBox ID="txt_s421oth" runat="server" CssClass="form-control"></asp:TextBox>
                            </td>
                            <td class="col-md-2 text-center">
                                <asp:DropDownList ID="drop_s521" runat="server" CssClass="form-control">
                                    <asp:ListItem Value="999" Text="Select one"></asp:ListItem>
                                    <asp:ListItem Value="1" Text="Flat rate based on dollar amount of sales "></asp:ListItem>
                                    <asp:ListItem Value="2" Text="Variable rate based on dollar amount of sales"></asp:ListItem>
                                    <asp:ListItem Value="3" Text="Flat rate based on gross profit on the sale"></asp:ListItem>
                                    <asp:ListItem Value="4" Text="Variable rate based on gross profit on the sale"></asp:ListItem>
                                    <asp:ListItem Value="5" Text="Varies by product sold"></asp:ListItem>
                                    <asp:ListItem Value="6" Text="Other"></asp:ListItem>
                                </asp:DropDownList>
                                <br />
                                If Other, please specify:
                                <asp:TextBox ID="txt_s521oth" runat="server" CssClass="form-control"></asp:TextBox>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <hr />



        <table class="table-condensed centered">
            <tr>
                <td class="text-center" colspan="3">
                    <asp:CheckBox ID="cb_sBranchComplete" runat="server" Text="This section is complete." />
                </td>
            </tr>
             <tr>
                <td class="text-center">
                    <asp:Button ID="btn_previous" runat="server" CssClass="btn btn-primary" Text="Previous Section" Width="141px" />
                 </td>
                <td class="text-center">
                    <asp:Button ID="btn_save" runat="server" CssClass="btn btn-primary" Text="Save" Width="141px" />
                 </td>
                <td class="text-center">
                    <asp:Button ID="btn_next" runat="server" CssClass="btn btn-primary" Text="Next Section" Width="141px" />
                 </td>
            </tr>
             </table>
    </div>


    <!-- Modal -->
            <div id="userModal" class="modal fade" role="dialog">
              <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"><strong>Add Location</strong></h4>
                  </div>
                  <div class="modal-body">
                    <p>Enter a location name to add to your account.</p>

                      <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="text-danger" ValidationGroup="Modal" />

                      <div class="form-group">
                          <asp:RequiredFieldValidator ID="RequiredFieldValidator17" runat="server" ControlToValidate="txt_LocationName" CssClass="text-danger" ErrorMessage="Location Name is required." ValidationGroup="Modal" Text="*"></asp:RequiredFieldValidator>

                        <label for="txt_authName">Location Name:</label>
                        <asp:TextBox type="text" CssClass="form-control" id="txt_LocationName" runat="server"></asp:TextBox>
                      </div>

                      
                      <div class="form-group">
                          <asp:Button ID="btn_AddLocation" runat="server" CssClass="btn btn-success" Text="Add Location" Visible="true" ValidationGroup="Modal" />
                      </div>
                      

                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  </div>
                </div>

              </div>
            </div>

</asp:Content>

