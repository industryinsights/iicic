﻿
Imports System.Net.Mail
Imports Models
Imports Persits.PDF

Partial Class Surveys_Financial_TableOfContents
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Session("LocationID") Is Nothing Then
            Response.Redirect("../../account/login.aspx")
        End If
        If Not IsPostBack Then
            ddl_location.Items.Clear()
            Dim locations As List(Of ListItem) = SurveyHelper.ReturnLocationList(Session("OrganizationId"))
            For Each l In locations
                ddl_location.Items.Add(l)
            Next
            ddl_location.SelectedIndex = 0
            tbl_location.Visible = False
            CheckSurveyStatus(Session("LocationID"))

            If Session("IsAdmin") = True Then
                tr_addUsers.Visible = True
            Else
                tr_addUsers.Visible = False
            End If


            imgbtn_excel.HRef = String.Format("../../Resources/{0}", SurveyHelper.ReturnExcelFilePath(Session("Client")))
            lb_excel.HRef = String.Format("../../Resources/{0}", SurveyHelper.ReturnExcelFilePath(Session("Client")))
            imgbtn_pdf.HRef = String.Format("../../Resources/{0}", SurveyHelper.ReturnExcelFilePath(Session("Client"))).Replace(".xlsx", ".pdf")
            lb_pdf.HRef = String.Format("../../Resources/{0}", SurveyHelper.ReturnExcelFilePath(Session("Client"))).Replace(".xlsx", ".pdf")


            'Disable Survey if not active
            Dim client As String = String.Empty
            If Session("Client") IsNot Nothing Then
                client = Session("Client")
            End If
            Using db As New IICICEntities
                Dim project As New Project
                project = (From p In db.Projects Where p.Name = client).FirstOrDefault
                If Not project.SurveyIsActive Then
                    btn_start.Visible = False
                    btn_submit.Enabled = False
                    lbl_surveyClosed.Visible = True
                    lb_Compensation.Enabled = False
                    lb_CompByJob.Enabled = False
                    lb_BranchCompBen.Enabled = False
                    lb_Benefits.Enabled = False
                    lb_Profile.Enabled = False
                    lb_Recruiting.Enabled = False
                    lb_Sales.Enabled = False
                End If
            End Using
        End If
    End Sub
    Sub CheckSurveyStatus(ByVal LocationID As Integer)
        Using db As New IICICEntities
            Dim survey As New CompSurvey
            Dim branch As New CompBranchSurvey
            survey = (From s In db.CompSurveys Where s.LocationID = LocationID).FirstOrDefault
            branch = (From b In db.CompBranchSurveys Where b.LocationID = LocationID).FirstOrDefault
            If survey IsNot Nothing Then
                If survey.section1Complete And survey.section2Complete And survey.section3Complete Then
                    btn_submit.Enabled = True
                    btn_submit.CssClass = "btn btn-lg btn-success"
                    If survey.SubmitDate IsNot Nothing Then
                        lbl_submit.Text = String.Format("Completed:  {0}", survey.SubmitDate)
                        lbl_submit.CssClass = "text-success"
                        lbl_deadline.Text = String.Format("(Editable until {0} end of day)", SurveyHelper.ReturnDeadline(Session("Client")))
                    End If
                End If
                If survey.section1Complete IsNot Nothing Then
                    If survey.section1Complete = True Then
                        lbl_statusProfile.Text = "Complete"
                        lbl_statusProfile.CssClass = "text-success"
                    Else
                        lbl_statusProfile.Text = "In progress"
                        lbl_statusProfile.CssClass = "text-info"
                    End If
                End If
                If survey.section2Complete IsNot Nothing Then
                    If survey.section2Complete = True Then
                        lbl_statusRecruiting.Text = "Complete"
                        lbl_statusRecruiting.CssClass = "text-success"
                    Else
                        lbl_statusRecruiting.Text = "In progress"
                        lbl_statusRecruiting.CssClass = "text-info"
                    End If
                End If
                If survey.section3Complete IsNot Nothing Then
                    If survey.section3Complete = True Then
                        lbl_statusCompensation.Text = "Complete"
                        lbl_statusCompensation.CssClass = "text-success"
                    Else
                        lbl_statusCompensation.Text = "In progress"
                        lbl_statusCompensation.CssClass = "text-info"
                    End If
                End If
                If survey.section4Complete IsNot Nothing Then
                    If survey.section4Complete = True Then
                        lbl_statusCompByJob.Text = "Complete"
                        lbl_statusCompByJob.CssClass = "text-success"
                    Else
                        lbl_statusCompByJob.Text = "In progress"
                        lbl_statusCompByJob.CssClass = "text-info"
                    End If
                End If
                If survey.section5Complete IsNot Nothing Then
                    If survey.section5Complete = True Then
                        lbl_statusBenefits.Text = "Complete"
                        lbl_statusBenefits.CssClass = "text-success"
                    Else
                        lbl_statusBenefits.Text = "In progress"
                        lbl_statusBenefits.CssClass = "text-info"
                    End If
                End If
                If survey.section6Complete IsNot Nothing Then
                    If survey.section6Complete = True Then
                        lbl_statusSales.Text = "Complete"
                        lbl_statusSales.CssClass = "text-success"
                    Else
                        lbl_statusSales.Text = "In progress"
                        lbl_statusSales.CssClass = "text-info"
                    End If
                End If
                If branch IsNot Nothing Then
                    If branch.sBranchComplete IsNot Nothing Then
                        If branch.sBranchComplete = True Then
                            lbl_statusBranchCompBen.Text = "Complete"
                            lbl_statusBranchCompBen.CssClass = "text-success"
                        Else
                            lbl_statusBranchCompBen.Text = "In progress"
                            lbl_statusBranchCompBen.CssClass = "text-info"
                        End If
                    End If
                End If

            End If
        End Using
    End Sub

    Protected Sub btn_submit_Click(sender As Object, e As EventArgs) Handles btn_submit.Click
        Dim LocationID As Integer = Session("LocationID")
        Using db As New IICICEntities
            Dim survey As New CompSurvey
            survey = (From s In db.CompSurveys Where s.LocationID = LocationID).FirstOrDefault
            If survey IsNot Nothing Then
                survey.SubmitDate = Now
                db.SaveChanges()
                lbl_submit.Text = String.Format("Completed:  {0}", survey.SubmitDate)
                lbl_submit.CssClass = "text-success"
            End If
        End Using
    End Sub


    Protected Sub ddl_location_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddl_location.SelectedIndexChanged
        Session("LocationID") = ddl_location.SelectedValue
        ResetLabels()
        CheckSurveyStatus(Session("LocationID"))
    End Sub
    Sub ResetLabels()
        lbl_statusProfile.Text = "Not Started"
        lbl_statusProfile.CssClass = "text-muted"

        lbl_statusRecruiting.Text = "Not Started"
        lbl_statusRecruiting.CssClass = "text-muted"

        lbl_statusCompensation.Text = "Not Started"
        lbl_statusCompensation.CssClass = "text-muted"

        lbl_statusCompByJob.Text = "Not Started"
        lbl_statusCompByJob.CssClass = "text-muted"

        lbl_statusBenefits.Text = "Not Started"
        lbl_statusBenefits.CssClass = "text-muted"

        lbl_statusSales.Text = "Not Started"
        lbl_statusSales.CssClass = "text-muted"

        lbl_statusBranchCompBen.Text = "Not Started"
        lbl_statusBranchCompBen.CssClass = "text-muted"

        lbl_submit.Text = "Not Complete"
        lbl_submit.CssClass = "text-muted"

        btn_submit.CssClass = "btn btn-lg btn-primary disabled"
        btn_submit.Enabled = False
    End Sub


    Protected Sub btn_AddUser_Click(sender As Object, e As EventArgs) Handles btn_AddUser.Click

        'confirm name and valid email
        If String.IsNullOrEmpty(txt_authEmail.Text) Or String.IsNullOrEmpty(txt_authName.Text) Then
            'error
            Dim errMOD As New CustomValidator With {.IsValid = False, .ValidationGroup = "AddAuthorized", .ErrorMessage = "Name or email missing."}
            Page.Validators.Add(errMOD)
            Return
        End If

        'confirm email not already in system
        Using db As New IICICEntities
            Dim user As New User
            user = (From u In db.Users Where u.UserName = txt_authEmail.Text Or u.Email = txt_authEmail.Text).FirstOrDefault
            If user IsNot Nothing Then
                Dim err As New CustomValidator With {.IsValid = False, .ValidationGroup = "AddAuthorized", .ErrorMessage = "Unable to add user. User name or email is already in use."}
                Page.Validators.Add(err)
            Else

                user = New User
                user.OrganizationId = Session("OrganizationId").ToString

                user.UserName = txt_authEmail.Text
                user.Password = GeneratePassword()    'txt_password.Text

                user.FullName = txt_authName.Text
                user.Email = txt_authEmail.Text

                user.IsActive = True
                user.CreateDate = Now

                db.Users.Add(user)
                db.SaveChanges()

                Dim UDS As New UserDataSet
                UDS.UserId = user.UserId
                UDS.LocationID = Session("LocationID").ToString

                db.UserDataSets.Add(UDS)
                db.SaveChanges()

                If cb_admin.Checked Then
                    Dim role As New UserRole With {.RoleId = 3, .UserId = user.UserId}
                    db.UserRoles.Add(role)
                End If

                db.SaveChanges()


                'update main form with message they've been added and clear form on close (disable submit button)
                lblAuthUser.Text = txt_authEmail.Text & " has been added to your account as an authorized user for this survey.  To make changes to authorized users, please access the <a href='https://www.compensationbenchmarking.com/Account/OrganizationAdmin.aspx'>Organization</a> section."
                lblAuthUser.Visible = True

                SendEmail(user.UserName, user.Password) 'send email to authorized user


                'Reset Modal form
                txt_authEmail.Text = ""
                txt_authName.Text = ""
                cb_admin.Checked = False

            End If

        End Using

    End Sub

    Function GeneratePassword() As String
        Dim s As String = "ACDEFGHJKLMNPQRSTUVWXYZabxdefghijkmnpqrstuvwxyz2345679!"
        Dim r As New Random
        Dim sb As New StringBuilder
        For i As Integer = 1 To 8
            Dim idx As Integer = r.Next(0, 35)
            sb.Append(s.Substring(idx, 1))
        Next
        Return sb.ToString
    End Function
    Sub SendEmail(ByVal username As String, ByVal password As String)
        Using client As New SmtpClient
            Dim message As New MailMessage
            Dim address As Object = Nothing
            address = New MailAddress(username)
            message.From = New MailAddress(message.From.Address, "Mike Ferris")
            message.IsBodyHtml = True
            message.To.Add(address)
            message.Subject = "Welcome to the Cross-Industry Compensation Portal!"
            message.Body = GetEmailHtml(username, password)
            client.Send(message)
        End Using
    End Sub

    Private Function GetEmailHtml(ByVal username As String, ByVal password As String) As String
        Dim url As String = "https://compensationbenchmarking.com"
        If Session("Client") IsNot Nothing Then
            url = String.Format("https://compensationbenchmarking.com/{0}", Session("Client"))
        End If
        Dim html As New StringBuilder()
        html.Append("<html>")
        html.Append("<head>")
        html.Append("<title></title>")
        html.Append("<style type=""text/css"">")
        html.Append("html, body")
        html.Append("{")
        html.Append("margin: 0;")
        html.Append("font-family: Arial;")
        html.Append("font-size: 10pt;")
        html.Append("}")
        html.Append("")
        html.Append("</style>")
        html.Append("</head>")
        html.Append("<body>")
        html.Append("<p>You have been given access to the Cross-Industry Compensation Portal by your Organization.</p>")
        html.Append("<p>Your login credentials are:</p>")
        html.AppendFormat("<p>Username:  {0}</p>", username)
        html.AppendFormat("<p>Password:  {0}</p>", password)

        html.AppendFormat("<p>To login, please visit the <a href=""{0}"">Cross-Industry Compensation Portal Login</a>.</p>", url)

        html.Append("<p>If you have any questions or believe that you have received this email in error, please contact me by using the information below.</p>")
        html.Append("<p>Thanks,</p>")
        html.Append("<p style=""color:#7F7F7F"">Mike Ferris<br/>Industry Insights, Inc.<br/>614.389.2100 ext. 241<br/><a href=""mailto:comp@industryinsights.com"">comp@industryinsights.com</a></p>")
        html.Append("</body>")
        html.Append("</html>")
        Return html.ToString()

    End Function

End Class
