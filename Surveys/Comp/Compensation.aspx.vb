﻿
Imports Models

Partial Class Surveys_Compensation
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load

        

        If Session("LocationID") Is Nothing Then
            If Not String.IsNullOrEmpty(txt_LocationID.Text) Then
                Session("LocationID") = Convert.ToInt32(txt_LocationID.Text)
            Else
                Response.Redirect("../../account/login.aspx")
            End If
        End If
        If Session("UserId") Is Nothing Then
            If Not String.IsNullOrEmpty(txt_userid.Text) Then
                Session("UserId") = Convert.ToInt32(txt_userid.Text)
            Else
                Response.Redirect("../../account/login.aspx")
            End If
        End If
        If Not IsPostBack Then
            Dim LocationID As Integer = Session("LocationID")
            txt_LocationID.Text = LocationID
            Dim userid As Integer = Session("UserId")
            txt_userid.Text = userid
            If Session("Client") IsNot Nothing And Not String.IsNullOrEmpty(Session("Client")) Then
                lbl_deadline.Text = SurveyHelper.ReturnDeadline(Session("Client").ToString)
            Else
                lbl_deadline.Text = "Friday, April 13, 2018"
            End If
            ddl_location.Items.Clear()
            Dim locations As List(Of ListItem) = SurveyHelper.ReturnLocationList(Session("OrganizationId"))
            For Each l In locations
                ddl_location.Items.Add(l)
            Next
            ddl_location.SelectedValue = Session("LocationID")
            tbl_location.Visible = False
            LoadSurvey()
        End If
    End Sub
    Sub LoadSurvey()
        Using db As New IICICEntities
            Dim LocationID As Integer = txt_LocationID.Text
            Dim survey As New CompSurvey
            survey = (From s In db.CompSurveys Where s.LocationID = LocationID And s.SurveyYear = Now.Year).FirstOrDefault
            If survey IsNot Nothing Then
                lbl_lastSave.Text = survey.ModifiedDate


                '__Custom Show/hide based on assocation____________________________\/

                NAWdiv.Visible = False 'default

                If survey.s1914 IsNot Nothing Then
                    'show NAW sections
                    If survey.s1914 = True Then NAWdiv.Visible = True
                End If
                '__Custom Show/hide based on assocation____________________________/\


                '---PASTE LOAD STATEMENTS---

                If survey.s69 IsNot Nothing Then
                    drop_s69.SelectedValue = survey.s69
                End If
                If survey.s92 IsNot Nothing Then
                    drop_s92.SelectedValue = survey.s92
                End If
                If survey.s115 IsNot Nothing Then
                    drop_s115.SelectedValue = survey.s115
                End If
                If survey.s138 IsNot Nothing Then
                    drop_s138.SelectedValue = survey.s138
                End If
                If survey.s161 IsNot Nothing Then
                    drop_s161.SelectedValue = survey.s161
                End If
                If survey.s603 IsNot Nothing Then
                    drop_s603.SelectedValue = survey.s603
                End If
                If survey.s628 IsNot Nothing Then
                    drop_s628.SelectedValue = survey.s628
                End If



                If survey.s67 IsNot Nothing Then
                        txt_s67.Text = String.Format(survey.s67, "N0")
                    End If
                    If survey.s90 IsNot Nothing Then
                        txt_s90.Text = String.Format(survey.s90, "N0")
                    End If
                    If survey.s113 IsNot Nothing Then
                        txt_s113.Text = String.Format(survey.s113, "N0")
                    End If
                    If survey.s136 IsNot Nothing Then
                        txt_s136.Text = String.Format(survey.s136, "N0")
                    End If
                    If survey.s159 IsNot Nothing Then
                        txt_s159.Text = String.Format(survey.s159, "N0")
                    End If
                    If survey.s68 IsNot Nothing Then
                        txt_s68.Text = String.Format(survey.s68, "N0")
                    End If
                    If survey.s91 IsNot Nothing Then
                        txt_s91.Text = String.Format(survey.s91, "N0")
                    End If
                    If survey.s114 IsNot Nothing Then
                        txt_s114.Text = String.Format(survey.s114, "N0")
                    End If
                    If survey.s137 IsNot Nothing Then
                        txt_s137.Text = String.Format(survey.s137, "N0")
                    End If
                    If survey.s160 IsNot Nothing Then
                        txt_s160.Text = String.Format(survey.s160, "N0")
                    End If
                    If survey.s70 IsNot Nothing Then
                        txt_s70.Text = String.Format(survey.s70, "N0")
                    End If
                    If survey.s93 IsNot Nothing Then
                        txt_s93.Text = String.Format(survey.s93, "N0")
                    End If
                    If survey.s116 IsNot Nothing Then
                        txt_s116.Text = String.Format(survey.s116, "N0")
                    End If
                    If survey.s139 IsNot Nothing Then
                        txt_s139.Text = String.Format(survey.s139, "N0")
                    End If
                    If survey.s162 IsNot Nothing Then
                        txt_s162.Text = String.Format(survey.s162, "N0")
                    End If
                    If survey.s71 IsNot Nothing Then
                        txt_s71.Text = String.Format(survey.s71, "N0")
                    End If
                    If survey.s94 IsNot Nothing Then
                        txt_s94.Text = String.Format(survey.s94, "N0")
                    End If
                    If survey.s117 IsNot Nothing Then
                        txt_s117.Text = String.Format(survey.s117, "N0")
                    End If
                    If survey.s140 IsNot Nothing Then
                        txt_s140.Text = String.Format(survey.s140, "N0")
                    End If
                    If survey.s163 IsNot Nothing Then
                        txt_s163.Text = String.Format(survey.s163, "N0")
                    End If
                    If survey.s72 IsNot Nothing Then
                        txt_s72.Text = String.Format(survey.s72, "N0")
                    End If
                    If survey.s95 IsNot Nothing Then
                        txt_s95.Text = String.Format(survey.s95, "N0")
                    End If
                    If survey.s118 IsNot Nothing Then
                        txt_s118.Text = String.Format(survey.s118, "N0")
                    End If
                    If survey.s141 IsNot Nothing Then
                        txt_s141.Text = String.Format(survey.s141, "N0")
                    End If
                    If survey.s164 IsNot Nothing Then
                        txt_s164.Text = String.Format(survey.s164, "N0")
                    End If
                    If survey.s73 IsNot Nothing Then
                        txt_s73.Text = String.Format(survey.s73, "N0")
                    End If
                    If survey.s96 IsNot Nothing Then
                        txt_s96.Text = String.Format(survey.s96, "N0")
                    End If
                    If survey.s119 IsNot Nothing Then
                        txt_s119.Text = String.Format(survey.s119, "N0")
                    End If
                    If survey.s142 IsNot Nothing Then
                        txt_s142.Text = String.Format(survey.s142, "N0")
                    End If
                    If survey.s165 IsNot Nothing Then
                        txt_s165.Text = String.Format(survey.s165, "N0")
                    End If
                    If survey.s74 IsNot Nothing Then
                        txt_s74.Text = String.Format(survey.s74, "N0")
                    End If
                    If survey.s97 IsNot Nothing Then
                        txt_s97.Text = String.Format(survey.s97, "N0")
                    End If
                    If survey.s120 IsNot Nothing Then
                        txt_s120.Text = String.Format(survey.s120, "N0")
                    End If
                    If survey.s143 IsNot Nothing Then
                        txt_s143.Text = String.Format(survey.s143, "N0")
                    End If
                    If survey.s166 IsNot Nothing Then
                        txt_s166.Text = String.Format(survey.s166, "N0")
                    End If
                    If survey.s601 IsNot Nothing Then
                        txt_s601.Text = String.Format(survey.s601, "N0")
                    End If
                    If survey.s626 IsNot Nothing Then
                        txt_s626.Text = String.Format(survey.s626, "N0")
                    End If
                    If survey.s602 IsNot Nothing Then
                        txt_s602.Text = String.Format(survey.s602, "N0")
                    End If
                    If survey.s627 IsNot Nothing Then
                        txt_s627.Text = String.Format(survey.s627, "N0")
                    End If
                    If survey.s604 IsNot Nothing Then
                        txt_s604.Text = String.Format(survey.s604, "N0")
                    End If
                    If survey.s629 IsNot Nothing Then
                        txt_s629.Text = String.Format(survey.s629, "N0")
                    End If
                    If survey.s605 IsNot Nothing Then
                        txt_s605.Text = String.Format(survey.s605, "N0")
                    End If
                    If survey.s630 IsNot Nothing Then
                        txt_s630.Text = String.Format(survey.s630, "N0")
                    End If
                    If survey.s606 IsNot Nothing Then
                        txt_s606.Text = String.Format(survey.s606, "N0")
                    End If
                    If survey.s631 IsNot Nothing Then
                        txt_s631.Text = String.Format(survey.s631, "N0")
                    End If
                    If survey.s607 IsNot Nothing Then
                        txt_s607.Text = String.Format(survey.s607, "N0")
                    End If
                    If survey.s632 IsNot Nothing Then
                        txt_s632.Text = String.Format(survey.s632, "N0")
                    End If
                    If survey.s608 IsNot Nothing Then
                        txt_s608.Text = String.Format(survey.s608, "N0")
                    End If
                    If survey.s633 IsNot Nothing Then
                        txt_s633.Text = String.Format(survey.s633, "N0")
                    End If

                    If survey.s75 IsNot Nothing Then
                        cb_s75.Checked = survey.s75
                    End If
                    If survey.s98 IsNot Nothing Then
                        cb_s98.Checked = survey.s98
                    End If
                    If survey.s121 IsNot Nothing Then
                        cb_s121.Checked = survey.s121
                    End If
                    If survey.s144 IsNot Nothing Then
                        cb_s144.Checked = survey.s144
                    End If
                    If survey.s167 IsNot Nothing Then
                        cb_s167.Checked = survey.s167
                    End If
                    If survey.s530 IsNot Nothing Then
                        cb_s530.Checked = survey.s530
                    End If
                    If survey.s531 IsNot Nothing Then
                        cb_s531.Checked = survey.s531
                    End If
                    If survey.s532 IsNot Nothing Then
                        cb_s532.Checked = survey.s532
                    End If
                    If survey.s533 IsNot Nothing Then
                        cb_s533.Checked = survey.s533
                    End If
                    If survey.s534 IsNot Nothing Then
                        cb_s534.Checked = survey.s534
                    End If
                    If survey.s76 IsNot Nothing Then
                        cb_s76.Checked = survey.s76
                    End If
                    If survey.s99 IsNot Nothing Then
                        cb_s99.Checked = survey.s99
                    End If
                    If survey.s122 IsNot Nothing Then
                        cb_s122.Checked = survey.s122
                    End If
                    If survey.s145 IsNot Nothing Then
                        cb_s145.Checked = survey.s145
                    End If
                    If survey.s168 IsNot Nothing Then
                        cb_s168.Checked = survey.s168
                    End If
                    If survey.s77 IsNot Nothing Then
                        cb_s77.Checked = survey.s77
                    End If
                    If survey.s100 IsNot Nothing Then
                        cb_s100.Checked = survey.s100
                    End If
                    If survey.s123 IsNot Nothing Then
                        cb_s123.Checked = survey.s123
                    End If
                    If survey.s146 IsNot Nothing Then
                        cb_s146.Checked = survey.s146
                    End If
                    If survey.s169 IsNot Nothing Then
                        cb_s169.Checked = survey.s169
                    End If
                    If survey.s78 IsNot Nothing Then
                        cb_s78.Checked = survey.s78
                    End If
                    If survey.s101 IsNot Nothing Then
                        cb_s101.Checked = survey.s101
                    End If
                    If survey.s124 IsNot Nothing Then
                        cb_s124.Checked = survey.s124
                    End If
                    If survey.s147 IsNot Nothing Then
                        cb_s147.Checked = survey.s147
                    End If
                    If survey.s170 IsNot Nothing Then
                        cb_s170.Checked = survey.s170
                    End If
                    If survey.s79 IsNot Nothing Then
                        cb_s79.Checked = survey.s79
                    End If
                    If survey.s102 IsNot Nothing Then
                        cb_s102.Checked = survey.s102
                    End If
                    If survey.s125 IsNot Nothing Then
                        cb_s125.Checked = survey.s125
                    End If
                    If survey.s148 IsNot Nothing Then
                        cb_s148.Checked = survey.s148
                    End If
                    If survey.s171 IsNot Nothing Then
                        cb_s171.Checked = survey.s171
                    End If
                    If survey.s80 IsNot Nothing Then
                        cb_s80.Checked = survey.s80
                    End If
                    If survey.s103 IsNot Nothing Then
                        cb_s103.Checked = survey.s103
                    End If
                    If survey.s126 IsNot Nothing Then
                        cb_s126.Checked = survey.s126
                    End If
                    If survey.s149 IsNot Nothing Then
                        cb_s149.Checked = survey.s149
                    End If
                    If survey.s172 IsNot Nothing Then
                        cb_s172.Checked = survey.s172
                    End If
                    If survey.s81 IsNot Nothing Then
                        cb_s81.Checked = survey.s81
                    End If
                    If survey.s104 IsNot Nothing Then
                        cb_s104.Checked = survey.s104
                    End If
                    If survey.s127 IsNot Nothing Then
                        cb_s127.Checked = survey.s127
                    End If
                    If survey.s150 IsNot Nothing Then
                        cb_s150.Checked = survey.s150
                    End If
                    If survey.s173 IsNot Nothing Then
                        cb_s173.Checked = survey.s173
                    End If
                    If survey.s82 IsNot Nothing Then
                        cb_s82.Checked = survey.s82
                    End If
                    If survey.s105 IsNot Nothing Then
                        cb_s105.Checked = survey.s105
                    End If
                    If survey.s128 IsNot Nothing Then
                        cb_s128.Checked = survey.s128
                    End If
                    If survey.s151 IsNot Nothing Then
                        cb_s151.Checked = survey.s151
                    End If
                    If survey.s174 IsNot Nothing Then
                        cb_s174.Checked = survey.s174
                    End If
                    If survey.s83 IsNot Nothing Then
                        cb_s83.Checked = survey.s83
                    End If
                    If survey.s106 IsNot Nothing Then
                        cb_s106.Checked = survey.s106
                    End If
                    If survey.s129 IsNot Nothing Then
                        cb_s129.Checked = survey.s129
                    End If
                    If survey.s152 IsNot Nothing Then
                        cb_s152.Checked = survey.s152
                    End If
                    If survey.s175 IsNot Nothing Then
                        cb_s175.Checked = survey.s175
                    End If
                    If survey.s535 IsNot Nothing Then
                        cb_s535.Checked = survey.s535
                    End If
                    If survey.s536 IsNot Nothing Then
                        cb_s536.Checked = survey.s536
                    End If
                    If survey.s537 IsNot Nothing Then
                        cb_s537.Checked = survey.s537
                    End If
                    If survey.s538 IsNot Nothing Then
                        cb_s538.Checked = survey.s538
                    End If
                    If survey.s539 IsNot Nothing Then
                        cb_s539.Checked = survey.s539
                    End If
                    If survey.s84 IsNot Nothing Then
                        cb_s84.Checked = survey.s84
                    End If
                    If survey.s107 IsNot Nothing Then
                        cb_s107.Checked = survey.s107
                    End If
                    If survey.s130 IsNot Nothing Then
                        cb_s130.Checked = survey.s130
                    End If
                    If survey.s153 IsNot Nothing Then
                        cb_s153.Checked = survey.s153
                    End If
                    If survey.s176 IsNot Nothing Then
                        cb_s176.Checked = survey.s176
                    End If
                    If survey.s85 IsNot Nothing Then
                        cb_s85.Checked = survey.s85
                    End If
                    If survey.s108 IsNot Nothing Then
                        cb_s108.Checked = survey.s108
                    End If
                    If survey.s131 IsNot Nothing Then
                        cb_s131.Checked = survey.s131
                    End If
                    If survey.s154 IsNot Nothing Then
                        cb_s154.Checked = survey.s154
                    End If
                    If survey.s177 IsNot Nothing Then
                        cb_s177.Checked = survey.s177
                    End If
                    If survey.s86 IsNot Nothing Then
                        cb_s86.Checked = survey.s86
                    End If
                    If survey.s109 IsNot Nothing Then
                        cb_s109.Checked = survey.s109
                    End If
                    If survey.s132 IsNot Nothing Then
                        cb_s132.Checked = survey.s132
                    End If
                    If survey.s155 IsNot Nothing Then
                        cb_s155.Checked = survey.s155
                    End If
                    If survey.s178 IsNot Nothing Then
                        cb_s178.Checked = survey.s178
                    End If
                    If survey.s87 IsNot Nothing Then
                        cb_s87.Checked = survey.s87
                    End If
                    If survey.s110 IsNot Nothing Then
                        cb_s110.Checked = survey.s110
                    End If
                    If survey.s133 IsNot Nothing Then
                        cb_s133.Checked = survey.s133
                    End If
                    If survey.s156 IsNot Nothing Then
                        cb_s156.Checked = survey.s156
                    End If
                    If survey.s179 IsNot Nothing Then
                        cb_s179.Checked = survey.s179
                    End If
                    If survey.s88 IsNot Nothing Then
                        cb_s88.Checked = survey.s88
                    End If
                    If survey.s111 IsNot Nothing Then
                        cb_s111.Checked = survey.s111
                    End If
                    If survey.s134 IsNot Nothing Then
                        cb_s134.Checked = survey.s134
                    End If
                    If survey.s157 IsNot Nothing Then
                        cb_s157.Checked = survey.s157
                    End If
                    If survey.s180 IsNot Nothing Then
                        cb_s180.Checked = survey.s180
                    End If
                    If survey.s89 IsNot Nothing Then
                        cb_s89.Checked = survey.s89
                    End If
                    If survey.s112 IsNot Nothing Then
                        cb_s112.Checked = survey.s112
                    End If
                    If survey.s135 IsNot Nothing Then
                        cb_s135.Checked = survey.s135
                    End If
                    If survey.s158 IsNot Nothing Then
                        cb_s158.Checked = survey.s158
                    End If
                    If survey.s181 IsNot Nothing Then
                        cb_s181.Checked = survey.s181
                    End If
                    If survey.s609 IsNot Nothing Then
                        cb_s609.Checked = survey.s609
                    End If
                    If survey.s634 IsNot Nothing Then
                        cb_s634.Checked = survey.s634
                    End If
                    If survey.s610 IsNot Nothing Then
                        cb_s610.Checked = survey.s610
                    End If
                    If survey.s635 IsNot Nothing Then
                        cb_s635.Checked = survey.s635
                    End If
                    If survey.s611 IsNot Nothing Then
                        cb_s611.Checked = survey.s611
                    End If
                    If survey.s636 IsNot Nothing Then
                        cb_s636.Checked = survey.s636
                    End If
                    If survey.s612 IsNot Nothing Then
                        cb_s612.Checked = survey.s612
                    End If
                    If survey.s637 IsNot Nothing Then
                        cb_s637.Checked = survey.s637
                    End If
                    If survey.s613 IsNot Nothing Then
                        cb_s613.Checked = survey.s613
                    End If
                    If survey.s638 IsNot Nothing Then
                        cb_s638.Checked = survey.s638
                    End If
                    If survey.s614 IsNot Nothing Then
                        cb_s614.Checked = survey.s614
                    End If
                    If survey.s639 IsNot Nothing Then
                        cb_s639.Checked = survey.s639
                    End If
                    If survey.s615 IsNot Nothing Then
                        cb_s615.Checked = survey.s615
                    End If
                    If survey.s640 IsNot Nothing Then
                        cb_s640.Checked = survey.s640
                    End If
                    If survey.s616 IsNot Nothing Then
                        cb_s616.Checked = survey.s616
                    End If
                    If survey.s641 IsNot Nothing Then
                        cb_s641.Checked = survey.s641
                    End If
                    If survey.s617 IsNot Nothing Then
                        cb_s617.Checked = survey.s617
                    End If
                    If survey.s642 IsNot Nothing Then
                        cb_s642.Checked = survey.s642
                    End If
                    If survey.s618 IsNot Nothing Then
                        cb_s618.Checked = survey.s618
                    End If
                    If survey.s643 IsNot Nothing Then
                        cb_s643.Checked = survey.s643
                    End If
                    If survey.s619 IsNot Nothing Then
                        cb_s619.Checked = survey.s619
                    End If
                    If survey.s644 IsNot Nothing Then
                        cb_s644.Checked = survey.s644
                    End If
                    If survey.s620 IsNot Nothing Then
                        cb_s620.Checked = survey.s620
                    End If
                    If survey.s645 IsNot Nothing Then
                        cb_s645.Checked = survey.s645
                    End If
                    If survey.s621 IsNot Nothing Then
                        cb_s621.Checked = survey.s621
                    End If
                    If survey.s646 IsNot Nothing Then
                        cb_s646.Checked = survey.s646
                    End If
                    If survey.s622 IsNot Nothing Then
                        cb_s622.Checked = survey.s622
                    End If
                    If survey.s647 IsNot Nothing Then
                        cb_s647.Checked = survey.s647
                    End If
                    If survey.s623 IsNot Nothing Then
                        cb_s623.Checked = survey.s623
                    End If
                    If survey.s648 IsNot Nothing Then
                        cb_s648.Checked = survey.s648
                    End If
                    If survey.s624 IsNot Nothing Then
                        cb_s624.Checked = survey.s624
                    End If
                    If survey.s649 IsNot Nothing Then
                        cb_s649.Checked = survey.s649
                    End If
                    If survey.s625 IsNot Nothing Then
                        cb_s625.Checked = survey.s625
                    End If
                    If survey.s650 IsNot Nothing Then
                        cb_s650.Checked = survey.s650
                    End If
                    If survey.s541 IsNot Nothing Then
                        cb_s541.Checked = survey.s541
                    End If
                    If survey.s543 IsNot Nothing Then
                        cb_s543.Checked = survey.s543
                    End If
                    If survey.s545 IsNot Nothing Then
                        cb_s545.Checked = survey.s545
                    End If
                    If survey.s542 IsNot Nothing Then
                        cb_s542.Checked = survey.s542
                    End If
                    If survey.s544 IsNot Nothing Then
                        cb_s544.Checked = survey.s544
                    End If
                    If survey.s546 IsNot Nothing Then
                        cb_s546.Checked = survey.s546
                    End If
                    If survey.section3Complete IsNot Nothing Then
                        cb_section3Complete.Checked = survey.section3Complete
                    End If

                    If survey.s540 IsNot Nothing Then
                        If survey.s540 = 1 Then rb_s540_1.Checked = True
                    End If
                    If survey.s540 IsNot Nothing Then
                        If survey.s540 = 2 Then rb_s540_2.Checked = True
                    End If





                End If
        End Using
    End Sub

    Sub SaveSurvey(ByVal IsSubmit As Boolean)
        Using db As New Models.IICICEntities
            Dim existingSurvey As Boolean = False
            Dim LocationID As Integer = txt_LocationID.Text
            Dim userid As Integer = Convert.ToInt16(Session("UserId"))
            Dim survey As New CompSurvey
            survey = (From s In db.CompSurveys Where s.LocationID = LocationID And s.SurveyYear = Now.Year).FirstOrDefault
            If survey Is Nothing Then
                survey = New CompSurvey
                db.CompSurveys.Add(survey)
                db.SaveChanges()
            End If
            survey.LocationID = LocationID
            survey.LastModUserId = userid
            survey.ModifiedDate = Now
            survey.SurveyYear = Now.Year
            If IsSubmit Then
                survey.SubmitDate = Now
            End If

            '---PASTE SAVE STATEMENTS---
            If Not String.IsNullOrEmpty(drop_s69.SelectedValue) Then
                survey.s69 = drop_s69.SelectedValue
            Else
                survey.s69 = Nothing
            End If
            If Not String.IsNullOrEmpty(drop_s92.SelectedValue) Then
                survey.s92 = drop_s92.SelectedValue
            Else
                survey.s92 = Nothing
            End If
            If Not String.IsNullOrEmpty(drop_s115.SelectedValue) Then
                survey.s115 = drop_s115.SelectedValue
            Else
                survey.s115 = Nothing
            End If
            If Not String.IsNullOrEmpty(drop_s138.SelectedValue) Then
                survey.s138 = drop_s138.SelectedValue
            Else
                survey.s138 = Nothing
            End If
            If Not String.IsNullOrEmpty(drop_s161.SelectedValue) Then
                survey.s161 = drop_s161.SelectedValue
            Else
                survey.s161 = Nothing
            End If
            If Not String.IsNullOrEmpty(drop_s603.SelectedValue) Then
                survey.s603 = drop_s603.SelectedValue
            Else
                survey.s603 = Nothing
            End If
            If Not String.IsNullOrEmpty(drop_s628.SelectedValue) Then
                survey.s628 = drop_s628.SelectedValue
            Else
                survey.s628 = Nothing
            End If

            If Not String.IsNullOrEmpty(txt_s67.Text) Then
                survey.s67 = txt_s67.Text.Replace(",", String.Empty)
            Else
                survey.s67 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s90.Text) Then
                survey.s90 = txt_s90.Text.Replace(",", String.Empty)
            Else
                survey.s90 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s113.Text) Then
                survey.s113 = txt_s113.Text.Replace(",", String.Empty)
            Else
                survey.s113 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s136.Text) Then
                survey.s136 = txt_s136.Text.Replace(",", String.Empty)
            Else
                survey.s136 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s159.Text) Then
                survey.s159 = txt_s159.Text.Replace(",", String.Empty)
            Else
                survey.s159 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s68.Text) Then
                survey.s68 = txt_s68.Text.Replace(",", String.Empty)
            Else
                survey.s68 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s91.Text) Then
                survey.s91 = txt_s91.Text.Replace(",", String.Empty)
            Else
                survey.s91 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s114.Text) Then
                survey.s114 = txt_s114.Text.Replace(",", String.Empty)
            Else
                survey.s114 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s137.Text) Then
                survey.s137 = txt_s137.Text.Replace(",", String.Empty)
            Else
                survey.s137 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s160.Text) Then
                survey.s160 = txt_s160.Text.Replace(",", String.Empty)
            Else
                survey.s160 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s70.Text) Then
                survey.s70 = txt_s70.Text.Replace(",", String.Empty)
            Else
                survey.s70 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s93.Text) Then
                survey.s93 = txt_s93.Text.Replace(",", String.Empty)
            Else
                survey.s93 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s116.Text) Then
                survey.s116 = txt_s116.Text.Replace(",", String.Empty)
            Else
                survey.s116 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s139.Text) Then
                survey.s139 = txt_s139.Text.Replace(",", String.Empty)
            Else
                survey.s139 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s162.Text) Then
                survey.s162 = txt_s162.Text.Replace(",", String.Empty)
            Else
                survey.s162 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s71.Text) Then
                survey.s71 = txt_s71.Text.Replace(",", String.Empty)
            Else
                survey.s71 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s94.Text) Then
                survey.s94 = txt_s94.Text.Replace(",", String.Empty)
            Else
                survey.s94 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s117.Text) Then
                survey.s117 = txt_s117.Text.Replace(",", String.Empty)
            Else
                survey.s117 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s140.Text) Then
                survey.s140 = txt_s140.Text.Replace(",", String.Empty)
            Else
                survey.s140 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s163.Text) Then
                survey.s163 = txt_s163.Text.Replace(",", String.Empty)
            Else
                survey.s163 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s72.Text) Then
                survey.s72 = txt_s72.Text.Replace(",", String.Empty)
            Else
                survey.s72 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s95.Text) Then
                survey.s95 = txt_s95.Text.Replace(",", String.Empty)
            Else
                survey.s95 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s118.Text) Then
                survey.s118 = txt_s118.Text.Replace(",", String.Empty)
            Else
                survey.s118 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s141.Text) Then
                survey.s141 = txt_s141.Text.Replace(",", String.Empty)
            Else
                survey.s141 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s164.Text) Then
                survey.s164 = txt_s164.Text.Replace(",", String.Empty)
            Else
                survey.s164 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s73.Text) Then
                survey.s73 = txt_s73.Text.Replace(",", String.Empty)
            Else
                survey.s73 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s96.Text) Then
                survey.s96 = txt_s96.Text.Replace(",", String.Empty)
            Else
                survey.s96 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s119.Text) Then
                survey.s119 = txt_s119.Text.Replace(",", String.Empty)
            Else
                survey.s119 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s142.Text) Then
                survey.s142 = txt_s142.Text.Replace(",", String.Empty)
            Else
                survey.s142 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s165.Text) Then
                survey.s165 = txt_s165.Text.Replace(",", String.Empty)
            Else
                survey.s165 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s74.Text) Then
                survey.s74 = txt_s74.Text.Replace(",", String.Empty)
            Else
                survey.s74 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s97.Text) Then
                survey.s97 = txt_s97.Text.Replace(",", String.Empty)
            Else
                survey.s97 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s120.Text) Then
                survey.s120 = txt_s120.Text.Replace(",", String.Empty)
            Else
                survey.s120 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s143.Text) Then
                survey.s143 = txt_s143.Text.Replace(",", String.Empty)
            Else
                survey.s143 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s166.Text) Then
                survey.s166 = txt_s166.Text.Replace(",", String.Empty)
            Else
                survey.s166 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s601.Text) Then
                survey.s601 = txt_s601.Text.Replace(",", String.Empty)
            Else
                survey.s601 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s626.Text) Then
                survey.s626 = txt_s626.Text.Replace(",", String.Empty)
            Else
                survey.s626 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s602.Text) Then
                survey.s602 = txt_s602.Text.Replace(",", String.Empty)
            Else
                survey.s602 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s627.Text) Then
                survey.s627 = txt_s627.Text.Replace(",", String.Empty)
            Else
                survey.s627 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s604.Text) Then
                survey.s604 = txt_s604.Text.Replace(",", String.Empty)
            Else
                survey.s604 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s629.Text) Then
                survey.s629 = txt_s629.Text.Replace(",", String.Empty)
            Else
                survey.s629 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s605.Text) Then
                survey.s605 = txt_s605.Text.Replace(",", String.Empty)
            Else
                survey.s605 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s630.Text) Then
                survey.s630 = txt_s630.Text.Replace(",", String.Empty)
            Else
                survey.s630 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s606.Text) Then
                survey.s606 = txt_s606.Text.Replace(",", String.Empty)
            Else
                survey.s606 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s631.Text) Then
                survey.s631 = txt_s631.Text.Replace(",", String.Empty)
            Else
                survey.s631 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s607.Text) Then
                survey.s607 = txt_s607.Text.Replace(",", String.Empty)
            Else
                survey.s607 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s632.Text) Then
                survey.s632 = txt_s632.Text.Replace(",", String.Empty)
            Else
                survey.s632 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s608.Text) Then
                survey.s608 = txt_s608.Text.Replace(",", String.Empty)
            Else
                survey.s608 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s633.Text) Then
                survey.s633 = txt_s633.Text.Replace(",", String.Empty)
            Else
                survey.s633 = Nothing
            End If

            survey.s75 = cb_s75.Checked
            survey.s98 = cb_s98.Checked
            survey.s121 = cb_s121.Checked
            survey.s144 = cb_s144.Checked
            survey.s167 = cb_s167.Checked
            survey.s530 = cb_s530.Checked
            survey.s531 = cb_s531.Checked
            survey.s532 = cb_s532.Checked
            survey.s533 = cb_s533.Checked
            survey.s534 = cb_s534.Checked
            survey.s76 = cb_s76.Checked
            survey.s99 = cb_s99.Checked
            survey.s122 = cb_s122.Checked
            survey.s145 = cb_s145.Checked
            survey.s168 = cb_s168.Checked
            survey.s77 = cb_s77.Checked
            survey.s100 = cb_s100.Checked
            survey.s123 = cb_s123.Checked
            survey.s146 = cb_s146.Checked
            survey.s169 = cb_s169.Checked
            survey.s78 = cb_s78.Checked
            survey.s101 = cb_s101.Checked
            survey.s124 = cb_s124.Checked
            survey.s147 = cb_s147.Checked
            survey.s170 = cb_s170.Checked
            survey.s79 = cb_s79.Checked
            survey.s102 = cb_s102.Checked
            survey.s125 = cb_s125.Checked
            survey.s148 = cb_s148.Checked
            survey.s171 = cb_s171.Checked
            survey.s80 = cb_s80.Checked
            survey.s103 = cb_s103.Checked
            survey.s126 = cb_s126.Checked
            survey.s149 = cb_s149.Checked
            survey.s172 = cb_s172.Checked
            survey.s81 = cb_s81.Checked
            survey.s104 = cb_s104.Checked
            survey.s127 = cb_s127.Checked
            survey.s150 = cb_s150.Checked
            survey.s173 = cb_s173.Checked
            survey.s82 = cb_s82.Checked
            survey.s105 = cb_s105.Checked
            survey.s128 = cb_s128.Checked
            survey.s151 = cb_s151.Checked
            survey.s174 = cb_s174.Checked
            survey.s83 = cb_s83.Checked
            survey.s106 = cb_s106.Checked
            survey.s129 = cb_s129.Checked
            survey.s152 = cb_s152.Checked
            survey.s175 = cb_s175.Checked
            survey.s535 = cb_s535.Checked
            survey.s536 = cb_s536.Checked
            survey.s537 = cb_s537.Checked
            survey.s538 = cb_s538.Checked
            survey.s539 = cb_s539.Checked
            survey.s84 = cb_s84.Checked
            survey.s107 = cb_s107.Checked
            survey.s130 = cb_s130.Checked
            survey.s153 = cb_s153.Checked
            survey.s176 = cb_s176.Checked
            survey.s85 = cb_s85.Checked
            survey.s108 = cb_s108.Checked
            survey.s131 = cb_s131.Checked
            survey.s154 = cb_s154.Checked
            survey.s177 = cb_s177.Checked
            survey.s86 = cb_s86.Checked
            survey.s109 = cb_s109.Checked
            survey.s132 = cb_s132.Checked
            survey.s155 = cb_s155.Checked
            survey.s178 = cb_s178.Checked
            survey.s87 = cb_s87.Checked
            survey.s110 = cb_s110.Checked
            survey.s133 = cb_s133.Checked
            survey.s156 = cb_s156.Checked
            survey.s179 = cb_s179.Checked
            survey.s88 = cb_s88.Checked
            survey.s111 = cb_s111.Checked
            survey.s134 = cb_s134.Checked
            survey.s157 = cb_s157.Checked
            survey.s180 = cb_s180.Checked
            survey.s89 = cb_s89.Checked
            survey.s112 = cb_s112.Checked
            survey.s135 = cb_s135.Checked
            survey.s158 = cb_s158.Checked
            survey.s181 = cb_s181.Checked
            survey.s609 = cb_s609.Checked
            survey.s634 = cb_s634.Checked
            survey.s610 = cb_s610.Checked
            survey.s635 = cb_s635.Checked
            survey.s611 = cb_s611.Checked
            survey.s636 = cb_s636.Checked
            survey.s612 = cb_s612.Checked
            survey.s637 = cb_s637.Checked
            survey.s613 = cb_s613.Checked
            survey.s638 = cb_s638.Checked
            survey.s614 = cb_s614.Checked
            survey.s639 = cb_s639.Checked
            survey.s615 = cb_s615.Checked
            survey.s640 = cb_s640.Checked
            survey.s616 = cb_s616.Checked
            survey.s641 = cb_s641.Checked
            survey.s617 = cb_s617.Checked
            survey.s642 = cb_s642.Checked
            survey.s618 = cb_s618.Checked
            survey.s643 = cb_s643.Checked
            survey.s619 = cb_s619.Checked
            survey.s644 = cb_s644.Checked
            survey.s620 = cb_s620.Checked
            survey.s645 = cb_s645.Checked
            survey.s621 = cb_s621.Checked
            survey.s646 = cb_s646.Checked
            survey.s622 = cb_s622.Checked
            survey.s647 = cb_s647.Checked
            survey.s623 = cb_s623.Checked
            survey.s648 = cb_s648.Checked
            survey.s624 = cb_s624.Checked
            survey.s649 = cb_s649.Checked
            survey.s625 = cb_s625.Checked
            survey.s650 = cb_s650.Checked
            survey.s541 = cb_s541.Checked
            survey.s543 = cb_s543.Checked
            survey.s545 = cb_s545.Checked
            survey.s542 = cb_s542.Checked
            survey.s544 = cb_s544.Checked
            survey.s546 = cb_s546.Checked
            survey.section3Complete = cb_section3Complete.Checked

            If rb_s540_1.Checked Then survey.s540 = 1
            If rb_s540_2.Checked Then survey.s540 = 2


            survey.IsAuditComplete = False
            db.SaveChanges()
            lbl_lastSave.Text = survey.ModifiedDate
        End Using
    End Sub
    Protected Sub btn_previous_Click(sender As Object, e As EventArgs) Handles btn_previous.Click
        SaveSurvey(False)
        Response.Redirect("Recruiting.aspx")
    End Sub
    Protected Sub btn_save_Click(sender As Object, e As EventArgs) Handles btn_save.Click
        SaveSurvey(False)
    End Sub
    Protected Sub btn_next_Click(sender As Object, e As EventArgs) Handles btn_next.Click
        SaveSurvey(False)
        Response.Redirect("CompByJob.aspx")
    End Sub
    Protected Sub ddl_location_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddl_location.SelectedIndexChanged
        SaveSurvey(False)
        Session("LocationID") = ddl_location.SelectedValue
        txt_LocationID.Text = ddl_location.SelectedValue
        Response.Redirect("Compensation.aspx")
    End Sub
    Protected Sub lb_toc_bot_Click(sender As Object, e As EventArgs) Handles lb_toc_bot.Click
        SaveSurvey(False)
        Response.Redirect("TableOfContents.aspx")
    End Sub
End Class
