﻿
Imports Models

Partial Class Surveys_BranchCompBen
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Session("OrganizationId") Is Nothing Or Session("OrganizationId") = 0 Then
            Response.Redirect("../../account/login.aspx")
        End If


        If Session("LocationID") Is Nothing Then
            If Not String.IsNullOrEmpty(txt_LocationID.Text) Then
                Session("LocationID") = txt_LocationID.Text
            Else
                Response.Redirect("../../account/login.aspx")
            End If
        End If
        If Session("UserId") Is Nothing Then
            If Not String.IsNullOrEmpty(txt_userid.Text) Then
                Session("UserId") = Convert.ToInt32(txt_userid.Text)
            Else
                Response.Redirect("../../account/login.aspx")
            End If
        End If
        If Not IsPostBack Then
            Dim LocationID As Integer = Session("LocationID")
            txt_LocationID.Text = LocationID
            Dim userid As Integer = Session("UserId")
            txt_userid.Text = userid
            If Session("Client") IsNot Nothing And Not String.IsNullOrEmpty(Session("Client")) Then
                lbl_deadline.Text = SurveyHelper.ReturnDeadline(Session("Client").ToString)
            Else
                lbl_deadline.Text = "Friday, April 13, 2018"
            End If
            btn_next.Text = "Home"
            ddl_location.Items.Clear()
            Dim locations As List(Of ListItem) = SurveyHelper.ReturnLocationList(Session("OrganizationId"))
            For Each l In locations
                ddl_location.Items.Add(l)
            Next
            ddl_location.SelectedValue = Session("LocationID")
            tbl_location.Visible = True
            LoadSurvey()
        End If
    End Sub
    Sub LoadSurvey()
        Using db As New IICICEntities
            Dim LocationID As Integer = txt_LocationID.Text
            Dim survey As New CompBranchSurvey
            survey = (From s In db.CompBranchSurveys Where s.LocationID = LocationID And s.SurveyYear = Now.Year).FirstOrDefault
            If survey IsNot Nothing Then
                lbl_lastSave.Text = survey.ModifiedDate

                '---PASTE LOAD STATEMENTS---
                If Not String.IsNullOrEmpty(survey.s388oth) Then
                    txt_s388oth.Text = survey.s388oth
                End If
                If Not String.IsNullOrEmpty(survey.s389oth) Then
                    txt_s389oth.Text = survey.s389oth
                End If
                If survey.s390 IsNot Nothing Then
                    txt_s390.Text = String.Format(survey.s390, "N0")
                End If
                If survey.s391 IsNot Nothing Then
                    txt_s391.Text = String.Format(survey.s391, "N0")
                End If
                If survey.s392 IsNot Nothing Then
                    txt_s392.Text = String.Format(survey.s392, "N0")
                End If
                If survey.s393 IsNot Nothing Then
                    txt_s393.Text = String.Format(survey.s393, "N0")
                End If
                If survey.s394 IsNot Nothing Then
                    txt_s394.Text = String.Format(survey.s394, "N0")
                End If
                If survey.s395 IsNot Nothing Then
                    txt_s395.Text = String.Format(survey.s395, "N0")
                End If
                If Not String.IsNullOrEmpty(survey.s401oth) Then
                    txt_s401oth.Text = survey.s401oth
                End If
                If survey.s402 IsNot Nothing Then
                    txt_s402.Text = String.Format(survey.s402, "N0")
                End If
                If survey.s502 IsNot Nothing Then
                    txt_s502.Text = String.Format(survey.s502, "N0")
                End If
                If survey.s403 IsNot Nothing Then
                    txt_s403.Text = String.Format(survey.s403, "N0")
                End If
                If survey.s503 IsNot Nothing Then
                    txt_s503.Text = String.Format(survey.s503, "N0")
                End If
                If survey.s404 IsNot Nothing Then
                    txt_s404.Text = String.Format(survey.s404, "N0")
                End If
                If survey.s504 IsNot Nothing Then
                    txt_s504.Text = String.Format(survey.s504, "N0")
                End If
                If survey.s405 IsNot Nothing Then
                    txt_s405.Text = String.Format(survey.s405, "N0")
                End If
                If survey.s505 IsNot Nothing Then
                    txt_s505.Text = String.Format(survey.s505, "N0")
                End If
                If survey.s406 IsNot Nothing Then
                    txt_s406.Text = String.Format(survey.s406, "N0")
                End If
                If survey.s506 IsNot Nothing Then
                    txt_s506.Text = String.Format(survey.s506, "N0")
                End If
                If survey.s407 IsNot Nothing Then
                    txt_s407.Text = String.Format(survey.s407, "N0")
                End If
                If survey.s507 IsNot Nothing Then
                    txt_s507.Text = String.Format(survey.s507, "N0")
                End If
                If survey.s408 IsNot Nothing Then
                    txt_s408.Text = String.Format(survey.s408, "N0")
                End If
                If survey.s508 IsNot Nothing Then
                    txt_s508.Text = String.Format(survey.s508, "N0")
                End If
                If survey.s409 IsNot Nothing Then
                    txt_s409.Text = String.Format(survey.s409, "N0")
                End If
                If survey.s509 IsNot Nothing Then
                    txt_s509.Text = String.Format(survey.s509, "N0")
                End If
                If survey.s410 IsNot Nothing Then
                    txt_s410.Text = String.Format(survey.s410, "N0")
                End If
                If survey.s510 IsNot Nothing Then
                    txt_s510.Text = String.Format(survey.s510, "N0")
                End If
                If survey.s411 IsNot Nothing Then
                    txt_s411.Text = String.Format(survey.s411, "N0")
                End If
                If survey.s511 IsNot Nothing Then
                    txt_s511.Text = String.Format(survey.s511, "N0")
                End If
                If survey.s412 IsNot Nothing Then
                    txt_s412.Text = String.Format(survey.s412, "N0")
                End If
                If survey.s512 IsNot Nothing Then
                    txt_s512.Text = String.Format(survey.s512, "N0")
                End If
                If survey.s413 IsNot Nothing Then
                    txt_s413.Text = String.Format(survey.s413, "N0")
                End If
                If survey.s513 IsNot Nothing Then
                    txt_s513.Text = String.Format(survey.s513, "N0")
                End If

                If survey.s691 IsNot Nothing Then
                    txt_s691.Text = String.Format(survey.s691, "N0")
                End If
                If survey.s692 IsNot Nothing Then
                    txt_s692.Text = String.Format(survey.s692, "N0")
                End If
                If survey.s693 IsNot Nothing Then
                    txt_s693.Text = String.Format(survey.s693, "N0")
                End If
                If survey.s694 IsNot Nothing Then
                    txt_s694.Text = String.Format(survey.s694, "N0")
                End If
                If survey.s695 IsNot Nothing Then
                    txt_s695.Text = String.Format(survey.s695, "N0")
                End If
                If survey.s696 IsNot Nothing Then
                    txt_s696.Text = String.Format(survey.s696, "N0")
                End If

                If Not String.IsNullOrEmpty(survey.s420oth) Then
                    txt_s420oth.Text = survey.s420oth
                End If
                If Not String.IsNullOrEmpty(survey.s520oth) Then
                    txt_s520oth.Text = survey.s520oth
                End If
                If Not String.IsNullOrEmpty(survey.s421oth) Then
                    txt_s421oth.Text = survey.s421oth
                End If
                If Not String.IsNullOrEmpty(survey.s521oth) Then
                    txt_s521oth.Text = survey.s521oth
                End If

                If survey.s397 IsNot Nothing Then
                    cb_s397.Checked = survey.s397
                End If
                If survey.s398 IsNot Nothing Then
                    cb_s398.Checked = survey.s398
                End If
                If survey.s573 IsNot Nothing Then
                    cb_s573.Checked = survey.s573
                End If
                If survey.s399 IsNot Nothing Then
                    cb_s399.Checked = survey.s399
                End If
                If survey.s400 IsNot Nothing Then
                    cb_s400.Checked = survey.s400
                End If
                If survey.s401 IsNot Nothing Then
                    cb_s401.Checked = survey.s401
                End If
                If survey.s415 IsNot Nothing Then
                    cb_s415.Checked = survey.s415
                End If
                If survey.s416 IsNot Nothing Then
                    cb_s416.Checked = survey.s416
                End If
                If survey.s417 IsNot Nothing Then
                    cb_s417.Checked = survey.s417
                End If
                If survey.s418 IsNot Nothing Then
                    cb_s418.Checked = survey.s418
                End If
                If survey.s419 IsNot Nothing Then
                    cb_s419.Checked = survey.s419
                End If
                If survey.s515 IsNot Nothing Then
                    cb_s515.Checked = survey.s515
                End If
                If survey.s516 IsNot Nothing Then
                    cb_s516.Checked = survey.s516
                End If
                If survey.s517 IsNot Nothing Then
                    cb_s517.Checked = survey.s517
                End If
                If survey.s518 IsNot Nothing Then
                    cb_s518.Checked = survey.s518
                End If
                If survey.s519 IsNot Nothing Then
                    cb_s519.Checked = survey.s519
                End If
                If survey.sBranchComplete IsNot Nothing Then
                    cb_sBranchComplete.Checked = survey.sBranchComplete
                End If

                If survey.s396 IsNot Nothing Then
                    If survey.s396 = 1 Then rb_s396_1.Checked = True
                End If
                If survey.s396 IsNot Nothing Then
                    If survey.s396 = 2 Then rb_s396_2.Checked = True
                End If
                If survey.s414 IsNot Nothing Then
                    If survey.s414 = 1 Then rb_s414_1.Checked = True
                End If
                If survey.s414 IsNot Nothing Then
                    If survey.s414 = 2 Then rb_s414_2.Checked = True
                End If
                If survey.s514 IsNot Nothing Then
                    If survey.s514 = 1 Then rb_s514_1.Checked = True
                End If
                If survey.s514 IsNot Nothing Then
                    If survey.s514 = 2 Then rb_s514_2.Checked = True
                End If

                If survey.s420 IsNot Nothing Then
                    drop_s420.SelectedValue = survey.s420
                End If
                If survey.s520 IsNot Nothing Then
                    drop_s520.SelectedValue = survey.s520
                End If
                If survey.s421 IsNot Nothing Then
                    drop_s421.SelectedValue = survey.s421
                End If
                If survey.s521 IsNot Nothing Then
                    drop_s521.SelectedValue = survey.s521
                End If




            End If
        End Using
    End Sub
    Sub SaveSurvey(ByVal IsSubmit As Boolean)
        Using db As New Models.IICICEntities
            Dim existingSurvey As Boolean = False
            Dim LocationID As Integer = txt_LocationID.Text
            Dim userid As Integer = Convert.ToInt16(Session("UserId"))
            Dim survey As New CompBranchSurvey
            survey = (From s In db.CompBranchSurveys Where s.LocationID = LocationID And s.SurveyYear = Now.Year).FirstOrDefault
            If survey Is Nothing Then
                survey = New CompBranchSurvey
                db.CompBranchSurveys.Add(survey)
                db.SaveChanges()
            End If
            survey.LocationID = LocationID
            survey.LastModUserId = userid
            survey.ModifiedDate = Now
            survey.SurveyYear = Now.Year
            If IsSubmit Then
                survey.SubmitDate = Now
            End If

            '---PASTE SAVE STATEMENTS---
            If Not String.IsNullOrEmpty(txt_s388oth.Text) Then
                survey.s388oth = txt_s388oth.Text.Replace(",", String.Empty)
            Else
                survey.s388oth = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s389oth.Text) Then
                survey.s389oth = txt_s389oth.Text.Replace(",", String.Empty)
            Else
                survey.s389oth = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s390.Text) Then
                survey.s390 = txt_s390.Text.Replace(",", String.Empty)
            Else
                survey.s390 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s391.Text) Then
                survey.s391 = txt_s391.Text.Replace(",", String.Empty)
            Else
                survey.s391 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s392.Text) Then
                survey.s392 = txt_s392.Text.Replace(",", String.Empty)
            Else
                survey.s392 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s393.Text) Then
                survey.s393 = txt_s393.Text.Replace(",", String.Empty)
            Else
                survey.s393 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s394.Text) Then
                survey.s394 = txt_s394.Text.Replace(",", String.Empty)
            Else
                survey.s394 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s395.Text) Then
                survey.s395 = txt_s395.Text.Replace(",", String.Empty)
            Else
                survey.s395 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s401oth.Text) Then
                survey.s401oth = txt_s401oth.Text.Replace(",", String.Empty)
            Else
                survey.s401oth = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s402.Text) Then
                survey.s402 = txt_s402.Text.Replace(",", String.Empty)
            Else
                survey.s402 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s502.Text) Then
                survey.s502 = txt_s502.Text.Replace(",", String.Empty)
            Else
                survey.s502 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s403.Text) Then
                survey.s403 = txt_s403.Text.Replace(",", String.Empty)
            Else
                survey.s403 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s503.Text) Then
                survey.s503 = txt_s503.Text.Replace(",", String.Empty)
            Else
                survey.s503 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s404.Text) Then
                survey.s404 = txt_s404.Text.Replace(",", String.Empty)
            Else
                survey.s404 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s504.Text) Then
                survey.s504 = txt_s504.Text.Replace(",", String.Empty)
            Else
                survey.s504 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s405.Text) Then
                survey.s405 = txt_s405.Text.Replace(",", String.Empty)
            Else
                survey.s405 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s505.Text) Then
                survey.s505 = txt_s505.Text.Replace(",", String.Empty)
            Else
                survey.s505 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s406.Text) Then
                survey.s406 = txt_s406.Text.Replace(",", String.Empty)
            Else
                survey.s406 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s506.Text) Then
                survey.s506 = txt_s506.Text.Replace(",", String.Empty)
            Else
                survey.s506 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s407.Text) Then
                survey.s407 = txt_s407.Text.Replace(",", String.Empty)
            Else
                survey.s407 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s507.Text) Then
                survey.s507 = txt_s507.Text.Replace(",", String.Empty)
            Else
                survey.s507 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s408.Text) Then
                survey.s408 = txt_s408.Text.Replace(",", String.Empty)
            Else
                survey.s408 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s508.Text) Then
                survey.s508 = txt_s508.Text.Replace(",", String.Empty)
            Else
                survey.s508 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s409.Text) Then
                survey.s409 = txt_s409.Text.Replace(",", String.Empty)
            Else
                survey.s409 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s509.Text) Then
                survey.s509 = txt_s509.Text.Replace(",", String.Empty)
            Else
                survey.s509 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s410.Text) Then
                survey.s410 = txt_s410.Text.Replace(",", String.Empty)
            Else
                survey.s410 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s510.Text) Then
                survey.s510 = txt_s510.Text.Replace(",", String.Empty)
            Else
                survey.s510 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s411.Text) Then
                survey.s411 = txt_s411.Text.Replace(",", String.Empty)
            Else
                survey.s411 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s511.Text) Then
                survey.s511 = txt_s511.Text.Replace(",", String.Empty)
            Else
                survey.s511 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s412.Text) Then
                survey.s412 = txt_s412.Text.Replace(",", String.Empty)
            Else
                survey.s412 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s512.Text) Then
                survey.s512 = txt_s512.Text.Replace(",", String.Empty)
            Else
                survey.s512 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s413.Text) Then
                survey.s413 = txt_s413.Text.Replace(",", String.Empty)
            Else
                survey.s413 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s513.Text) Then
                survey.s513 = txt_s513.Text.Replace(",", String.Empty)
            Else
                survey.s513 = Nothing
            End If


            If Not String.IsNullOrEmpty(txt_s691.Text) Then
                survey.s691 = txt_s691.Text.Replace(",", String.Empty)
            Else
                survey.s691 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s692.Text) Then
                survey.s692 = txt_s692.Text.Replace(",", String.Empty)
            Else
                survey.s692 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s693.Text) Then
                survey.s693 = txt_s693.Text.Replace(",", String.Empty)
            Else
                survey.s693 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s694.Text) Then
                survey.s694 = txt_s694.Text.Replace(",", String.Empty)
            Else
                survey.s694 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s695.Text) Then
                survey.s695 = txt_s695.Text.Replace(",", String.Empty)
            Else
                survey.s695 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s696.Text) Then
                survey.s696 = txt_s696.Text.Replace(",", String.Empty)
            Else
                survey.s696 = Nothing
            End If

            If Not String.IsNullOrEmpty(txt_s420oth.Text) Then
                survey.s420oth = txt_s420oth.Text.Replace(",", String.Empty)
            Else
                survey.s420oth = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s520oth.Text) Then
                survey.s520oth = txt_s520oth.Text.Replace(",", String.Empty)
            Else
                survey.s520oth = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s421oth.Text) Then
                survey.s421oth = txt_s421oth.Text.Replace(",", String.Empty)
            Else
                survey.s421oth = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s521oth.Text) Then
                survey.s521oth = txt_s521oth.Text.Replace(",", String.Empty)
            Else
                survey.s521oth = Nothing
            End If

            survey.s397 = cb_s397.Checked
            survey.s398 = cb_s398.Checked
            survey.s573 = cb_s573.Checked
            survey.s399 = cb_s399.Checked
            survey.s400 = cb_s400.Checked
            survey.s401 = cb_s401.Checked
            survey.s415 = cb_s415.Checked
            survey.s416 = cb_s416.Checked
            survey.s417 = cb_s417.Checked
            survey.s418 = cb_s418.Checked
            survey.s419 = cb_s419.Checked
            survey.s515 = cb_s515.Checked
            survey.s516 = cb_s516.Checked
            survey.s517 = cb_s517.Checked
            survey.s518 = cb_s518.Checked
            survey.s519 = cb_s519.Checked
            survey.sBranchComplete = cb_sBranchComplete.Checked

            If rb_s396_1.Checked Then survey.s396 = 1
            If rb_s396_2.Checked Then survey.s396 = 2
            If rb_s414_1.Checked Then survey.s414 = 1
            If rb_s414_2.Checked Then survey.s414 = 2
            If rb_s514_1.Checked Then survey.s514 = 1
            If rb_s514_2.Checked Then survey.s514 = 2

            If Not String.IsNullOrEmpty(drop_s420.SelectedValue) Then
                survey.s420 = drop_s420.SelectedValue
            Else
                survey.s420 = Nothing
            End If
            If Not String.IsNullOrEmpty(drop_s520.SelectedValue) Then
                survey.s520 = drop_s520.SelectedValue
            Else
                survey.s520 = Nothing
            End If
            If Not String.IsNullOrEmpty(drop_s421.SelectedValue) Then
                survey.s421 = drop_s421.SelectedValue
            Else
                survey.s421 = Nothing
            End If
            If Not String.IsNullOrEmpty(drop_s521.SelectedValue) Then
                survey.s521 = drop_s521.SelectedValue
            Else
                survey.s521 = Nothing
            End If



            survey.IsAuditComplete = False
            db.SaveChanges()
            lbl_lastSave.Text = survey.ModifiedDate
        End Using
    End Sub
    Protected Sub btn_previous_Click(sender As Object, e As EventArgs) Handles btn_previous.Click
        SaveSurvey(False)
        Session("LocationID") = SurveyHelper.ReturnDefaultLocation(Session("OrganizationId"))
        Response.Redirect("Sales.aspx")
    End Sub
    Protected Sub btn_save_Click(sender As Object, e As EventArgs) Handles btn_save.Click
        SaveSurvey(False)
    End Sub
    Protected Sub btn_next_Click(sender As Object, e As EventArgs) Handles btn_next.Click
        SaveSurvey(False)
        Session("LocationID") = SurveyHelper.ReturnDefaultLocation(Session("OrganizationId"))
        Response.Redirect("TableOfContents.aspx")
    End Sub
    Protected Sub ddl_location_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddl_location.SelectedIndexChanged
        SaveSurvey(False)
        Session("LocationID") = ddl_location.SelectedValue
        txt_LocationID.Text = ddl_location.SelectedValue
        Response.Redirect("BranchCompBen.aspx")
    End Sub
    Protected Sub btn_AddLocation_Click(sender As Object, e As EventArgs) Handles btn_AddLocation.Click
        'txt_s389oth

        Try
            SaveSurvey(False)

            Using db As New IICICEntities
                Dim orgid As Integer = Session("OrganizationId")
                If orgid <> 0 Then
                    Dim ds As New DataSet With {.CreateDate = Now(), .Name = txt_LocationName.Text, .OrganizationId = orgid}
                    db.DataSets.Add(ds)
                    db.SaveChanges()

                    Session("LocationID") = ds.LocationID 'NOTE: LocationID is being used instead of IICode per SH
                    Response.Redirect("BranchCompBen.aspx")
                Else
                    Dim err As New CustomValidator With {.IsValid = False, .ValidationGroup = "Modal", .ErrorMessage = "Unable to add location."}
                    Page.Validators.Add(err)
                    'Response.End()
                End If
            End Using



        Catch ex As Exception
            Dim err As New CustomValidator With {.IsValid = False, .ValidationGroup = "Modal", .ErrorMessage = "Unable to add location."}
            Page.Validators.Add(err)
        End Try

    End Sub
End Class
