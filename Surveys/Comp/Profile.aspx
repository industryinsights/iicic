﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Survey.master"  MaintainScrollPositionOnPostback="true" AutoEventWireup="false" CodeFile="Profile.aspx.vb" Inherits="Surveys_Profile" %>

<%@ Register Src="~/Controls/SurveyNavigation.ascx" TagPrefix="uc1" TagName="SurveyNavigation" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script type="text/javascript">
        $(document).ready(function () {
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
    <script src="../../Scripts/autoNumeric/autoNumeric-min.js"></script>
    <script src="../../Scripts/Profile.js?v=1"></script>
    <asp:TextBox ID="txt_LocationID" runat="server" Visible="false"></asp:TextBox>
    <asp:TextBox ID="txt_userid" runat="server" Visible="false"></asp:TextBox>
    <div class="bg-content">
        <div class="row">
            <table class="table-condensed centered" runat="server" id="tbl_location">
                <tr>
                    <td>
                        <asp:DropDownList ID="ddl_location" runat="server" CssClass="form-control" AutoPostBack="true"></asp:DropDownList>
                    </td>
                    <td><span class="glyphicon glyphicon-info-sign icon-info" data-placement="left" data-toggle="tooltip" title="This box is disabled by default.  If you are submitting for multiple locations, please add the locations on the Organization Admin page and you'll be able to cycle through them here."></span></td>
                </tr>
            </table>

        </div>

        <div class="">
            <h3 class="text-center">Deadline: 
                <asp:Label ID="lbl_deadline" runat="server" Text=""></asp:Label></h3>
        </div>
        <div class=""><span class="col-md-12 text-center text-muted"><em>Last updated: 
            <asp:Label ID="lbl_lastSave" runat="server" Text=""></asp:Label></em></span></div>

        <h2 class="page-header">General Information</h2>
        <h3 class="h3-header">Association Membership</h3>

        <p>&nbsp;</p>
        <div class="row tr-question">
            <div class="col-md-12">To which of the following associations is your organization a member?</div>
        </div>
        <div class="row">
            <div class="col-md-8">
                <asp:CheckBox CssClass="checkbox-inline" ID="cb_s1900" runat="server" Text="AED - Associated Equipment Distributors" />
            </div>
        </div>
        <div class="row">
            <div class="col-md-8">
                <asp:CheckBox CssClass="checkbox-inline" ID="cb_s1901" runat="server" Text="ASA - American Supply Association" />
            </div>
        </div>
        <div class="row">
            <div class="col-md-8">
                <asp:CheckBox CssClass="checkbox-inline" ID="cb_s1902" runat="server" Text="CDA - Convenience Distribution Association (formerly AWMA)" />
            </div>
        </div>
        <div class="row">
            <div class="col-md-8">
                <asp:CheckBox CssClass="checkbox-inline" ID="cb_s1903" runat="server" Text="CTDA - Ceramic Tile Distributors Association" />
            </div>
        </div>
        <div class="row">
            <div class="col-md-8">
                <asp:CheckBox CssClass="checkbox-inline" ID="cb_s1904" runat="server" Text="FPDA - The FPDA Motion & Control Network" />
            </div>
        </div>
        <div class="row">
            <div class="col-md-8">
                <asp:CheckBox CssClass="checkbox-inline" ID="cb_s1905" runat="server" Text="HARDI - Heating, Air-conditioning and Refrigeration Distributors International" />
            </div>
        </div>
        <div class="row">
            <div class="col-md-8">
                <asp:CheckBox CssClass="checkbox-inline" ID="cb_s1906" runat="server" Text="HDA - Healthcare Distribution Alliance" />
            </div>
        </div>
        <div class="row">
            <div class="col-md-8">
                <asp:CheckBox CssClass="checkbox-inline" ID="cb_s1907" runat="server" Text="IAPD - International Association of Plastics Distribution" />
            </div>
        </div>
        <div class="row">
            <div class="col-md-8">
                <asp:CheckBox CssClass="checkbox-inline" ID="cb_s1908" runat="server" Text="IFDA - International Foodservice Distributors Association" />
            </div>
        </div>
        <div class="row">
            <div class="col-md-8">
                <asp:CheckBox CssClass="checkbox-inline" ID="cb_s1909" runat="server" Text="ISA - Industrial Supply Association" />
            </div>
        </div>
        <div class="row">
            <div class="col-md-8">
                <asp:CheckBox CssClass="checkbox-inline" ID="cb_s1910" runat="server" Text="MHEDA - Material Handling Equipment Distributors Association" />
            </div>
        </div>
        <div class="row">
            <div class="col-md-8">
                <asp:CheckBox CssClass="checkbox-inline" ID="cb_s1911" runat="server" Text="NAED - National Association of Electrical Distributors" />
            </div>
        </div>
        <div class="row">
            <div class="col-md-8">
                <asp:CheckBox CssClass="checkbox-inline" ID="cb_s1912" runat="server" Text="NAFCD - North American Association of Floor Covering Distributors" />
            </div>
        </div>
        <div class="row">
            <div class="col-md-8">
                <asp:CheckBox CssClass="checkbox-inline" ID="cb_s1913" runat="server" Text="NAHAD - The Association for Hose & Accessories Distribution" />
            </div>
        </div>
        <div class="row">
            <div class="col-md-8">
                <asp:CheckBox CssClass="checkbox-inline" ID="cb_s1914" runat="server" Text="NAW - National Association of Wholesaler-Distributors" />
            </div>
        </div>
        <div class="row">
            <div class="col-md-8">
                <asp:CheckBox CssClass="checkbox-inline" ID="cb_s1915" runat="server" Text="NAWLA - North American Wholesale Lumber Association" />
            </div>
        </div>
        <div class="row">
            <div class="col-md-8">
                <asp:CheckBox CssClass="checkbox-inline" ID="cb_s1916" runat="server" Text="NBMDA - North American Building Material Distribution Association" />
            </div>
        </div>
        <div class="row">
            <div class="col-md-8">
                <asp:CheckBox CssClass="checkbox-inline" ID="cb_s1917" runat="server" Text="NBWA - National Beer Wholesalers Association" />
            </div>
        </div>
        <div class="row">
            <div class="col-md-8">
                <asp:CheckBox CssClass="checkbox-inline" ID="cb_s1918" runat="server" Text="NPTA - NPTA Alliance" />
            </div>
        </div>
        <div class="row">
            <div class="col-md-8">
                <asp:CheckBox CssClass="checkbox-inline" ID="cb_s1919" runat="server" Text="PEI - The Petroleum Equipment Institute" />
            </div>
        </div>
        <div class="row">
            <div class="col-md-8">
                <asp:CheckBox CssClass="checkbox-inline" ID="cb_s1920" runat="server" Text="PIDA - Pet Industry Distributors Association " />
            </div>
        </div>
        <div class="row">
            <div class="col-md-8">
                <asp:CheckBox CssClass="checkbox-inline" ID="cb_s1921" runat="server" Text="STAFDA - Specialty Tools & Fasteners Distributors Association" />
            </div>
        </div>
        <div class="row">
            <div class="col-md-8">
                <asp:CheckBox CssClass="checkbox-inline" ID="cb_s1922" runat="server" Text="WF&FSA - Wholesale Florist & Florist Supplier Association" />
            </div>
        </div>
        <div class="row hidden">
            <div class="col-md-8">
                <asp:CheckBox CssClass="checkbox-inline" ID="cb_s1923" runat="server" Text="WF&FSA - Wholesale Florist & Florist Supplier Association" />
            </div>
        </div>
        <div class="row hidden">
            <div class="col-md-8">
                <asp:CheckBox CssClass="checkbox-inline" ID="cb_s1924" runat="server" Text="TBD" />
            </div>
        </div>
        <div class="row hidden">
            <div class="col-md-8">
                <asp:CheckBox CssClass="checkbox-inline" ID="cb_s1925" runat="server" Text="TBD" />
            </div>
        </div>
        <div class="row hidden">
            <div class="col-md-8">
                <asp:CheckBox CssClass="checkbox-inline" ID="cb_s1926" runat="server" Text="TBD" />
            </div>
        </div>
        <div class="row hidden">
            <div class="col-md-8">
                <asp:CheckBox CssClass="checkbox-inline" ID="cb_s1927" runat="server" Text="TBD" />
            </div>
        </div>
        <div class="row hidden">
            <div class="col-md-8">
                <asp:CheckBox CssClass="checkbox-inline" ID="cb_s1928" runat="server" Text="TBD" />
            </div>
        </div>
        <div class="row hidden">
            <div class="col-md-8">
                <asp:CheckBox CssClass="checkbox-inline" ID="cb_s1929" runat="server" Text="TBD" />
            </div>
        </div>
        <div class="row hidden">
            <div class="col-md-8">
                <asp:CheckBox CssClass="checkbox-inline" ID="cb_s1930" runat="server" Text="TBD" />
            </div>
        </div>
        <div class="row hidden">
            <div class="col-md-8">
                <asp:CheckBox CssClass="checkbox-inline" ID="cb_s1931" runat="server" Text="TBD" />
            </div>
        </div>
        <div class="row hidden">
            <div class="col-md-8">
                <asp:CheckBox CssClass="checkbox-inline" ID="cb_s1932" runat="server" Text="TBD" />
            </div>
        </div>
        <div class="row hidden">
            <div class="col-md-8">
                <asp:CheckBox CssClass="checkbox-inline" ID="cb_s1933" runat="server" Text="TBD" />
            </div>
        </div>
        <div class="row hidden">
            <div class="col-md-8">
                <asp:CheckBox CssClass="checkbox-inline" ID="cb_s1934" runat="server" Text="TBD" />
            </div>
        </div>
        <div class="row hidden">
            <div class="col-md-8">
                <asp:CheckBox CssClass="checkbox-inline" ID="cb_s1935" runat="server" Text="TBD" />
            </div>
        </div>
        <div class="row hidden">
            <div class="col-md-8">
                <asp:CheckBox CssClass="checkbox-inline" ID="cb_s1936" runat="server" Text="TBD" />
            </div>
        </div>
        <div class="row hidden">
            <div class="col-md-8">
                <asp:CheckBox CssClass="checkbox-inline" ID="cb_s1937" runat="server" Text="TBD" />
            </div>
        </div>
        <div class="row hidden">
            <div class="col-md-8">
                <asp:CheckBox CssClass="checkbox-inline" ID="cb_s1938" runat="server" Text="TBD" />
            </div>
        </div>
        <div class="row hidden">
            <div class="col-md-8">
                <asp:CheckBox CssClass="checkbox-inline" ID="cb_s1939" runat="server" Text="TBD" />
            </div>
        </div>
        <div class="row hidden">
            <div class="col-md-8">
                <asp:CheckBox CssClass="checkbox-inline" ID="cb_s1940" runat="server" Text="TBD" />
            </div>
        </div>
        <div class="row hidden">
            <div class="col-md-8">
                <asp:CheckBox CssClass="checkbox-inline" ID="cb_s1941" runat="server" Text="TBD" />
            </div>
        </div>
        <div class="row hidden">
            <div class="col-md-8">
                <asp:CheckBox CssClass="checkbox-inline" ID="cb_s1942" runat="server" Text="TBD" />
            </div>
        </div>
        <div class="row hidden">
            <div class="col-md-8">
                <asp:CheckBox CssClass="checkbox-inline" ID="cb_s1943" runat="server" Text="TBD" />
            </div>
        </div>
        <div class="row hidden">
            <div class="col-md-8">
                <asp:CheckBox CssClass="checkbox-inline" ID="cb_s1944" runat="server" Text="TBD" />
            </div>
        </div>
        <div class="row hidden">
            <div class="col-md-8">
                <asp:CheckBox CssClass="checkbox-inline" ID="cb_s1945" runat="server" Text="TBD" />
            </div>
        </div>
        <div class="row hidden">
            <div class="col-md-8">
                <asp:CheckBox CssClass="checkbox-inline" ID="cb_s1946" runat="server" Text="TBD" />
            </div>
        </div>
        <div class="row hidden">
            <div class="col-md-8">
                <asp:CheckBox CssClass="checkbox-inline" ID="cb_s1947" runat="server" Text="TBD" />
            </div>
        </div>
        <div class="row hidden">
            <div class="col-md-8">
                <asp:CheckBox CssClass="checkbox-inline" ID="cb_s1948" runat="server" Text="TBD" />
            </div>
        </div>
        <div class="row hidden">
            <div class="col-md-8">
                <asp:CheckBox CssClass="checkbox-inline" ID="cb_s1949" runat="server" Text="TBD" />
            </div>
        </div>
        <div class="row hidden">
            <div class="col-md-8">
                <asp:CheckBox CssClass="checkbox-inline" ID="cb_s1950" runat="server" Text="TBD" />
            </div>
        </div>
        <hr />

        <p>&nbsp;</p>
        <h3 class="h3-header">Please tell us about your company (all locations, including headquarters)</h3>

        <p>&nbsp;</p>


        <div style="display: none;" id="DIV1900">
            AED
    <div class="row tr-question">
        <div class="col-md-12">What is the primary type of business for which you are reporting?</div>
    </div>
            <div class="row">
                <div class="col-md-8">
                    <asp:RadioButton ID="rb_s700_1" runat="server" GroupName="s700" Text="Equipment Over 100 HP (heavy)" />
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    <asp:RadioButton ID="rb_s700_2" runat="server" GroupName="s700" Text="Equipment Under 100 HP (medium)" />
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    <asp:RadioButton ID="rb_s700_3" runat="server" GroupName="s700" Text="Specialized Equipment" />
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    <asp:RadioButton ID="rb_s700_4" runat="server" GroupName="s700" Text="All Other" />
                    <br />
                    <asp:TextBox ID="txt_s700oth" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
            <hr />
        </div>

        <div id="Q701" class="hidden">
            <div class="row tr-question">
                <div class="col-md-12">What is the primary type of business for which you are reporting? <span class="alert-danger">Who is this? DELETE?</span></div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    <asp:RadioButton ID="rb_s701_1" runat="server" GroupName="s701" Text="New Equipment " />
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    <asp:RadioButton ID="rb_s701_2" runat="server" GroupName="s701" Text="New Equipment with Service" />
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    <asp:RadioButton ID="rb_s701_3" runat="server" GroupName="s701" Text="New and Used Equipment" />
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    <asp:RadioButton ID="rb_s701_4" runat="server" GroupName="s701" Text="Industrial Supplies" />
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    <asp:RadioButton ID="rb_s701_5" runat="server" GroupName="s701" Text="New Equipment and Industrial Supplies" />
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    <asp:RadioButton ID="rb_s701_6" runat="server" GroupName="s701" Text="Other" />
                    <br />
                    <asp:TextBox ID="txt_s701oth" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
            <hr />
        </div>


        <div style="display: none;" id="DIV1909">
            ISA
    <div class="row tr-question">
        <div class="col-md-12">What is the primary type of business for which you are reporting?</div>
    </div>
            <div class="row">
                <div class="col-md-8">
                    <asp:RadioButton ID="rb_s702_1" runat="server" GroupName="s702" Text="General Line Firm (90% of sales from more than 5 product categories)" />
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    <asp:RadioButton ID="rb_s702_2" runat="server" GroupName="s702" Text="Specialty Firm (90% of sales from 5 or fewer product categories)" />
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    <asp:RadioButton ID="rb_s702_3" runat="server" GroupName="s702" Text="Limited Firm (90% or more sales from one product category)" />
                </div>
            </div>
            <hr />
        </div>


        <div id="Q703" class="hidden">
            <div class="row tr-question">
                <div class="col-md-12">What is the primary type of business for which you are reporting? <span class="alert-danger">Who is this? DELETE?</span></div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    <asp:RadioButton ID="rb_s703_1" runat="server" GroupName="s703" Text="Chemical Products (chemicals, soaps & detergents)" />
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    <asp:RadioButton ID="rb_s703_2" runat="server" GroupName="s703" Text="Equipment Products (gas, propane, tools, safety equipment, mops, etc.)" />
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    <asp:RadioButton ID="rb_s703_3" runat="server" GroupName="s703" Text="Paper Products (paper, can liners and other disposables) " />
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    <!--PASTE-->
                    <asp:RadioButton ID="rb_s703_4" runat="server" GroupName="s703" Text="Other Products (no particular product emphasis)" />
                    <br />
                    <asp:TextBox ID="txt_s703oth" runat="server" CssClass="form-control text-left"></asp:TextBox>
                </div>
            </div>
            <hr />
        </div>


        <div style="display: none;" id="DIV1910">
            MHEDA
    <div class="row tr-question">
        <div class="col-md-12">What is the primary type of business for which you are reporting?</div>
    </div>
            <div class="row">
                <div class="col-md-8">
                    <asp:RadioButton ID="rb_s704_1" runat="server" GroupName="s704" Text="Lift Truck Distributor" />
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    <asp:RadioButton ID="rb_s704_2" runat="server" GroupName="s704" Text="Engineered Systems Distributor" />
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    <asp:RadioButton ID="rb_s704_3" runat="server" GroupName="s704" Text="Storage and Handling Products (non-powered trucks, casters, racks, etc.)" />
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    <asp:RadioButton ID="rb_s704_4" runat="server" GroupName="s704" Text="Complete Line Distributor" />
                </div>
            </div>
            <hr />
        </div>


        <div style="display: none;" id="DIV1911">
            NAED
    <div class="row tr-question">
        <div class="col-md-12">What is the primary type of business for which you are reporting?</div>
    </div>
            <div class="row">
                <div class="col-md-8">
                    <asp:RadioButton ID="rb_s705_1" runat="server" GroupName="s705" Text="Residential Contractor" />
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    <asp:RadioButton ID="rb_s705_2" runat="server" GroupName="s705" Text="Commercial Contractor" />
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    <asp:RadioButton ID="rb_s705_3" runat="server" GroupName="s705" Text="Industrial Sales" />
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    <asp:RadioButton ID="rb_s705_4" runat="server" GroupName="s705" Text="Mixed Sales" />
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    <asp:RadioButton ID="rb_s705_5" runat="server" GroupName="s705" Text="Mixed with Commercial" />
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    <asp:RadioButton ID="rb_s705_6" runat="server" GroupName="s705" Text="Mixed with Industrial" />
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    <asp:RadioButton ID="rb_s705_7" runat="server" GroupName="s705" Text="Utility Sales" />
                </div>
            </div>
            <hr />
        </div>


        <div style="display: none;" id="Q706">
            NAFCD/NBMDA
    <div class="row tr-question">
        <div class="col-md-12">What is the primary type of business for which you are reporting?</div>
    </div>
            <div class="row">
                <div class="col-md-8">
                    <asp:RadioButton ID="rb_s706_1" runat="server" GroupName="s706" Text="Industrial Forest Products (hardboard, plywood, particleboard, MDF, etc.)" />
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    <asp:RadioButton ID="rb_s706_2" runat="server" GroupName="s706" Text="Cabinet Hardware and/or Laminates" />
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    <asp:RadioButton ID="rb_s706_3" runat="server" GroupName="s706" Text="Specialty Building Products" />
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    <asp:RadioButton ID="rb_s706_4" runat="server" GroupName="s706" Text="Millwork Products (doors, windows, mouldings, jambs, etc.)" />
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    <asp:RadioButton ID="rb_s706_5" runat="server" GroupName="s706" Text="Floorcovering" />
                </div>
            </div>
            <hr />
        </div>


        <div style="display: none;" id="DIV1918">
            NPTA
    <div class="row tr-question">
        <div class="col-md-12">What is the primary type of business for which you are reporting?</div>
    </div>
            <div class="row">
                <div class="col-md-8">
                    <asp:RadioButton ID="rb_s707_1" runat="server" GroupName="s707" Text="Industrial Paper (disposables, industrial packaging, janitorial supplies, etc.)" />
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    <asp:RadioButton ID="rb_s707_2" runat="server" GroupName="s707" Text="Printing Paper (commercial printing, business imaging, publishing, etc.)" />
                </div>
            </div>
            <hr />
        </div>

        <div style="display: none;" id="DIV1901">
            ASA
    <div class="row tr-question">
        <div class="col-md-12">What is the primary type of business for which you are reporting?</div>
    </div>
            <div class="row">
                <div class="col-md-8">
                    <asp:RadioButton ID="rb_s708_1" runat="server" GroupName="s708" Text="Plumbing-Heating-Cooling-Piping (PHCP)" />
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    <asp:RadioButton ID="rb_s708_2" runat="server" GroupName="s708" Text="Industrial Pipe-Valve-Fitting (PVF) " />
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    <asp:RadioButton ID="rb_s708_3" runat="server" GroupName="s708" Text="PHCP & PVF" />
                </div>
            </div>
            <hr />
        </div>



        <div class="row tr-question">
            <div class="col-md-12">1. Primary product (highest shipment dollar volume) that your company distributes?</div>
        </div>
        <p><em class="small">(e.g., motors, packaged food, apparel)</em></p>
        <div class="row">
            <div class="col-md-12">
                <span class="input-group"><span class="input-group">
                    <asp:TextBox ID="txt_s1" runat="server" CssClass="form-control"></asp:TextBox>

                </span></span>
            </div>
        </div>
        <hr />


        <div class="row tr-question">
            <div class="col-md-12">2. Public or private?</div>
        </div>
        <div class="row">
            <div class="col-md-8">
                <asp:RadioButton CssClass="radio-inline" ID="rb_s2_1" runat="server" GroupName="s2" Text="Public" />
            </div>
        </div>
        <div class="row">
            <div class="col-md-8">
                <asp:RadioButton CssClass="radio-inline" ID="rb_s2_2" runat="server" GroupName="s2" Text="Private" />
            </div>
        </div>
        <hr />

        <div class="row tr-question">
            <div class="col-md-12">3. Business type?</div>
        </div>
        <div class="row">
            <div class="col-md-8">
                <asp:RadioButton CssClass="radio-inline" ID="rb_s3_1" runat="server" GroupName="s3" Text="C-Corp" />
            </div>
        </div>
        <div class="row">
            <div class="col-md-8">
                <asp:RadioButton CssClass="radio-inline" ID="rb_s3_2" runat="server" GroupName="s3" Text="S-Corp" />
            </div>
        </div>
        <div class="row">
            <div class="col-md-8">
                <asp:RadioButton CssClass="radio-inline" ID="rb_s3_3" runat="server" GroupName="s3" Text="LLC" />
            </div>
        </div>
        <div class="row">
            <div class="col-md-8">
                <asp:RadioButton CssClass="radio-inline" ID="rb_s3_4" runat="server" GroupName="s999" Text="Other" />
                <br />
                <asp:TextBox ID="txt_s3oth" runat="server" CssClass="form-control"></asp:TextBox>
            </div>
        </div>
        <hr />

        <div class="row tr-question">
            <div class="col-md-12">4. Territory served?</div>
        </div>
        <div class="row">
            <div class="col-md-8">
                <asp:RadioButton CssClass="radio-inline" ID="rb_s4_1" runat="server" GroupName="s4" Text="Local" />
            </div>
        </div>
        <div class="row">
            <div class="col-md-8">
                <asp:RadioButton CssClass="radio-inline" ID="rb_s4_2" runat="server" GroupName="s4" Text="Regional" />
            </div>
        </div>
        <div class="row">
            <div class="col-md-8">
                <asp:RadioButton CssClass="radio-inline" ID="rb_s4_3" runat="server" GroupName="s4" Text="National" />
            </div>
        </div>
        <div class="row">
            <div class="col-md-8">
                <asp:RadioButton CssClass="radio-inline" ID="rb_s4_4" runat="server" GroupName="s4" Text="International/Global" />
            </div>
        </div>
        <hr />

        <div class="row tr-question">
            <div class="col-md-12">5. Number of stand-alone locations (including headquarters and distribution facilities – exclude showrooms)?</div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <span class="input-group"><span class="input-group">
                    <span class="input-group-addon">#</span>
                    <asp:TextBox ID="txt_s5" runat="server" CssClass="form-control text-right"></asp:TextBox>
                </span></span>
            </div>
        </div>

        <p>&nbsp;</p>
        <div class="row tr-question">
            <div class="col-md-12">Number of showrooms?</div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <span class="input-group"><span class="input-group">
                    <span class="input-group-addon">#</span>
                    <asp:TextBox ID="txt_s522" runat="server" CssClass="form-control text-right"></asp:TextBox>
                </span></span>
            </div>
        </div>
        <hr />

        <div class="row tr-question">
            <div class="col-md-12">6. Zip/Postal code of headquarters location?</div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <span class="input-group"><span class="input-group">
                    <span class="input-group-addon">#</span>
                    <asp:TextBox ID="txt_s6" runat="server" CssClass="form-control text-right"></asp:TextBox>
                </span></span>
            </div>
        </div>
        <hr />

        <div class="row tr-question">
            <div class="col-md-12">7. Annual sales volume (or shipment volume) in 2016 and 2017?</div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <table class="table-condensed table-inner-borders">
                    <tr>
                        <td colspan="2" class="text-right">2016</td>
                        <td class="text-center">
                            <span class="input-group"><span class="input-group">
                                <span class="input-group-addon">$</span>
                                <asp:TextBox ID="txt_s7" runat="server" CssClass="form-control text-right"></asp:TextBox>
                            </span></span>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" class="text-right">2017</td>
                        <td class="text-center">
                            <span class="input-group"><span class="input-group">
                                <span class="input-group-addon">$</span>
                                <asp:TextBox ID="txt_s8" runat="server" CssClass="form-control text-right"></asp:TextBox>
                            </span></span>
                        </td>
                    </tr>
                </table>

            </div>
        </div>
        <hr />

        <div class="row tr-question">
            <div class="col-md-12">8. Seasonality of business?</div>
        </div>
        <div class="row">
            <div class="col-md-8">
                <asp:RadioButton CssClass="radio-inline" ID="rb_s9_1" runat="server" GroupName="s9" Text="Very seasonal" />
            </div>
        </div>
        <div class="row">
            <div class="col-md-8">
                <asp:RadioButton CssClass="radio-inline" ID="rb_s9_2" runat="server" GroupName="s9" Text="Somewhat seasonal" />
            </div>
        </div>
        <div class="row">
            <div class="col-md-8">
                <asp:RadioButton CssClass="radio-inline" ID="rb_s9_3" runat="server" GroupName="s9" Text="Not seasonal" />
            </div>
        </div>
        <hr />

        <div class="row tr-question">
            <div class="col-md-12">9. Total employees - expressed in Full-Time Equivalents (FTEs) (all locations)</div>
        </div>
        <p><em class="small">Each salaried or hourly full-time employee (any employee who consistently works 35-40 hours per week) is counted as 1 FTE. To convert part-time employees to FTEs, determine the total hours worked by all part-time employees and divide by 2,080 hours.</em></p>
        <div class="row">
            <div class="col-md-12">
                <span class="input-group"><span class="input-group">
                    <span class="input-group-addon">#</span>
                    <asp:TextBox ID="txt_s10" runat="server" CssClass="form-control text-right"></asp:TextBox>
                </span></span>
            </div>
        </div>
        <hr />

        <div class="row tr-question">
            <div class="col-md-12">10. Union representation?</div>
        </div>
        <div class="row">
            <div class="col-md-8">
                <asp:RadioButton CssClass="radio-inline" ID="rb_s11_1" runat="server" GroupName="s11" Text="No workers" />
            </div>
        </div>
        <div class="row">
            <div class="col-md-8">
                <asp:RadioButton CssClass="radio-inline" ID="rb_s11_2" runat="server" GroupName="s11" Text="Some workers" />
            </div>
        </div>
        <div class="row">
            <div class="col-md-8">
                <asp:RadioButton CssClass="radio-inline" ID="rb_s11_3" runat="server" GroupName="s11" Text="All workers" />
            </div>
        </div>
        <hr />

        <div class="row tr-question">
            <div class="col-md-12">11. Which of the following is a major concern/issue of your company? (Select up to 5)</div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <asp:CheckBox ID="cb_s12" runat="server" Text="Absenteeism" CssClass="checkbox-inline" />
            </div>
            <div class="col-md-4">
                <asp:CheckBox ID="cb_s20" runat="server" Text="Employee safety" CssClass="checkbox-inline" />
            </div>
            <div class="col-md-4">
                <asp:CheckBox ID="cb_s28" runat="server" Text="Manufacturer relations" CssClass="checkbox-inline" />
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <asp:CheckBox ID="cb_s13" runat="server" Text="Alcohol/drug use violations" CssClass="checkbox-inline" />
            </div>
            <div class="col-md-4">
                <asp:CheckBox ID="cb_s21" runat="server" Text="Environmental impacts" CssClass="checkbox-inline" />
            </div>
            <div class="col-md-4">
                <asp:CheckBox ID="cb_s29" runat="server" Text="Manufacturers moving offshore" CssClass="checkbox-inline" />
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <asp:CheckBox ID="cb_s14" runat="server" Text="Client satisfaction" CssClass="checkbox-inline" />
            </div>
            <div class="col-md-4">
                <asp:CheckBox ID="cb_s22" runat="server" Text="Finding qualified employees " CssClass="checkbox-inline" />
            </div>
            <div class="col-md-4">
                <asp:CheckBox ID="cb_s30" runat="server" Text="Manufacturers selling direct" CssClass="checkbox-inline" />
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <asp:CheckBox ID="cb_s15" runat="server" Text="Costs of healthcare" CssClass="checkbox-inline" />
            </div>
            <div class="col-md-4">
                <asp:CheckBox ID="cb_s23" runat="server" Text="Foreign competition" CssClass="checkbox-inline" />
            </div>
            <div class="col-md-4">
                <asp:CheckBox ID="cb_s31" runat="server" Text="Price competition" CssClass="checkbox-inline" />
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <asp:CheckBox ID="cb_s16" runat="server" Text="Customers going out of business" CssClass="checkbox-inline" />
            </div>
            <div class="col-md-4">
                <asp:CheckBox ID="cb_s24" runat="server" Text="Government regulations" CssClass="checkbox-inline" />
            </div>
            <div class="col-md-4">
                <asp:CheckBox ID="cb_s32" runat="server" Text="Retaining employees" CssClass="checkbox-inline" />
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <asp:CheckBox ID="cb_s17" runat="server" Text="Ecommerce" CssClass="checkbox-inline" />
            </div>
            <div class="col-md-4">
                <asp:CheckBox ID="cb_s25" runat="server" Text="Increased operating costs" CssClass="checkbox-inline" />
            </div>
            <div class="col-md-4">
                <asp:CheckBox ID="cb_s33" runat="server" Text="Tax policies" CssClass="checkbox-inline" />
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <asp:CheckBox ID="cb_s18" runat="server" Text="Economic uncertainty" CssClass="checkbox-inline" />
            </div>
            <div class="col-md-4">
                <asp:CheckBox ID="cb_s26" runat="server" Text="Lack of skilled workers" CssClass="checkbox-inline" />
            </div>
            <div class="col-md-4">
                <table>
                    <tr>
                        <td>
                            <asp:CheckBox ID="cb_s34" runat="server" Text="Other" CssClass="checkbox-inline" /></td>
                        <td>
                            <asp:TextBox ID="txt_s34oth" runat="server" CssClass="form-control"></asp:TextBox></td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <asp:CheckBox ID="cb_s19" runat="server" Text="Employee morale" CssClass="checkbox-inline" />
            </div>
            <div class="col-md-4">
                <asp:CheckBox ID="cb_s27" runat="server" Text="Maintaining profit levels" CssClass="checkbox-inline" />
            </div>
            <div class="col-md-4">
                <asp:CheckBox ID="cb_s35" runat="server" Text="None of these" CssClass="checkbox-inline" />
            </div>
        </div>
        <hr />








        <table class="table-condensed centered">
            <tr>
                <td class="text-center" colspan="3">
                    <asp:CheckBox ID="cb_section1Complete" runat="server" Text="This section is complete." />
                </td>
            </tr>
            <tr>
                <td class="text-center">
                    <asp:Button ID="btn_previous" runat="server" CssClass="btn btn-primary" Text="Previous Section" Width="141px" />
                </td>
                <td class="text-center">
                    <asp:Button ID="btn_save" runat="server" CssClass="btn btn-primary" Text="Save" Width="141px" />
                </td>
                <td class="text-center">
                    <asp:Button ID="btn_next" runat="server" CssClass="btn btn-primary" Text="Next Section" Width="141px" />
                </td>
            </tr>
            <tr>
                <td class="text-center">&nbsp;</td>
                <td class="text-center">
                    &nbsp;</td>
                <td class="text-center">&nbsp;</td>
            </tr>
        </table>
    </div>
</asp:Content>

