﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Survey.master" AutoEventWireup="false" MaintainScrollPositionOnPostback="true" CodeFile="Benefits.aspx.vb" Inherits="Surveys_Benefits" %>

<%@ Register Src="~/Controls/SurveyNavigation.ascx" TagPrefix="uc1" TagName="SurveyNavigation" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script type="text/javascript">
        $(document).ready(function () {
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
    <script src="../../Scripts/autoNumeric/autoNumeric-min.js"></script>
    <script src="../../Scripts/Benefits.js?v=1"></script>
    <asp:TextBox ID="txt_LocationID" runat="server" Visible="false"></asp:TextBox>
    <asp:TextBox ID="txt_userid" runat="server" Visible="false"></asp:TextBox>
    <div class="bg-content">
        <div class="row">
            <table class="table-condensed centered" runat="server" id="tbl_location">
                <tr>
                    <td>
                        <asp:DropDownList ID="ddl_location" runat="server" CssClass="form-control" AutoPostBack="true"></asp:DropDownList>
                    </td>
                    <td><span class="glyphicon glyphicon-info-sign icon-info" data-placement="left" data-toggle="tooltip" title="This box is disabled by default.  If you are submitting for multiple locations, please add the locations on the Organization Admin page and you'll be able to cycle through them here."></span></td>
                </tr>
            </table>

        </div>

        <div class="">
            <h3 class="text-center">Deadline: 
                <asp:Label ID="lbl_deadline" runat="server" Text=""></asp:Label></h3>
        </div>
        <div class="">
            <span class="col-md-12 text-center text-muted"><em>Last updated: 
            <asp:Label ID="lbl_lastSave" runat="server" Text=""></asp:Label></em></span>
        </div>


        <h2 class="page-header">Benefits</h2>



        <h3 class="h3-header">Healthcare</h3>

        <p>&nbsp;</p>
        <div class="row tr-question">
            <div class="col-md-12">
                1. Does your company provide medical insurance for:
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <table class="table-condensed table-inner-borders">
                    <tr>
                        <td>&nbsp;</td>
                        <td class="text-center"><strong>Employees</strong></td>
                        <td class="text-center"><strong>Dependents</strong></td>
                    </tr>
                    <tr>
                        <td class="text-right">Executive employees</td>
                        <td class="text-center">
                            <asp:RadioButton ID="rb_s278_1" runat="server" GroupName="s278" Text="Yes" />&nbsp;&nbsp;&nbsp;
                        <asp:RadioButton ID="rb_s278_2" runat="server" GroupName="s278" Text="No" />
                        </td>
                        <td class="text-center">
                            <asp:RadioButton ID="rb_s281_1" runat="server" GroupName="s281" Text="Yes" />&nbsp;&nbsp;&nbsp;
                        <asp:RadioButton ID="rb_s281_2" runat="server" GroupName="s281" Text="No" />
                        </td>
                    </tr>
                    <tr>
                        <td class="text-right">Full-time employees</td>
                        <td class="text-center">
                            <asp:RadioButton ID="rb_s279_1" runat="server" GroupName="s279" Text="Yes" />&nbsp;&nbsp;&nbsp;
                        <asp:RadioButton ID="rb_s279_2" runat="server" GroupName="s279" Text="No" />
                        </td>
                        <td class="text-center">
                            <asp:RadioButton ID="rb_s282_1" runat="server" GroupName="s282" Text="Yes" />&nbsp;&nbsp;&nbsp;
                        <asp:RadioButton ID="rb_s282_2" runat="server" GroupName="s282" Text="No" />
                        </td>
                    </tr>
                    <tr>
                        <td class="text-right">Part-time employees</td>
                        <td class="text-center">
                            <asp:RadioButton ID="rb_s280_1" runat="server" GroupName="s280" Text="Yes" />&nbsp;&nbsp;&nbsp;
                        <asp:RadioButton ID="rb_s280_2" runat="server" GroupName="s280" Text="No" />
                        </td>
                        <td class="text-center">
                            <asp:RadioButton ID="rb_s283_1" runat="server" GroupName="s283" Text="Yes" />&nbsp;&nbsp;&nbsp;
                        <asp:RadioButton ID="rb_s283_2" runat="server" GroupName="s283" Text="No" />
                        </td>
                    </tr>
                </table>

            </div>
        </div>



        <p>&nbsp;</p>
        <div class="row tr-question">
            <div class="col-md-12">2. What percentage of the insurance premium is paid by the company?</div>
        </div>
        <p><em class="small">Enter "0" if the plan is offered but the company pays no premium.</em></p>
        <div class="row">
            <div class="col-md-12">
                <table class="table-condensed table-inner-borders">
                    <thead>
                        <tr>
                            <th>&nbsp;</th>
                            <th class="text-center"><strong>Employees</strong></th>
                            <th class="text-center"><strong>Dependents</strong></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="text-right">Executive employees</td>
                            <td class="text-center">
                                <span class="input-group"><span class="input-group">
                                    <asp:TextBox ID="txt_s284" runat="server" CssClass="form-control text-right"></asp:TextBox>
                                    <span class="input-group-addon">%</span>
                                </span></span>
                            </td>
                            <td class="text-center">
                                <span class="input-group"><span class="input-group">
                                    <asp:TextBox ID="txt_s287" runat="server" CssClass="form-control text-right"></asp:TextBox>
                                    <span class="input-group-addon">%</span>
                                </span></span>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right">Full-time employees</td>
                            <td class="text-center">
                                <span class="input-group"><span class="input-group">
                                    <asp:TextBox ID="txt_s285" runat="server" CssClass="form-control text-right"></asp:TextBox>
                                    <span class="input-group-addon">%</span>
                                </span></span>
                            </td>
                            <td class="text-center">
                                <span class="input-group"><span class="input-group">
                                    <asp:TextBox ID="txt_s288" runat="server" CssClass="form-control text-right"></asp:TextBox>
                                    <span class="input-group-addon">%</span>
                                </span></span>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right">Part-time employees</td>
                            <td class="text-center">
                                <span class="input-group"><span class="input-group">
                                    <asp:TextBox ID="txt_s286" runat="server" CssClass="form-control text-right"></asp:TextBox>
                                    <span class="input-group-addon">%</span>
                                </span></span>
                            </td>
                            <td class="text-center">
                                <span class="input-group"><span class="input-group">
                                    <asp:TextBox ID="txt_s289" runat="server" CssClass="form-control text-right"></asp:TextBox>
                                    <span class="input-group-addon">%</span>
                                </span></span>
                            </td>
                        </tr>
                    </tbody>
                </table>

            </div>
        </div>
        <hr />

        <div class="row tr-question">
            <div class="col-md-12">3. What type(s) of medical plans are offered by your company?   (Check all that apply)</div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <table class="table-condensed">
                    <tr>
                        <td class="col-md-4">
                            <asp:CheckBox ID="cb_s290" runat="server" Text="None" CssClass="checkbox-inline" />
                        </td>
                        <td class="col-md-4">
                            <asp:CheckBox ID="cb_s294" runat="server" Text="HRA (Health Reimbursement Account)" CssClass="checkbox-inline" />
                        </td>
                        <td class="col-md-4">
                            <asp:CheckBox ID="cb_s298" runat="server" Text="PPO (Preferred Provider Org.)" CssClass="checkbox-inline" />
                        </td>
                    </tr>
                    <tr>
                        <td class="col-md-4">
                            <asp:CheckBox ID="cb_s291" runat="server" Text="Canadian public health plan" CssClass="checkbox-inline" />
                        </td>
                        <td class="col-md-4">
                            <asp:CheckBox ID="cb_s295" runat="server" Text="HSA (Health Savings Accounts)" CssClass="checkbox-inline" />
                        </td>
                        <td class="col-md-4">
                            <asp:CheckBox ID="cb_s299" runat="server" Text="Self-insured (fully)" CssClass="checkbox-inline" />
                        </td>
                    </tr>
                    <tr>
                        <td class="col-md-4">
                            <asp:CheckBox ID="cb_s292" runat="server" Text="High-deductible plan" CssClass="checkbox-inline" />
                        </td>
                        <td class="col-md-4">
                            <asp:CheckBox ID="cb_s296" runat="server" Text="Opt-out of coverage " CssClass="checkbox-inline" />
                        </td>
                        <td class="col-md-4">
                            <asp:CheckBox ID="cb_s300" runat="server" Text="Self-insured (partially)" CssClass="checkbox-inline" />
                        </td>
                    </tr>
                    <tr>
                        <td class="col-md-4">
                            <asp:CheckBox ID="cb_s293" runat="server" Text="HMO (Health Maintenance Organization)" CssClass="checkbox-inline" />
                        </td>
                        <td class="col-md-4">
                            <asp:CheckBox ID="cb_s297" runat="server" Text="POS (Point of Service)" CssClass="checkbox-inline" />
                        </td>
                        <td class="col-md-4">
                            <asp:CheckBox ID="cb_s301" runat="server" Text="Traditional indemnity" CssClass="checkbox-inline" />
                        </td>
                    </tr>
                    <tr>
                        <td class="col-md-4">&nbsp;
                        </td>
                        <td class="col-md-4">&nbsp;
                        </td>
                        <td class="col-md-4">
                            <table>
                                <tr>
                                    <td>
                                        <asp:CheckBox ID="cb_s302" runat="server" Text="Other" CssClass="checkbox-inline" /></td>
                                    <td>
                                        <asp:TextBox ID="txt_s302oth" runat="server" CssClass="form-control text-left"></asp:TextBox></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>

            </div>
        </div>
        <hr />

        <div id="NAWtbl" runat="server">

            <p>&nbsp;</p>
            <p><strong>If HDHP plans are offered, answer the following (otherwise skip to Typical Plan Coverage - #6)</strong></p>

            <div class="row tr-question">
                <div class="col-md-12">4. Does the firm fund the HRA/HSA plan(s)?</div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    <asp:RadioButton CssClass="radio-inline" ID="rb_s651_1" runat="server" GroupName="s651" Text="Yes" />
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    <asp:RadioButton CssClass="radio-inline" ID="rb_s651_2" runat="server" GroupName="s651" Text="No" />
                </div>
            </div>
            <hr />

            <div class="row tr-question">
                <div class="col-md-12">5. If yes, what is the <strong>annual</strong> $ funding?</div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <table class="table-condensed table-inner-borders">
                        <tr>
                            <td>&nbsp;</td>
                            <td class="text-center"><strong>Singles</strong></td>
                            <td class="text-center"><strong>Families</strong></td>
                        </tr>
                        <tr>
                            <td class="text-right">HRA (Health Reimbursement Account)</td>
                            <td>
                                <span class="input-group">
                                    <span class="input-group-addon">$</span>
                                    <asp:TextBox ID="txt_s652" runat="server" CssClass="form-control text-right"></asp:TextBox>
                                </span>
                            </td>
                            <td>
                                <span class="input-group">
                                    <span class="input-group-addon">$</span>
                                    <asp:TextBox ID="txt_s654" runat="server" CssClass="form-control text-right"></asp:TextBox>
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right">HSA (Health Savings Account)</td>
                            <td>
                                <span class="input-group">
                                    <span class="input-group-addon">$</span>
                                    <asp:TextBox ID="txt_s653" runat="server" CssClass="form-control text-right"></asp:TextBox>
                                </span>
                            </td>
                            <td>
                                <span class="input-group">
                                    <span class="input-group-addon">$</span>
                                    <asp:TextBox ID="txt_s655" runat="server" CssClass="form-control text-right"></asp:TextBox>
                                </span>
                            </td>
                        </tr>
                    </table>

                </div>
            </div>
            <hr />


            <div class="row tr-question">
                <div class="col-md-12">6. Typical Plan Coverage <small>(Leave blanks for plans which are not offered.  If rates vary by age/gender, report for a 35 year old male employee.)</small></div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <table class="table table-condensed">
                        <thead>
                            <tr class="td-bg-primary">
                                <th>&nbsp;</th>
                                <th>&nbsp;</th>
                                <th class="text-center">Monthly Premium $ Employee + Employer</th>
                                <th class="text-center">Employer Paid %</th>
                                <th class="text-center">Annual Deductible $ per Person</th>
                                <th class="text-center"># of Family Members That Need to Meet the Deductible</th>
                            </tr>
                        </thead>

                        <tbody>
                            <tr>
                                <td class="col-md-2">Traditional</td>
                                <td class="col-md-2">Single, Employee Only</td>
                                <td class="col-md-2">
                                    <span class="input-group">
                                        <span class="input-group-addon">$</span>
                                        <asp:TextBox ID="txt_s656" runat="server" CssClass="form-control text-right"></asp:TextBox>
                                    </span>
                                </td>
                                <td class="col-md-2">
                                    <span class="input-group">
                                        <asp:TextBox ID="txt_s666" runat="server" CssClass="form-control text-right"></asp:TextBox>
                                        <span class="input-group-addon">%</span>
                                    </span>
                                </td>
                                <td class="col-md-2">
                                    <span class="input-group">
                                        <span class="input-group-addon">$</span>
                                        <asp:TextBox ID="txt_s676" runat="server" CssClass="form-control text-right"></asp:TextBox>
                                    </span>
                                </td>
                                <td class="col-md-2" style="background-color: #808080; left: 1016px;">&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td class="col-md-2">&nbsp;</td>
                                <td class="col-md-2">Employee + Family</td>
                                <td class="col-md-2">
                                    <span class="input-group">
                                        <span class="input-group-addon">$</span>
                                        <asp:TextBox ID="txt_s657" runat="server" CssClass="form-control text-right"></asp:TextBox>
                                    </span>
                                </td>
                                <td class="col-md-2">
                                    <span class="input-group">
                                        <asp:TextBox ID="txt_s667" runat="server" CssClass="form-control text-right"></asp:TextBox>
                                        <span class="input-group-addon">%</span>
                                    </span>
                                </td>
                                <td class="col-md-2">
                                    <span class="input-group">
                                        <span class="input-group-addon">$</span>
                                        <asp:TextBox ID="txt_s677" runat="server" CssClass="form-control text-right"></asp:TextBox>
                                    </span>
                                </td>
                                <td class="col-md-2">
                                    <span class="input-group">
                                        <span class="input-group-addon">#</span>
                                        <asp:TextBox ID="txt_s686" runat="server" CssClass="form-control text-right"></asp:TextBox>
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td class="col-md-2 td-bg-gray">HMO/EPO</td>
                                <td class="col-md-2 td-bg-gray">Single, Employee Only</td>
                                <td class="col-md-2 td-bg-gray">
                                    <span class="input-group">
                                        <span class="input-group-addon">$</span>
                                        <asp:TextBox ID="txt_s658" runat="server" CssClass="form-control text-right"></asp:TextBox>
                                    </span>
                                </td>
                                <td class="col-md-2 td-bg-gray">
                                    <span class="input-group">
                                        <asp:TextBox ID="txt_s668" runat="server" CssClass="form-control text-right"></asp:TextBox>
                                        <span class="input-group-addon">%</span>
                                    </span>
                                </td>
                                <td class="col-md-2 td-bg-gray">
                                    <span class="input-group">
                                        <span class="input-group-addon">$</span>
                                        <asp:TextBox ID="txt_s678" runat="server" CssClass="form-control text-right"></asp:TextBox>
                                    </span>
                                </td>
                                <td class="col-md-2 td-bg-gray" style="background-color: #808080;">&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td class="col-md-2 td-bg-gray">&nbsp;</td>
                                <td class="col-md-2 td-bg-gray">Employee + Family</td>
                                <td class="col-md-2 td-bg-gray">
                                    <span class="input-group">
                                        <span class="input-group-addon">$</span>
                                        <asp:TextBox ID="txt_s659" runat="server" CssClass="form-control text-right"></asp:TextBox>
                                    </span>
                                </td>
                                <td class="col-md-2 td-bg-gray">
                                    <span class="input-group">
                                        <asp:TextBox ID="txt_s669" runat="server" CssClass="form-control text-right"></asp:TextBox>
                                        <span class="input-group-addon">%</span>
                                    </span>
                                </td>
                                <td class="col-md-2 td-bg-gray">
                                    <span class="input-group">
                                        <span class="input-group-addon">$</span>
                                        <asp:TextBox ID="txt_s679" runat="server" CssClass="form-control text-right"></asp:TextBox>
                                    </span>
                                </td>
                                <td class="col-md-2 td-bg-gray">
                                    <span class="input-group">
                                        <span class="input-group-addon">#</span>
                                        <asp:TextBox ID="txt_s687" runat="server" CssClass="form-control text-right"></asp:TextBox>
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td class="col-md-2">POS</td>
                                <td class="col-md-2">Single, Employee Only</td>
                                <td class="col-md-2">
                                    <span class="input-group">
                                        <span class="input-group-addon">$</span>
                                        <asp:TextBox ID="txt_s660" runat="server" CssClass="form-control text-right"></asp:TextBox>
                                    </span>
                                </td>
                                <td class="col-md-2">
                                    <span class="input-group">
                                        <asp:TextBox ID="txt_s670" runat="server" CssClass="form-control text-right"></asp:TextBox>
                                        <span class="input-group-addon">%</span>
                                    </span>
                                </td>
                                <td class="col-md-2">
                                    <span class="input-group">
                                        <span class="input-group-addon">$</span>
                                        <asp:TextBox ID="txt_s680" runat="server" CssClass="form-control text-right"></asp:TextBox>
                                    </span>
                                </td>
                                <td class="col-md-2" style="background-color: #808080;">&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td class="col-md-2">&nbsp;</td>
                                <td class="col-md-2">Employee + Family</td>
                                <td class="col-md-2">
                                    <span class="input-group">
                                        <span class="input-group-addon">$</span>
                                        <asp:TextBox ID="txt_s661" runat="server" CssClass="form-control text-right"></asp:TextBox>
                                    </span>
                                </td>
                                <td class="col-md-2">
                                    <span class="input-group">
                                        <asp:TextBox ID="txt_s671" runat="server" CssClass="form-control text-right"></asp:TextBox>
                                        <span class="input-group-addon">%</span>
                                    </span>
                                </td>
                                <td class="col-md-2">
                                    <span class="input-group">
                                        <span class="input-group-addon">$</span>
                                        <asp:TextBox ID="txt_s681" runat="server" CssClass="form-control text-right"></asp:TextBox>
                                    </span>
                                </td>
                                <td class="col-md-2">
                                    <span class="input-group">
                                        <span class="input-group-addon">#</span>
                                        <asp:TextBox ID="txt_s688" runat="server" CssClass="form-control text-right"></asp:TextBox>
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td class="col-md-2  td-bg-gray">PPO</td>
                                <td class="col-md-2  td-bg-gray">Single, Employee Only</td>
                                <td class="col-md-2  td-bg-gray">
                                    <span class="input-group">
                                        <span class="input-group-addon">$</span>
                                        <asp:TextBox ID="txt_s662" runat="server" CssClass="form-control text-right"></asp:TextBox>
                                    </span>
                                </td>
                                <td class="col-md-2  td-bg-gray">
                                    <span class="input-group">
                                        <asp:TextBox ID="txt_s672" runat="server" CssClass="form-control text-right"></asp:TextBox>
                                        <span class="input-group-addon">%</span>
                                    </span>
                                </td>
                                <td class="col-md-2  td-bg-gray">
                                    <span class="input-group">
                                        <span class="input-group-addon">$</span>
                                        <asp:TextBox ID="txt_s682" runat="server" CssClass="form-control text-right"></asp:TextBox>
                                    </span>
                                </td>
                                <td class="col-md-2" style="background-color: #808080;">&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td class="col-md-2  td-bg-gray">&nbsp;</td>
                                <td class="col-md-2  td-bg-gray">Employee + Family</td>
                                <td class="col-md-2  td-bg-gray">
                                    <span class="input-group">
                                        <span class="input-group-addon">$</span>
                                        <asp:TextBox ID="txt_s663" runat="server" CssClass="form-control text-right"></asp:TextBox>
                                    </span>
                                </td>
                                <td class="col-md-2  td-bg-gray">
                                    <span class="input-group">
                                        <asp:TextBox ID="txt_s673" runat="server" CssClass="form-control text-right"></asp:TextBox>
                                        <span class="input-group-addon">%</span>
                                    </span>
                                </td>
                                <td class="col-md-2  td-bg-gray">
                                    <span class="input-group">
                                        <span class="input-group-addon">$</span>
                                        <asp:TextBox ID="txt_s683" runat="server" CssClass="form-control text-right"></asp:TextBox>
                                    </span>
                                </td>
                                <td class="col-md-2">
                                    <span class="input-group">
                                        <span class="input-group-addon">#</span>
                                        <asp:TextBox ID="txt_s689" runat="server" CssClass="form-control text-right"></asp:TextBox>
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td class="col-md-2">HDHP</td>
                                <td class="col-md-2">Single, Employee Only</td>
                                <td class="col-md-2">
                                    <span class="input-group">
                                        <span class="input-group-addon">$</span>
                                        <asp:TextBox ID="txt_s664" runat="server" CssClass="form-control text-right"></asp:TextBox>
                                    </span>
                                </td>
                                <td class="col-md-2">
                                    <span class="input-group">
                                        <asp:TextBox ID="txt_s674" runat="server" CssClass="form-control text-right"></asp:TextBox>
                                        <span class="input-group-addon">%</span>
                                    </span>
                                </td>
                                <td class="col-md-2">
                                    <span class="input-group">
                                        <span class="input-group-addon">$</span>
                                        <asp:TextBox ID="txt_s684" runat="server" CssClass="form-control text-right"></asp:TextBox>
                                    </span>
                                </td>
                                <td class="col-md-2" style="background-color: #808080;">&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td class="col-md-2">&nbsp;</td>
                                <td class="col-md-2">Employee + Family</td>
                                <td class="col-md-2">
                                    <span class="input-group">
                                        <span class="input-group-addon">$</span>
                                        <asp:TextBox ID="txt_s665" runat="server" CssClass="form-control text-right"></asp:TextBox>
                                    </span>
                                </td>
                                <td class="col-md-2">
                                    <span class="input-group">
                                        <asp:TextBox ID="txt_s675" runat="server" CssClass="form-control text-right"></asp:TextBox>
                                        <span class="input-group-addon">%</span>
                                    </span>
                                </td>
                                <td class="col-md-2">
                                    <span class="input-group">
                                        <span class="input-group-addon">$</span>
                                        <asp:TextBox ID="txt_s685" runat="server" CssClass="form-control text-right"></asp:TextBox>
                                    </span>
                                </td>
                                <td class="col-md-2">
                                    <span class="input-group">
                                        <span class="input-group-addon">#</span>
                                        <asp:TextBox ID="txt_s690" runat="server" CssClass="form-control text-right"></asp:TextBox>
                                    </span>
                                </td>
                            </tr>
                        </tbody>
                    </table>

                </div>
            </div>
            <hr />

        </div>
        <!--End NAW-->
                    <div class="row tr-question">
                <div class="col-md-12">&nbsp;Does your company adjust or break down medical or dental family plan pricing to account for the number of dependents in the family?</div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    <asp:RadioButton ID="rb_s324_1" runat="server" GroupName="s324" Text="Yes" />
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    <asp:RadioButton ID="rb_s324_2" runat="server" GroupName="s324" Text="No" />
                </div>
            </div>
            <hr />

            <div class="row tr-question">
                <div class="col-md-12">What was the total cost of your company’s annual health care expenditures (total contribution for all healthcare plans) as a percentage of total annual payroll expense?</div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <span class="input-group"><span class="input-group">
                        <asp:TextBox ID="txt_s556" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        <span class="input-group-addon">%</span>
                    </span></span>
                </div>
            </div>
            <hr />
        <h3 class="h3-header">Retirement</h3>

        <p>&nbsp;</p>
        <div class="row tr-question">
            <div class="col-md-12">1. Does your company offer a retirement plan to employees?</div>
        </div>
        <div class="row">
            <div class="col-md-8">
                <asp:RadioButton ID="rb_s326_1" runat="server" GroupName="s326" Text="Yes" />
            </div>
        </div>
        <div class="row">
            <div class="col-md-8">
                <asp:RadioButton ID="rb_s326_2" runat="server" GroupName="s326" Text="No" />
            </div>
        </div>

        <p>&nbsp;</p>
        <div class="row tr-question">
            <div class="col-md-12">If “yes,” what types of plans are offered to employees?  (Check all that apply)</div>
        </div>
        <p>
            Defined Contribution Plan <em class="small">(A retirement plan in which a certain amount or percentage of money is set aside each year by a company for the benefit of the employee.  There are restrictions as to when and how you can withdraw these funds without penalties.)</em>
        </p>
        <div class="row">
            <div class="col-md-8">
                <asp:CheckBox ID="cb_s327" runat="server" Text="ESOP" CssClass="checkbox-inline" />
            </div>
        </div>
        <div class="row">
            <div class="col-md-8">
                <asp:CheckBox ID="cb_s328" runat="server" Text="Profit-sharing plan" CssClass="checkbox-inline" />
            </div>
        </div>
        <div class="row">
            <div class="col-md-8">
                <asp:CheckBox ID="cb_s329" runat="server" Text="401(k) salary deferral " CssClass="checkbox-inline" />
            </div>
        </div>
        <div class="row">
            <div class="col-md-8">
                <asp:CheckBox CssClass="checkbox-inline" ID="cb_s330" runat="server" Text="SEP (Simplified Employee Pension)" />
            </div>
        </div>
        <div class="row">
            <div class="col-md-8">
                <asp:CheckBox ID="cb_s331" runat="server" Text="SIMPLE 401(k)" CssClass="checkbox-inline" />
            </div>
        </div>
        <div class="row">
            <div class="col-md-8">
                <asp:CheckBox ID="cb_s332" runat="server" Text="SIMPLE IRA" CssClass="checkbox-inline" />
            </div>
        </div>
        <div class="row">
            <div class="col-md-8">
                <asp:CheckBox ID="cb_s334" runat="server" Text="Defined benefit pension plan " CssClass="checkbox-inline" />
                <em class="small">(An employer-sponsored retirement plan where employee benefits are sorted out based on a formula using factors such as salary history and duration of employment.  Investment risk and portfolio management are entirely under the control of the company.  There are also restrictions on when and how you can withdraw these funds without penalties.  Also known as “qualified benefit plan” or “non-qualified benefit plan.”)</em>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8">
                <table>
                    <tr>
                        <td>
                            <asp:CheckBox ID="cb_s333" runat="server" Text="Other" CssClass="checkbox-inline" /></td>
                        <td>
                            <asp:TextBox ID="txt_s333oth" runat="server" CssClass="form-control text-left"></asp:TextBox></td>
                    </tr>
                </table>
            </div>
        </div>
        <hr />

        <div class="row tr-question">
            <div class="col-md-12">2. If a 401(k) is offered… Are employees automatically enrolled in the plan?</div>
        </div>
        <div class="row">
            <div class="col-md-8">
                <asp:RadioButton ID="rb_s335_1" runat="server" GroupName="s335" Text="Yes" />
            </div>
        </div>
        <div class="row">
            <div class="col-md-8">
                <asp:RadioButton ID="rb_s335_2" runat="server" GroupName="s335" Text="No" />
            </div>
        </div>
        <hr />

        <div class="row tr-question">
            <div class="col-md-12">3. If 401(k) is offered…Does your company match employee contributions?</div>
        </div>
        <div class="row">
            <div class="col-md-8">
                <asp:RadioButton ID="rb_s336_1" runat="server" GroupName="s336" Text="Yes" />
            </div>
        </div>
        <div class="row">
            <div class="col-md-8">
                <asp:RadioButton ID="rb_s336_2" runat="server" GroupName="s336" Text="No" />
            </div>
        </div>


        <p>&nbsp;</p>
        <div class="row tr-question">
            <div class="col-md-12">If yes, what are the terms for matching? (Check all that apply)</div>
        </div>
        <div class="row">
            <div class="col-md-7">
                <table class="table-condensed table-inner-borders">
                    <thead>
                        <tr>
                            <th>Term Offered</th>
                            <th>Maximum % of salary matched</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="col-md-2 td-vertical-bottom">
                                <asp:CheckBox ID="cb_s337" runat="server" Text="100% match" CssClass="checkbox-inline" />
                            </td>
                            <td class="col-md-4 td-vertical-mid">&nbsp;<span class="input-group"><span class="input-group"><asp:TextBox ID="txt_s338" runat="server" CssClass="form-control text-right"></asp:TextBox>
                                    <span class="input-group-addon">%</span>
                                </span></span>
                            </td>
                        </tr>
                        <tr>
                            <td class="col-md-2 td-vertical-bottom">
                                <asp:CheckBox ID="cb_s339" runat="server" Text="50% match" CssClass="checkbox-inline" />
                            </td>
                            <td class="col-md-4 td-vertical-mid">&nbsp;<span class="input-group"><span class="input-group"><asp:TextBox ID="txt_s340" runat="server" CssClass="form-control text-right"></asp:TextBox>
                                    <span class="input-group-addon">%</span>
                                </span></span>
                            </td>
                        </tr>
                        <tr>
                            <td class="col-md-2 td-vertical-bottom">
                

                                <asp:CheckBox ID="cb_s341" runat="server" Text="Other matching terms" CssClass="checkbox-inline" /><br />
                 
                                            <asp:TextBox ID="txt_s341oth" runat="server" CssClass="form-control text-left"></asp:TextBox>
                 
                            </td>
                            <td class="col-md-4 td-vertical-mid">&nbsp;<span class="input-group"><span class="input-group"><asp:TextBox ID="txt_s342" runat="server" CssClass="form-control text-right"></asp:TextBox>
                                    <span class="input-group-addon">%</span>
                                </span></span>
                            </td>
                        </tr>
                    </tbody>
                </table>

            </div>
        </div>
        <hr />

        <div class="row tr-question">
            <div class="col-md-12">4. What was the total cost of your company’s annual gross retirement expenditure (total contribution for all retirement plans) as a percentage of total annual payroll expense?</div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <span class="input-group"><span class="input-group">
                    <asp:TextBox ID="txt_s557" runat="server" CssClass="form-control text-right"></asp:TextBox>
                    <span class="input-group-addon">%</span>
                </span></span>
            </div>
        </div>
        <hr />

        <h3 class="h3-header">Other Employee Benefits</h3>

        <p>&nbsp;</p>
        <div class="row tr-question">
            <div class="col-md-12">1. What other benefits does your company provide to its employees? (Check all that apply)</div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <asp:CheckBox ID="cb_s303" runat="server" Text="None" CssClass="checkbox-inline" />
            </div>
            <div class="col-md-4">&nbsp;</div>
            <div class="col-md-4">&nbsp;</div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <asp:CheckBox ID="cb_s304" runat="server" Text="AD&D insurance" CssClass="checkbox-inline" />
            </div>
            <div class="col-md-4">
                <asp:CheckBox ID="cb_s311" runat="server" Text="Life insurance" CssClass="checkbox-inline" />
            </div>
            <div class="col-md-4">
                <asp:CheckBox ID="cb_s318" runat="server" Text="Retiree medical insurance coverage (FASB 106)" CssClass="checkbox-inline" />
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <asp:CheckBox ID="cb_s305" runat="server" Text="Auto/auto allowance" CssClass="checkbox-inline" />
            </div>
            <div class="col-md-4">
                <asp:CheckBox ID="cb_s312" runat="server" Text="Long-term care insurance" CssClass="checkbox-inline" />
            </div>
            <div class="col-md-4">
                <asp:CheckBox ID="cb_s319" runat="server" Text="Short-term disability" CssClass="checkbox-inline" />
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <asp:CheckBox ID="cb_s306" runat="server" Text="Club memberships" CssClass="checkbox-inline" />
            </div>
            <div class="col-md-4">
                <asp:CheckBox ID="cb_s313" runat="server" Text="Long-term disability" CssClass="checkbox-inline" />
            </div>
            <div class="col-md-4">
                <asp:CheckBox ID="cb_s320" runat="server" Text="Travel/accident insurance" CssClass="checkbox-inline" />
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <asp:CheckBox ID="cb_s307" runat="server" Text="Dental plan" CssClass="checkbox-inline" />
            </div>
            <div class="col-md-4">
                <asp:CheckBox ID="cb_s314" runat="server" Text="Mail order drug plan" CssClass="checkbox-inline" />
            </div>
            <div class="col-md-4">
                <asp:CheckBox ID="cb_s321" runat="server" Text="Tuition reimbursement" CssClass="checkbox-inline" />
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <asp:CheckBox ID="cb_s308" runat="server" Text="Discounted financing/loans" CssClass="checkbox-inline" />
            </div>
            <div class="col-md-4">
                <asp:CheckBox ID="cb_s315" runat="server" Text="Pre-tax section 125 plan (cafeteria)" CssClass="checkbox-inline" />
            </div>
            <div class="col-md-4">
                <asp:CheckBox ID="cb_s322" runat="server" Text="Vision plan" CssClass="checkbox-inline" />
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <asp:CheckBox ID="cb_s574" runat="server" Text="Flexible Spending Account (FSA)" CssClass="checkbox-inline" />
            </div>
            <div class="col-md-4">
                <asp:CheckBox ID="cb_s576" runat="server" Text="Tax preparation services" CssClass="checkbox-inline" />
            </div>
            <div class="col-md-4">
                <asp:CheckBox ID="cb_s578" runat="server" Text="Childcare services/reimbursement" CssClass="checkbox-inline" />
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <asp:CheckBox ID="cb_s575" runat="server" Text="Year-end/holiday gift" CssClass="checkbox-inline" />
            </div>
            <div class="col-md-4">
                <asp:CheckBox ID="cb_s577" runat="server" Text="Corporate holiday party" CssClass="checkbox-inline" />
            </div>
            <div class="col-md-4">
                <asp:CheckBox ID="cb_s579" runat="server" Text="Wellness program" CssClass="checkbox-inline" />
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <asp:CheckBox ID="cb_s309" runat="server" Text="Employee assistance program" CssClass="checkbox-inline" />
            </div>
            <div class="col-md-4">
                <asp:CheckBox ID="cb_s316" runat="server" Text="Prescription drug plan" CssClass="checkbox-inline" />
            </div>
            <div class="col-md-4">
                <table>
                    <tr>
                        <td>
                            <asp:CheckBox ID="cb_s323" runat="server" Text="Other" CssClass="checkbox-inline" /></td>
                        <td>
                            <asp:TextBox ID="txt_s323oth" runat="server" CssClass="form-control text-left"></asp:TextBox></td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <asp:CheckBox ID="cb_s310" runat="server" Text="Flexible work schedule" CssClass="checkbox-inline" />
            </div>
            <div class="col-md-4">
                <asp:CheckBox ID="cb_s317" runat="server" Text="Professional dues" CssClass="checkbox-inline" />
            </div>
            <div class="col-md-4">
                &nbsp;
            </div>
        </div>
        <hr />



        <h3 class="h3-header">Paid Time Off (PTO)</h3>

        <p>&nbsp;</p>
        <div class="row tr-question">
            <div class="col-md-12">1. Does your company use a PTO program (combining vacation and sick leave)?</div>
        </div>
        <div class="row">
            <div class="col-md-8">
                <asp:RadioButton ID="rb_s343_1" runat="server" GroupName="s343" Text="Yes" />
            </div>
        </div>
        <div class="row">
            <div class="col-md-8">
                <asp:RadioButton ID="rb_s343_2" runat="server" GroupName="s343" Text="No" />
            </div>
        </div>

        <p>&nbsp;</p>
        <div class="row tr-question">
            <div class="col-md-12">If yes, after how many months is an employee eligible for PTO?</div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <span class="input-group"><span class="input-group">
                    <asp:TextBox ID="txt_s344" runat="server" CssClass="form-control text-right"></asp:TextBox>
                    <span class="input-group-addon">mos.</span>
                </span></span>
            </div>
        </div>
        <hr />

        <div class="row tr-question">
            <div class="col-md-12">2. How many PTO days are given to employees with 5 years of service?</div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <span class="input-group"><span class="input-group">
                    <asp:TextBox ID="txt_s345" runat="server" CssClass="form-control text-right"></asp:TextBox>
                    <span class="input-group-addon">days</span>
                </span></span>
            </div>
        </div>
        <hr />

        <div class="row tr-question">
            <div class="col-md-12">3. Can unused PTO carry over to the next year?</div>
        </div>
        <div class="row">
            <div class="col-md-8">
                <asp:RadioButton ID="rb_s346_1" runat="server" GroupName="s346" Text="Yes" />
            </div>
        </div>
        <div class="row">
            <div class="col-md-8">
                <asp:RadioButton ID="rb_s346_2" runat="server" GroupName="s346" Text="No" />
            </div>
        </div>

        <p>&nbsp;</p>
        <div class="row tr-question">
            <div class="col-md-12">If “yes,” how many days?</div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <span class="input-group"><span class="input-group">
                    <asp:TextBox ID="txt_s347" runat="server" CssClass="form-control text-right"></asp:TextBox>
                    <span class="input-group-addon">days</span>
                </span></span>
            </div>
        </div>
        <hr />

        <div class="row tr-question">
            <div class="col-md-12">4. Can employees receive compensation for unused PTO days?</div>
        </div>
        <div class="row">
            <div class="col-md-8">
                <asp:RadioButton ID="rb_s348_1" runat="server" GroupName="s348" Text="Yes" />
            </div>
        </div>
        <div class="row">
            <div class="col-md-8">
                <asp:RadioButton ID="rb_s348_2" runat="server" GroupName="s348" Text="No" />
            </div>
        </div>



        <h3 class="h3-header">Vacation, Sick and Other Leave</h3>

        <p>&nbsp;</p>
        <div class="row tr-question">
            <div class="col-md-12">
                1. Number of days of paid vacation per year
                <br />
                <em class="small">(Indicate "0" if none-do not leave blank)</em>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <table class="table-condensed table-inner-borders">
                    <tr>
                        <td>&nbsp;</td>
                        <td class="text-center"><strong>Full-Time Employees</strong></td>
                        <td class="text-center"><strong>Part-Time Employees</strong></td>
                    </tr>
                    <tr>
                        <td class="text-right">Less than 1 year on job</td>
                        <td>
                            <span class="input-group"><span class="input-group">
                                <asp:TextBox ID="txt_s349" runat="server" CssClass="form-control text-right"></asp:TextBox>
                                <span class="input-group-addon">days</span>
                            </span></span>
                        </td>
                        <td>
                            <span class="input-group"><span class="input-group">
                                <asp:TextBox ID="txt_s353" runat="server" CssClass="form-control text-right"></asp:TextBox>
                                <span class="input-group-addon">days</span>
                            </span></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="text-right">1 year</td>
                        <td>
                            <span class="input-group"><span class="input-group">
                                <asp:TextBox ID="txt_s350" runat="server" CssClass="form-control text-right"></asp:TextBox>
                                <span class="input-group-addon">days</span>
                            </span></span>
                        </td>
                        <td>
                            <span class="input-group"><span class="input-group">
                                <asp:TextBox ID="txt_s354" runat="server" CssClass="form-control text-right"></asp:TextBox>
                                <span class="input-group-addon">days</span>
                            </span></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="text-right">5 years</td>
                        <td>
                            <span class="input-group"><span class="input-group">
                                <asp:TextBox ID="txt_s351" runat="server" CssClass="form-control text-right"></asp:TextBox>
                                <span class="input-group-addon">days</span>
                            </span></span>
                        </td>
                        <td>
                            <span class="input-group"><span class="input-group">
                                <asp:TextBox ID="txt_s355" runat="server" CssClass="form-control text-right"></asp:TextBox>
                                <span class="input-group-addon">days</span>
                            </span></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="text-right">10 years</td>
                        <td>
                            <span class="input-group"><span class="input-group">
                                <asp:TextBox ID="txt_s352" runat="server" CssClass="form-control text-right"></asp:TextBox>
                                <span class="input-group-addon">days</span>
                            </span></span>
                        </td>
                        <td>
                            <span class="input-group"><span class="input-group">
                                <asp:TextBox ID="txt_s356" runat="server" CssClass="form-control text-right"></asp:TextBox>
                                <span class="input-group-addon">days</span>
                            </span></span>
                        </td>
                    </tr>
                </table>

            </div>
        </div>
        <hr />


        <div class="row tr-question">
            <div class="col-md-12">2. Can unused vacation carry over to the next year?</div>
        </div>
        <div class="row">
            <div class="col-md-8">
                <asp:RadioButton ID="rb_s357_1" runat="server" GroupName="s357" Text="Yes" />
            </div>
        </div>
        <div class="row">
            <div class="col-md-8">
                <asp:RadioButton ID="rb_s357_2" runat="server" GroupName="s357" Text="No" />
            </div>
        </div>

        <p>&nbsp;</p>
        <div class="row tr-question">
            <div class="col-md-12">If “yes,” how many days?</div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <span class="input-group"><span class="input-group">
                    <asp:TextBox ID="txt_s358" runat="server" CssClass="form-control text-right"></asp:TextBox>
                    <span class="input-group-addon">days</span>
                </span></span>
            </div>
        </div>
        <hr />


        <div class="row tr-question">
            <div class="col-md-12">3. Can employees receive compensation for unused vacation days?</div>
        </div>
        <div class="row">
            <div class="col-md-8">
                <asp:RadioButton ID="rb_s359_1" runat="server" GroupName="s359" Text="Yes" />
            </div>
        </div>
        <div class="row">
            <div class="col-md-8">
                <asp:RadioButton ID="rb_s359_2" runat="server" GroupName="s359" Text="No" />
            </div>
        </div>
        <hr />

        <div class="row tr-question">
            <div class="col-md-12">4. How many paid sick days are allowed per year?</div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <table class="table-condensed table-inner-borders">
                    <tr>
                        <td colspan="2" class="text-right">Full-time employees</td>
                        <td class="text-center">
                            <span class="input-group"><span class="input-group">
                                <asp:TextBox ID="txt_s360" runat="server" CssClass="form-control text-right"></asp:TextBox>
                                <span class="input-group-addon">days</span>
                            </span></span>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" class="text-right">Part-time employees</td>
                        <td class="text-center">
                            <span class="input-group"><span class="input-group">
                                <asp:TextBox ID="txt_s361" runat="server" CssClass="form-control text-right"></asp:TextBox>
                                <span class="input-group-addon">days</span>
                            </span></span>
                        </td>
                    </tr>
                </table>

            </div>
        </div>
        <hr />


        <h3 class="h3-header">Other Paid Time-Off</h3>

        <p>&nbsp;</p>
        <div class="row tr-question">
            <div class="col-md-12">1. Number of paid holidays per year?</div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <span class="input-group"><span class="input-group">
                    <asp:TextBox ID="txt_s362" runat="server" CssClass="form-control text-right"></asp:TextBox>
                    <span class="input-group-addon">days</span>
                </span></span>
            </div>
        </div>
        <hr />

        <div class="row tr-question">
            <div class="col-md-12">2. For which of the following does your company offer paid time off? (check all that apply)</div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <asp:CheckBox ID="cb_s363" runat="server" Text="Parental leave (maternity/paternity, adoption leave)" CssClass="checkbox-inline" />
            </div>
            <div class="col-md-6">
                <asp:CheckBox ID="cb_s366" runat="server" Text="Military leave" CssClass="checkbox-inline" />
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <asp:CheckBox ID="cb_s364" runat="server" Text="Floating holidays" CssClass="checkbox-inline" />
            </div>
            <div class="col-md-6">
                <table>
                    <tr>
                        <td>
                            <asp:CheckBox ID="cb_s367" runat="server" Text="Other" CssClass="checkbox-inline" /></td>
                        <td>
                            <asp:TextBox ID="txt_s367oth" runat="server" CssClass="form-control text-left"></asp:TextBox></td>
                    </tr>
                </table>

            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <asp:CheckBox ID="cb_s365" runat="server" Text="Jury duty" CssClass="checkbox-inline" />
            </div>
            <div class="col-md-6">
                <asp:CheckBox ID="cb_s368" runat="server" Text="None" CssClass="checkbox-inline" />
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <asp:CheckBox ID="cb_s558" runat="server" Text="Bereavement" CssClass="checkbox-inline" />
            </div>
            <div class="col-md-6">
                &nbsp;
            </div>
        </div>
        <hr />


        <div class="row tr-question">
            <div class="col-md-12">3. After how many years of service does vacation/PTO accrual max out?</div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <span class="input-group"><span class="input-group">
                    <asp:TextBox ID="txt_s369" runat="server" CssClass="form-control text-right"></asp:TextBox>
                    <span class="input-group-addon">years</span>
                </span></span>
            </div>
        </div>
        <hr />



        <table class="table-condensed centered">
            <tr>
                <td class="text-center" colspan="3">
                    <asp:CheckBox ID="cb_section5Complete" runat="server" Text="This section is complete." />
                </td>
            </tr>
            <tr>
                <td class="text-center">
                    <asp:Button ID="btn_previous" runat="server" CssClass="btn btn-primary" Text="Previous Section" Width="141px" />
                </td>
                <td class="text-center">
                    <asp:Button ID="btn_save" runat="server" CssClass="btn btn-primary" Text="Save" Width="141px" />
                </td>
                <td class="text-center">
                    <asp:Button ID="btn_next" runat="server" CssClass="btn btn-primary" Text="Next Section" Width="141px" />
                </td>
            </tr>
            <tr>
                <td class="text-center">&nbsp;</td>
                <td class="text-center">
                    <asp:LinkButton ID="lb_toc_bot" runat="server" CssClass="text-bold">Home</asp:LinkButton>
                </td>
                <td class="text-center">&nbsp;</td>
            </tr>
        </table>
    </div>
</asp:Content>

