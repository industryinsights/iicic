﻿Imports Models
Imports DevExpress.Web

Partial Class Surveys_Financial_OfflineSurvey
    Inherits System.Web.UI.Page
    Private Property FilePath() As String
        Get
            Return If(Session("FilePath") Is Nothing, String.Empty, Session("FilePath").ToString())
        End Get
        Set(ByVal value As String)
            Session("FilePath") = value
        End Set
    End Property

    Protected Sub Upload_FileUploadComplete(ByVal sender As Object, ByVal e As DevExpress.Web.FileUploadCompleteEventArgs)
        FilePath = Page.MapPath(String.Format("~/Surveys/Comp/Imports/{0}_{1}_{2}", Session("LocationID"), DateTime.Now.ToString("yyyy-MM-dd hh-mm-ss"), e.UploadedFile.FileName))
        e.UploadedFile.SaveAs(FilePath)

        'Record upload in Log
        Using db As New IICICEntities
            Dim UserId As Integer = Session("UserId")
            Dim ds As New UserUpload With {.UserId = UserId, .FileName = e.UploadedFile.FileName, .UploadDate = Now()}

            db.UserUploads.Add(ds)
            db.SaveChanges()
            Dim loc As New Int32
            If Session("LocationId") IsNot Nothing Then
                loc = Session("LocationId")
            End If
            Dim survey As New CompSurvey
            survey = (From s In db.CompSurveys Where s.LocationID = loc And s.SurveyYear = Now.Year).FirstOrDefault

            If survey Is Nothing Then
                db.CompSurveys.Add(survey)
            End If
            survey.LocationID = loc
            survey.section1Complete = True
            survey.section2Complete = True
            survey.section3Complete = True
            survey.section4Complete = True
            survey.section5Complete = True
            survey.section6Complete = True
            survey.SubmitDate = Now
            survey.ModifiedDate = Now
            survey.IsUpload = True
            db.SaveChanges()
        End Using

        Call PopulateUploadTBL()   'THIS DIDN'T WORK, LET'S JUST REDIRECT

    End Sub

    Sub SaveSurvey(ByVal IsSubmit As Boolean)
        Exit Sub
    End Sub

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load

        If Session("UserId") Is Nothing Then
            Response.Redirect("../../account/login.aspx")
        End If

        Call PopulateUploadTBL()

    End Sub
    'Private Function CreateData() As SqlDataSource
    '    Dim conn As String = "Data Source=66.11.14.181;Initial Catalog=IICIC;User ID=II_USER;Password=IIReport1394*nf;MultipleActiveResultSets=True;Application Name=EntityFramework"
    '    Dim userid As Integer = Session("UserId")
    '    Dim selectCmnd As String = "SELECT [UploadDate],[FileName] FROM [UserUploads] WHERE [UserId] = " & userid & " ORDER BY [UploadDate] DESC"
    '    Return New SqlDataSource(conn, selectCmnd)
    'End Function

    Sub PopulateUploadTBL()

        'ASPxGridView1.DataSource = CreateData()
        'ASPxGridView1.DataBind()

        SummaryTBL.Visible = True   'actually, let's default to true so user can refresh after first upload--False  'default summary list to invisible

        Dim userid As Integer = Session("UserId")
        Using db As New IICICEntities

            Dim uploads = Nothing
            uploads = (From u In db.UserUploads Where u.UserId = userid Order By u.UploadDate Descending)

            If uploads IsNot Nothing Then
                'generate list of uploads

                For Each u As UserUpload In uploads

                    Dim tRow As New TableRow()
                    Dim tCell As New TableCell()
                    tCell.Text = u.UploadDate

                    Dim tCell2 As New TableCell()
                    tCell2.Text = u.FileName

                    tRow.Cells.Add(tCell)   ' Add new TableCell object to row (TEXT)
                    tRow.Cells.Add(tCell2)   ' Add new TableCell object to row (STATUS)

                    SummaryTBL.Rows.Add(tRow)

                    SummaryTBL.Visible = True   'mark visible (need to do inside For Each Loop b/c uploads IsNot Nothing won't work JS 2.23.18)
                Next

            End If

        End Using
    End Sub

End Class
