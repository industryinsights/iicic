﻿
Imports Models

Partial Class Surveys_CompByJob
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load



        If Session("LocationID") Is Nothing Then
            If Not String.IsNullOrEmpty(txt_LocationID.Text) Then
                Session("LocationID") = Convert.ToInt32(txt_LocationID.Text)
            Else
                Response.Redirect("../../account/login.aspx")
            End If
        End If
        If Session("UserId") Is Nothing Then
            If Not String.IsNullOrEmpty(txt_userid.Text) Then
                Session("UserId") = Convert.ToInt32(txt_userid.Text)
            Else
                Response.Redirect("../../account/login.aspx")
            End If
        End If
        If Not IsPostBack Then
            Dim LocationID As Integer = Session("LocationID")
            txt_LocationID.Text = LocationID
            Dim userid As Integer = Session("UserId")
            txt_userid.Text = userid
            If Session("Client") IsNot Nothing And Not String.IsNullOrEmpty(Session("Client")) Then
                lbl_deadline.Text = SurveyHelper.ReturnDeadline(Session("Client").ToString)
            Else
                lbl_deadline.Text = "Friday, April 13, 2018"
            End If
            ddl_location.Items.Clear()
            Dim locations As List(Of ListItem) = SurveyHelper.ReturnLocationList(Session("OrganizationId"))
            For Each l In locations
                ddl_location.Items.Add(l)
            Next
            ddl_location.SelectedValue = Session("LocationID")
            tbl_location.Visible = False
            LoadSurvey()
        End If
    End Sub
    Sub LoadSurvey()
        Using db As New IICICEntities
            Dim LocationID As Integer = txt_LocationID.Text
            Dim survey As New CompSurvey
            survey = (From s In db.CompSurveys Where s.LocationID = LocationID And s.SurveyYear = Now.Year).FirstOrDefault
            If survey IsNot Nothing Then
                lbl_lastSave.Text = survey.ModifiedDate


                '__Custom Show/hide based on assocation____________________________\/

                'default set all to not visible (won't trigger if user does not yet have a record in survey table)
                '   [so default every row to hidden in html - these statements won't be necessary, then]
                'Q900.Visible = False
                'Q903.Visible = False
                'Q906.Visible = False
                'Q909.Visible = False
                'Q912.Visible = False
                'Q720.Visible = False
                'Q723.Visible = False
                'Q726.Visible = False
                'Q729.Visible = False
                'Q732.Visible = False
                'Q735.Visible = False
                'Q738.Visible = False
                'Q741.Visible = False
                'Q744.Visible = False
                'Q747.Visible = False
                'Q750.Visible = False
                'Q753.Visible = False
                'Q756.Visible = False
                'Q759.Visible = False
                'Q762.Visible = False
                'Q765.Visible = False
                'Q768.Visible = False
                'Q771.Visible = False
                'Q774.Visible = False
                'Q777.Visible = False
                'Q780.Visible = False
                'Q783.Visible = False
                'Q786.Visible = False
                'Q789.Visible = False
                'Q792.Visible = False
                'Q795.Visible = False
                'Q798.Visible = False
                'Q801.Visible = False
                'Q804.Visible = False
                'Q807.Visible = False
                'Q810.Visible = False
                'Q813.Visible = False
                'Q816.Visible = False
                'Q819.Visible = False
                'Q822.Visible = False
                'Q825.Visible = False
                'Q828.Visible = False
                'Q831.Visible = False
                'Q834.Visible = False
                'Q837.Visible = False
                'Q840.Visible = False
                'Q843.Visible = False
                'Q846.Visible = False
                'Q849.Visible = False
                'Q852.Visible = False
                'Q855.Visible = False
                'Q858.Visible = False
                'Q861.Visible = False
                'Q864.Visible = False
                'Q965.Visible = False
                'Q867.Visible = False
                'Q870.Visible = False
                'Q873.Visible = False
                'Q876.Visible = False
                'Q879.Visible = False
                'Q882.Visible = False
                'Q885.Visible = False
                'Q888.Visible = False


                'show individual "Other Positions" based on association affiliation/membership
                If survey.s1900 IsNot Nothing Then
                    'show sections
                    If survey.s1900 Then tr_otherpositions.Visible = True
                    If survey.s1900 = True Then Q900.Visible = True
                    If survey.s1900 = True Then Q720.Visible = True
                    If survey.s1900 = True Then Q723.Visible = True
                    If survey.s1900 = True Then Q726.Visible = True
                    If survey.s1900 = True Then Q729.Visible = True
                    If survey.s1900 = True Then Q732.Visible = True
                    If survey.s1900 = True Then Q735.Visible = True
                    If survey.s1900 = True Then Q738.Visible = True
                    If survey.s1900 = True Then Q741.Visible = True
                    If survey.s1900 = True Then Q906.Visible = True
                    If survey.s1900 = True Then Q909.Visible = True
                    If survey.s1900 = True Then Q912.Visible = True
                End If
                If survey.s1901 IsNot Nothing Then
                    'show sections
                    If survey.s1901 Then tr_otherpositions.Visible = True
                    If survey.s1901 = True Then Q744.Visible = True
                    If survey.s1901 = True Then Q747.Visible = True
                    If survey.s1901 = True Then Q750.Visible = True
                    If survey.s1901 = True Then Q753.Visible = True
                    If survey.s1901 = True Then Q756.Visible = True
                    If survey.s1901 = True Then Q759.Visible = True
                    If survey.s1901 = True Then Q762.Visible = True
                    If survey.s1901 = True Then Q765.Visible = True
                    If survey.s1901 = True Then Q903.Visible = True
                End If
                'If survey.s1902 IsNot Nothing Then
                '    'show sections
                '    'PASTE
                'End If
                'If survey.s1903 IsNot Nothing Then
                '    'show sections
                '    'PASTE
                'End If
                If survey.s1904 IsNot Nothing Then
                    'show sections
                    If survey.s1904 Then tr_otherpositions.Visible = True
                    If survey.s1904 = True Then Q768.Visible = True
                    If survey.s1904 = True Then Q771.Visible = True
                    If survey.s1904 = True Then Q774.Visible = True
                    If survey.s1904 = True Then Q777.Visible = True
                    If survey.s1904 = True Then Q780.Visible = True
                    If survey.s1904 = True Then Q783.Visible = True
                    If survey.s1904 = True Then Q786.Visible = True
                    If survey.s1904 = True Then Q789.Visible = True
                    If survey.s1904 = True Then Q792.Visible = True
                    If survey.s1904 = True Then Q795.Visible = True
                End If
                If survey.s1905 IsNot Nothing Then
                    'show sections
                    If survey.s1905 Then tr_otherpositions.Visible = True
                    If survey.s1905 = True Then Q798.Visible = True
                    If survey.s1905 = True Then Q801.Visible = True
                    If survey.s1905 = True Then Q804.Visible = True
                    If survey.s1905 = True Then Q807.Visible = True
                    If survey.s1905 = True Then Q810.Visible = True
                    If survey.s1905 = True Then Q813.Visible = True
                End If
                'If survey.s1906 IsNot Nothing Then
                '    'show sections
                '    'PASTE
                'End If
                'If survey.s1907 IsNot Nothing Then
                '    'show sections
                '    'PASTE
                'End If
                'If survey.s1908 IsNot Nothing Then
                '    'show sections
                '    'PASTE
                'End If
                'If survey.s1909 IsNot Nothing Then
                '    'show sections
                '    'PASTE
                'End If
                If survey.s1908 IsNot Nothing Then
                    If survey.s1908 Then
                        tr_otherpositions.Visible = True
                        Q840.Visible = True
                    End If
                End If
                If survey.s1910 IsNot Nothing Then
                    'show sections
                    If survey.s1910 Then tr_otherpositions.Visible = True
                    If survey.s1910 = True Then Q816.Visible = True
                    If survey.s1910 = True Then Q819.Visible = True
                    If survey.s1910 = True Then Q822.Visible = True
                    If survey.s1910 = True Then Q825.Visible = True
                    If survey.s1910 = True Then Q828.Visible = True
                    If survey.s1910 = True Then Q831.Visible = True
                    If survey.s1910 = True Then Q834.Visible = True
                    If survey.s1910 = True Then Q900.Visible = True
                    If survey.s1910 = True Then Q903.Visible = True
                    If survey.s1910 = True Then Q906.Visible = True
                    If survey.s1910 = True Then Q909.Visible = True
                    If survey.s1910 = True Then Q912.Visible = True
                End If
                'If survey.s1911 IsNot Nothing Then
                '    'show sections
                '    'PASTE
                'End If
                If survey.s1912 IsNot Nothing Then
                    'show sections
                    If survey.s1912 Then tr_otherpositions.Visible = True
                    If survey.s1912 = True Then Q855.Visible = True
                    If survey.s1912 = True Then Q858.Visible = True
                End If
                'If survey.s1913 IsNot Nothing Then
                '    'show sections
                '    'PASTE
                'End If
                If survey.s1914 IsNot Nothing Then
                    'show sections
                    If survey.s1914 Then tr_otherpositions.Visible = True
                    If survey.s1914 = True Then Q849.Visible = True
                End If
                'If survey.s1915 IsNot Nothing Then
                '    'show sections
                '    'PASTE
                'End If
                If survey.s1916 IsNot Nothing Then
                    'show sections
                    If survey.s1916 Then tr_otherpositions.Visible = True
                    If survey.s1916 = True Then Q855.Visible = True
                    If survey.s1916 = True Then Q858.Visible = True
                End If
                'If survey.s1917 IsNot Nothing Then
                '    'show sections
                '    'PASTE
                'End If
                'If survey.s1918 IsNot Nothing Then
                '    'show sections
                '    'PASTE
                'End If
                If survey.s1919 IsNot Nothing Then
                    'show sections
                    If survey.s1919 Then tr_otherpositions.Visible = True
                    If survey.s1919 = True Then Q861.Visible = True
                    If survey.s1919 = True Then Q864.Visible = True
                    If survey.s1919 = True Then Q867.Visible = True
                    If survey.s1919 = True Then Q870.Visible = True
                    If survey.s1919 = True Then Q965.Visible = True
                End If
                'If survey.s1920 IsNot Nothing Then
                '    'show sections
                '    'PASTE
                'End If
                If survey.s1921 IsNot Nothing Then
                    'show sections
                    If survey.s1921 Then tr_otherpositions.Visible = True
                    If survey.s1921 = True Then Q873.Visible = True
                    If survey.s1921 = True Then Q906.Visible = True
                End If
                If survey.s1922 IsNot Nothing Then
                    'show sections
                    If survey.s1922 Then tr_otherpositions.Visible = True
                    If survey.s1922 = True Then Q876.Visible = True
                    If survey.s1922 = True Then Q879.Visible = True
                    If survey.s1922 = True Then Q882.Visible = True
                    If survey.s1922 = True Then Q885.Visible = True
                    If survey.s1922 = True Then Q888.Visible = True
                End If
                '__Custom Show/hide based on assocation____________________________/\

                '---PASTE LOAD STATEMENTS---
                If survey.s182 IsNot Nothing Then
                    txt_s182.Text = String.Format(survey.s182, "N0")
                End If
                If survey.s183 IsNot Nothing Then
                    txt_s183.Text = String.Format(survey.s183, "N0")
                End If
                If survey.s184 IsNot Nothing Then
                    txt_s184.Text = String.Format(survey.s184, "N0")
                End If
                If survey.s185 IsNot Nothing Then
                    txt_s185.Text = String.Format(survey.s185, "N0")
                End If
                If survey.s186 IsNot Nothing Then
                    txt_s186.Text = String.Format(survey.s186, "N0")
                End If
                If survey.s187 IsNot Nothing Then
                    txt_s187.Text = String.Format(survey.s187, "N0")
                End If
                If survey.s188 IsNot Nothing Then
                    txt_s188.Text = String.Format(survey.s188, "N0")
                End If
                If survey.s189 IsNot Nothing Then
                    txt_s189.Text = String.Format(survey.s189, "N0")
                End If
                If survey.s190 IsNot Nothing Then
                    txt_s190.Text = String.Format(survey.s190, "N0")
                End If
                If survey.s191 IsNot Nothing Then
                    txt_s191.Text = String.Format(survey.s191, "N0")
                End If
                If survey.s192 IsNot Nothing Then
                    txt_s192.Text = String.Format(survey.s192, "N0")
                End If
                If survey.s193 IsNot Nothing Then
                    txt_s193.Text = String.Format(survey.s193, "N0")
                End If
                If survey.s194 IsNot Nothing Then
                    txt_s194.Text = String.Format(survey.s194, "N0")
                End If
                If survey.s195 IsNot Nothing Then
                    txt_s195.Text = String.Format(survey.s195, "N0")
                End If
                If survey.s196 IsNot Nothing Then
                    txt_s196.Text = String.Format(survey.s196, "N0")
                End If
                If survey.s197 IsNot Nothing Then
                    txt_s197.Text = String.Format(survey.s197, "N0")
                End If
                If survey.s198 IsNot Nothing Then
                    txt_s198.Text = String.Format(survey.s198, "N0")
                End If
                If survey.s199 IsNot Nothing Then
                    txt_s199.Text = String.Format(survey.s199, "N0")
                End If
                If survey.s200 IsNot Nothing Then
                    txt_s200.Text = String.Format(survey.s200, "N0")
                End If
                If survey.s201 IsNot Nothing Then
                    txt_s201.Text = String.Format(survey.s201, "N0")
                End If
                If survey.s202 IsNot Nothing Then
                    txt_s202.Text = String.Format(survey.s202, "N0")
                End If
                If survey.s203 IsNot Nothing Then
                    txt_s203.Text = String.Format(survey.s203, "N0")
                End If
                If survey.s204 IsNot Nothing Then
                    txt_s204.Text = String.Format(survey.s204, "N0")
                End If
                If survey.s205 IsNot Nothing Then
                    txt_s205.Text = String.Format(survey.s205, "N0")
                End If
                If survey.s206 IsNot Nothing Then
                    txt_s206.Text = String.Format(survey.s206, "N0")
                End If
                If survey.s207 IsNot Nothing Then
                    txt_s207.Text = String.Format(survey.s207, "N0")
                End If
                If survey.s208 IsNot Nothing Then
                    txt_s208.Text = String.Format(survey.s208, "N0")
                End If
                If survey.s209 IsNot Nothing Then
                    txt_s209.Text = String.Format(survey.s209, "N0")
                End If
                If survey.s210 IsNot Nothing Then
                    txt_s210.Text = String.Format(survey.s210, "N0")
                End If
                If survey.s211 IsNot Nothing Then
                    txt_s211.Text = String.Format(survey.s211, "N0")
                End If
                If survey.s212 IsNot Nothing Then
                    txt_s212.Text = String.Format(survey.s212, "N0")
                End If
                If survey.s213 IsNot Nothing Then
                    txt_s213.Text = String.Format(survey.s213, "N0")
                End If
                If survey.s214 IsNot Nothing Then
                    txt_s214.Text = String.Format(survey.s214, "N0")
                End If
                If survey.s547 IsNot Nothing Then
                    txt_s547.Text = String.Format(survey.s547, "N0")
                End If
                If survey.s548 IsNot Nothing Then
                    txt_s548.Text = String.Format(survey.s548, "N0")
                End If
                If survey.s549 IsNot Nothing Then
                    txt_s549.Text = String.Format(survey.s549, "N0")
                End If
                If survey.s215 IsNot Nothing Then
                    txt_s215.Text = String.Format(survey.s215, "N0")
                End If
                If survey.s216 IsNot Nothing Then
                    txt_s216.Text = String.Format(survey.s216, "N0")
                End If
                If survey.s217 IsNot Nothing Then
                    txt_s217.Text = String.Format(survey.s217, "N0")
                End If
                If survey.s550 IsNot Nothing Then
                    txt_s550.Text = String.Format(survey.s550, "N0")
                End If
                If survey.s551 IsNot Nothing Then
                    txt_s551.Text = String.Format(survey.s551, "N0")
                End If
                If survey.s552 IsNot Nothing Then
                    txt_s552.Text = String.Format(survey.s552, "N0")
                End If
                If survey.s218 IsNot Nothing Then
                    txt_s218.Text = String.Format(survey.s218, "N0")
                End If
                If survey.s219 IsNot Nothing Then
                    txt_s219.Text = String.Format(survey.s219, "N0")
                End If
                If survey.s220 IsNot Nothing Then
                    txt_s220.Text = String.Format(survey.s220, "N0")
                End If
                If survey.s221 IsNot Nothing Then
                    txt_s221.Text = String.Format(survey.s221, "N0")
                End If
                If survey.s222 IsNot Nothing Then
                    txt_s222.Text = String.Format(survey.s222, "N0")
                End If
                If survey.s223 IsNot Nothing Then
                    txt_s223.Text = String.Format(survey.s223, "N0")
                End If
                If survey.s224 IsNot Nothing Then
                    txt_s224.Text = String.Format(survey.s224, "N0")
                End If
                If survey.s225 IsNot Nothing Then
                    txt_s225.Text = String.Format(survey.s225, "N0")
                End If
                If survey.s226 IsNot Nothing Then
                    txt_s226.Text = String.Format(survey.s226, "N0")
                End If
                If survey.s227 IsNot Nothing Then
                    txt_s227.Text = String.Format(survey.s227, "N0")
                End If
                If survey.s228 IsNot Nothing Then
                    txt_s228.Text = String.Format(survey.s228, "N0")
                End If
                If survey.s229 IsNot Nothing Then
                    txt_s229.Text = String.Format(survey.s229, "N0")
                End If
                If survey.s230 IsNot Nothing Then
                    txt_s230.Text = String.Format(survey.s230, "N0")
                End If
                If survey.s231 IsNot Nothing Then
                    txt_s231.Text = String.Format(survey.s231, "N0")
                End If
                If survey.s232 IsNot Nothing Then
                    txt_s232.Text = String.Format(survey.s232, "N0")
                End If
                If survey.s233 IsNot Nothing Then
                    txt_s233.Text = String.Format(survey.s233, "N0")
                End If
                If survey.s234 IsNot Nothing Then
                    txt_s234.Text = String.Format(survey.s234, "N0")
                End If
                If survey.s235 IsNot Nothing Then
                    txt_s235.Text = String.Format(survey.s235, "N0")
                End If
                If survey.s236 IsNot Nothing Then
                    txt_s236.Text = String.Format(survey.s236, "N0")
                End If
                If survey.s237 IsNot Nothing Then
                    txt_s237.Text = String.Format(survey.s237, "N0")
                End If
                If survey.s238 IsNot Nothing Then
                    txt_s238.Text = String.Format(survey.s238, "N0")
                End If
                If survey.s239 IsNot Nothing Then
                    txt_s239.Text = String.Format(survey.s239, "N0")
                End If
                If survey.s240 IsNot Nothing Then
                    txt_s240.Text = String.Format(survey.s240, "N0")
                End If
                If survey.s241 IsNot Nothing Then
                    txt_s241.Text = String.Format(survey.s241, "N0")
                End If
                If survey.s242 IsNot Nothing Then
                    txt_s242.Text = String.Format(survey.s242, "N0")
                End If
                If survey.s243 IsNot Nothing Then
                    txt_s243.Text = String.Format(survey.s243, "N0")
                End If
                If survey.s244 IsNot Nothing Then
                    txt_s244.Text = String.Format(survey.s244, "N0")
                End If
                If survey.s245 IsNot Nothing Then
                    txt_s245.Text = String.Format(survey.s245, "N0")
                End If
                If survey.s246 IsNot Nothing Then
                    txt_s246.Text = String.Format(survey.s246, "N0")
                End If
                If survey.s247 IsNot Nothing Then
                    txt_s247.Text = String.Format(survey.s247, "N0")
                End If
                If survey.s248 IsNot Nothing Then
                    txt_s248.Text = String.Format(survey.s248, "N0")
                End If
                If survey.s249 IsNot Nothing Then
                    txt_s249.Text = String.Format(survey.s249, "N0")
                End If
                If survey.s250 IsNot Nothing Then
                    txt_s250.Text = String.Format(survey.s250, "N0")
                End If
                If survey.s251 IsNot Nothing Then
                    txt_s251.Text = String.Format(survey.s251, "N0")
                End If
                If survey.s252 IsNot Nothing Then
                    txt_s252.Text = String.Format(survey.s252, "N0")
                End If
                If survey.s253 IsNot Nothing Then
                    txt_s253.Text = String.Format(survey.s253, "N0")
                End If
                If survey.s254 IsNot Nothing Then
                    txt_s254.Text = String.Format(survey.s254, "N0")
                End If
                If survey.s255 IsNot Nothing Then
                    txt_s255.Text = String.Format(survey.s255, "N0")
                End If
                If survey.s256 IsNot Nothing Then
                    txt_s256.Text = String.Format(survey.s256, "N0")
                End If
                If survey.s257 IsNot Nothing Then
                    txt_s257.Text = String.Format(survey.s257, "N0")
                End If
                If survey.s258 IsNot Nothing Then
                    txt_s258.Text = String.Format(survey.s258, "N0")
                End If
                If survey.s259 IsNot Nothing Then
                    txt_s259.Text = String.Format(survey.s259, "N0")
                End If
                If survey.s260 IsNot Nothing Then
                    txt_s260.Text = String.Format(survey.s260, "N0")
                End If
                If survey.s261 IsNot Nothing Then
                    txt_s261.Text = String.Format(survey.s261, "N0")
                End If
                If survey.s262 IsNot Nothing Then
                    txt_s262.Text = String.Format(survey.s262, "N0")
                End If
                If survey.s263 IsNot Nothing Then
                    txt_s263.Text = String.Format(survey.s263, "N0")
                End If
                If survey.s264 IsNot Nothing Then
                    txt_s264.Text = String.Format(survey.s264, "N0")
                End If
                If survey.s265 IsNot Nothing Then
                    txt_s265.Text = String.Format(survey.s265, "N0")
                End If
                If survey.s266 IsNot Nothing Then
                    txt_s266.Text = String.Format(survey.s266, "N0")
                End If
                If survey.s267 IsNot Nothing Then
                    txt_s267.Text = String.Format(survey.s267, "N0")
                End If
                If survey.s268 IsNot Nothing Then
                    txt_s268.Text = String.Format(survey.s268, "N0")
                End If
                If survey.s269 IsNot Nothing Then
                    txt_s269.Text = String.Format(survey.s269, "N0")
                End If
                If survey.s270 IsNot Nothing Then
                    txt_s270.Text = String.Format(survey.s270, "N0")
                End If
                If survey.s271 IsNot Nothing Then
                    txt_s271.Text = String.Format(survey.s271, "N0")
                End If
                If survey.s272 IsNot Nothing Then
                    txt_s272.Text = String.Format(survey.s272, "N0")
                End If
                If survey.s273 IsNot Nothing Then
                    txt_s273.Text = String.Format(survey.s273, "N0")
                End If
                If survey.s274 IsNot Nothing Then
                    txt_s274.Text = String.Format(survey.s274, "N0")
                End If
                If survey.s275 IsNot Nothing Then
                    txt_s275.Text = String.Format(survey.s275, "N0")
                End If
                If survey.s276 IsNot Nothing Then
                    txt_s276.Text = String.Format(survey.s276, "N0")
                End If
                If survey.s277 IsNot Nothing Then
                    txt_s277.Text = String.Format(survey.s277, "N0")
                End If
                If survey.s553 IsNot Nothing Then
                    txt_s553.Text = String.Format(survey.s553, "N0")
                End If
                If survey.s554 IsNot Nothing Then
                    txt_s554.Text = String.Format(survey.s554, "N0")
                End If
                If survey.s555 IsNot Nothing Then
                    txt_s555.Text = String.Format(survey.s555, "N0")
                End If
                If survey.s900 IsNot Nothing Then
                    txt_s900.Text = String.Format(survey.s900, "N0")
                End If
                If survey.s901 IsNot Nothing Then
                    txt_s901.Text = String.Format(survey.s901, "N0")
                End If
                If survey.s902 IsNot Nothing Then
                    txt_s902.Text = String.Format(survey.s902, "N0")
                End If
                If survey.s903 IsNot Nothing Then
                    txt_s903.Text = String.Format(survey.s903, "N0")
                End If
                If survey.s904 IsNot Nothing Then
                    txt_s904.Text = String.Format(survey.s904, "N0")
                End If
                If survey.s905 IsNot Nothing Then
                    txt_s905.Text = String.Format(survey.s905, "N0")
                End If
                If survey.s906 IsNot Nothing Then
                    txt_s906.Text = String.Format(survey.s906, "N0")
                End If
                If survey.s907 IsNot Nothing Then
                    txt_s907.Text = String.Format(survey.s907, "N0")
                End If
                If survey.s908 IsNot Nothing Then
                    txt_s908.Text = String.Format(survey.s908, "N0")
                End If
                If survey.s909 IsNot Nothing Then
                    txt_s909.Text = String.Format(survey.s909, "N0")
                End If
                If survey.s910 IsNot Nothing Then
                    txt_s910.Text = String.Format(survey.s910, "N0")
                End If
                If survey.s911 IsNot Nothing Then
                    txt_s911.Text = String.Format(survey.s911, "N0")
                End If
                If survey.s912 IsNot Nothing Then
                    txt_s912.Text = String.Format(survey.s912, "N0")
                End If
                If survey.s913 IsNot Nothing Then
                    txt_s913.Text = String.Format(survey.s913, "N0")
                End If
                If survey.s914 IsNot Nothing Then
                    txt_s914.Text = String.Format(survey.s914, "N0")
                End If
                If survey.s720 IsNot Nothing Then
                    txt_s720.Text = String.Format(survey.s720, "N0")
                End If
                If survey.s721 IsNot Nothing Then
                    txt_s721.Text = String.Format(survey.s721, "N0")
                End If
                If survey.s722 IsNot Nothing Then
                    txt_s722.Text = String.Format(survey.s722, "N0")
                End If
                If survey.s723 IsNot Nothing Then
                    txt_s723.Text = String.Format(survey.s723, "N0")
                End If
                If survey.s724 IsNot Nothing Then
                    txt_s724.Text = String.Format(survey.s724, "N0")
                End If
                If survey.s725 IsNot Nothing Then
                    txt_s725.Text = String.Format(survey.s725, "N0")
                End If
                If survey.s726 IsNot Nothing Then
                    txt_s726.Text = String.Format(survey.s726, "N0")
                End If
                If survey.s727 IsNot Nothing Then
                    txt_s727.Text = String.Format(survey.s727, "N0")
                End If
                If survey.s728 IsNot Nothing Then
                    txt_s728.Text = String.Format(survey.s728, "N0")
                End If
                If survey.s729 IsNot Nothing Then
                    txt_s729.Text = String.Format(survey.s729, "N0")
                End If
                If survey.s730 IsNot Nothing Then
                    txt_s730.Text = String.Format(survey.s730, "N0")
                End If
                If survey.s731 IsNot Nothing Then
                    txt_s731.Text = String.Format(survey.s731, "N0")
                End If
                If survey.s732 IsNot Nothing Then
                    txt_s732.Text = String.Format(survey.s732, "N0")
                End If
                If survey.s733 IsNot Nothing Then
                    txt_s733.Text = String.Format(survey.s733, "N0")
                End If
                If survey.s734 IsNot Nothing Then
                    txt_s734.Text = String.Format(survey.s734, "N0")
                End If
                If survey.s735 IsNot Nothing Then
                    txt_s735.Text = String.Format(survey.s735, "N0")
                End If
                If survey.s736 IsNot Nothing Then
                    txt_s736.Text = String.Format(survey.s736, "N0")
                End If
                If survey.s737 IsNot Nothing Then
                    txt_s737.Text = String.Format(survey.s737, "N0")
                End If
                If survey.s738 IsNot Nothing Then
                    txt_s738.Text = String.Format(survey.s738, "N0")
                End If
                If survey.s739 IsNot Nothing Then
                    txt_s739.Text = String.Format(survey.s739, "N0")
                End If
                If survey.s740 IsNot Nothing Then
                    txt_s740.Text = String.Format(survey.s740, "N0")
                End If
                If survey.s741 IsNot Nothing Then
                    txt_s741.Text = String.Format(survey.s741, "N0")
                End If
                If survey.s742 IsNot Nothing Then
                    txt_s742.Text = String.Format(survey.s742, "N0")
                End If
                If survey.s743 IsNot Nothing Then
                    txt_s743.Text = String.Format(survey.s743, "N0")
                End If
                If survey.s744 IsNot Nothing Then
                    txt_s744.Text = String.Format(survey.s744, "N0")
                End If
                If survey.s745 IsNot Nothing Then
                    txt_s745.Text = String.Format(survey.s745, "N0")
                End If
                If survey.s746 IsNot Nothing Then
                    txt_s746.Text = String.Format(survey.s746, "N0")
                End If
                If survey.s747 IsNot Nothing Then
                    txt_s747.Text = String.Format(survey.s747, "N0")
                End If
                If survey.s748 IsNot Nothing Then
                    txt_s748.Text = String.Format(survey.s748, "N0")
                End If
                If survey.s749 IsNot Nothing Then
                    txt_s749.Text = String.Format(survey.s749, "N0")
                End If
                If survey.s750 IsNot Nothing Then
                    txt_s750.Text = String.Format(survey.s750, "N0")
                End If
                If survey.s751 IsNot Nothing Then
                    txt_s751.Text = String.Format(survey.s751, "N0")
                End If
                If survey.s752 IsNot Nothing Then
                    txt_s752.Text = String.Format(survey.s752, "N0")
                End If
                If survey.s753 IsNot Nothing Then
                    txt_s753.Text = String.Format(survey.s753, "N0")
                End If
                If survey.s754 IsNot Nothing Then
                    txt_s754.Text = String.Format(survey.s754, "N0")
                End If
                If survey.s755 IsNot Nothing Then
                    txt_s755.Text = String.Format(survey.s755, "N0")
                End If
                If survey.s756 IsNot Nothing Then
                    txt_s756.Text = String.Format(survey.s756, "N0")
                End If
                If survey.s757 IsNot Nothing Then
                    txt_s757.Text = String.Format(survey.s757, "N0")
                End If
                If survey.s758 IsNot Nothing Then
                    txt_s758.Text = String.Format(survey.s758, "N0")
                End If
                If survey.s759 IsNot Nothing Then
                    txt_s759.Text = String.Format(survey.s759, "N0")
                End If
                If survey.s760 IsNot Nothing Then
                    txt_s760.Text = String.Format(survey.s760, "N0")
                End If
                If survey.s761 IsNot Nothing Then
                    txt_s761.Text = String.Format(survey.s761, "N0")
                End If
                If survey.s762 IsNot Nothing Then
                    txt_s762.Text = String.Format(survey.s762, "N0")
                End If
                If survey.s763 IsNot Nothing Then
                    txt_s763.Text = String.Format(survey.s763, "N0")
                End If
                If survey.s764 IsNot Nothing Then
                    txt_s764.Text = String.Format(survey.s764, "N0")
                End If
                If survey.s765 IsNot Nothing Then
                    txt_s765.Text = String.Format(survey.s765, "N0")
                End If
                If survey.s766 IsNot Nothing Then
                    txt_s766.Text = String.Format(survey.s766, "N0")
                End If
                If survey.s767 IsNot Nothing Then
                    txt_s767.Text = String.Format(survey.s767, "N0")
                End If
                If survey.s768 IsNot Nothing Then
                    txt_s768.Text = String.Format(survey.s768, "N0")
                End If
                If survey.s769 IsNot Nothing Then
                    txt_s769.Text = String.Format(survey.s769, "N0")
                End If
                If survey.s770 IsNot Nothing Then
                    txt_s770.Text = String.Format(survey.s770, "N0")
                End If
                If survey.s771 IsNot Nothing Then
                    txt_s771.Text = String.Format(survey.s771, "N0")
                End If
                If survey.s772 IsNot Nothing Then
                    txt_s772.Text = String.Format(survey.s772, "N0")
                End If
                If survey.s773 IsNot Nothing Then
                    txt_s773.Text = String.Format(survey.s773, "N0")
                End If
                If survey.s774 IsNot Nothing Then
                    txt_s774.Text = String.Format(survey.s774, "N0")
                End If
                If survey.s775 IsNot Nothing Then
                    txt_s775.Text = String.Format(survey.s775, "N0")
                End If
                If survey.s776 IsNot Nothing Then
                    txt_s776.Text = String.Format(survey.s776, "N0")
                End If
                If survey.s777 IsNot Nothing Then
                    txt_s777.Text = String.Format(survey.s777, "N0")
                End If
                If survey.s778 IsNot Nothing Then
                    txt_s778.Text = String.Format(survey.s778, "N0")
                End If
                If survey.s779 IsNot Nothing Then
                    txt_s779.Text = String.Format(survey.s779, "N0")
                End If
                If survey.s780 IsNot Nothing Then
                    txt_s780.Text = String.Format(survey.s780, "N0")
                End If
                If survey.s781 IsNot Nothing Then
                    txt_s781.Text = String.Format(survey.s781, "N0")
                End If
                If survey.s782 IsNot Nothing Then
                    txt_s782.Text = String.Format(survey.s782, "N0")
                End If
                If survey.s783 IsNot Nothing Then
                    txt_s783.Text = String.Format(survey.s783, "N0")
                End If
                If survey.s784 IsNot Nothing Then
                    txt_s784.Text = String.Format(survey.s784, "N0")
                End If
                If survey.s785 IsNot Nothing Then
                    txt_s785.Text = String.Format(survey.s785, "N0")
                End If
                If survey.s786 IsNot Nothing Then
                    txt_s786.Text = String.Format(survey.s786, "N0")
                End If
                If survey.s787 IsNot Nothing Then
                    txt_s787.Text = String.Format(survey.s787, "N0")
                End If
                If survey.s788 IsNot Nothing Then
                    txt_s788.Text = String.Format(survey.s788, "N0")
                End If
                If survey.s789 IsNot Nothing Then
                    txt_s789.Text = String.Format(survey.s789, "N0")
                End If
                If survey.s790 IsNot Nothing Then
                    txt_s790.Text = String.Format(survey.s790, "N0")
                End If
                If survey.s791 IsNot Nothing Then
                    txt_s791.Text = String.Format(survey.s791, "N0")
                End If
                If survey.s792 IsNot Nothing Then
                    txt_s792.Text = String.Format(survey.s792, "N0")
                End If
                If survey.s793 IsNot Nothing Then
                    txt_s793.Text = String.Format(survey.s793, "N0")
                End If
                If survey.s794 IsNot Nothing Then
                    txt_s794.Text = String.Format(survey.s794, "N0")
                End If
                If survey.s795 IsNot Nothing Then
                    txt_s795.Text = String.Format(survey.s795, "N0")
                End If
                If survey.s796 IsNot Nothing Then
                    txt_s796.Text = String.Format(survey.s796, "N0")
                End If
                If survey.s797 IsNot Nothing Then
                    txt_s797.Text = String.Format(survey.s797, "N0")
                End If
                If survey.s798 IsNot Nothing Then
                    txt_s798.Text = String.Format(survey.s798, "N0")
                End If
                If survey.s799 IsNot Nothing Then
                    txt_s799.Text = String.Format(survey.s799, "N0")
                End If
                If survey.s800 IsNot Nothing Then
                    txt_s800.Text = String.Format(survey.s800, "N0")
                End If
                If survey.s801 IsNot Nothing Then
                    txt_s801.Text = String.Format(survey.s801, "N0")
                End If
                If survey.s802 IsNot Nothing Then
                    txt_s802.Text = String.Format(survey.s802, "N0")
                End If
                If survey.s803 IsNot Nothing Then
                    txt_s803.Text = String.Format(survey.s803, "N0")
                End If
                If survey.s804 IsNot Nothing Then
                    txt_s804.Text = String.Format(survey.s804, "N0")
                End If
                If survey.s805 IsNot Nothing Then
                    txt_s805.Text = String.Format(survey.s805, "N0")
                End If
                If survey.s806 IsNot Nothing Then
                    txt_s806.Text = String.Format(survey.s806, "N0")
                End If
                If survey.s807 IsNot Nothing Then
                    txt_s807.Text = String.Format(survey.s807, "N0")
                End If
                If survey.s808 IsNot Nothing Then
                    txt_s808.Text = String.Format(survey.s808, "N0")
                End If
                If survey.s809 IsNot Nothing Then
                    txt_s809.Text = String.Format(survey.s809, "N0")
                End If
                If survey.s810 IsNot Nothing Then
                    txt_s810.Text = String.Format(survey.s810, "N0")
                End If
                If survey.s811 IsNot Nothing Then
                    txt_s811.Text = String.Format(survey.s811, "N0")
                End If
                If survey.s812 IsNot Nothing Then
                    txt_s812.Text = String.Format(survey.s812, "N0")
                End If
                If survey.s813 IsNot Nothing Then
                    txt_s813.Text = String.Format(survey.s813, "N0")
                End If
                If survey.s814 IsNot Nothing Then
                    txt_s814.Text = String.Format(survey.s814, "N0")
                End If
                If survey.s815 IsNot Nothing Then
                    txt_s815.Text = String.Format(survey.s815, "N0")
                End If
                If survey.s816 IsNot Nothing Then
                    txt_s816.Text = String.Format(survey.s816, "N0")
                End If
                If survey.s817 IsNot Nothing Then
                    txt_s817.Text = String.Format(survey.s817, "N0")
                End If
                If survey.s818 IsNot Nothing Then
                    txt_s818.Text = String.Format(survey.s818, "N0")
                End If
                If survey.s819 IsNot Nothing Then
                    txt_s819.Text = String.Format(survey.s819, "N0")
                End If
                If survey.s820 IsNot Nothing Then
                    txt_s820.Text = String.Format(survey.s820, "N0")
                End If
                If survey.s821 IsNot Nothing Then
                    txt_s821.Text = String.Format(survey.s821, "N0")
                End If
                If survey.s822 IsNot Nothing Then
                    txt_s822.Text = String.Format(survey.s822, "N0")
                End If
                If survey.s823 IsNot Nothing Then
                    txt_s823.Text = String.Format(survey.s823, "N0")
                End If
                If survey.s824 IsNot Nothing Then
                    txt_s824.Text = String.Format(survey.s824, "N0")
                End If
                If survey.s825 IsNot Nothing Then
                    txt_s825.Text = String.Format(survey.s825, "N0")
                End If
                If survey.s826 IsNot Nothing Then
                    txt_s826.Text = String.Format(survey.s826, "N0")
                End If
                If survey.s827 IsNot Nothing Then
                    txt_s827.Text = String.Format(survey.s827, "N0")
                End If
                If survey.s828 IsNot Nothing Then
                    txt_s828.Text = String.Format(survey.s828, "N0")
                End If
                If survey.s829 IsNot Nothing Then
                    txt_s829.Text = String.Format(survey.s829, "N0")
                End If
                If survey.s830 IsNot Nothing Then
                    txt_s830.Text = String.Format(survey.s830, "N0")
                End If
                If survey.s831 IsNot Nothing Then
                    txt_s831.Text = String.Format(survey.s831, "N0")
                End If
                If survey.s832 IsNot Nothing Then
                    txt_s832.Text = String.Format(survey.s832, "N0")
                End If
                If survey.s833 IsNot Nothing Then
                    txt_s833.Text = String.Format(survey.s833, "N0")
                End If
                If survey.s834 IsNot Nothing Then
                    txt_s834.Text = String.Format(survey.s834, "N0")
                End If
                If survey.s835 IsNot Nothing Then
                    txt_s835.Text = String.Format(survey.s835, "N0")
                End If
                If survey.s836 IsNot Nothing Then
                    txt_s836.Text = String.Format(survey.s836, "N0")
                End If
                If survey.s837 IsNot Nothing Then
                    txt_s837.Text = String.Format(survey.s837, "N0")
                End If
                If survey.s838 IsNot Nothing Then
                    txt_s838.Text = String.Format(survey.s838, "N0")
                End If
                If survey.s839 IsNot Nothing Then
                    txt_s839.Text = String.Format(survey.s839, "N0")
                End If
                If survey.s840 IsNot Nothing Then
                    txt_s840.Text = String.Format(survey.s840, "N0")
                End If
                If survey.s841 IsNot Nothing Then
                    txt_s841.Text = String.Format(survey.s841, "N0")
                End If
                If survey.s842 IsNot Nothing Then
                    txt_s842.Text = String.Format(survey.s842, "N0")
                End If
                If survey.s843 IsNot Nothing Then
                    txt_s843.Text = String.Format(survey.s843, "N0")
                End If
                If survey.s844 IsNot Nothing Then
                    txt_s844.Text = String.Format(survey.s844, "N0")
                End If
                If survey.s845 IsNot Nothing Then
                    txt_s845.Text = String.Format(survey.s845, "N0")
                End If
                If survey.s846 IsNot Nothing Then
                    txt_s846.Text = String.Format(survey.s846, "N0")
                End If
                If survey.s847 IsNot Nothing Then
                    txt_s847.Text = String.Format(survey.s847, "N0")
                End If
                If survey.s848 IsNot Nothing Then
                    txt_s848.Text = String.Format(survey.s848, "N0")
                End If
                If survey.s849 IsNot Nothing Then
                    txt_s849.Text = String.Format(survey.s849, "N0")
                End If
                If survey.s850 IsNot Nothing Then
                    txt_s850.Text = String.Format(survey.s850, "N0")
                End If
                If survey.s851 IsNot Nothing Then
                    txt_s851.Text = String.Format(survey.s851, "N0")
                End If
                If survey.s852 IsNot Nothing Then
                    txt_s852.Text = String.Format(survey.s852, "N0")
                End If
                If survey.s853 IsNot Nothing Then
                    txt_s853.Text = String.Format(survey.s853, "N0")
                End If
                If survey.s854 IsNot Nothing Then
                    txt_s854.Text = String.Format(survey.s854, "N0")
                End If
                If survey.s855 IsNot Nothing Then
                    txt_s855.Text = String.Format(survey.s855, "N0")
                End If
                If survey.s856 IsNot Nothing Then
                    txt_s856.Text = String.Format(survey.s856, "N0")
                End If
                If survey.s857 IsNot Nothing Then
                    txt_s857.Text = String.Format(survey.s857, "N0")
                End If
                If survey.s858 IsNot Nothing Then
                    txt_s858.Text = String.Format(survey.s858, "N0")
                End If
                If survey.s859 IsNot Nothing Then
                    txt_s859.Text = String.Format(survey.s859, "N0")
                End If
                If survey.s860 IsNot Nothing Then
                    txt_s860.Text = String.Format(survey.s860, "N0")
                End If
                If survey.s861 IsNot Nothing Then
                    txt_s861.Text = String.Format(survey.s861, "N0")
                End If
                If survey.s862 IsNot Nothing Then
                    txt_s862.Text = String.Format(survey.s862, "N0")
                End If
                If survey.s863 IsNot Nothing Then
                    txt_s863.Text = String.Format(survey.s863, "N0")
                End If
                If survey.s864 IsNot Nothing Then
                    txt_s864.Text = String.Format(survey.s864, "N0")
                End If
                If survey.s865 IsNot Nothing Then
                    txt_s865.Text = String.Format(survey.s865, "N0")
                End If
                If survey.s866 IsNot Nothing Then
                    txt_s866.Text = String.Format(survey.s866, "N0")
                End If
                If survey.s965 IsNot Nothing Then
                    txt_s965.Text = String.Format(survey.s965, "N0")
                End If
                If survey.s966 IsNot Nothing Then
                    txt_s966.Text = String.Format(survey.s966, "N0")
                End If
                If survey.s967 IsNot Nothing Then
                    txt_s967.Text = String.Format(survey.s967, "N0")
                End If
                If survey.s867 IsNot Nothing Then
                    txt_s867.Text = String.Format(survey.s867, "N0")
                End If
                If survey.s868 IsNot Nothing Then
                    txt_s868.Text = String.Format(survey.s868, "N0")
                End If
                If survey.s869 IsNot Nothing Then
                    txt_s869.Text = String.Format(survey.s869, "N0")
                End If
                If survey.s870 IsNot Nothing Then
                    txt_s870.Text = String.Format(survey.s870, "N0")
                End If
                If survey.s871 IsNot Nothing Then
                    txt_s871.Text = String.Format(survey.s871, "N0")
                End If
                If survey.s872 IsNot Nothing Then
                    txt_s872.Text = String.Format(survey.s872, "N0")
                End If
                If survey.s873 IsNot Nothing Then
                    txt_s873.Text = String.Format(survey.s873, "N0")
                End If
                If survey.s874 IsNot Nothing Then
                    txt_s874.Text = String.Format(survey.s874, "N0")
                End If
                If survey.s875 IsNot Nothing Then
                    txt_s875.Text = String.Format(survey.s875, "N0")
                End If
                If survey.s876 IsNot Nothing Then
                    txt_s876.Text = String.Format(survey.s876, "N0")
                End If
                If survey.s877 IsNot Nothing Then
                    txt_s877.Text = String.Format(survey.s877, "N0")
                End If
                If survey.s878 IsNot Nothing Then
                    txt_s878.Text = String.Format(survey.s878, "N0")
                End If
                If survey.s879 IsNot Nothing Then
                    txt_s879.Text = String.Format(survey.s879, "N0")
                End If
                If survey.s880 IsNot Nothing Then
                    txt_s880.Text = String.Format(survey.s880, "N0")
                End If
                If survey.s881 IsNot Nothing Then
                    txt_s881.Text = String.Format(survey.s881, "N0")
                End If
                If survey.s882 IsNot Nothing Then
                    txt_s882.Text = String.Format(survey.s882, "N0")
                End If
                If survey.s883 IsNot Nothing Then
                    txt_s883.Text = String.Format(survey.s883, "N0")
                End If
                If survey.s884 IsNot Nothing Then
                    txt_s884.Text = String.Format(survey.s884, "N0")
                End If
                If survey.s885 IsNot Nothing Then
                    txt_s885.Text = String.Format(survey.s885, "N0")
                End If
                If survey.s886 IsNot Nothing Then
                    txt_s886.Text = String.Format(survey.s886, "N0")
                End If
                If survey.s887 IsNot Nothing Then
                    txt_s887.Text = String.Format(survey.s887, "N0")
                End If
                If survey.s888 IsNot Nothing Then
                    txt_s888.Text = String.Format(survey.s888, "N0")
                End If
                If survey.s889 IsNot Nothing Then
                    txt_s889.Text = String.Format(survey.s889, "N0")
                End If
                If survey.s890 IsNot Nothing Then
                    txt_s890.Text = String.Format(survey.s890, "N0")
                End If

                If survey.section4Complete IsNot Nothing Then
                    cb_section4Complete.Checked = survey.section4Complete
                End If






            End If
        End Using
    End Sub

    Sub SaveSurvey(ByVal IsSubmit As Boolean)
        Using db As New Models.IICICEntities
            Dim existingSurvey As Boolean = False
            Dim LocationID As Integer = txt_LocationID.Text
            Dim userid As Integer = Convert.ToInt16(Session("UserId"))
            Dim survey As New CompSurvey
            survey = (From s In db.CompSurveys Where s.LocationID = LocationID And s.SurveyYear = Now.Year).FirstOrDefault
            If survey Is Nothing Then
                survey = New CompSurvey
                db.CompSurveys.Add(survey)
                db.SaveChanges()
            End If
            survey.LocationID = LocationID
            survey.LastModUserId = userid
            survey.ModifiedDate = Now
            survey.SurveyYear = Now.Year
            If IsSubmit Then
                survey.SubmitDate = Now
            End If

            '---PASTE SAVE STATEMENTS---
            If Not String.IsNullOrEmpty(txt_s182.Text) Then
                survey.s182 = txt_s182.Text.Replace(",", String.Empty)
            Else
                survey.s182 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s183.Text) Then
                survey.s183 = txt_s183.Text.Replace(",", String.Empty)
            Else
                survey.s183 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s184.Text) Then
                survey.s184 = txt_s184.Text.Replace(",", String.Empty)
            Else
                survey.s184 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s185.Text) Then
                survey.s185 = txt_s185.Text.Replace(",", String.Empty)
            Else
                survey.s185 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s186.Text) Then
                survey.s186 = txt_s186.Text.Replace(",", String.Empty)
            Else
                survey.s186 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s187.Text) Then
                survey.s187 = txt_s187.Text.Replace(",", String.Empty)
            Else
                survey.s187 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s188.Text) Then
                survey.s188 = txt_s188.Text.Replace(",", String.Empty)
            Else
                survey.s188 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s189.Text) Then
                survey.s189 = txt_s189.Text.Replace(",", String.Empty)
            Else
                survey.s189 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s190.Text) Then
                survey.s190 = txt_s190.Text.Replace(",", String.Empty)
            Else
                survey.s190 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s191.Text) Then
                survey.s191 = txt_s191.Text.Replace(",", String.Empty)
            Else
                survey.s191 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s192.Text) Then
                survey.s192 = txt_s192.Text.Replace(",", String.Empty)
            Else
                survey.s192 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s193.Text) Then
                survey.s193 = txt_s193.Text.Replace(",", String.Empty)
            Else
                survey.s193 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s194.Text) Then
                survey.s194 = txt_s194.Text.Replace(",", String.Empty)
            Else
                survey.s194 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s195.Text) Then
                survey.s195 = txt_s195.Text.Replace(",", String.Empty)
            Else
                survey.s195 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s196.Text) Then
                survey.s196 = txt_s196.Text.Replace(",", String.Empty)
            Else
                survey.s196 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s197.Text) Then
                survey.s197 = txt_s197.Text.Replace(",", String.Empty)
            Else
                survey.s197 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s198.Text) Then
                survey.s198 = txt_s198.Text.Replace(",", String.Empty)
            Else
                survey.s198 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s199.Text) Then
                survey.s199 = txt_s199.Text.Replace(",", String.Empty)
            Else
                survey.s199 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s200.Text) Then
                survey.s200 = txt_s200.Text.Replace(",", String.Empty)
            Else
                survey.s200 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s201.Text) Then
                survey.s201 = txt_s201.Text.Replace(",", String.Empty)
            Else
                survey.s201 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s202.Text) Then
                survey.s202 = txt_s202.Text.Replace(",", String.Empty)
            Else
                survey.s202 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s203.Text) Then
                survey.s203 = txt_s203.Text.Replace(",", String.Empty)
            Else
                survey.s203 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s204.Text) Then
                survey.s204 = txt_s204.Text.Replace(",", String.Empty)
            Else
                survey.s204 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s205.Text) Then
                survey.s205 = txt_s205.Text.Replace(",", String.Empty)
            Else
                survey.s205 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s206.Text) Then
                survey.s206 = txt_s206.Text.Replace(",", String.Empty)
            Else
                survey.s206 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s207.Text) Then
                survey.s207 = txt_s207.Text.Replace(",", String.Empty)
            Else
                survey.s207 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s208.Text) Then
                survey.s208 = txt_s208.Text.Replace(",", String.Empty)
            Else
                survey.s208 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s209.Text) Then
                survey.s209 = txt_s209.Text.Replace(",", String.Empty)
            Else
                survey.s209 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s210.Text) Then
                survey.s210 = txt_s210.Text.Replace(",", String.Empty)
            Else
                survey.s210 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s211.Text) Then
                survey.s211 = txt_s211.Text.Replace(",", String.Empty)
            Else
                survey.s211 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s212.Text) Then
                survey.s212 = txt_s212.Text.Replace(",", String.Empty)
            Else
                survey.s212 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s213.Text) Then
                survey.s213 = txt_s213.Text.Replace(",", String.Empty)
            Else
                survey.s213 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s214.Text) Then
                survey.s214 = txt_s214.Text.Replace(",", String.Empty)
            Else
                survey.s214 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s547.Text) Then
                survey.s547 = txt_s547.Text.Replace(",", String.Empty)
            Else
                survey.s547 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s548.Text) Then
                survey.s548 = txt_s548.Text.Replace(",", String.Empty)
            Else
                survey.s548 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s549.Text) Then
                survey.s549 = txt_s549.Text.Replace(",", String.Empty)
            Else
                survey.s549 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s215.Text) Then
                survey.s215 = txt_s215.Text.Replace(",", String.Empty)
            Else
                survey.s215 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s216.Text) Then
                survey.s216 = txt_s216.Text.Replace(",", String.Empty)
            Else
                survey.s216 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s217.Text) Then
                survey.s217 = txt_s217.Text.Replace(",", String.Empty)
            Else
                survey.s217 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s550.Text) Then
                survey.s550 = txt_s550.Text.Replace(",", String.Empty)
            Else
                survey.s550 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s551.Text) Then
                survey.s551 = txt_s551.Text.Replace(",", String.Empty)
            Else
                survey.s551 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s552.Text) Then
                survey.s552 = txt_s552.Text.Replace(",", String.Empty)
            Else
                survey.s552 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s218.Text) Then
                survey.s218 = txt_s218.Text.Replace(",", String.Empty)
            Else
                survey.s218 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s219.Text) Then
                survey.s219 = txt_s219.Text.Replace(",", String.Empty)
            Else
                survey.s219 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s220.Text) Then
                survey.s220 = txt_s220.Text.Replace(",", String.Empty)
            Else
                survey.s220 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s221.Text) Then
                survey.s221 = txt_s221.Text.Replace(",", String.Empty)
            Else
                survey.s221 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s222.Text) Then
                survey.s222 = txt_s222.Text.Replace(",", String.Empty)
            Else
                survey.s222 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s223.Text) Then
                survey.s223 = txt_s223.Text.Replace(",", String.Empty)
            Else
                survey.s223 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s224.Text) Then
                survey.s224 = txt_s224.Text.Replace(",", String.Empty)
            Else
                survey.s224 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s225.Text) Then
                survey.s225 = txt_s225.Text.Replace(",", String.Empty)
            Else
                survey.s225 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s226.Text) Then
                survey.s226 = txt_s226.Text.Replace(",", String.Empty)
            Else
                survey.s226 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s227.Text) Then
                survey.s227 = txt_s227.Text.Replace(",", String.Empty)
            Else
                survey.s227 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s228.Text) Then
                survey.s228 = txt_s228.Text.Replace(",", String.Empty)
            Else
                survey.s228 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s229.Text) Then
                survey.s229 = txt_s229.Text.Replace(",", String.Empty)
            Else
                survey.s229 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s230.Text) Then
                survey.s230 = txt_s230.Text.Replace(",", String.Empty)
            Else
                survey.s230 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s231.Text) Then
                survey.s231 = txt_s231.Text.Replace(",", String.Empty)
            Else
                survey.s231 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s232.Text) Then
                survey.s232 = txt_s232.Text.Replace(",", String.Empty)
            Else
                survey.s232 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s233.Text) Then
                survey.s233 = txt_s233.Text.Replace(",", String.Empty)
            Else
                survey.s233 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s234.Text) Then
                survey.s234 = txt_s234.Text.Replace(",", String.Empty)
            Else
                survey.s234 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s235.Text) Then
                survey.s235 = txt_s235.Text.Replace(",", String.Empty)
            Else
                survey.s235 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s236.Text) Then
                survey.s236 = txt_s236.Text.Replace(",", String.Empty)
            Else
                survey.s236 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s237.Text) Then
                survey.s237 = txt_s237.Text.Replace(",", String.Empty)
            Else
                survey.s237 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s238.Text) Then
                survey.s238 = txt_s238.Text.Replace(",", String.Empty)
            Else
                survey.s238 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s239.Text) Then
                survey.s239 = txt_s239.Text.Replace(",", String.Empty)
            Else
                survey.s239 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s240.Text) Then
                survey.s240 = txt_s240.Text.Replace(",", String.Empty)
            Else
                survey.s240 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s241.Text) Then
                survey.s241 = txt_s241.Text.Replace(",", String.Empty)
            Else
                survey.s241 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s242.Text) Then
                survey.s242 = txt_s242.Text.Replace(",", String.Empty)
            Else
                survey.s242 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s243.Text) Then
                survey.s243 = txt_s243.Text.Replace(",", String.Empty)
            Else
                survey.s243 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s244.Text) Then
                survey.s244 = txt_s244.Text.Replace(",", String.Empty)
            Else
                survey.s244 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s245.Text) Then
                survey.s245 = txt_s245.Text.Replace(",", String.Empty)
            Else
                survey.s245 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s246.Text) Then
                survey.s246 = txt_s246.Text.Replace(",", String.Empty)
            Else
                survey.s246 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s247.Text) Then
                survey.s247 = txt_s247.Text.Replace(",", String.Empty)
            Else
                survey.s247 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s248.Text) Then
                survey.s248 = txt_s248.Text.Replace(",", String.Empty)
            Else
                survey.s248 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s249.Text) Then
                survey.s249 = txt_s249.Text.Replace(",", String.Empty)
            Else
                survey.s249 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s250.Text) Then
                survey.s250 = txt_s250.Text.Replace(",", String.Empty)
            Else
                survey.s250 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s251.Text) Then
                survey.s251 = txt_s251.Text.Replace(",", String.Empty)
            Else
                survey.s251 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s252.Text) Then
                survey.s252 = txt_s252.Text.Replace(",", String.Empty)
            Else
                survey.s252 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s253.Text) Then
                survey.s253 = txt_s253.Text.Replace(",", String.Empty)
            Else
                survey.s253 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s254.Text) Then
                survey.s254 = txt_s254.Text.Replace(",", String.Empty)
            Else
                survey.s254 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s255.Text) Then
                survey.s255 = txt_s255.Text.Replace(",", String.Empty)
            Else
                survey.s255 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s256.Text) Then
                survey.s256 = txt_s256.Text.Replace(",", String.Empty)
            Else
                survey.s256 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s257.Text) Then
                survey.s257 = txt_s257.Text.Replace(",", String.Empty)
            Else
                survey.s257 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s258.Text) Then
                survey.s258 = txt_s258.Text.Replace(",", String.Empty)
            Else
                survey.s258 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s259.Text) Then
                survey.s259 = txt_s259.Text.Replace(",", String.Empty)
            Else
                survey.s259 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s260.Text) Then
                survey.s260 = txt_s260.Text.Replace(",", String.Empty)
            Else
                survey.s260 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s261.Text) Then
                survey.s261 = txt_s261.Text.Replace(",", String.Empty)
            Else
                survey.s261 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s262.Text) Then
                survey.s262 = txt_s262.Text.Replace(",", String.Empty)
            Else
                survey.s262 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s263.Text) Then
                survey.s263 = txt_s263.Text.Replace(",", String.Empty)
            Else
                survey.s263 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s264.Text) Then
                survey.s264 = txt_s264.Text.Replace(",", String.Empty)
            Else
                survey.s264 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s265.Text) Then
                survey.s265 = txt_s265.Text.Replace(",", String.Empty)
            Else
                survey.s265 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s266.Text) Then
                survey.s266 = txt_s266.Text.Replace(",", String.Empty)
            Else
                survey.s266 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s267.Text) Then
                survey.s267 = txt_s267.Text.Replace(",", String.Empty)
            Else
                survey.s267 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s268.Text) Then
                survey.s268 = txt_s268.Text.Replace(",", String.Empty)
            Else
                survey.s268 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s269.Text) Then
                survey.s269 = txt_s269.Text.Replace(",", String.Empty)
            Else
                survey.s269 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s270.Text) Then
                survey.s270 = txt_s270.Text.Replace(",", String.Empty)
            Else
                survey.s270 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s271.Text) Then
                survey.s271 = txt_s271.Text.Replace(",", String.Empty)
            Else
                survey.s271 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s272.Text) Then
                survey.s272 = txt_s272.Text.Replace(",", String.Empty)
            Else
                survey.s272 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s273.Text) Then
                survey.s273 = txt_s273.Text.Replace(",", String.Empty)
            Else
                survey.s273 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s274.Text) Then
                survey.s274 = txt_s274.Text.Replace(",", String.Empty)
            Else
                survey.s274 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s275.Text) Then
                survey.s275 = txt_s275.Text.Replace(",", String.Empty)
            Else
                survey.s275 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s276.Text) Then
                survey.s276 = txt_s276.Text.Replace(",", String.Empty)
            Else
                survey.s276 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s277.Text) Then
                survey.s277 = txt_s277.Text.Replace(",", String.Empty)
            Else
                survey.s277 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s553.Text) Then
                survey.s553 = txt_s553.Text.Replace(",", String.Empty)
            Else
                survey.s553 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s554.Text) Then
                survey.s554 = txt_s554.Text.Replace(",", String.Empty)
            Else
                survey.s554 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s555.Text) Then
                survey.s555 = txt_s555.Text.Replace(",", String.Empty)
            Else
                survey.s555 = Nothing
            End If

            If Not String.IsNullOrEmpty(txt_s900.Text) Then
                survey.s900 = txt_s900.Text.Replace(",", String.Empty)
            Else
                survey.s900 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s901.Text) Then
                survey.s901 = txt_s901.Text.Replace(",", String.Empty)
            Else
                survey.s901 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s902.Text) Then
                survey.s902 = txt_s902.Text.Replace(",", String.Empty)
            Else
                survey.s902 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s903.Text) Then
                survey.s903 = txt_s903.Text.Replace(",", String.Empty)
            Else
                survey.s903 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s904.Text) Then
                survey.s904 = txt_s904.Text.Replace(",", String.Empty)
            Else
                survey.s904 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s905.Text) Then
                survey.s905 = txt_s905.Text.Replace(",", String.Empty)
            Else
                survey.s905 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s906.Text) Then
                survey.s906 = txt_s906.Text.Replace(",", String.Empty)
            Else
                survey.s906 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s907.Text) Then
                survey.s907 = txt_s907.Text.Replace(",", String.Empty)
            Else
                survey.s907 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s908.Text) Then
                survey.s908 = txt_s908.Text.Replace(",", String.Empty)
            Else
                survey.s908 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s909.Text) Then
                survey.s909 = txt_s909.Text.Replace(",", String.Empty)
            Else
                survey.s909 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s910.Text) Then
                survey.s910 = txt_s910.Text.Replace(",", String.Empty)
            Else
                survey.s910 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s911.Text) Then
                survey.s911 = txt_s911.Text.Replace(",", String.Empty)
            Else
                survey.s911 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s912.Text) Then
                survey.s912 = txt_s912.Text.Replace(",", String.Empty)
            Else
                survey.s912 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s913.Text) Then
                survey.s913 = txt_s913.Text.Replace(",", String.Empty)
            Else
                survey.s913 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s914.Text) Then
                survey.s914 = txt_s914.Text.Replace(",", String.Empty)
            Else
                survey.s914 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s720.Text) Then
                survey.s720 = txt_s720.Text.Replace(",", String.Empty)
            Else
                survey.s720 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s721.Text) Then
                survey.s721 = txt_s721.Text.Replace(",", String.Empty)
            Else
                survey.s721 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s722.Text) Then
                survey.s722 = txt_s722.Text.Replace(",", String.Empty)
            Else
                survey.s722 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s723.Text) Then
                survey.s723 = txt_s723.Text.Replace(",", String.Empty)
            Else
                survey.s723 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s724.Text) Then
                survey.s724 = txt_s724.Text.Replace(",", String.Empty)
            Else
                survey.s724 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s725.Text) Then
                survey.s725 = txt_s725.Text.Replace(",", String.Empty)
            Else
                survey.s725 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s726.Text) Then
                survey.s726 = txt_s726.Text.Replace(",", String.Empty)
            Else
                survey.s726 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s727.Text) Then
                survey.s727 = txt_s727.Text.Replace(",", String.Empty)
            Else
                survey.s727 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s728.Text) Then
                survey.s728 = txt_s728.Text.Replace(",", String.Empty)
            Else
                survey.s728 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s729.Text) Then
                survey.s729 = txt_s729.Text.Replace(",", String.Empty)
            Else
                survey.s729 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s730.Text) Then
                survey.s730 = txt_s730.Text.Replace(",", String.Empty)
            Else
                survey.s730 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s731.Text) Then
                survey.s731 = txt_s731.Text.Replace(",", String.Empty)
            Else
                survey.s731 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s732.Text) Then
                survey.s732 = txt_s732.Text.Replace(",", String.Empty)
            Else
                survey.s732 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s733.Text) Then
                survey.s733 = txt_s733.Text.Replace(",", String.Empty)
            Else
                survey.s733 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s734.Text) Then
                survey.s734 = txt_s734.Text.Replace(",", String.Empty)
            Else
                survey.s734 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s735.Text) Then
                survey.s735 = txt_s735.Text.Replace(",", String.Empty)
            Else
                survey.s735 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s736.Text) Then
                survey.s736 = txt_s736.Text.Replace(",", String.Empty)
            Else
                survey.s736 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s737.Text) Then
                survey.s737 = txt_s737.Text.Replace(",", String.Empty)
            Else
                survey.s737 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s738.Text) Then
                survey.s738 = txt_s738.Text.Replace(",", String.Empty)
            Else
                survey.s738 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s739.Text) Then
                survey.s739 = txt_s739.Text.Replace(",", String.Empty)
            Else
                survey.s739 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s740.Text) Then
                survey.s740 = txt_s740.Text.Replace(",", String.Empty)
            Else
                survey.s740 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s741.Text) Then
                survey.s741 = txt_s741.Text.Replace(",", String.Empty)
            Else
                survey.s741 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s742.Text) Then
                survey.s742 = txt_s742.Text.Replace(",", String.Empty)
            Else
                survey.s742 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s743.Text) Then
                survey.s743 = txt_s743.Text.Replace(",", String.Empty)
            Else
                survey.s743 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s744.Text) Then
                survey.s744 = txt_s744.Text.Replace(",", String.Empty)
            Else
                survey.s744 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s745.Text) Then
                survey.s745 = txt_s745.Text.Replace(",", String.Empty)
            Else
                survey.s745 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s746.Text) Then
                survey.s746 = txt_s746.Text.Replace(",", String.Empty)
            Else
                survey.s746 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s747.Text) Then
                survey.s747 = txt_s747.Text.Replace(",", String.Empty)
            Else
                survey.s747 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s748.Text) Then
                survey.s748 = txt_s748.Text.Replace(",", String.Empty)
            Else
                survey.s748 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s749.Text) Then
                survey.s749 = txt_s749.Text.Replace(",", String.Empty)
            Else
                survey.s749 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s750.Text) Then
                survey.s750 = txt_s750.Text.Replace(",", String.Empty)
            Else
                survey.s750 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s751.Text) Then
                survey.s751 = txt_s751.Text.Replace(",", String.Empty)
            Else
                survey.s751 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s752.Text) Then
                survey.s752 = txt_s752.Text.Replace(",", String.Empty)
            Else
                survey.s752 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s753.Text) Then
                survey.s753 = txt_s753.Text.Replace(",", String.Empty)
            Else
                survey.s753 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s754.Text) Then
                survey.s754 = txt_s754.Text.Replace(",", String.Empty)
            Else
                survey.s754 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s755.Text) Then
                survey.s755 = txt_s755.Text.Replace(",", String.Empty)
            Else
                survey.s755 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s756.Text) Then
                survey.s756 = txt_s756.Text.Replace(",", String.Empty)
            Else
                survey.s756 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s757.Text) Then
                survey.s757 = txt_s757.Text.Replace(",", String.Empty)
            Else
                survey.s757 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s758.Text) Then
                survey.s758 = txt_s758.Text.Replace(",", String.Empty)
            Else
                survey.s758 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s759.Text) Then
                survey.s759 = txt_s759.Text.Replace(",", String.Empty)
            Else
                survey.s759 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s760.Text) Then
                survey.s760 = txt_s760.Text.Replace(",", String.Empty)
            Else
                survey.s760 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s761.Text) Then
                survey.s761 = txt_s761.Text.Replace(",", String.Empty)
            Else
                survey.s761 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s762.Text) Then
                survey.s762 = txt_s762.Text.Replace(",", String.Empty)
            Else
                survey.s762 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s763.Text) Then
                survey.s763 = txt_s763.Text.Replace(",", String.Empty)
            Else
                survey.s763 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s764.Text) Then
                survey.s764 = txt_s764.Text.Replace(",", String.Empty)
            Else
                survey.s764 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s765.Text) Then
                survey.s765 = txt_s765.Text.Replace(",", String.Empty)
            Else
                survey.s765 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s766.Text) Then
                survey.s766 = txt_s766.Text.Replace(",", String.Empty)
            Else
                survey.s766 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s767.Text) Then
                survey.s767 = txt_s767.Text.Replace(",", String.Empty)
            Else
                survey.s767 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s768.Text) Then
                survey.s768 = txt_s768.Text.Replace(",", String.Empty)
            Else
                survey.s768 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s769.Text) Then
                survey.s769 = txt_s769.Text.Replace(",", String.Empty)
            Else
                survey.s769 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s770.Text) Then
                survey.s770 = txt_s770.Text.Replace(",", String.Empty)
            Else
                survey.s770 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s771.Text) Then
                survey.s771 = txt_s771.Text.Replace(",", String.Empty)
            Else
                survey.s771 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s772.Text) Then
                survey.s772 = txt_s772.Text.Replace(",", String.Empty)
            Else
                survey.s772 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s773.Text) Then
                survey.s773 = txt_s773.Text.Replace(",", String.Empty)
            Else
                survey.s773 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s774.Text) Then
                survey.s774 = txt_s774.Text.Replace(",", String.Empty)
            Else
                survey.s774 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s775.Text) Then
                survey.s775 = txt_s775.Text.Replace(",", String.Empty)
            Else
                survey.s775 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s776.Text) Then
                survey.s776 = txt_s776.Text.Replace(",", String.Empty)
            Else
                survey.s776 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s777.Text) Then
                survey.s777 = txt_s777.Text.Replace(",", String.Empty)
            Else
                survey.s777 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s778.Text) Then
                survey.s778 = txt_s778.Text.Replace(",", String.Empty)
            Else
                survey.s778 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s779.Text) Then
                survey.s779 = txt_s779.Text.Replace(",", String.Empty)
            Else
                survey.s779 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s780.Text) Then
                survey.s780 = txt_s780.Text.Replace(",", String.Empty)
            Else
                survey.s780 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s781.Text) Then
                survey.s781 = txt_s781.Text.Replace(",", String.Empty)
            Else
                survey.s781 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s782.Text) Then
                survey.s782 = txt_s782.Text.Replace(",", String.Empty)
            Else
                survey.s782 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s783.Text) Then
                survey.s783 = txt_s783.Text.Replace(",", String.Empty)
            Else
                survey.s783 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s784.Text) Then
                survey.s784 = txt_s784.Text.Replace(",", String.Empty)
            Else
                survey.s784 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s785.Text) Then
                survey.s785 = txt_s785.Text.Replace(",", String.Empty)
            Else
                survey.s785 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s786.Text) Then
                survey.s786 = txt_s786.Text.Replace(",", String.Empty)
            Else
                survey.s786 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s787.Text) Then
                survey.s787 = txt_s787.Text.Replace(",", String.Empty)
            Else
                survey.s787 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s788.Text) Then
                survey.s788 = txt_s788.Text.Replace(",", String.Empty)
            Else
                survey.s788 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s789.Text) Then
                survey.s789 = txt_s789.Text.Replace(",", String.Empty)
            Else
                survey.s789 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s790.Text) Then
                survey.s790 = txt_s790.Text.Replace(",", String.Empty)
            Else
                survey.s790 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s791.Text) Then
                survey.s791 = txt_s791.Text.Replace(",", String.Empty)
            Else
                survey.s791 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s792.Text) Then
                survey.s792 = txt_s792.Text.Replace(",", String.Empty)
            Else
                survey.s792 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s793.Text) Then
                survey.s793 = txt_s793.Text.Replace(",", String.Empty)
            Else
                survey.s793 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s794.Text) Then
                survey.s794 = txt_s794.Text.Replace(",", String.Empty)
            Else
                survey.s794 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s795.Text) Then
                survey.s795 = txt_s795.Text.Replace(",", String.Empty)
            Else
                survey.s795 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s796.Text) Then
                survey.s796 = txt_s796.Text.Replace(",", String.Empty)
            Else
                survey.s796 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s797.Text) Then
                survey.s797 = txt_s797.Text.Replace(",", String.Empty)
            Else
                survey.s797 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s798.Text) Then
                survey.s798 = txt_s798.Text.Replace(",", String.Empty)
            Else
                survey.s798 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s799.Text) Then
                survey.s799 = txt_s799.Text.Replace(",", String.Empty)
            Else
                survey.s799 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s800.Text) Then
                survey.s800 = txt_s800.Text.Replace(",", String.Empty)
            Else
                survey.s800 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s801.Text) Then
                survey.s801 = txt_s801.Text.Replace(",", String.Empty)
            Else
                survey.s801 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s802.Text) Then
                survey.s802 = txt_s802.Text.Replace(",", String.Empty)
            Else
                survey.s802 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s803.Text) Then
                survey.s803 = txt_s803.Text.Replace(",", String.Empty)
            Else
                survey.s803 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s804.Text) Then
                survey.s804 = txt_s804.Text.Replace(",", String.Empty)
            Else
                survey.s804 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s805.Text) Then
                survey.s805 = txt_s805.Text.Replace(",", String.Empty)
            Else
                survey.s805 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s806.Text) Then
                survey.s806 = txt_s806.Text.Replace(",", String.Empty)
            Else
                survey.s806 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s807.Text) Then
                survey.s807 = txt_s807.Text.Replace(",", String.Empty)
            Else
                survey.s807 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s808.Text) Then
                survey.s808 = txt_s808.Text.Replace(",", String.Empty)
            Else
                survey.s808 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s809.Text) Then
                survey.s809 = txt_s809.Text.Replace(",", String.Empty)
            Else
                survey.s809 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s810.Text) Then
                survey.s810 = txt_s810.Text.Replace(",", String.Empty)
            Else
                survey.s810 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s811.Text) Then
                survey.s811 = txt_s811.Text.Replace(",", String.Empty)
            Else
                survey.s811 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s812.Text) Then
                survey.s812 = txt_s812.Text.Replace(",", String.Empty)
            Else
                survey.s812 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s813.Text) Then
                survey.s813 = txt_s813.Text.Replace(",", String.Empty)
            Else
                survey.s813 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s814.Text) Then
                survey.s814 = txt_s814.Text.Replace(",", String.Empty)
            Else
                survey.s814 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s815.Text) Then
                survey.s815 = txt_s815.Text.Replace(",", String.Empty)
            Else
                survey.s815 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s816.Text) Then
                survey.s816 = txt_s816.Text.Replace(",", String.Empty)
            Else
                survey.s816 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s817.Text) Then
                survey.s817 = txt_s817.Text.Replace(",", String.Empty)
            Else
                survey.s817 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s818.Text) Then
                survey.s818 = txt_s818.Text.Replace(",", String.Empty)
            Else
                survey.s818 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s819.Text) Then
                survey.s819 = txt_s819.Text.Replace(",", String.Empty)
            Else
                survey.s819 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s820.Text) Then
                survey.s820 = txt_s820.Text.Replace(",", String.Empty)
            Else
                survey.s820 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s821.Text) Then
                survey.s821 = txt_s821.Text.Replace(",", String.Empty)
            Else
                survey.s821 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s822.Text) Then
                survey.s822 = txt_s822.Text.Replace(",", String.Empty)
            Else
                survey.s822 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s823.Text) Then
                survey.s823 = txt_s823.Text.Replace(",", String.Empty)
            Else
                survey.s823 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s824.Text) Then
                survey.s824 = txt_s824.Text.Replace(",", String.Empty)
            Else
                survey.s824 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s825.Text) Then
                survey.s825 = txt_s825.Text.Replace(",", String.Empty)
            Else
                survey.s825 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s826.Text) Then
                survey.s826 = txt_s826.Text.Replace(",", String.Empty)
            Else
                survey.s826 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s827.Text) Then
                survey.s827 = txt_s827.Text.Replace(",", String.Empty)
            Else
                survey.s827 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s828.Text) Then
                survey.s828 = txt_s828.Text.Replace(",", String.Empty)
            Else
                survey.s828 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s829.Text) Then
                survey.s829 = txt_s829.Text.Replace(",", String.Empty)
            Else
                survey.s829 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s830.Text) Then
                survey.s830 = txt_s830.Text.Replace(",", String.Empty)
            Else
                survey.s830 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s831.Text) Then
                survey.s831 = txt_s831.Text.Replace(",", String.Empty)
            Else
                survey.s831 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s832.Text) Then
                survey.s832 = txt_s832.Text.Replace(",", String.Empty)
            Else
                survey.s832 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s833.Text) Then
                survey.s833 = txt_s833.Text.Replace(",", String.Empty)
            Else
                survey.s833 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s834.Text) Then
                survey.s834 = txt_s834.Text.Replace(",", String.Empty)
            Else
                survey.s834 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s835.Text) Then
                survey.s835 = txt_s835.Text.Replace(",", String.Empty)
            Else
                survey.s835 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s836.Text) Then
                survey.s836 = txt_s836.Text.Replace(",", String.Empty)
            Else
                survey.s836 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s837.Text) Then
                survey.s837 = txt_s837.Text.Replace(",", String.Empty)
            Else
                survey.s837 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s838.Text) Then
                survey.s838 = txt_s838.Text.Replace(",", String.Empty)
            Else
                survey.s838 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s839.Text) Then
                survey.s839 = txt_s839.Text.Replace(",", String.Empty)
            Else
                survey.s839 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s840.Text) Then
                survey.s840 = txt_s840.Text.Replace(",", String.Empty)
            Else
                survey.s840 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s841.Text) Then
                survey.s841 = txt_s841.Text.Replace(",", String.Empty)
            Else
                survey.s841 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s842.Text) Then
                survey.s842 = txt_s842.Text.Replace(",", String.Empty)
            Else
                survey.s842 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s843.Text) Then
                survey.s843 = txt_s843.Text.Replace(",", String.Empty)
            Else
                survey.s843 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s844.Text) Then
                survey.s844 = txt_s844.Text.Replace(",", String.Empty)
            Else
                survey.s844 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s845.Text) Then
                survey.s845 = txt_s845.Text.Replace(",", String.Empty)
            Else
                survey.s845 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s846.Text) Then
                survey.s846 = txt_s846.Text.Replace(",", String.Empty)
            Else
                survey.s846 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s847.Text) Then
                survey.s847 = txt_s847.Text.Replace(",", String.Empty)
            Else
                survey.s847 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s848.Text) Then
                survey.s848 = txt_s848.Text.Replace(",", String.Empty)
            Else
                survey.s848 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s849.Text) Then
                survey.s849 = txt_s849.Text.Replace(",", String.Empty)
            Else
                survey.s849 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s850.Text) Then
                survey.s850 = txt_s850.Text.Replace(",", String.Empty)
            Else
                survey.s850 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s851.Text) Then
                survey.s851 = txt_s851.Text.Replace(",", String.Empty)
            Else
                survey.s851 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s852.Text) Then
                survey.s852 = txt_s852.Text.Replace(",", String.Empty)
            Else
                survey.s852 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s853.Text) Then
                survey.s853 = txt_s853.Text.Replace(",", String.Empty)
            Else
                survey.s853 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s854.Text) Then
                survey.s854 = txt_s854.Text.Replace(",", String.Empty)
            Else
                survey.s854 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s855.Text) Then
                survey.s855 = txt_s855.Text.Replace(",", String.Empty)
            Else
                survey.s855 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s856.Text) Then
                survey.s856 = txt_s856.Text.Replace(",", String.Empty)
            Else
                survey.s856 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s857.Text) Then
                survey.s857 = txt_s857.Text.Replace(",", String.Empty)
            Else
                survey.s857 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s858.Text) Then
                survey.s858 = txt_s858.Text.Replace(",", String.Empty)
            Else
                survey.s858 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s859.Text) Then
                survey.s859 = txt_s859.Text.Replace(",", String.Empty)
            Else
                survey.s859 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s860.Text) Then
                survey.s860 = txt_s860.Text.Replace(",", String.Empty)
            Else
                survey.s860 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s861.Text) Then
                survey.s861 = txt_s861.Text.Replace(",", String.Empty)
            Else
                survey.s861 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s862.Text) Then
                survey.s862 = txt_s862.Text.Replace(",", String.Empty)
            Else
                survey.s862 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s863.Text) Then
                survey.s863 = txt_s863.Text.Replace(",", String.Empty)
            Else
                survey.s863 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s864.Text) Then
                survey.s864 = txt_s864.Text.Replace(",", String.Empty)
            Else
                survey.s864 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s865.Text) Then
                survey.s865 = txt_s865.Text.Replace(",", String.Empty)
            Else
                survey.s865 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s866.Text) Then
                survey.s866 = txt_s866.Text.Replace(",", String.Empty)
            Else
                survey.s866 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s965.Text) Then
                survey.s965 = txt_s965.Text.Replace(",", String.Empty)
            Else
                survey.s965 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s966.Text) Then
                survey.s966 = txt_s966.Text.Replace(",", String.Empty)
            Else
                survey.s966 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s967.Text) Then
                survey.s967 = txt_s967.Text.Replace(",", String.Empty)
            Else
                survey.s967 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s867.Text) Then
                survey.s867 = txt_s867.Text.Replace(",", String.Empty)
            Else
                survey.s867 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s868.Text) Then
                survey.s868 = txt_s868.Text.Replace(",", String.Empty)
            Else
                survey.s868 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s869.Text) Then
                survey.s869 = txt_s869.Text.Replace(",", String.Empty)
            Else
                survey.s869 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s870.Text) Then
                survey.s870 = txt_s870.Text.Replace(",", String.Empty)
            Else
                survey.s870 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s871.Text) Then
                survey.s871 = txt_s871.Text.Replace(",", String.Empty)
            Else
                survey.s871 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s872.Text) Then
                survey.s872 = txt_s872.Text.Replace(",", String.Empty)
            Else
                survey.s872 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s873.Text) Then
                survey.s873 = txt_s873.Text.Replace(",", String.Empty)
            Else
                survey.s873 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s874.Text) Then
                survey.s874 = txt_s874.Text.Replace(",", String.Empty)
            Else
                survey.s874 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s875.Text) Then
                survey.s875 = txt_s875.Text.Replace(",", String.Empty)
            Else
                survey.s875 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s876.Text) Then
                survey.s876 = txt_s876.Text.Replace(",", String.Empty)
            Else
                survey.s876 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s877.Text) Then
                survey.s877 = txt_s877.Text.Replace(",", String.Empty)
            Else
                survey.s877 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s878.Text) Then
                survey.s878 = txt_s878.Text.Replace(",", String.Empty)
            Else
                survey.s878 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s879.Text) Then
                survey.s879 = txt_s879.Text.Replace(",", String.Empty)
            Else
                survey.s879 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s880.Text) Then
                survey.s880 = txt_s880.Text.Replace(",", String.Empty)
            Else
                survey.s880 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s881.Text) Then
                survey.s881 = txt_s881.Text.Replace(",", String.Empty)
            Else
                survey.s881 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s882.Text) Then
                survey.s882 = txt_s882.Text.Replace(",", String.Empty)
            Else
                survey.s882 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s883.Text) Then
                survey.s883 = txt_s883.Text.Replace(",", String.Empty)
            Else
                survey.s883 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s884.Text) Then
                survey.s884 = txt_s884.Text.Replace(",", String.Empty)
            Else
                survey.s884 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s885.Text) Then
                survey.s885 = txt_s885.Text.Replace(",", String.Empty)
            Else
                survey.s885 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s886.Text) Then
                survey.s886 = txt_s886.Text.Replace(",", String.Empty)
            Else
                survey.s886 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s887.Text) Then
                survey.s887 = txt_s887.Text.Replace(",", String.Empty)
            Else
                survey.s887 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s888.Text) Then
                survey.s888 = txt_s888.Text.Replace(",", String.Empty)
            Else
                survey.s888 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s889.Text) Then
                survey.s889 = txt_s889.Text.Replace(",", String.Empty)
            Else
                survey.s889 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s890.Text) Then
                survey.s890 = txt_s890.Text.Replace(",", String.Empty)
            Else
                survey.s890 = Nothing
            End If


            survey.section4Complete = cb_section4Complete.Checked



            survey.IsAuditComplete = False
            db.SaveChanges()
            lbl_lastSave.Text = survey.ModifiedDate
        End Using
    End Sub
    Protected Sub btn_previous_Click(sender As Object, e As EventArgs) Handles btn_previous.Click
        SaveSurvey(False)
        Response.Redirect("Compensation.aspx")
    End Sub
    Protected Sub btn_save_Click(sender As Object, e As EventArgs) Handles btn_save.Click
        SaveSurvey(False)
    End Sub
    Protected Sub btn_next_Click(sender As Object, e As EventArgs) Handles btn_next.Click
        SaveSurvey(False)
        Response.Redirect("Benefits.aspx")
    End Sub
    Protected Sub ddl_location_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddl_location.SelectedIndexChanged
        SaveSurvey(False)
        Session("LocationID") = ddl_location.SelectedValue
        txt_LocationID.Text = ddl_location.SelectedValue
        Response.Redirect("CompByJob.aspx")
    End Sub
    Protected Sub lb_toc_bot_Click(sender As Object, e As EventArgs) Handles lb_toc_bot.Click
        SaveSurvey(False)
        Response.Redirect("TableOfContents.aspx")
    End Sub
End Class
