﻿
Imports Models

Partial Class Surveys_Recruiting
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Session("LocationID") Is Nothing Then
            If Not String.IsNullOrEmpty(txt_LocationID.Text) Then
                Session("LocationID") = Convert.ToInt32(txt_LocationID.Text)
            Else
                Response.Redirect("../../account/login.aspx")
            End If
        End If
        If Session("UserId") Is Nothing Then
            If Not String.IsNullOrEmpty(txt_userid.Text) Then
                Session("UserId") = Convert.ToInt32(txt_userid.Text)
            Else
                Response.Redirect("../../account/login.aspx")
            End If
        End If
        If Not IsPostBack Then
            Dim LocationID As Integer = Session("LocationID")
            txt_LocationID.Text = LocationID
            Dim userid As Integer = Session("UserId")
            txt_userid.Text = userid
            If Session("Client") IsNot Nothing And Not String.IsNullOrEmpty(Session("Client")) Then
                lbl_deadline.Text = SurveyHelper.ReturnDeadline(Session("Client").ToString)
            Else
                lbl_deadline.Text = "Friday, April 13, 2018"
            End If

            ddl_location.Items.Clear()
            Dim locations As List(Of ListItem) = SurveyHelper.ReturnLocationList(Session("OrganizationId"))
            For Each l In locations
                ddl_location.Items.Add(l)
            Next
            ddl_location.SelectedValue = Session("LocationID")
            tbl_location.Visible = False
            LoadSurvey()
        End If
    End Sub
    Sub LoadSurvey()
        Using db As New IICICEntities
            Dim LocationID As Integer = txt_LocationID.Text
            Dim survey As New CompSurvey
            survey = (From s In db.CompSurveys Where s.LocationID = LocationID And s.SurveyYear = Now.Year).FirstOrDefault
            If survey IsNot Nothing Then
                lbl_lastSave.Text = survey.ModifiedDate

                '---PASTE LOAD STATEMENTS---

                If survey.s36 IsNot Nothing Then
                    txt_s36.Text = String.Format(survey.s36, "N0")
                End If
                If survey.s39 IsNot Nothing Then
                    txt_s39.Text = String.Format(survey.s39, "N0")
                End If
                If survey.s37 IsNot Nothing Then
                    txt_s37.Text = String.Format(survey.s37, "N0")
                End If
                If survey.s40 IsNot Nothing Then
                    txt_s40.Text = String.Format(survey.s40, "N0")
                End If
                If survey.s38 IsNot Nothing Then
                    txt_s38.Text = String.Format(survey.s38, "N0")
                End If
                If survey.s41 IsNot Nothing Then
                    txt_s41.Text = String.Format(survey.s41, "N0")
                End If
                If survey.s42 IsNot Nothing Then
                    txt_s42.Text = String.Format(survey.s42, "N0")
                End If
                If survey.s43 IsNot Nothing Then
                    txt_s43.Text = String.Format(survey.s43, "N0")
                End If
                If survey.s44 IsNot Nothing Then
                    txt_s44.Text = String.Format(survey.s44, "N0")
                End If
                If survey.s53 IsNot Nothing Then
                    txt_s53.Text = String.Format(survey.s53, "N0")
                End If
                If survey.s55 IsNot Nothing Then
                    txt_s55.Text = String.Format(survey.s55, "N0")
                End If
                If Not String.IsNullOrEmpty(survey.s57oth) Then
                    txt_s57oth.Text = survey.s57oth
                End If
                If survey.s59 IsNot Nothing Then
                    txt_s59.Text = String.Format(survey.s59, "N0")
                End If
                If survey.s63 IsNot Nothing Then
                    txt_s63.Text = String.Format(survey.s63, "N0")
                End If
                If survey.s60 IsNot Nothing Then
                    txt_s60.Text = String.Format(survey.s60, "N0")
                End If
                If survey.s64 IsNot Nothing Then
                    txt_s64.Text = String.Format(survey.s64, "N0")
                End If
                If survey.s61 IsNot Nothing Then
                    txt_s61.Text = String.Format(survey.s61, "N0")
                End If
                If survey.s65 IsNot Nothing Then
                    txt_s65.Text = String.Format(survey.s65, "N0")
                End If
                If survey.s62 IsNot Nothing Then
                    txt_s62.Text = String.Format(survey.s62, "N0")
                End If
                If survey.s66 IsNot Nothing Then
                    txt_s66.Text = String.Format(survey.s66, "N0")
                End If
                If Not String.IsNullOrEmpty(survey.s529oth) Then
                    txt_s529oth.Text = survey.s529oth
                End If

                If survey.s45 IsNot Nothing Then
                    cb_s45.Checked = survey.s45
                End If
                If survey.s52 IsNot Nothing Then
                    cb_s52.Checked = survey.s52
                End If
                If survey.s46 IsNot Nothing Then
                    cb_s46.Checked = survey.s46
                End If
                If survey.s47 IsNot Nothing Then
                    cb_s47.Checked = survey.s47
                End If
                If survey.s54 IsNot Nothing Then
                    cb_s54.Checked = survey.s54
                End If
                If survey.s48 IsNot Nothing Then
                    cb_s48.Checked = survey.s48
                End If
                If survey.s523 IsNot Nothing Then
                    cb_s523.Checked = survey.s523
                End If
                If survey.s524 IsNot Nothing Then
                    cb_s524.Checked = survey.s524
                End If
                If survey.s49 IsNot Nothing Then
                    cb_s49.Checked = survey.s49
                End If
                If survey.s56 IsNot Nothing Then
                    cb_s56.Checked = survey.s56
                End If
                If survey.s50 IsNot Nothing Then
                    cb_s50.Checked = survey.s50
                End If
                If survey.s57 IsNot Nothing Then
                    cb_s57.Checked = survey.s57
                End If
                If survey.s51 IsNot Nothing Then
                    cb_s51.Checked = survey.s51
                End If
                If survey.s58 IsNot Nothing Then
                    cb_s58.Checked = survey.s58
                End If
                If survey.s526 IsNot Nothing Then
                    cb_s526.Checked = survey.s526
                End If
                If survey.s527 IsNot Nothing Then
                    cb_s527.Checked = survey.s527
                End If
                If survey.s528 IsNot Nothing Then
                    cb_s528.Checked = survey.s528
                End If
                If survey.s529 IsNot Nothing Then
                    cb_s529.Checked = survey.s529
                End If
                If survey.section2Complete IsNot Nothing Then
                    cb_section2Complete.Checked = survey.section2Complete
                End If

                If survey.s525 IsNot Nothing Then
                    If survey.s525 = 1 Then rb_s525_1.Checked = True
                End If
                If survey.s525 IsNot Nothing Then
                    If survey.s525 = 2 Then rb_s525_2.Checked = True
                End If




            End If
        End Using
    End Sub
    Sub SaveSurvey(ByVal IsSubmit As Boolean)
        Using db As New Models.IICICEntities
            Dim existingSurvey As Boolean = False
            Dim LocationID As Integer = txt_LocationID.Text
            Dim userid As Integer = Convert.ToInt16(Session("UserId"))
            Dim survey As New CompSurvey
            survey = (From s In db.CompSurveys Where s.LocationID = LocationID And s.SurveyYear = Now.Year).FirstOrDefault
            If survey Is Nothing Then
                survey = New CompSurvey
                db.CompSurveys.Add(survey)
                db.SaveChanges()
            End If
            survey.LocationID = LocationID
            survey.LastModUserId = userid
            survey.ModifiedDate = Now
            survey.SurveyYear = Now.Year
            If IsSubmit Then
                survey.SubmitDate = Now
            End If

            '---PASTE SAVE STATEMENTS---
            If Not String.IsNullOrEmpty(txt_s36.Text) Then
                survey.s36 = txt_s36.Text.Replace(",", String.Empty)
            Else
                survey.s36 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s39.Text) Then
                survey.s39 = txt_s39.Text.Replace(",", String.Empty)
            Else
                survey.s39 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s37.Text) Then
                survey.s37 = txt_s37.Text.Replace(",", String.Empty)
            Else
                survey.s37 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s40.Text) Then
                survey.s40 = txt_s40.Text.Replace(",", String.Empty)
            Else
                survey.s40 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s38.Text) Then
                survey.s38 = txt_s38.Text.Replace(",", String.Empty)
            Else
                survey.s38 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s41.Text) Then
                survey.s41 = txt_s41.Text.Replace(",", String.Empty)
            Else
                survey.s41 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s42.Text) Then
                survey.s42 = txt_s42.Text.Replace(",", String.Empty)
            Else
                survey.s42 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s43.Text) Then
                survey.s43 = txt_s43.Text.Replace(",", String.Empty)
            Else
                survey.s43 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s44.Text) Then
                survey.s44 = txt_s44.Text.Replace(",", String.Empty)
            Else
                survey.s44 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s53.Text) Then
                survey.s53 = txt_s53.Text.Replace(",", String.Empty)
            Else
                survey.s53 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s55.Text) Then
                survey.s55 = txt_s55.Text.Replace(",", String.Empty)
            Else
                survey.s55 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s57oth.Text) Then
                survey.s57oth = txt_s57oth.Text.Replace(",", String.Empty)
            Else
                survey.s57oth = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s59.Text) Then
                survey.s59 = txt_s59.Text.Replace(",", String.Empty)
            Else
                survey.s59 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s63.Text) Then
                survey.s63 = txt_s63.Text.Replace(",", String.Empty)
            Else
                survey.s63 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s60.Text) Then
                survey.s60 = txt_s60.Text.Replace(",", String.Empty)
            Else
                survey.s60 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s64.Text) Then
                survey.s64 = txt_s64.Text.Replace(",", String.Empty)
            Else
                survey.s64 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s61.Text) Then
                survey.s61 = txt_s61.Text.Replace(",", String.Empty)
            Else
                survey.s61 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s65.Text) Then
                survey.s65 = txt_s65.Text.Replace(",", String.Empty)
            Else
                survey.s65 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s62.Text) Then
                survey.s62 = txt_s62.Text.Replace(",", String.Empty)
            Else
                survey.s62 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s66.Text) Then
                survey.s66 = txt_s66.Text.Replace(",", String.Empty)
            Else
                survey.s66 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s529oth.Text) Then
                survey.s529oth = txt_s529oth.Text.Replace(",", String.Empty)
            Else
                survey.s529oth = Nothing
            End If

            survey.s45 = cb_s45.Checked
            survey.s52 = cb_s52.Checked
            survey.s46 = cb_s46.Checked
            survey.s47 = cb_s47.Checked
            survey.s54 = cb_s54.Checked
            survey.s48 = cb_s48.Checked
            survey.s523 = cb_s523.Checked
            survey.s524 = cb_s524.Checked
            survey.s49 = cb_s49.Checked
            survey.s56 = cb_s56.Checked
            survey.s50 = cb_s50.Checked
            survey.s57 = cb_s57.Checked
            survey.s51 = cb_s51.Checked
            survey.s58 = cb_s58.Checked
            survey.s526 = cb_s526.Checked
            survey.s527 = cb_s527.Checked
            survey.s528 = cb_s528.Checked
            survey.s529 = cb_s529.Checked
            survey.section2Complete = cb_section2Complete.Checked

            If rb_s525_1.Checked Then survey.s525 = 1
            If rb_s525_2.Checked Then survey.s525 = 2




            survey.IsAuditComplete = False
            db.SaveChanges()
            lbl_lastSave.Text = survey.ModifiedDate
        End Using
    End Sub
    Protected Sub btn_previous_Click(sender As Object, e As EventArgs) Handles btn_previous.Click
        SaveSurvey(False)
        Response.Redirect("Profile.aspx")
    End Sub
    Protected Sub btn_save_Click(sender As Object, e As EventArgs) Handles btn_save.Click
        SaveSurvey(False)
    End Sub
    Protected Sub btn_next_Click(sender As Object, e As EventArgs) Handles btn_next.Click
        SaveSurvey(False)
        Response.Redirect("Compensation.aspx")
    End Sub
    Protected Sub ddl_location_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddl_location.SelectedIndexChanged
        SaveSurvey(False)
        Session("LocationID") = ddl_location.SelectedValue
        txt_LocationID.Text = ddl_location.SelectedValue
        Response.Redirect("Recruiting.aspx")
    End Sub
    Protected Sub lb_toc_bot_Click(sender As Object, e As EventArgs) Handles lb_toc_bot.Click
        SaveSurvey(False)
        Response.Redirect("TableOfContents.aspx")
    End Sub
End Class
