﻿
Imports Models

Partial Class Surveys_Sales
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load

        

        If Session("LocationID") Is Nothing Then
            If Not String.IsNullOrEmpty(txt_LocationID.Text) Then
                Session("LocationID") = Convert.ToInt32(txt_LocationID.Text)
            Else
                Response.Redirect("../../account/login.aspx")
            End If
        End If
        If Session("UserId") Is Nothing Then
            If Not String.IsNullOrEmpty(txt_userid.Text) Then
                Session("UserId") = Convert.ToInt32(txt_userid.Text)
            Else
                Response.Redirect("../../account/login.aspx")
            End If
        End If
        If Not IsPostBack Then
            Dim LocationID As Integer = Session("LocationID")
            txt_LocationID.Text = LocationID
            Dim userid As Integer = Session("UserId")
            txt_userid.Text = userid
            If Session("Client") IsNot Nothing And Not String.IsNullOrEmpty(Session("Client")) Then
                lbl_deadline.Text = SurveyHelper.ReturnDeadline(Session("Client").ToString)
            Else
                lbl_deadline.Text = "Friday, April 13, 2018"
            End If
            ddl_location.Items.Clear()
            Dim locations As List(Of ListItem) = SurveyHelper.ReturnLocationList(Session("OrganizationId"))
            For Each l In locations
                ddl_location.Items.Add(l)
            Next
            ddl_location.SelectedValue = Session("LocationID")
            tbl_location.Visible = False
            LoadSurvey()
        End If
    End Sub
    Sub LoadSurvey()
        Using db As New IICICEntities
            Dim LocationID As Integer = txt_LocationID.Text
            Dim survey As New CompSurvey
            survey = (From s In db.CompSurveys Where s.LocationID = LocationID And s.SurveyYear = Now.Year).FirstOrDefault
            If survey IsNot Nothing Then
                lbl_lastSave.Text = survey.ModifiedDate


                '__Custom Show/hide based on assocation____________________________\/

                MHEDAtbl.Visible = False 'default

                If survey.s1910 IsNot Nothing Then
                    'show NAW sections
                    If survey.s1910 = True Then MHEDAtbl.Visible = True
                End If
                '__Custom Show/hide based on assocation____________________________/\


                '---PASTE LOAD STATEMENTS---
                If survey.s375 IsNot Nothing Then
                    txt_s375.Text = String.Format(survey.s375, "N0")
                End If
                If survey.s377 IsNot Nothing Then
                    txt_s377.Text = String.Format(survey.s377, "N0")
                End If
                If Not String.IsNullOrEmpty(survey.s378oth) Then
                    txt_s378oth.Text = survey.s378oth
                End If
                If survey.s378 IsNot Nothing Then
                    txt_s378.Text = String.Format(survey.s378, "N0")
                End If
                If Not String.IsNullOrEmpty(survey.s385oth) Then
                    txt_s385oth.Text = survey.s385oth
                End If
                If Not String.IsNullOrEmpty(survey.s386oth) Then
                    txt_s386oth.Text = survey.s386oth
                End If
                If Not String.IsNullOrEmpty(survey.s563oth) Then
                    txt_s563oth.Text = survey.s563oth
                End If
                If Not String.IsNullOrEmpty(survey.s387oth) Then
                    txt_s387oth.Text = survey.s387oth
                End If
                If Not String.IsNullOrEmpty(survey.s571oth) Then
                    txt_s571oth.Text = survey.s571oth
                End If
                If Not String.IsNullOrEmpty(survey.s572oth) Then
                    txt_s572oth.Text = survey.s572oth
                End If
                If survey.s915 IsNot Nothing Then
                    txt_s915.Text = String.Format(survey.s915, "N0")
                End If
                If survey.s925 IsNot Nothing Then
                    txt_s925.Text = String.Format(survey.s925, "N0")
                End If
                If survey.s935 IsNot Nothing Then
                    txt_s935.Text = String.Format(survey.s935, "N0")
                End If
                If survey.s945 IsNot Nothing Then
                    txt_s945.Text = String.Format(survey.s945, "N0")
                End If
                If survey.s955 IsNot Nothing Then
                    txt_s955.Text = String.Format(survey.s955, "N0")
                End If
                If survey.s916 IsNot Nothing Then
                    txt_s916.Text = String.Format(survey.s916, "N0")
                End If
                If survey.s926 IsNot Nothing Then
                    txt_s926.Text = String.Format(survey.s926, "N0")
                End If
                If survey.s936 IsNot Nothing Then
                    txt_s936.Text = String.Format(survey.s936, "N0")
                End If
                If survey.s946 IsNot Nothing Then
                    txt_s946.Text = String.Format(survey.s946, "N0")
                End If
                If survey.s956 IsNot Nothing Then
                    txt_s956.Text = String.Format(survey.s956, "N0")
                End If
                If survey.s917 IsNot Nothing Then
                    txt_s917.Text = String.Format(survey.s917, "N0")
                End If
                If survey.s927 IsNot Nothing Then
                    txt_s927.Text = String.Format(survey.s927, "N0")
                End If
                If survey.s937 IsNot Nothing Then
                    txt_s937.Text = String.Format(survey.s937, "N0")
                End If
                If survey.s947 IsNot Nothing Then
                    txt_s947.Text = String.Format(survey.s947, "N0")
                End If
                If survey.s957 IsNot Nothing Then
                    txt_s957.Text = String.Format(survey.s957, "N0")
                End If
                If survey.s918 IsNot Nothing Then
                    txt_s918.Text = String.Format(survey.s918, "N0")
                End If
                If survey.s928 IsNot Nothing Then
                    txt_s928.Text = String.Format(survey.s928, "N0")
                End If
                If survey.s938 IsNot Nothing Then
                    txt_s938.Text = String.Format(survey.s938, "N0")
                End If
                If survey.s948 IsNot Nothing Then
                    txt_s948.Text = String.Format(survey.s948, "N0")
                End If
                If survey.s958 IsNot Nothing Then
                    txt_s958.Text = String.Format(survey.s958, "N0")
                End If
                If survey.s919 IsNot Nothing Then
                    txt_s919.Text = String.Format(survey.s919, "N0")
                End If
                If survey.s929 IsNot Nothing Then
                    txt_s929.Text = String.Format(survey.s929, "N0")
                End If
                If survey.s939 IsNot Nothing Then
                    txt_s939.Text = String.Format(survey.s939, "N0")
                End If
                If survey.s949 IsNot Nothing Then
                    txt_s949.Text = String.Format(survey.s949, "N0")
                End If
                If survey.s959 IsNot Nothing Then
                    txt_s959.Text = String.Format(survey.s959, "N0")
                End If
                If survey.s920 IsNot Nothing Then
                    txt_s920.Text = String.Format(survey.s920, "N0")
                End If
                If survey.s930 IsNot Nothing Then
                    txt_s930.Text = String.Format(survey.s930, "N0")
                End If
                If survey.s940 IsNot Nothing Then
                    txt_s940.Text = String.Format(survey.s940, "N0")
                End If
                If survey.s950 IsNot Nothing Then
                    txt_s950.Text = String.Format(survey.s950, "N0")
                End If
                If survey.s960 IsNot Nothing Then
                    txt_s960.Text = String.Format(survey.s960, "N0")
                End If
                If survey.s921 IsNot Nothing Then
                    txt_s921.Text = String.Format(survey.s921, "N0")
                End If
                If survey.s931 IsNot Nothing Then
                    txt_s931.Text = String.Format(survey.s931, "N0")
                End If
                If survey.s941 IsNot Nothing Then
                    txt_s941.Text = String.Format(survey.s941, "N0")
                End If
                If survey.s951 IsNot Nothing Then
                    txt_s951.Text = String.Format(survey.s951, "N0")
                End If
                If survey.s961 IsNot Nothing Then
                    txt_s961.Text = String.Format(survey.s961, "N0")
                End If
                If survey.s922 IsNot Nothing Then
                    txt_s922.Text = String.Format(survey.s922, "N0")
                End If
                If survey.s932 IsNot Nothing Then
                    txt_s932.Text = String.Format(survey.s932, "N0")
                End If
                If survey.s942 IsNot Nothing Then
                    txt_s942.Text = String.Format(survey.s942, "N0")
                End If
                If survey.s952 IsNot Nothing Then
                    txt_s952.Text = String.Format(survey.s952, "N0")
                End If
                If survey.s962 IsNot Nothing Then
                    txt_s962.Text = String.Format(survey.s962, "N0")
                End If
                If survey.s923 IsNot Nothing Then
                    txt_s923.Text = String.Format(survey.s923, "N0")
                End If
                If survey.s933 IsNot Nothing Then
                    txt_s933.Text = String.Format(survey.s933, "N0")
                End If
                If survey.s943 IsNot Nothing Then
                    txt_s943.Text = String.Format(survey.s943, "N0")
                End If
                If survey.s953 IsNot Nothing Then
                    txt_s953.Text = String.Format(survey.s953, "N0")
                End If
                If survey.s963 IsNot Nothing Then
                    txt_s963.Text = String.Format(survey.s963, "N0")
                End If
                If survey.s924 IsNot Nothing Then
                    txt_s924.Text = String.Format(survey.s924, "N0")
                End If
                If survey.s934 IsNot Nothing Then
                    txt_s934.Text = String.Format(survey.s934, "N0")
                End If
                If survey.s944 IsNot Nothing Then
                    txt_s944.Text = String.Format(survey.s944, "N0")
                End If
                If survey.s954 IsNot Nothing Then
                    txt_s954.Text = String.Format(survey.s954, "N0")
                End If
                If survey.s964 IsNot Nothing Then
                    txt_s964.Text = String.Format(survey.s964, "N0")
                End If

                If survey.s381 IsNot Nothing Then
                    cb_s381.Checked = survey.s381
                End If
                If survey.s384 IsNot Nothing Then
                    cb_s384.Checked = survey.s384
                End If
                If survey.s382 IsNot Nothing Then
                    cb_s382.Checked = survey.s382
                End If
                If survey.s385 IsNot Nothing Then
                    cb_s385.Checked = survey.s385
                End If
                If survey.s383 IsNot Nothing Then
                    cb_s383.Checked = survey.s383
                End If
                If survey.s559 IsNot Nothing Then
                    cb_s559.Checked = survey.s559
                End If
                If survey.s562 IsNot Nothing Then
                    cb_s562.Checked = survey.s562
                End If
                If survey.s560 IsNot Nothing Then
                    cb_s560.Checked = survey.s560
                End If
                If survey.s563 IsNot Nothing Then
                    cb_s563.Checked = survey.s563
                End If
                If survey.s561 IsNot Nothing Then
                    cb_s561.Checked = survey.s561
                End If
                If survey.s564 IsNot Nothing Then
                    cb_s564.Checked = survey.s564
                End If
                If survey.s566 IsNot Nothing Then
                    cb_s566.Checked = survey.s566
                End If
                If survey.s568 IsNot Nothing Then
                    cb_s568.Checked = survey.s568
                End If
                If survey.s565 IsNot Nothing Then
                    cb_s565.Checked = survey.s565
                End If
                If survey.s567 IsNot Nothing Then
                    cb_s567.Checked = survey.s567
                End If
                If survey.s569 IsNot Nothing Then
                    cb_s569.Checked = survey.s569
                End If
                If survey.section6Complete IsNot Nothing Then
                    cb_section6Complete.Checked = survey.section6Complete
                End If

                If survey.s379 IsNot Nothing Then
                    If survey.s379 = 1 Then rb_s379_1.Checked = True
                End If
                If survey.s379 IsNot Nothing Then
                    If survey.s379 = 2 Then rb_s379_2.Checked = True
                End If
                If survey.s380 IsNot Nothing Then
                    If survey.s380 = 1 Then rb_s380_1.Checked = True
                End If
                If survey.s380 IsNot Nothing Then
                    If survey.s380 = 2 Then rb_s380_2.Checked = True
                End If
                If survey.s386 IsNot Nothing Then
                    If survey.s386 = 1 Then rb_s386_1.Checked = True
                End If
                If survey.s386 IsNot Nothing Then
                    If survey.s386 = 4 Then rb_s386_4.Checked = True
                End If
                If survey.s386 IsNot Nothing Then
                    If survey.s386 = 2 Then rb_s386_2.Checked = True
                End If
                If survey.s386 IsNot Nothing Then
                    If survey.s386 = 5 Then rb_s386_5.Checked = True
                End If
                If survey.s386 IsNot Nothing Then
                    If survey.s386 = 3 Then rb_s386_3.Checked = True
                End If
                If survey.s386 IsNot Nothing Then
                    If survey.s386 = 6 Then rb_s386_6.Checked = True
                End If
                If survey.s387 IsNot Nothing Then
                    If survey.s387 = 1 Then rb_s387_1.Checked = True
                End If
                If survey.s387 IsNot Nothing Then
                    If survey.s387 = 4 Then rb_s387_4.Checked = True
                End If
                If survey.s387 IsNot Nothing Then
                    If survey.s387 = 2 Then rb_s387_2.Checked = True
                End If
                If survey.s387 IsNot Nothing Then
                    If survey.s387 = 5 Then rb_s387_5.Checked = True
                End If
                If survey.s387 IsNot Nothing Then
                    If survey.s387 = 3 Then rb_s387_3.Checked = True
                End If
                If survey.s387 IsNot Nothing Then
                    If survey.s387 = 6 Then rb_s387_6.Checked = True
                End If
                If survey.s570 IsNot Nothing Then
                    If survey.s570 = 1 Then rb_s570_1.Checked = True
                End If
                If survey.s570 IsNot Nothing Then
                    If survey.s570 = 2 Then rb_s570_2.Checked = True
                End If
                If survey.s570 IsNot Nothing Then
                    If survey.s570 = 3 Then rb_s570_3.Checked = True
                End If
                If survey.s570 IsNot Nothing Then
                    If survey.s570 = 4 Then rb_s570_4.Checked = True
                End If
                If survey.s571 IsNot Nothing Then
                    If survey.s571 = 1 Then rb_s571_1.Checked = True
                End If
                If survey.s571 IsNot Nothing Then
                    If survey.s571 = 2 Then rb_s571_2.Checked = True
                End If
                If survey.s571 IsNot Nothing Then
                    If survey.s571 = 3 Then rb_s571_3.Checked = True
                End If
                If survey.s571 IsNot Nothing Then
                    If survey.s571 = 4 Then rb_s571_4.Checked = True
                End If
                If survey.s572 IsNot Nothing Then
                    If survey.s572 = 1 Then rb_s572_1.Checked = True
                End If
                If survey.s572 IsNot Nothing Then
                    If survey.s572 = 2 Then rb_s572_2.Checked = True
                End If
                If survey.s572 IsNot Nothing Then
                    If survey.s572 = 3 Then rb_s572_3.Checked = True
                End If
                If survey.s572 IsNot Nothing Then
                    If survey.s572 = 4 Then rb_s572_4.Checked = True
                End If




            End If
        End Using
    End Sub
    Sub SaveSurvey(ByVal IsSubmit As Boolean)
        Using db As New Models.IICICEntities
            Dim existingSurvey As Boolean = False
            Dim LocationID As Integer = txt_LocationID.Text
            Dim userid As Integer = Convert.ToInt16(Session("UserId"))
            Dim survey As New CompSurvey
            survey = (From s In db.CompSurveys Where s.LocationID = LocationID And s.SurveyYear = Now.Year).FirstOrDefault
            If survey Is Nothing Then
                survey = New CompSurvey
                db.CompSurveys.Add(survey)
                db.SaveChanges()
            End If
            survey.LocationID = LocationID
            survey.LastModUserId = userid
            survey.ModifiedDate = Now
            survey.SurveyYear = Now.Year
            If IsSubmit Then
                survey.SubmitDate = Now
            End If

            '---PASTE SAVE STATEMENTS---
            If Not String.IsNullOrEmpty(txt_s375.Text) Then
                survey.s375 = txt_s375.Text.Replace(",", String.Empty)
            Else
                survey.s375 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s377.Text) Then
                survey.s377 = txt_s377.Text.Replace(",", String.Empty)
            Else
                survey.s377 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s378oth.Text) Then
                survey.s378oth = txt_s378oth.Text.Replace(",", String.Empty)
            Else
                survey.s378oth = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s378.Text) Then
                survey.s378 = txt_s378.Text.Replace(",", String.Empty)
            Else
                survey.s378 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s385oth.Text) Then
                survey.s385oth = txt_s385oth.Text.Replace(",", String.Empty)
            Else
                survey.s385oth = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s386oth.Text) Then
                survey.s386oth = txt_s386oth.Text.Replace(",", String.Empty)
            Else
                survey.s386oth = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s563oth.Text) Then
                survey.s563oth = txt_s563oth.Text.Replace(",", String.Empty)
            Else
                survey.s563oth = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s387oth.Text) Then
                survey.s387oth = txt_s387oth.Text.Replace(",", String.Empty)
            Else
                survey.s387oth = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s571oth.Text) Then
                survey.s571oth = txt_s571oth.Text.Replace(",", String.Empty)
            Else
                survey.s571oth = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s572oth.Text) Then
                survey.s572oth = txt_s572oth.Text.Replace(",", String.Empty)
            Else
                survey.s572oth = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s915.Text) Then
                survey.s915 = txt_s915.Text.Replace(",", String.Empty)
            Else
                survey.s915 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s925.Text) Then
                survey.s925 = txt_s925.Text.Replace(",", String.Empty)
            Else
                survey.s925 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s935.Text) Then
                survey.s935 = txt_s935.Text.Replace(",", String.Empty)
            Else
                survey.s935 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s945.Text) Then
                survey.s945 = txt_s945.Text.Replace(",", String.Empty)
            Else
                survey.s945 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s955.Text) Then
                survey.s955 = txt_s955.Text.Replace(",", String.Empty)
            Else
                survey.s955 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s916.Text) Then
                survey.s916 = txt_s916.Text.Replace(",", String.Empty)
            Else
                survey.s916 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s926.Text) Then
                survey.s926 = txt_s926.Text.Replace(",", String.Empty)
            Else
                survey.s926 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s936.Text) Then
                survey.s936 = txt_s936.Text.Replace(",", String.Empty)
            Else
                survey.s936 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s946.Text) Then
                survey.s946 = txt_s946.Text.Replace(",", String.Empty)
            Else
                survey.s946 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s956.Text) Then
                survey.s956 = txt_s956.Text.Replace(",", String.Empty)
            Else
                survey.s956 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s917.Text) Then
                survey.s917 = txt_s917.Text.Replace(",", String.Empty)
            Else
                survey.s917 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s927.Text) Then
                survey.s927 = txt_s927.Text.Replace(",", String.Empty)
            Else
                survey.s927 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s937.Text) Then
                survey.s937 = txt_s937.Text.Replace(",", String.Empty)
            Else
                survey.s937 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s947.Text) Then
                survey.s947 = txt_s947.Text.Replace(",", String.Empty)
            Else
                survey.s947 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s957.Text) Then
                survey.s957 = txt_s957.Text.Replace(",", String.Empty)
            Else
                survey.s957 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s918.Text) Then
                survey.s918 = txt_s918.Text.Replace(",", String.Empty)
            Else
                survey.s918 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s928.Text) Then
                survey.s928 = txt_s928.Text.Replace(",", String.Empty)
            Else
                survey.s928 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s938.Text) Then
                survey.s938 = txt_s938.Text.Replace(",", String.Empty)
            Else
                survey.s938 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s948.Text) Then
                survey.s948 = txt_s948.Text.Replace(",", String.Empty)
            Else
                survey.s948 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s958.Text) Then
                survey.s958 = txt_s958.Text.Replace(",", String.Empty)
            Else
                survey.s958 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s919.Text) Then
                survey.s919 = txt_s919.Text.Replace(",", String.Empty)
            Else
                survey.s919 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s929.Text) Then
                survey.s929 = txt_s929.Text.Replace(",", String.Empty)
            Else
                survey.s929 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s939.Text) Then
                survey.s939 = txt_s939.Text.Replace(",", String.Empty)
            Else
                survey.s939 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s949.Text) Then
                survey.s949 = txt_s949.Text.Replace(",", String.Empty)
            Else
                survey.s949 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s959.Text) Then
                survey.s959 = txt_s959.Text.Replace(",", String.Empty)
            Else
                survey.s959 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s920.Text) Then
                survey.s920 = txt_s920.Text.Replace(",", String.Empty)
            Else
                survey.s920 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s930.Text) Then
                survey.s930 = txt_s930.Text.Replace(",", String.Empty)
            Else
                survey.s930 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s940.Text) Then
                survey.s940 = txt_s940.Text.Replace(",", String.Empty)
            Else
                survey.s940 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s950.Text) Then
                survey.s950 = txt_s950.Text.Replace(",", String.Empty)
            Else
                survey.s950 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s960.Text) Then
                survey.s960 = txt_s960.Text.Replace(",", String.Empty)
            Else
                survey.s960 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s921.Text) Then
                survey.s921 = txt_s921.Text.Replace(",", String.Empty)
            Else
                survey.s921 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s931.Text) Then
                survey.s931 = txt_s931.Text.Replace(",", String.Empty)
            Else
                survey.s931 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s941.Text) Then
                survey.s941 = txt_s941.Text.Replace(",", String.Empty)
            Else
                survey.s941 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s951.Text) Then
                survey.s951 = txt_s951.Text.Replace(",", String.Empty)
            Else
                survey.s951 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s961.Text) Then
                survey.s961 = txt_s961.Text.Replace(",", String.Empty)
            Else
                survey.s961 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s922.Text) Then
                survey.s922 = txt_s922.Text.Replace(",", String.Empty)
            Else
                survey.s922 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s932.Text) Then
                survey.s932 = txt_s932.Text.Replace(",", String.Empty)
            Else
                survey.s932 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s942.Text) Then
                survey.s942 = txt_s942.Text.Replace(",", String.Empty)
            Else
                survey.s942 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s952.Text) Then
                survey.s952 = txt_s952.Text.Replace(",", String.Empty)
            Else
                survey.s952 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s962.Text) Then
                survey.s962 = txt_s962.Text.Replace(",", String.Empty)
            Else
                survey.s962 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s923.Text) Then
                survey.s923 = txt_s923.Text.Replace(",", String.Empty)
            Else
                survey.s923 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s933.Text) Then
                survey.s933 = txt_s933.Text.Replace(",", String.Empty)
            Else
                survey.s933 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s943.Text) Then
                survey.s943 = txt_s943.Text.Replace(",", String.Empty)
            Else
                survey.s943 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s953.Text) Then
                survey.s953 = txt_s953.Text.Replace(",", String.Empty)
            Else
                survey.s953 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s963.Text) Then
                survey.s963 = txt_s963.Text.Replace(",", String.Empty)
            Else
                survey.s963 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s924.Text) Then
                survey.s924 = txt_s924.Text.Replace(",", String.Empty)
            Else
                survey.s924 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s934.Text) Then
                survey.s934 = txt_s934.Text.Replace(",", String.Empty)
            Else
                survey.s934 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s944.Text) Then
                survey.s944 = txt_s944.Text.Replace(",", String.Empty)
            Else
                survey.s944 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s954.Text) Then
                survey.s954 = txt_s954.Text.Replace(",", String.Empty)
            Else
                survey.s954 = Nothing
            End If
            If Not String.IsNullOrEmpty(txt_s964.Text) Then
                survey.s964 = txt_s964.Text.Replace(",", String.Empty)
            Else
                survey.s964 = Nothing
            End If

            survey.s381 = cb_s381.Checked
            survey.s384 = cb_s384.Checked
            survey.s382 = cb_s382.Checked
            survey.s385 = cb_s385.Checked
            survey.s383 = cb_s383.Checked
            survey.s559 = cb_s559.Checked
            survey.s562 = cb_s562.Checked
            survey.s560 = cb_s560.Checked
            survey.s563 = cb_s563.Checked
            survey.s561 = cb_s561.Checked
            survey.s564 = cb_s564.Checked
            survey.s566 = cb_s566.Checked
            survey.s568 = cb_s568.Checked
            survey.s565 = cb_s565.Checked
            survey.s567 = cb_s567.Checked
            survey.s569 = cb_s569.Checked
            survey.section6Complete = cb_section6Complete.Checked

            If rb_s379_1.Checked Then survey.s379 = 1
            If rb_s379_2.Checked Then survey.s379 = 2
            If rb_s380_1.Checked Then survey.s380 = 1
            If rb_s380_2.Checked Then survey.s380 = 2
            If rb_s386_1.Checked Then survey.s386 = 1
            If rb_s386_4.Checked Then survey.s386 = 4
            If rb_s386_2.Checked Then survey.s386 = 2
            If rb_s386_5.Checked Then survey.s386 = 5
            If rb_s386_3.Checked Then survey.s386 = 3
            If rb_s386_6.Checked Then survey.s386 = 6
            If rb_s387_1.Checked Then survey.s387 = 1
            If rb_s387_4.Checked Then survey.s387 = 4
            If rb_s387_2.Checked Then survey.s387 = 2
            If rb_s387_5.Checked Then survey.s387 = 5
            If rb_s387_3.Checked Then survey.s387 = 3
            If rb_s387_6.Checked Then survey.s387 = 6
            If rb_s570_1.Checked Then survey.s570 = 1
            If rb_s570_2.Checked Then survey.s570 = 2
            If rb_s570_3.Checked Then survey.s570 = 3
            If rb_s570_4.Checked Then survey.s570 = 4
            If rb_s571_1.Checked Then survey.s571 = 1
            If rb_s571_2.Checked Then survey.s571 = 2
            If rb_s571_3.Checked Then survey.s571 = 3
            If rb_s571_4.Checked Then survey.s571 = 4
            If rb_s572_1.Checked Then survey.s572 = 1
            If rb_s572_2.Checked Then survey.s572 = 2
            If rb_s572_3.Checked Then survey.s572 = 3
            If rb_s572_4.Checked Then survey.s572 = 4

            survey.IsAuditComplete = False
            db.SaveChanges()
            lbl_lastSave.Text = survey.ModifiedDate
        End Using
    End Sub
    Protected Sub btn_previous_Click(sender As Object, e As EventArgs) Handles btn_previous.Click
        SaveSurvey(False)
        Response.Redirect("Benefits.aspx")
    End Sub
    Protected Sub btn_save_Click(sender As Object, e As EventArgs) Handles btn_save.Click
        SaveSurvey(False)
    End Sub
    Protected Sub btn_next_Click(sender As Object, e As EventArgs) Handles btn_next.Click
        SaveSurvey(False)
        Response.Redirect("BranchCompBen.aspx")
    End Sub
    Protected Sub ddl_location_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddl_location.SelectedIndexChanged
        SaveSurvey(False)
        Session("LocationID") = ddl_location.SelectedValue
        txt_LocationID.Text = ddl_location.SelectedValue
        Response.Redirect("Sales.aspx")
    End Sub
    Protected Sub lb_toc_bot_Click(sender As Object, e As EventArgs) Handles lb_toc_bot.Click
        SaveSurvey(False)
        Response.Redirect("TableOfContents.aspx")
    End Sub
End Class
