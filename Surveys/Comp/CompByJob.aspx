﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Survey.master"  MaintainScrollPositionOnPostback="true" AutoEventWireup="false" CodeFile="CompByJob.aspx.vb" Inherits="Surveys_CompByJob" %>

<%@ Register Src="~/Controls/SurveyNavigation.ascx" TagPrefix="uc1" TagName="SurveyNavigation" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    </asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script type="text/javascript">
        $(document).ready(function () {
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
    <script src="../../Scripts/autoNumeric/autoNumeric-min.js"></script>
    <script src="../../Scripts/CompByJob.js?v=1"></script>
    <asp:TextBox ID="txt_LocationID" runat="server" Visible="false"></asp:TextBox>
    <asp:TextBox ID="txt_userid" runat="server" Visible="false"></asp:TextBox>
    <div class="bg-content">
        <div class="row">
            <table class="table-condensed centered" id="tbl_location" runat="server">
                <tr>
                    <td>
                        <asp:DropDownList ID="ddl_location" runat="server" CssClass="form-control" AutoPostBack="true"></asp:DropDownList>
                    </td>
                    <td><span class="glyphicon glyphicon-info-sign icon-info" data-placement="left" data-toggle="tooltip" title="This box is disabled by default.  If you are submitting for multiple locations, please add the locations on the Organization Admin page and you'll be able to cycle through them here."></span></td>
                </tr>
            </table>

        </div>
        <div class="">
            <h3 class="text-center">Deadline: 
                <asp:Label ID="lbl_deadline" runat="server" Text=""></asp:Label></h3>
        </div>
        <div class=""><span class="col-md-12 text-center text-muted"><em>Last updated: 
            <asp:Label ID="lbl_lastSave" runat="server" Text=""></asp:Label></em></span></div>


        <h2 class="page-header">Compensation By Job Function for Full-time Employees</h2>

        <p>Please focus on the job function rather than job title. Employees in your company may have different titles, but perform similar roles as to what is described in the job descriptions. Refer to the enclosed position description as a guide for employee functions and classifications. If applicable, provide an average annual base salary and additional cash compensation for <strong>full-time employees only</strong>. Additional cash compensation includes commission/incentive pay and bonuses.  Do not include deferred compensation.  We recognize at some companies employees perform multiple duties. In such cases, choose the position that best reflects their “primary” duties.</p>
        <p><em><strong>Note:  Information for Outside/Inside Sales Representatives will be requested later in the survey.</strong></em></p>
        <table class="table-condensed">
            <tr>
                <td><a href="../../Resources/JobDescriptions.pdf" target="_blank">
                    <img src="../../Content/images/icon-pdf.png" /></a></td>
                <td><a href="../../Resources/JobDescriptions.pdf" target="_blank">Job Descriptions</a></td>
            </tr>
        </table>


        <table class="table table-condensed table-hover table-survey centered">
            <thead>
                <tr class="td-bg-primary">
                    <th class="text-center">Convert hourly employees to annual base salary by multiplying hourly wage x 2,080.</th>
                    <th class="text-center">Number of Full-Time Employees Reported by Position</th>
                    <th class="text-center">Average 2017
                        <br />
                        Annual Base Salary </th>
                    <th class="text-center">Average 2017 Annual
                        <br />
                        Additional Cash
                        <br />
                        Compensation*
                        <br />
                        per Person </th>
                </tr>
            </thead>

            <tbody>

                <tr class="td-bg-secondary">
                    <td colspan="6">General & Administrative</td>
                </tr>
                <tr>
                    <td class="col-md-2">Accountant</td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">#</span>
                            <asp:TextBox ID="txt_s182" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s183" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s184" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                </tr>
                <tr>
                    <td class="col-md-2">AR/Credit Manager</td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">#</span>
                            <asp:TextBox ID="txt_s185" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s186" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s187" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                </tr>
                <tr>
                    <td class="col-md-2">Accounting Clerk</td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">#</span>
                            <asp:TextBox ID="txt_s188" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s189" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s190" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                </tr>
                <tr>
                    <td class="col-md-2">Controller</td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">#</span>
                            <asp:TextBox ID="txt_s191" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s192" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s193" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                </tr>
                <tr>
                    <td class="col-md-2">Human Resources Manager</td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">#</span>
                            <asp:TextBox ID="txt_s194" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s195" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s196" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                </tr>
                <tr>
                    <td class="col-md-2">Office Manager</td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">#</span>
                            <asp:TextBox ID="txt_s197" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s198" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s199" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                </tr>
                <tr>
                    <td class="col-md-2">Office/Clerical Personnel </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">#</span>
                            <asp:TextBox ID="txt_s200" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s201" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s202" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                </tr>
                <tr>
                    <td class="col-md-2">Purchasing/Procurement Manager</td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">#</span>
                            <asp:TextBox ID="txt_s203" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s204" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s205" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                </tr>
                <tr>
                    <td class="col-md-2">Purchasing Agent/Buyer</td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">#</span>
                            <asp:TextBox ID="txt_s206" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s207" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s208" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                </tr>
                <tr>
                    <td class="col-md-2">Training Coordinator</td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">#</span>
                            <asp:TextBox ID="txt_s209" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s210" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s211" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                </tr>
                <tr class="td-bg-secondary">
                    <td colspan="6">Sales & Marketing</td>
                </tr>
                <tr>
                    <td colspan="6" class="text-center small alert-warning"><em>Note:  Information for Outside/Inside Sales Representatives will be requested later in the survey.</em></td>
                </tr>
                <tr>
                    <td class="col-md-2">Director of Sales</td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">#</span>
                            <asp:TextBox ID="txt_s212" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s213" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s214" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                </tr>
                <tr>
                    <td class="col-md-2">National Accounts Manager</td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">#</span>
                            <asp:TextBox ID="txt_s547" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s548" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s549" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                </tr>
                <tr>
                    <td class="col-md-2">Sales Manager</td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">#</span>
                            <asp:TextBox ID="txt_s215" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s216" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s217" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                </tr>
                <tr>
                    <td class="col-md-2">Showroom Manager</td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">#</span>
                            <asp:TextBox ID="txt_s550" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s551" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s552" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                </tr>
                <tr>
                    <td class="col-md-2">Sales Assistant</td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">#</span>
                            <asp:TextBox ID="txt_s218" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s219" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s220" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                </tr>
                <tr>
                    <td class="col-md-2">Counter Salesperson</td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">#</span>
                            <asp:TextBox ID="txt_s221" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s222" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s223" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                </tr>
                <tr>
                    <td class="col-md-2">Customer Service Representative</td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">#</span>
                            <asp:TextBox ID="txt_s224" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s225" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s226" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                </tr>
                <tr>
                    <td class="col-md-2">Advertising/Marketing Manager</td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">#</span>
                            <asp:TextBox ID="txt_s227" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s228" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s229" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                </tr>
                <tr class="td-bg-secondary">
                    <td colspan="6">Operations/Warehouse</td>
                </tr>
                <tr>
                    <td class="col-md-2">Operations/Warehouse Manager</td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">#</span>
                            <asp:TextBox ID="txt_s230" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s231" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s232" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                </tr>
                <tr>
                    <td class="col-md-2">Inventory Manager</td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">#</span>
                            <asp:TextBox ID="txt_s233" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s234" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s235" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                </tr>
                <tr>
                    <td class="col-md-2">Warehouse Supervisor&mdash;1<sup>st</sup> Shift</td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">#</span>
                            <asp:TextBox ID="txt_s236" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s237" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s238" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                </tr>
                <tr>
                    <td class="col-md-2">Warehouse Supervisor&mdash;2<sup>nd</sup> Shift</td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">#</span>
                            <asp:TextBox ID="txt_s239" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s240" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s241" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                </tr>
                <tr>
                    <td class="col-md-2">Warehouse Supervisor&mdash;3<sup>rd</sup> Shift</td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">#</span>
                            <asp:TextBox ID="txt_s242" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s243" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s244" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                </tr>
                <tr>
                    <td class="col-md-2">Warehouse Employee</td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">#</span>
                            <asp:TextBox ID="txt_s245" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s246" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s247" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                </tr>
                <tr>
                    <td class="col-md-2">Driver/Delivery Personnel</td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">#</span>
                            <asp:TextBox ID="txt_s248" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s249" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s250" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                </tr>
                <tr>
                    <td class="col-md-2">Freight Manager</td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">#</span>
                            <asp:TextBox ID="txt_s251" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s252" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s253" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                </tr>
                <tr>
                    <td class="col-md-2">Traffic Manager</td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">#</span>
                            <asp:TextBox ID="txt_s254" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s255" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s256" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                </tr>
                <tr>
                    <td class="col-md-2">Service/Fleet Manager</td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">#</span>
                            <asp:TextBox ID="txt_s257" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s258" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s259" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                </tr>
                <tr>
                    <td class="col-md-2">Quality Assurance Manager</td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">#</span>
                            <asp:TextBox ID="txt_s260" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s261" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s262" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                </tr>
                <tr>
                    <td class="col-md-2">Safety Manager</td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">#</span>
                            <asp:TextBox ID="txt_s263" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s264" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s265" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                </tr>
                <tr class="td-bg-secondary">
                    <td colspan="6">Information Technology</td>
                </tr>
                <tr>
                    <td class="col-md-2">IT/MIS Manager</td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">#</span>
                            <asp:TextBox ID="txt_s266" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s267" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s268" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                </tr>
                <tr>
                    <td class="col-md-2">IT Clerk</td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">#</span>
                            <asp:TextBox ID="txt_s269" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s270" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s271" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                </tr>
                <tr>
                    <td class="col-md-2">Database Administrator (DBA)</td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">#</span>
                            <asp:TextBox ID="txt_s272" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s273" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s274" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                </tr>
                <tr>
                    <td class="col-md-2">Network/Systems Administrator</td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">#</span>
                            <asp:TextBox ID="txt_s275" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s276" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s277" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                </tr>
                <tr>
                    <td class="col-md-2">Data Analyst</td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">#</span>
                            <asp:TextBox ID="txt_s553" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s554" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s555" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                </tr>
                
                <tr class="td-bg-secondary" runat="server" visible="false" id="tr_otherpositions">
                    <td colspan="6">Other Positions</td>
                </tr>
                <tr runat="server" visible="false" id="Q900">
                    <td class="col-md-2">Parts Manager</td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">#</span>
                            <asp:TextBox ID="txt_s900" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s901" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s902" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                </tr>
                <tr runat="server" visible="false" id="Q903">
                    <td class="col-md-2">Project Manager</td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">#</span>
                            <asp:TextBox ID="txt_s903" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s904" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s905" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                </tr>
                <tr runat="server" visible="false" id="Q906">
                    <td class="col-md-2">Rental Manager</td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">#</span>
                            <asp:TextBox ID="txt_s906" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s907" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s908" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                </tr>
                <tr runat="server" visible="false" id="Q909">
                    <td class="col-md-2">Service Manager</td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">#</span>
                            <asp:TextBox ID="txt_s909" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s910" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s911" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                </tr>
                <tr runat="server" visible="false" id="Q912">
                    <td class="col-md-2">Used Equipment Manager</td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">#</span>
                            <asp:TextBox ID="txt_s912" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s913" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s914" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                </tr>
                <tr runat="server" visible="false" id="Q720">
                    <td class="col-md-2">Field Mechanic</td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">#</span>
                            <asp:TextBox ID="txt_s720" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s721" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s722" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                </tr>
                <tr runat="server" visible="false" id="Q723">
                    <td class="col-md-2">Product Support Manager</td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">#</span>
                            <asp:TextBox ID="txt_s723" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s724" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s725" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                </tr>
                <tr runat="server" visible="false" id="Q726">
                    <td class="col-md-2">Product Support Salesperson</td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">#</span>
                            <asp:TextBox ID="txt_s726" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s727" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s728" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                </tr>
                <tr runat="server" visible="false" id="Q729">
                    <td class="col-md-2">Rental Salesperson</td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">#</span>
                            <asp:TextBox ID="txt_s729" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s730" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s731" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                </tr>
                <tr runat="server" visible="false" id="Q732">
                    <td class="col-md-2">Starting Shop Mechanic</td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">#</span>
                            <asp:TextBox ID="txt_s732" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s733" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s734" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                </tr>
                <tr runat="server" visible="false" id="Q735">
                    <td class="col-md-2">Territory Manager</td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">#</span>
                            <asp:TextBox ID="txt_s735" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s736" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s737" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                </tr>
                <tr runat="server" visible="false" id="Q738">
                    <td class="col-md-2">Top Shop Mechanic</td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">#</span>
                            <asp:TextBox ID="txt_s738" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s739" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s740" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                </tr>
                <tr runat="server" visible="false" id="Q741">
                    <td class="col-md-2">Used Equipment Salesperson</td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">#</span>
                            <asp:TextBox ID="txt_s741" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s742" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s743" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                </tr>
                <tr runat="server" visible="false" id="Q744">
                    <td class="col-md-2">Dispatcher</td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">#</span>
                            <asp:TextBox ID="txt_s744" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s745" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s746" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                </tr>
                <tr runat="server" visible="false" id="Q747">
                    <td class="col-md-2">Executive Assistant</td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">#</span>
                            <asp:TextBox ID="txt_s747" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s748" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s749" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                </tr>
                <tr runat="server" visible="false" id="Q750">
                    <td class="col-md-2">Inventory Specialist</td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">#</span>
                            <asp:TextBox ID="txt_s750" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s751" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s752" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                </tr>
                <tr runat="server" visible="false" id="Q753">
                    <td class="col-md-2">Management Trainee</td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">#</span>
                            <asp:TextBox ID="txt_s753" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s754" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s755" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                </tr>
                <tr runat="server" visible="false" id="Q756">
                    <td class="col-md-2">Quotations Coordinator</td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">#</span>
                            <asp:TextBox ID="txt_s756" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s757" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s758" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                </tr>
                <tr runat="server" visible="false" id="Q759">
                    <td class="col-md-2">Shipping/Receiving Supervisor</td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">#</span>
                            <asp:TextBox ID="txt_s759" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s760" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s761" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                </tr>
                <tr runat="server" visible="false" id="Q762">
                    <td class="col-md-2">Showroom Manager</td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">#</span>
                            <asp:TextBox ID="txt_s762" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s763" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s764" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                </tr>
                <tr runat="server" visible="false" id="Q765">
                    <td class="col-md-2">Showroom Salesperson</td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">#</span>
                            <asp:TextBox ID="txt_s765" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s766" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s767" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                </tr>
                <tr runat="server" visible="false" id="Q768">
                    <td class="col-md-2">Applications – Electronic Engineer</td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">#</span>
                            <asp:TextBox ID="txt_s768" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s769" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s770" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                </tr>
                <tr runat="server" visible="false" id="Q771">
                    <td class="col-md-2">Applications - Hydraulic Engineer  </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">#</span>
                            <asp:TextBox ID="txt_s771" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s772" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s773" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                </tr>
                <tr runat="server" visible="false" id="Q774">
                    <td class="col-md-2">Business Development Manager</td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">#</span>
                            <asp:TextBox ID="txt_s774" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s775" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s776" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                </tr>
                <tr runat="server" visible="false" id="Q777">
                    <td class="col-md-2">Customer Service Manager</td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">#</span>
                            <asp:TextBox ID="txt_s777" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s778" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s779" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                </tr>
                <tr runat="server" visible="false" id="Q780">
                    <td class="col-md-2">Inside Sales Manager </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">#</span>
                            <asp:TextBox ID="txt_s780" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s781" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s782" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                </tr>
                <tr runat="server" visible="false" id="Q783">
                    <td class="col-md-2">Power Unit Assembler</td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">#</span>
                            <asp:TextBox ID="txt_s783" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s784" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s785" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                </tr>
                <tr runat="server" visible="false" id="Q786">
                    <td class="col-md-2">Power Unit Designer </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">#</span>
                            <asp:TextBox ID="txt_s786" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s787" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s788" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                </tr>
                <tr runat="server" visible="false" id="Q789">
                    <td class="col-md-2">Power Unit Technician </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">#</span>
                            <asp:TextBox ID="txt_s789" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s790" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s791" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                </tr>
                <tr runat="server" visible="false" id="Q792">
                    <td class="col-md-2">Technical Product Support</td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">#</span>
                            <asp:TextBox ID="txt_s792" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s793" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s794" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                </tr>
                <tr runat="server" visible="false" id="Q795">
                    <td class="col-md-2">Warehouse Technician</td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">#</span>
                            <asp:TextBox ID="txt_s795" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s796" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s797" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                </tr>
                <tr runat="server" visible="false" id="Q798">
                    <td class="col-md-2">Market Business Manager/Market Manager</td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">#</span>
                            <asp:TextBox ID="txt_s798" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s799" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s800" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                </tr>
                <tr runat="server" visible="false" id="Q801">
                    <td class="col-md-2">Field Service Rep./Product Specialist</td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">#</span>
                            <asp:TextBox ID="txt_s801" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s802" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s803" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                </tr>
                <tr runat="server" visible="false" id="Q804">
                    <td class="col-md-2">Regional Manager</td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">#</span>
                            <asp:TextBox ID="txt_s804" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s805" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s806" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                </tr>
                <tr runat="server" visible="false" id="Q807">
                    <td class="col-md-2">Director/VP of Sales</td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">#</span>
                            <asp:TextBox ID="txt_s807" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s808" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s809" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                </tr>
                <tr runat="server" visible="false" id="Q810">
                    <td class="col-md-2">Director/VP of Marketing</td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">#</span>
                            <asp:TextBox ID="txt_s810" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s811" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s812" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                </tr>
                <tr runat="server" visible="false" id="Q813">
                    <td class="col-md-2">Marketing Specialist</td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">#</span>
                            <asp:TextBox ID="txt_s813" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s814" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s815" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                </tr>
                <tr runat="server" visible="false" id="Q816">
                    <td class="col-md-2">Aftermarket Sales Manager</td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">#</span>
                            <asp:TextBox ID="txt_s816" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s817" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s818" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                </tr>
                <tr runat="server" visible="false" id="Q819">
                    <td class="col-md-2">Application Engineer</td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">#</span>
                            <asp:TextBox ID="txt_s819" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s820" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s821" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                </tr>
                <tr runat="server" visible="false" id="Q822">
                    <td class="col-md-2">CAD Design Technician</td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">#</span>
                            <asp:TextBox ID="txt_s822" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s823" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s824" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                </tr>
                <tr runat="server" visible="false" id="Q825">
                    <td class="col-md-2">Project Coordinator</td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">#</span>
                            <asp:TextBox ID="txt_s825" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s826" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s827" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                </tr>
                <tr runat="server" visible="false" id="Q828">
                    <td class="col-md-2">Site Manager</td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">#</span>
                            <asp:TextBox ID="txt_s828" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s829" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s830" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                </tr>
                <tr runat="server" visible="false" id="Q831">
                    <td class="col-md-2">Starting Fork Lift Mechanic</td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">#</span>
                            <asp:TextBox ID="txt_s831" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s832" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s833" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                </tr>
                <tr runat="server" visible="false" id="Q834">
                    <td class="col-md-2">Top Fork Lift Mechanic</td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">#</span>
                            <asp:TextBox ID="txt_s834" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s835" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s836" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                </tr>
                
                <tr runat="server" visible="false" id="Q837">
                    <td class="col-md-2">Chief Human Relations Officer (CHRO)</td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">#</span>
                            <asp:TextBox ID="txt_s837" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s838" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s839" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                </tr>
                <tr runat="server" visible="false" id="Q840">
                    <td class="col-md-2">Chief Legal Officer (CLO)/General Counsel</td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">#</span>
                            <asp:TextBox ID="txt_s840" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s841" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s842" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                </tr>
                <tr runat="server" visible="false" id="Q843">
                    <td class="col-md-2">Data Analyst</td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">#</span>
                            <asp:TextBox ID="txt_s843" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s844" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s845" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                </tr>
                <tr runat="server" visible="false" id="Q846">
                    <td class="col-md-2">National Accounts Manager</td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">#</span>
                            <asp:TextBox ID="txt_s846" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s847" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s848" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                </tr>
                
                <tr runat="server" visible="false" id="Q849">
                    <td class="col-md-2">Regional Sales Vice President</td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">#</span>
                            <asp:TextBox ID="txt_s849" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s850" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s851" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                </tr>
                <!--
                <tr runat="server" visible="false" id="Q852">
                    <td class="col-md-2">Top Warehouse Executive</td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">#</span>
                            <asp:TextBox ID="txt_s852" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s853" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s854" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                </tr>
                -->
                <tr runat="server" visible="false" id="Q855">
                    <td class="col-md-2">Social Media Coordinator</td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">#</span>
                            <asp:TextBox ID="txt_s855" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s856" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s857" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                </tr>
                <tr runat="server" visible="false" id="Q858">
                    <td class="col-md-2">Specification Sales Representative</td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">#</span>
                            <asp:TextBox ID="txt_s858" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s859" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s860" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                </tr>
                <tr runat="server" visible="false" id="Q861">
                    <td class="col-md-2">Electronics Technician/Specialist</td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">#</span>
                            <asp:TextBox ID="txt_s861" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s862" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s863" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                </tr>
                <tr runat="server" visible="false" id="Q864">
                    <td class="col-md-2">Installation/Service Manager</td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">#</span>
                            <asp:TextBox ID="txt_s864" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s865" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s866" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                </tr>
                <tr runat="server" visible="false" id="Q965">
                    <td class="col-md-2">Installer/Service Helper</td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">#</span>
                            <asp:TextBox ID="txt_s965" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s966" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s967" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                </tr>
                <tr runat="server" visible="false" id="Q867">
                    <td class="col-md-2">Laborer/Heavy Duty Operator</td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">#</span>
                            <asp:TextBox ID="txt_s867" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s868" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s869" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                </tr>
                <tr runat="server" visible="false" id="Q870">
                    <td class="col-md-2">Job Superintendent/Foreman</td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">#</span>
                            <asp:TextBox ID="txt_s870" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s871" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s872" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                </tr>
                <tr runat="server" visible="false" id="Q873">
                    <td class="col-md-2">Mechanic/Service Tech</td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">#</span>
                            <asp:TextBox ID="txt_s873" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s874" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s875" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                </tr>
                <tr runat="server" visible="false" id="Q876">
                    <td class="col-md-2">Cut Flower Buyer</td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">#</span>
                            <asp:TextBox ID="txt_s876" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s877" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s878" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                </tr>
                <tr runat="server" visible="false" id="Q879">
                    <td class="col-md-2">Cut Flower Operations Manager</td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">#</span>
                            <asp:TextBox ID="txt_s879" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s880" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s881" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                </tr>
                <tr runat="server" visible="false" id="Q882">
                    <td class="col-md-2">Dispatch/Receiving Manager</td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">#</span>
                            <asp:TextBox ID="txt_s882" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s883" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s884" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                </tr>
                <tr runat="server" visible="false" id="Q885">
                    <td class="col-md-2">Hard Goods Buyer</td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">#</span>
                            <asp:TextBox ID="txt_s885" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s886" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s887" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                </tr>
                <tr runat="server" visible="false" id="Q888">
                    <td class="col-md-2">Supply Operations Manager</td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">#</span>
                            <asp:TextBox ID="txt_s888" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s889" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                    <td class="col-md-2">
                        <span class="input-group">
                            <span class="input-group-addon">$</span>
                            <asp:TextBox ID="txt_s890" runat="server" CssClass="form-control text-right"></asp:TextBox>
                        </span>
                    </td>
                </tr>

                <tr>
                    <td colspan="4"><p class="small">* Additional cash compensation includes commission/incentive pay and bonuses.</p></td>
                </tr>

            </tbody>
        </table>
        <p>&nbsp;</p>


        <table class="table-condensed centered">
            <tr>
                <td class="text-center" colspan="3">
                    <asp:CheckBox ID="cb_section4Complete" runat="server" Text="This section is complete." />
                </td>
            </tr>
            <tr>
                <td class="text-center">
                    <asp:Button ID="btn_previous" runat="server" CssClass="btn btn-primary" Text="Previous Section" Width="141px" />
                </td>
                <td class="text-center">
                    <asp:Button ID="btn_save" runat="server" CssClass="btn btn-primary" Text="Save" Width="141px" />
                </td>
                <td class="text-center">
                    <asp:Button ID="btn_next" runat="server" CssClass="btn btn-primary" Text="Next Section" Width="141px" />
                </td>
            </tr>
            <tr>
                <td class="text-center">&nbsp;</td>
                <td class="text-center">
                    <asp:LinkButton ID="lb_toc_bot" runat="server" CssClass="text-bold">Home</asp:LinkButton>
                </td>
                <td class="text-center">&nbsp;</td>
            </tr>
        </table>
    </div>
</asp:Content>

