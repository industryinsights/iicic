﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site.master" AutoEventWireup="false" CodeFile="Error.aspx.vb" Inherits="ErrorPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="bg-content">
        <h2 class="text-uppercase text-danger">Error</h2>
        <hr />
        <p>
            An unexpected error seems to have occurred.  Please try closing your browser and logging back into the site.
        </p>

        <p>We have been notified and are looking into the issue. </p>

        <p>Feel free to contact us if the problem persists.</p>


    </div>
</asp:Content>

