﻿<%@ WebHandler Language="VB" Class="KeepSessionAlive" %>

Imports System
Imports System.Web

Public Class KeepSessionAlive
    Implements IHttpHandler, IRequiresSessionState

    Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        context.Session("KeepSessionAlive") = DateTime.Now
    End Sub

    ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

End Class