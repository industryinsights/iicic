﻿<%@ Application Language="VB" %>
<%@ Import Namespace="System.Configuration" %>
<%@ Import Namespace="System.Net.Mail" %>
<%@ Import Namespace="WebSupergoo.ABCpdf8" %>

<script runat="server">

    Sub Application_Start(ByVal sender As Object, ByVal e As EventArgs)
    End Sub

    Sub Application_End(ByVal sender As Object, ByVal e As EventArgs)
        ' Code that runs on application shutdown
    End Sub

    Sub Application_Error(ByVal sender As Object, ByVal e As EventArgs)

        Try
            Dim client As New SmtpClient()
            Dim message As New MailMessage
            message.From = New MailAddress(ConfigurationManager.AppSettings("ErrorEmailFrom"))
            message.IsBodyHtml = True
            message.To.Add(ConfigurationManager.AppSettings("ErrorEmailTo"))
            message.Subject = "IICIC Portal Error"
            Dim username As String = Context.User.Identity.Name
            Dim project As String = "IICIC"
            Dim errorStackTrace As String = Server.GetLastError().ToString()
            message.Body = String.Format("<html><head></head><body><table border=""1""><tr><th align=""left"">Project:</th><td align=""left"">{0}</td></tr><tr><th align=""left"">Username:</th><td align=""left"">{1}</td></tr><tr><th align=""left"">Error Details:</th><td align=""left"">{2}</td></tr></table></body></html>", project, username, errorStackTrace)
            client.Send(message)
        Catch ex As Exception


        End Try

        Response.Redirect("~/Error.html")

    End Sub

    Sub Session_Start(ByVal sender As Object, ByVal e As EventArgs)
        ' Code that runs when a new session is started
    End Sub

    Sub Session_End(ByVal sender As Object, ByVal e As EventArgs)
        ' Code that runs when a session ends. 
        ' Note: The Session_End event is raised only when the sessionstate mode
        ' is set to InProc in the Web.config file. If session mode is set to StateServer 
        ' or SQLServer, the event is not raised.
    End Sub

    Protected Sub Application_BeginRequest(ByVal sender As Object, ByVal e As EventArgs)
        If Not System.Web.HttpContext.Current.Request.IsSecureConnection Then
            System.Web.HttpContext.Current.Response.Redirect(System.Web.HttpContext.Current.Request.Url.AbsoluteUri.Replace("http://", "https://"))
        End If
    End Sub


</script>