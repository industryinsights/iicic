﻿$(document).ready(function () {
    jQuery.fn.exists = function () { return this.length > 0; }
    //Diable Enter Key
    $(window).keydown(function (event) {
        if (event.keyCode == 13) {
            event.preventDefault();
            return false;
        }
    });

    $('#ContentPlaceHolder1_txt_s390').autoNumeric("init", { aSep: ',', mDec: '0' });
    $('#ContentPlaceHolder1_txt_s391').autoNumeric("init", { aSep: ',', mDec: '2' });
    $('#ContentPlaceHolder1_txt_s392').autoNumeric("init", { aSep: ',', mDec: '1' });
    $('#ContentPlaceHolder1_txt_s393').autoNumeric("init", { aSep: ',', mDec: '1' });
    $('#ContentPlaceHolder1_txt_s394').autoNumeric("init", { aSep: ',', mDec: '0' });
    $('#ContentPlaceHolder1_txt_s395').autoNumeric("init", { aSep: ',', mDec: '0' });
    $('#ContentPlaceHolder1_txt_s402').autoNumeric("init", { aSep: ',', mDec: '1' });
    $('#ContentPlaceHolder1_txt_s502').autoNumeric("init", { aSep: ',', mDec: '1' });
    $('#ContentPlaceHolder1_txt_s403').autoNumeric("init", { aSep: ',', mDec: '0' });
    $('#ContentPlaceHolder1_txt_s503').autoNumeric("init", { aSep: ',', mDec: '0' });
    $('#ContentPlaceHolder1_txt_s404').autoNumeric("init", { aSep: ',', mDec: '0' });
    $('#ContentPlaceHolder1_txt_s504').autoNumeric("init", { aSep: ',', mDec: '0' });
    $('#ContentPlaceHolder1_txt_s405').autoNumeric("init", { aSep: ',', mDec: '0' });
    $('#ContentPlaceHolder1_txt_s505').autoNumeric("init", { aSep: ',', mDec: '0' });
    $('#ContentPlaceHolder1_txt_s406').autoNumeric("init", { aSep: ',', mDec: '1' });
    $('#ContentPlaceHolder1_txt_s506').autoNumeric("init", { aSep: ',', mDec: '1' });
    $('#ContentPlaceHolder1_txt_s407').autoNumeric("init", { aSep: ',', mDec: '0' });
    $('#ContentPlaceHolder1_txt_s507').autoNumeric("init", { aSep: ',', mDec: '0' });
    $('#ContentPlaceHolder1_txt_s408').autoNumeric("init", { aSep: ',', mDec: '0' });
    $('#ContentPlaceHolder1_txt_s508').autoNumeric("init", { aSep: ',', mDec: '0' });
    $('#ContentPlaceHolder1_txt_s409').autoNumeric("init", { aSep: ',', mDec: '0' });
    $('#ContentPlaceHolder1_txt_s509').autoNumeric("init", { aSep: ',', mDec: '0' });
    $('#ContentPlaceHolder1_txt_s410').autoNumeric("init", { aSep: ',', mDec: '1' });
    $('#ContentPlaceHolder1_txt_s510').autoNumeric("init", { aSep: ',', mDec: '1' });
    $('#ContentPlaceHolder1_txt_s411').autoNumeric("init", { aSep: ',', mDec: '0' });
    $('#ContentPlaceHolder1_txt_s511').autoNumeric("init", { aSep: ',', mDec: '0' });
    $('#ContentPlaceHolder1_txt_s412').autoNumeric("init", { aSep: ',', mDec: '0' });
    $('#ContentPlaceHolder1_txt_s512').autoNumeric("init", { aSep: ',', mDec: '0' });
    $('#ContentPlaceHolder1_txt_s413').autoNumeric("init", { aSep: ',', mDec: '0' });
    $('#ContentPlaceHolder1_txt_s513').autoNumeric("init", { aSep: ',', mDec: '0' });
    $('#ContentPlaceHolder1_txt_s691').autoNumeric("init", { aSep: ',', mDec: '0' });
    $('#ContentPlaceHolder1_txt_s692').autoNumeric("init", { aSep: ',', mDec: '0' });
    $('#ContentPlaceHolder1_txt_s693').autoNumeric("init", { aSep: ',', mDec: '0' });
    $('#ContentPlaceHolder1_txt_s694').autoNumeric("init", { aSep: ',', mDec: '0' });
    $('#ContentPlaceHolder1_txt_s695').autoNumeric("init", { aSep: ',', mDec: '0' });
    $('#ContentPlaceHolder1_txt_s696').autoNumeric("init", { aSep: ',', mDec: '0' });





    //Show/Hide onchange

    RunScripts();
});

function RunScripts() {
    /**/

    //Show/Hide onload
}

function AddCommas(nStr) {
    if (nStr != null) {
        nStr += '';
        var x = nStr.split('.');
        var x1 = x[0];
        var x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + ',' + '$2');
        }
        return x1 + x2;
    }
}