﻿$(document).ready(function () {
    jQuery.fn.exists = function () { return this.length > 0; }
    //Diable Enter Key
    $(window).keydown(function (event) {
        if (event.keyCode == 13) {
            event.preventDefault();
            return false;
        }
    });

    $('#ContentPlaceHolder1_txt_s36').autoNumeric("init", { aSep: ',', mDec: '2' });
    $('#ContentPlaceHolder1_txt_s39').autoNumeric("init", { aSep: ',', mDec: '2' });
    $('#ContentPlaceHolder1_txt_s37').autoNumeric("init", { aSep: ',', mDec: '2' });
    $('#ContentPlaceHolder1_txt_s40').autoNumeric("init", { aSep: ',', mDec: '2' });
    $('#ContentPlaceHolder1_txt_s38').autoNumeric("init", { aSep: ',', mDec: '2' });
    $('#ContentPlaceHolder1_txt_s41').autoNumeric("init", { aSep: ',', mDec: '2' });
    $('#ContentPlaceHolder1_txt_s42').autoNumeric("init", { aSep: ',', mDec: '0' });
    $('#ContentPlaceHolder1_txt_s43').autoNumeric("init", { aSep: ',', mDec: '0' });
    $('#ContentPlaceHolder1_txt_s44').autoNumeric("init", { aSep: ',', mDec: '0' });
    $('#ContentPlaceHolder1_txt_s53').autoNumeric("init", { aSep: ',', mDec: '2' });
    $('#ContentPlaceHolder1_txt_s55').autoNumeric("init", { aSep: ',', mDec: '2' });
    $('#ContentPlaceHolder1_txt_s59').autoNumeric("init", { aSep: ',', mDec: '2' });
    $('#ContentPlaceHolder1_txt_s63').autoNumeric("init", { aSep: ',', mDec: '2' });
    $('#ContentPlaceHolder1_txt_s60').autoNumeric("init", { aSep: ',', mDec: '2' });
    $('#ContentPlaceHolder1_txt_s64').autoNumeric("init", { aSep: ',', mDec: '2' });
    $('#ContentPlaceHolder1_txt_s61').autoNumeric("init", { aSep: ',', mDec: '2' });
    $('#ContentPlaceHolder1_txt_s65').autoNumeric("init", { aSep: ',', mDec: '2' });
    $('#ContentPlaceHolder1_txt_s62').autoNumeric("init", { aSep: ',', mDec: '2' });
    $('#ContentPlaceHolder1_txt_s66').autoNumeric("init", { aSep: ',', mDec: '2' });





    //Show/Hide onchange

    RunScripts();
});

function RunScripts() {
    /**/

    //Show/Hide onload
}

function AddCommas(nStr) {
    if (nStr != null) {
        nStr += '';
        var x = nStr.split('.');
        var x1 = x[0];
        var x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + ',' + '$2');
        }
        return x1 + x2;
    }
}