﻿$(document).ready(function () {
    jQuery.fn.exists = function () { return this.length > 0; }
    //Diable Enter Key
    $(window).keydown(function (event) {
        if (event.keyCode == 13) {
            event.preventDefault();
            return false;
        }
    });

    $('#ContentPlaceHolder1_txt_s67').autoNumeric("init", { aSep: ',', mDec: '0' });
    $('#ContentPlaceHolder1_txt_s90').autoNumeric("init", { aSep: ',', mDec: '0' });
    $('#ContentPlaceHolder1_txt_s113').autoNumeric("init", { aSep: ',', mDec: '0' });
    $('#ContentPlaceHolder1_txt_s136').autoNumeric("init", { aSep: ',', mDec: '0' });
    $('#ContentPlaceHolder1_txt_s159').autoNumeric("init", { aSep: ',', mDec: '0' });
    $('#ContentPlaceHolder1_txt_s68').autoNumeric("init", { aSep: ',', mDec: '0' });
    $('#ContentPlaceHolder1_txt_s91').autoNumeric("init", { aSep: ',', mDec: '0' });
    $('#ContentPlaceHolder1_txt_s114').autoNumeric("init", { aSep: ',', mDec: '0' });
    $('#ContentPlaceHolder1_txt_s137').autoNumeric("init", { aSep: ',', mDec: '0' });
    $('#ContentPlaceHolder1_txt_s160').autoNumeric("init", { aSep: ',', mDec: '0' });
    $('#ContentPlaceHolder1_txt_s70').autoNumeric("init", { aSep: ',', mDec: '2' });
    $('#ContentPlaceHolder1_txt_s93').autoNumeric("init", { aSep: ',', mDec: '2' });
    $('#ContentPlaceHolder1_txt_s116').autoNumeric("init", { aSep: ',', mDec: '2' });
    $('#ContentPlaceHolder1_txt_s139').autoNumeric("init", { aSep: ',', mDec: '2' });
    $('#ContentPlaceHolder1_txt_s162').autoNumeric("init", { aSep: ',', mDec: '2' });
    $('#ContentPlaceHolder1_txt_s71').autoNumeric("init", { aSep: ',', mDec: '0' });
    $('#ContentPlaceHolder1_txt_s94').autoNumeric("init", { aSep: ',', mDec: '0' });
    $('#ContentPlaceHolder1_txt_s117').autoNumeric("init", { aSep: ',', mDec: '0' });
    $('#ContentPlaceHolder1_txt_s140').autoNumeric("init", { aSep: ',', mDec: '0' });
    $('#ContentPlaceHolder1_txt_s163').autoNumeric("init", { aSep: ',', mDec: '0' });
    $('#ContentPlaceHolder1_txt_s72').autoNumeric("init", { aSep: ',', mDec: '0' });
    $('#ContentPlaceHolder1_txt_s95').autoNumeric("init", { aSep: ',', mDec: '0' });
    $('#ContentPlaceHolder1_txt_s118').autoNumeric("init", { aSep: ',', mDec: '0' });
    $('#ContentPlaceHolder1_txt_s141').autoNumeric("init", { aSep: ',', mDec: '0' });
    $('#ContentPlaceHolder1_txt_s164').autoNumeric("init", { aSep: ',', mDec: '0' });
    $('#ContentPlaceHolder1_txt_s73').autoNumeric("init", { aSep: ',', mDec: '0' });
    $('#ContentPlaceHolder1_txt_s96').autoNumeric("init", { aSep: ',', mDec: '0' });
    $('#ContentPlaceHolder1_txt_s119').autoNumeric("init", { aSep: ',', mDec: '0' });
    $('#ContentPlaceHolder1_txt_s142').autoNumeric("init", { aSep: ',', mDec: '0' });
    $('#ContentPlaceHolder1_txt_s165').autoNumeric("init", { aSep: ',', mDec: '0' });
    $('#ContentPlaceHolder1_txt_s74').autoNumeric("init", { aSep: ',', mDec: '2' });
    $('#ContentPlaceHolder1_txt_s97').autoNumeric("init", { aSep: ',', mDec: '2' });
    $('#ContentPlaceHolder1_txt_s120').autoNumeric("init", { aSep: ',', mDec: '2' });
    $('#ContentPlaceHolder1_txt_s143').autoNumeric("init", { aSep: ',', mDec: '2' });
    $('#ContentPlaceHolder1_txt_s166').autoNumeric("init", { aSep: ',', mDec: '2' });
    $('#ContentPlaceHolder1_txt_s601').autoNumeric("init", { aSep: ',', mDec: '0' });
    $('#ContentPlaceHolder1_txt_s626').autoNumeric("init", { aSep: ',', mDec: '0' });
    $('#ContentPlaceHolder1_txt_s602').autoNumeric("init", { aSep: ',', mDec: '0' });
    $('#ContentPlaceHolder1_txt_s627').autoNumeric("init", { aSep: ',', mDec: '0' });
    $('#ContentPlaceHolder1_txt_s604').autoNumeric("init", { aSep: ',', mDec: '0' });
    $('#ContentPlaceHolder1_txt_s629').autoNumeric("init", { aSep: ',', mDec: '0' });
    $('#ContentPlaceHolder1_txt_s605').autoNumeric("init", { aSep: ',', mDec: '0' });
    $('#ContentPlaceHolder1_txt_s630').autoNumeric("init", { aSep: ',', mDec: '0' });
    $('#ContentPlaceHolder1_txt_s606').autoNumeric("init", { aSep: ',', mDec: '0' });
    $('#ContentPlaceHolder1_txt_s631').autoNumeric("init", { aSep: ',', mDec: '0' });
    $('#ContentPlaceHolder1_txt_s607').autoNumeric("init", { aSep: ',', mDec: '0' });
    $('#ContentPlaceHolder1_txt_s632').autoNumeric("init", { aSep: ',', mDec: '0' });
    $('#ContentPlaceHolder1_txt_s608').autoNumeric("init", { aSep: ',', mDec: '2' });
    $('#ContentPlaceHolder1_txt_s633').autoNumeric("init", { aSep: ',', mDec: '2' });

    $('#ContentPlaceHolder1_txt_s71').on('change', function () {
        SumS73();
    });
    $('#ContentPlaceHolder1_txt_s72').on('change', function () {
        SumS73();
    });
    $('#ContentPlaceHolder1_txt_s94').on('change', function () {
        SumS96();
    });
    $('#ContentPlaceHolder1_txt_s95').on('change', function () {
        SumS96();
    });
    $('#ContentPlaceHolder1_txt_s117').on('change', function () {
        SumS119();
    });
    $('#ContentPlaceHolder1_txt_s118').on('change', function () {
        SumS119();
    });
    $('#ContentPlaceHolder1_txt_s140').on('change', function () {
        SumS142();
    });
    $('#ContentPlaceHolder1_txt_s141').on('change', function () {
        SumS142();
    });
    $('#ContentPlaceHolder1_txt_s163').on('change', function () {
        SumS165();
    });
    $('#ContentPlaceHolder1_txt_s164').on('change', function () {
        SumS165();
    });
    $('#ContentPlaceHolder1_txt_s605').on('change', function () {
        SumS607();
    });
    $('#ContentPlaceHolder1_txt_s606').on('change', function () {
        SumS607();
    });
    $('#ContentPlaceHolder1_txt_s630').on('change', function () {
        SumS632();
    });
    $('#ContentPlaceHolder1_txt_s631').on('change', function () {
        SumS632();
    });
    //Show/Hide onchange

    RunScripts();
});

function RunScripts() {
    SumS73();
    SumS96();
    SumS119();
    SumS142();
    SumS165();
    SumS607();
    SumS632();
}


function SumS73() {
    if ($('#ContentPlaceHolder1_txt_s73').val() != null) {
        var s71 = parseFloat($('#ContentPlaceHolder1_txt_s71').val().replace(/,/g, '')) || 0;
        var s72 = parseFloat($('#ContentPlaceHolder1_txt_s72').val().replace(/,/g, '')) || 0;

        var total = s71 + s72;
        $('#ContentPlaceHolder1_txt_s73').val(AddCommas(total));
    }
}
function SumS96() {
    if ($('#ContentPlaceHolder1_txt_s96').val() != null) {
        var s94 = parseFloat($('#ContentPlaceHolder1_txt_s94').val().replace(/,/g, '')) || 0;
        var s95 = parseFloat($('#ContentPlaceHolder1_txt_s95').val().replace(/,/g, '')) || 0;

        var total = s94 + s95;
        $('#ContentPlaceHolder1_txt_s96').val(AddCommas(total));
    }
}
function SumS119() {
    if ($('#ContentPlaceHolder1_txt_s119').val() != null) {
        var s117 = parseFloat($('#ContentPlaceHolder1_txt_s117').val().replace(/,/g, '')) || 0;
        var s118 = parseFloat($('#ContentPlaceHolder1_txt_s118').val().replace(/,/g, '')) || 0;

        var total = s117 + s118;
        $('#ContentPlaceHolder1_txt_s119').val(AddCommas(total));
    }
}
function SumS142() {
    if ($('#ContentPlaceHolder1_txt_s142').val() != null) {
        var s140 = parseFloat($('#ContentPlaceHolder1_txt_s140').val().replace(/,/g, '')) || 0;
        var s141 = parseFloat($('#ContentPlaceHolder1_txt_s141').val().replace(/,/g, '')) || 0;

        var total = s140 + s141;
        $('#ContentPlaceHolder1_txt_s142').val(AddCommas(total));
    }
}
function SumS165() {
    if ($('#ContentPlaceHolder1_txt_s165').val() != null) {
        var s163 = parseFloat($('#ContentPlaceHolder1_txt_s163').val().replace(/,/g, '')) || 0;
        var s164 = parseFloat($('#ContentPlaceHolder1_txt_s164').val().replace(/,/g, '')) || 0;

        var total = s163 + s164;
        $('#ContentPlaceHolder1_txt_s165').val(AddCommas(total));
    }
}
function SumS607() {
    if ($('#ContentPlaceHolder1_txt_s607').val() != null) {
        var s605 = parseFloat($('#ContentPlaceHolder1_txt_s605').val().replace(/,/g, '')) || 0;
        var s606 = parseFloat($('#ContentPlaceHolder1_txt_s606').val().replace(/,/g, '')) || 0;

        var total = s605 + s606;
        $('#ContentPlaceHolder1_txt_s607').val(AddCommas(total));
    }
}
function SumS632() {
    if ($('#ContentPlaceHolder1_txt_s632').val() != null) {
        var s630 = parseFloat($('#ContentPlaceHolder1_txt_s630').val().replace(/,/g, '')) || 0;
        var s631 = parseFloat($('#ContentPlaceHolder1_txt_s631').val().replace(/,/g, '')) || 0;

        var total = s630 + s631;
        $('#ContentPlaceHolder1_txt_s632').val(AddCommas(total));
    }
}
function AddCommas(nStr) {
    if (nStr != null) {
        nStr += '';
        var x = nStr.split('.');
        var x1 = x[0];
        var x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + ',' + '$2');
        }
        return x1 + x2;
    }
}