﻿$(document).ready(function () {
    jQuery.fn.exists = function () { return this.length > 0; }
    //Diable Enter Key
    $(window).keydown(function (event) {
        if (event.keyCode == 13) {
            event.preventDefault();
            return false;
        }
    });

    $('#ContentPlaceHolder1_txt_s375').autoNumeric("init", { aSep: ',', mDec: '2' });
    $('#ContentPlaceHolder1_txt_s377').autoNumeric("init", { aSep: ',', mDec: '2' });
    $('#ContentPlaceHolder1_txt_s378').autoNumeric("init", { aSep: ',', mDec: '2' });
    $('#ContentPlaceHolder1_txt_s915').autoNumeric("init", { aSep: ',', mDec: '0' });
    $('#ContentPlaceHolder1_txt_s925').autoNumeric("init", { aSep: ',', mDec: '0' });
    $('#ContentPlaceHolder1_txt_s935').autoNumeric("init", { aSep: ',', mDec: '0' });
    $('#ContentPlaceHolder1_txt_s945').autoNumeric("init", { aSep: ',', mDec: '0' });
    $('#ContentPlaceHolder1_txt_s955').autoNumeric("init", { aSep: ',', mDec: '0' });
    $('#ContentPlaceHolder1_txt_s916').autoNumeric("init", { aSep: ',', mDec: '0' });
    $('#ContentPlaceHolder1_txt_s926').autoNumeric("init", { aSep: ',', mDec: '0' });
    $('#ContentPlaceHolder1_txt_s936').autoNumeric("init", { aSep: ',', mDec: '0' });
    $('#ContentPlaceHolder1_txt_s946').autoNumeric("init", { aSep: ',', mDec: '0' });
    $('#ContentPlaceHolder1_txt_s956').autoNumeric("init", { aSep: ',', mDec: '0' });
    $('#ContentPlaceHolder1_txt_s917').autoNumeric("init", { aSep: ',', mDec: '0' });
    $('#ContentPlaceHolder1_txt_s927').autoNumeric("init", { aSep: ',', mDec: '0' });
    $('#ContentPlaceHolder1_txt_s937').autoNumeric("init", { aSep: ',', mDec: '0' });
    $('#ContentPlaceHolder1_txt_s947').autoNumeric("init", { aSep: ',', mDec: '0' });
    $('#ContentPlaceHolder1_txt_s957').autoNumeric("init", { aSep: ',', mDec: '0' });
    $('#ContentPlaceHolder1_txt_s918').autoNumeric("init", { aSep: ',', mDec: '0' });
    $('#ContentPlaceHolder1_txt_s928').autoNumeric("init", { aSep: ',', mDec: '0' });
    $('#ContentPlaceHolder1_txt_s938').autoNumeric("init", { aSep: ',', mDec: '0' });
    $('#ContentPlaceHolder1_txt_s948').autoNumeric("init", { aSep: ',', mDec: '0' });
    $('#ContentPlaceHolder1_txt_s958').autoNumeric("init", { aSep: ',', mDec: '0' });
    $('#ContentPlaceHolder1_txt_s919').autoNumeric("init", { aSep: ',', mDec: '0' });
    $('#ContentPlaceHolder1_txt_s929').autoNumeric("init", { aSep: ',', mDec: '0' });
    $('#ContentPlaceHolder1_txt_s939').autoNumeric("init", { aSep: ',', mDec: '0' });
    $('#ContentPlaceHolder1_txt_s949').autoNumeric("init", { aSep: ',', mDec: '0' });
    $('#ContentPlaceHolder1_txt_s959').autoNumeric("init", { aSep: ',', mDec: '0' });
    $('#ContentPlaceHolder1_txt_s920').autoNumeric("init", { aSep: ',', mDec: '0' });
    $('#ContentPlaceHolder1_txt_s930').autoNumeric("init", { aSep: ',', mDec: '0' });
    $('#ContentPlaceHolder1_txt_s940').autoNumeric("init", { aSep: ',', mDec: '0' });
    $('#ContentPlaceHolder1_txt_s950').autoNumeric("init", { aSep: ',', mDec: '0' });
    $('#ContentPlaceHolder1_txt_s960').autoNumeric("init", { aSep: ',', mDec: '0' });
    $('#ContentPlaceHolder1_txt_s921').autoNumeric("init", { aSep: ',', mDec: '0' });
    $('#ContentPlaceHolder1_txt_s931').autoNumeric("init", { aSep: ',', mDec: '0' });
    $('#ContentPlaceHolder1_txt_s941').autoNumeric("init", { aSep: ',', mDec: '0' });
    $('#ContentPlaceHolder1_txt_s951').autoNumeric("init", { aSep: ',', mDec: '0' });
    $('#ContentPlaceHolder1_txt_s961').autoNumeric("init", { aSep: ',', mDec: '0' });
    $('#ContentPlaceHolder1_txt_s922').autoNumeric("init", { aSep: ',', mDec: '0' });
    $('#ContentPlaceHolder1_txt_s932').autoNumeric("init", { aSep: ',', mDec: '0' });
    $('#ContentPlaceHolder1_txt_s942').autoNumeric("init", { aSep: ',', mDec: '0' });
    $('#ContentPlaceHolder1_txt_s952').autoNumeric("init", { aSep: ',', mDec: '0' });
    $('#ContentPlaceHolder1_txt_s962').autoNumeric("init", { aSep: ',', mDec: '0' });
    $('#ContentPlaceHolder1_txt_s923').autoNumeric("init", { aSep: ',', mDec: '0' });
    $('#ContentPlaceHolder1_txt_s933').autoNumeric("init", { aSep: ',', mDec: '0' });
    $('#ContentPlaceHolder1_txt_s943').autoNumeric("init", { aSep: ',', mDec: '0' });
    $('#ContentPlaceHolder1_txt_s953').autoNumeric("init", { aSep: ',', mDec: '0' });
    $('#ContentPlaceHolder1_txt_s963').autoNumeric("init", { aSep: ',', mDec: '0' });
    $('#ContentPlaceHolder1_txt_s924').autoNumeric("init", { aSep: ',', mDec: '0' });
    $('#ContentPlaceHolder1_txt_s934').autoNumeric("init", { aSep: ',', mDec: '0' });
    $('#ContentPlaceHolder1_txt_s944').autoNumeric("init", { aSep: ',', mDec: '0' });
    $('#ContentPlaceHolder1_txt_s954').autoNumeric("init", { aSep: ',', mDec: '0' });
    $('#ContentPlaceHolder1_txt_s964').autoNumeric("init", { aSep: ',', mDec: '0' });


    //Auto CalcS
    $('#ContentPlaceHolder1_txt_s916').on('change', function () { SumTotalCash_Truck(); });
    $('#ContentPlaceHolder1_txt_s917').on('change', function () { SumTotalCash_Truck(); });
    $('#ContentPlaceHolder1_txt_s918').on('change', function () { SumTotalCash_Truck(); });
    $('#ContentPlaceHolder1_txt_s919').on('change', function () { SumTotalCash_Truck(); });
    $('#ContentPlaceHolder1_txt_s920').on('change', function () { SumTotalCash_Truck(); });

    $('#ContentPlaceHolder1_txt_s926').on('change', function () { SumTotalCash_Line(); });
    $('#ContentPlaceHolder1_txt_s927').on('change', function () { SumTotalCash_Line(); });
    $('#ContentPlaceHolder1_txt_s928').on('change', function () { SumTotalCash_Line(); });
    $('#ContentPlaceHolder1_txt_s929').on('change', function () { SumTotalCash_Line(); });
    $('#ContentPlaceHolder1_txt_s930').on('change', function () { SumTotalCash_Line(); });
    
    $('#ContentPlaceHolder1_txt_s936').on('change', function () { SumTotalCash_SalesSupp(); });
    $('#ContentPlaceHolder1_txt_s937').on('change', function () { SumTotalCash_SalesSupp(); });
    $('#ContentPlaceHolder1_txt_s938').on('change', function () { SumTotalCash_SalesSupp(); });
    $('#ContentPlaceHolder1_txt_s939').on('change', function () { SumTotalCash_SalesSupp(); });
    $('#ContentPlaceHolder1_txt_s940').on('change', function () { SumTotalCash_SalesSupp(); });

    $('#ContentPlaceHolder1_txt_s946').on('change', function () { SumTotalCash_Product(); });
    $('#ContentPlaceHolder1_txt_s947').on('change', function () { SumTotalCash_Product(); });
    $('#ContentPlaceHolder1_txt_s948').on('change', function () { SumTotalCash_Product(); });
    $('#ContentPlaceHolder1_txt_s949').on('change', function () { SumTotalCash_Product(); });
    $('#ContentPlaceHolder1_txt_s950').on('change', function () { SumTotalCash_Product(); });

    $('#ContentPlaceHolder1_txt_s956').on('change', function () { SumTotalCash_InsideSales(); });
    $('#ContentPlaceHolder1_txt_s957').on('change', function () { SumTotalCash_InsideSales(); });
    $('#ContentPlaceHolder1_txt_s958').on('change', function () { SumTotalCash_InsideSales(); });
    $('#ContentPlaceHolder1_txt_s959').on('change', function () { SumTotalCash_InsideSales(); });
    $('#ContentPlaceHolder1_txt_s960').on('change', function () { SumTotalCash_InsideSales(); });


    //Show/Hide onchange

    RunScripts();
});

function RunScripts() {
    /*InnerHTML only*/

    SumTotalCash_Truck();
    SumTotalCash_Line();
    SumTotalCash_SalesSupp();
    SumTotalCash_Product();
    SumTotalCash_InsideSales();
}



/****************/



//InnerHTML
function SumTotalCash_Truck() {
    //document.getElementById("bv" + (j+32)).innerHTML =
    var s916 = parseFloat($('#ContentPlaceHolder1_txt_s916').val().replace(/,/g, '')) || 0;
    var s917 = parseFloat($('#ContentPlaceHolder1_txt_s917').val().replace(/,/g, '')) || 0;
    var s918 = parseFloat($('#ContentPlaceHolder1_txt_s918').val().replace(/,/g, '')) || 0;
    var s919 = parseFloat($('#ContentPlaceHolder1_txt_s919').val().replace(/,/g, '')) || 0;
    var s920 = parseFloat($('#ContentPlaceHolder1_txt_s920').val().replace(/,/g, '')) || 0;

    var varSUM = s916 + s917 + s918 + s919 + s920;

    //varSUM = Math.round((varSUM * 100)) / 100;	//2 dec.
    varSUM = AddCommas(varSUM);
    $("td.SumTruck").html("<strong>$" + varSUM + "</strong>");

}
function SumTotalCash_Line() {
    //document.getElementById("bv" + (j+32)).innerHTML =
    var s926 = parseFloat($('#ContentPlaceHolder1_txt_s926').val().replace(/,/g, '')) || 0;
    var s927 = parseFloat($('#ContentPlaceHolder1_txt_s927').val().replace(/,/g, '')) || 0;
    var s928 = parseFloat($('#ContentPlaceHolder1_txt_s928').val().replace(/,/g, '')) || 0;
    var s929 = parseFloat($('#ContentPlaceHolder1_txt_s929').val().replace(/,/g, '')) || 0;
    var s930 = parseFloat($('#ContentPlaceHolder1_txt_s930').val().replace(/,/g, '')) || 0;

    var varSUM = s926 + s927 + s928 + s929 + s930;

    //varSUM = Math.round((varSUM * 100)) / 100;	//2 dec.
    varSUM = AddCommas(varSUM);
    $("td.SumLine").html("<strong>$" + varSUM + "</strong>");

}
function SumTotalCash_SalesSupp() {
    //document.getElementById("bv" + (j+32)).innerHTML =
    var s936 = parseFloat($('#ContentPlaceHolder1_txt_s936').val().replace(/,/g, '')) || 0;
    var s937 = parseFloat($('#ContentPlaceHolder1_txt_s937').val().replace(/,/g, '')) || 0;
    var s938 = parseFloat($('#ContentPlaceHolder1_txt_s938').val().replace(/,/g, '')) || 0;
    var s939 = parseFloat($('#ContentPlaceHolder1_txt_s939').val().replace(/,/g, '')) || 0;
    var s940 = parseFloat($('#ContentPlaceHolder1_txt_s940').val().replace(/,/g, '')) || 0;

    var varSUM = s936 + s937 + s938 + s939 + s940;

    //varSUM = Math.round((varSUM * 100)) / 100;	//2 dec.
    varSUM = AddCommas(varSUM);
    $("td.SumSalesSupp").html("<strong>$" + varSUM + "</strong>");

}
function SumTotalCash_Product() {
    //document.getElementById("bv" + (j+32)).innerHTML =
    var s946 = parseFloat($('#ContentPlaceHolder1_txt_s946').val().replace(/,/g, '')) || 0;
    var s947 = parseFloat($('#ContentPlaceHolder1_txt_s947').val().replace(/,/g, '')) || 0;
    var s948 = parseFloat($('#ContentPlaceHolder1_txt_s948').val().replace(/,/g, '')) || 0;
    var s949 = parseFloat($('#ContentPlaceHolder1_txt_s949').val().replace(/,/g, '')) || 0;
    var s950 = parseFloat($('#ContentPlaceHolder1_txt_s950').val().replace(/,/g, '')) || 0;

    var varSUM = s946 + s947 + s948 + s949 + s950;

    //varSUM = Math.round((varSUM * 100)) / 100;	//2 dec.
    varSUM = AddCommas(varSUM);
    $("td.SumProduct").html("<strong>$" + varSUM + "</strong>");

}
function SumTotalCash_InsideSales() {
    //document.getElementById("bv" + (j+32)).innerHTML =
    var s956 = parseFloat($('#ContentPlaceHolder1_txt_s956').val().replace(/,/g, '')) || 0;
    var s957 = parseFloat($('#ContentPlaceHolder1_txt_s957').val().replace(/,/g, '')) || 0;
    var s958 = parseFloat($('#ContentPlaceHolder1_txt_s958').val().replace(/,/g, '')) || 0;
    var s959 = parseFloat($('#ContentPlaceHolder1_txt_s959').val().replace(/,/g, '')) || 0;
    var s960 = parseFloat($('#ContentPlaceHolder1_txt_s960').val().replace(/,/g, '')) || 0;

    var varSUM = s956 + s957 + s958 + s959 + s960;

    //varSUM = Math.round((varSUM * 100)) / 100;	//2 dec.
    varSUM = AddCommas(varSUM);
    $("td.SumInsideSales").html("<strong>$" + varSUM + "</strong>");

}

function AddCommas(nStr) {
    if (nStr != null) {
        nStr += '';
        var x = nStr.split('.');
        var x1 = x[0];
        var x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + ',' + '$2');
        }
        return x1 + x2;
    }
}