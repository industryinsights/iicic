﻿$(document).ready(function () {
    jQuery.fn.exists = function () { return this.length > 0; }
    //Diable Enter Key
    $(window).keydown(function (event) {
        if (event.keyCode == 13) {
            event.preventDefault();
            return false;
        }
    });

    
    $('#ContentPlaceHolder1_txt_s5').autoNumeric("init", { aSep: ',', mDec: '0' });
    $('#ContentPlaceHolder1_txt_s522').autoNumeric("init", { aSep: ',', mDec: '0' });
    
    $('#ContentPlaceHolder1_txt_s7').autoNumeric("init", { aSep: ',', mDec: '0' });
    $('#ContentPlaceHolder1_txt_s8').autoNumeric("init", { aSep: ',', mDec: '0' });
    $('#ContentPlaceHolder1_txt_s10').autoNumeric("init", { aSep: ',', mDec: '2' });


    //Show/Hide onchange
    $('#ContentPlaceHolder1_cb_s1900').change(
    function () {
        if ($(this).is(':checked')) { $("#DIV1900").show(); } else { $("#DIV1900").hide(); }
    });
    $('#ContentPlaceHolder1_cb_s1901').change(
    function () {
        if ($(this).is(':checked')) { $("#DIV1901").show(); } else { $("#DIV1901").hide();}
    });
    $('#ContentPlaceHolder1_cb_s1909').change(
    function () {
        if ($(this).is(':checked')) { $("#DIV1909").show(); } else { $("#DIV1909").hide();}
    });
    $('#ContentPlaceHolder1_cb_s1910').change(
    function () {
        if ($(this).is(':checked')) { $("#DIV1910").show(); } else { $("#DIV1910").hide();}
    });
    $('#ContentPlaceHolder1_cb_s1911').change(
    function () {
        if ($(this).is(':checked')) { $("#DIV1911").show(); } else { $("#DIV1911").hide();}
    });
    $('#ContentPlaceHolder1_cb_s1912').change(
    function () {
        //if ($(this).is(':checked')) { $("#DIV1912").show(); } else { $("#DIV1912").hide();}
        if ($('#ContentPlaceHolder1_cb_s1912').is(':checked') || $('#ContentPlaceHolder1_cb_s1916').is(':checked')) { $("#Q706").show(); } else { $("#Q706").hide(); }
    });
    $('#ContentPlaceHolder1_cb_s1916').change(
    function () {
        //if ($(this).is(':checked')) { $("#DIV1916").show(); } else { $("#DIV1916").hide();}
        if ($('#ContentPlaceHolder1_cb_s1912').is(':checked') || $('#ContentPlaceHolder1_cb_s1916').is(':checked')) { $("#Q706").show(); } else { $("#Q706").hide(); }
    });

    $('#ContentPlaceHolder1_cb_s1918').change(
    function () {
        if ($(this).is(':checked')) { $("#DIV1918").show(); } else { $("#DIV1918").hide();}
    });


    RunScripts();
});
function RunScripts() {
    /*
    AS_S67();
    */

    //Show/Hide onload
    if ($('#ContentPlaceHolder1_cb_s1900').is(':checked')) { $("#DIV1900").show(); } else { $("#DIV1900").hide(); }
    if ($('#ContentPlaceHolder1_cb_s1901').is(':checked')) { $("#DIV1901").show(); } else { $("#DIV1901").hide(); }
    if ($('#ContentPlaceHolder1_cb_s1909').is(':checked')) { $("#DIV1909").show(); } else { $("#DIV1909").hide(); }
    if ($('#ContentPlaceHolder1_cb_s1910').is(':checked')) { $("#DIV1910").show(); } else { $("#DIV1910").hide(); }
    if ($('#ContentPlaceHolder1_cb_s1911').is(':checked')) { $("#DIV1911").show(); } else { $("#DIV1911").hide(); }

    //if ($('#ContentPlaceHolder1_cb_s1912').is(':checked')) { $("#DIV1912").show(); } else { $("#DIV1912").hide(); }
    //if ($('#ContentPlaceHolder1_cb_s1916').is(':checked')) { $("#DIV1916").show(); } else { $("#DIV1916").hide(); }
    if ($('#ContentPlaceHolder1_cb_s1912').is(':checked') || $('#ContentPlaceHolder1_cb_s1916').is(':checked')) { $("#Q706").show(); } else { $("#Q706").hide(); }

    if ($('#ContentPlaceHolder1_cb_s1918').is(':checked')) { $("#DIV1918").show(); } else { $("#DIV1918").hide(); }

}

/*
function AS_S67() {
    //Total Assets
    if ($('#ContentPlaceHolder1_txt_s67').val() != null) {
        var s65 = parseFloat($('#ContentPlaceHolder1_txt_s65').val().replace(/,/g, '')) || 0;
        var s66 = parseFloat($('#ContentPlaceHolder1_txt_s66').val().replace(/,/g, '')) || 0;
        var total = s65 + s66;

        $('#ContentPlaceHolder1_txt_s67').val(AddCommas(total.toFixed(2)));
    }
}*/

function AddCommas(nStr) {
    if (nStr != null) {
        nStr += '';
        var x = nStr.split('.');
        var x1 = x[0];
        var x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + ',' + '$2');
        }
        return x1 + x2;
    }
}