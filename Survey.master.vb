﻿
Partial Class Survey
    Inherits System.Web.UI.MasterPage

    Public Event ExecuteContentSave As EventHandler
    Protected Sub Page_Init(sender As Object, e As EventArgs) Handles Me.Load
        If Session("Client") IsNot Nothing Then
            lnk_styleSheet.Href = String.Format("Content/{0}.css", Session("Client"))
            img_logo.Src = String.Format("Content/images/logo-{0}.png", Session("Client"))
        End If
    End Sub
    Protected Sub lb_toc_Click(sender As Object, e As EventArgs) Handles lb_toc.Click
        CallContentSave()
        Response.Redirect("~/Surveys/Comp/TableOfContents.aspx")
    End Sub
    Protected Sub btn_rpts_Click(sender As Object, e As EventArgs) Handles btn_rpts.Click
        CallContentSave()
        Response.Redirect("~/Reports/Comp/Home.aspx")
    End Sub
    Sub CallContentSave()
        Dim cp As New ContentPlaceHolder
        cp = CType(FindControl("ContentPlaceHolder1"), ContentPlaceHolder)
        If Not cp.Page.ToString.Contains("tableofcontents") And Not cp.Page.ToString.Contains("organizationadmin") And Not cp.Page.ToString.Contains("myaccount") Then
            CallByName(cp.Page, "SaveSurvey", vbMethod, False)
            If cp.Page.ToString.Contains("branchcompben") Then
                Session("LocationID") = SurveyHelper.ReturnDefaultLocation(Session("OrganizationId"))
            End If
        End If
    End Sub
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not IsPostBack Then
            'Dim roles As List(Of Integer) = Session("UserRoles")
            'If roles.Contains(3) Then
            '    lb_orgAdmin.Visible = True
            'Else
            '    lb_orgAdmin.Visible = False
            'End If
        End If
    End Sub
    Protected Sub lb_Benefits_Click(sender As Object, e As EventArgs) Handles lb_Benefits.Click
        CallContentSave()
        Response.Redirect("~/Surveys/Comp/Benefits.aspx")
    End Sub
    Protected Sub lb_Compensation_Click(sender As Object, e As EventArgs) Handles lb_Compensation.Click
        CallContentSave()
        Response.Redirect("~/Surveys/Comp/Compensation.aspx")
    End Sub
    Protected Sub lb_CompByJob_Click(sender As Object, e As EventArgs) Handles lb_CompByJob.Click
        CallContentSave()
        Response.Redirect("~/Surveys/Comp/CompByJob.aspx")
    End Sub
    Protected Sub lb_Profile_Click(sender As Object, e As EventArgs) Handles lb_Profile.Click
        CallContentSave()
        Response.Redirect("~/Surveys/Comp/Profile.aspx")
    End Sub
    Protected Sub lb_Recruiting_Click(sender As Object, e As EventArgs) Handles lb_Recruiting.Click
        CallContentSave()
        Response.Redirect("~/Surveys/Comp/Recruiting.aspx")
    End Sub
    Protected Sub lb_Sales_Click(sender As Object, e As EventArgs) Handles lb_Sales.Click
        CallContentSave()
        Response.Redirect("~/Surveys/Comp/Sales.aspx")
    End Sub
    Protected Sub lb_BranchCompBen_Click(sender As Object, e As EventArgs) Handles lb_BranchCompBen.Click
        CallContentSave()
        Response.Redirect("~/Surveys/Comp/BranchCompBen.aspx")
    End Sub
End Class

