﻿Imports Microsoft.VisualBasic
Imports Models

Public Class PortalHelper
    Public Shared Function HasAccessToReporting(ByVal orgId As Integer) As Boolean
        Dim result As Boolean = False
        Using db As New IICICEntities
            Dim accessYears As OrganizationReportAccess = (From ora In db.OrganizationReportAccesses Where ora.OrganizationId = orgId Order By ora.ReportingYear Descending).FirstOrDefault
            If accessYears IsNot Nothing Then
                result = True
            End If
        End Using
        Return result
    End Function
    Public Shared Function GetReportingAccessYears(ByVal orgId As Integer) As List(Of Integer)
        Dim reportingYears As New List(Of Integer)
        Using db As New IICICEntities
            Dim years = (From ora In db.OrganizationReportAccesses Where ora.OrganizationId = orgId Order By ora.ReportingYear Descending)
            For Each y As OrganizationReportAccess In years
                reportingYears.Add(y.ReportingYear)
            Next
        End Using
        Return reportingYears
    End Function

    Public Shared Function GetParticipationDDLList(ByVal userId As Integer) As List(Of ListItem)
        Dim ddlList As New List(Of ListItem)
        Using db As New IICICEntities
            Dim userProjects = (From a In db.AdminProjects Where a.UserId = userId)

            For Each u In userProjects
                Dim project As New Project
                project = (From p In db.Projects Where p.ProjectId = u.ProjectId).FirstOrDefault
                ddlList.Add(New ListItem(project.Name, project.ProjectId))
            Next

        End Using
        Return ddlList
    End Function
End Class
