﻿Imports System.Drawing
Imports HtmlAgilityPack
Imports Microsoft.VisualBasic
Imports OfficeOpenXml
Imports OfficeOpenXml.Drawing
Imports OfficeOpenXml.Style
Imports System.Web.HttpUtility
Public Class ExportHelper
    Public Shared Sub GenerateExcelWS(ByVal html As String, ByVal reportName As String, ByVal ep As ExcelPackage)
        html = html.Replace("&nbsp;", String.Empty)
        Dim htmlDoc As New HtmlDocument()
        htmlDoc.LoadHtml(html)
        Dim epMaxCells = 0
        ep.Workbook.Worksheets.Add(reportName)
        Dim worksheet As ExcelWorksheet = ep.Workbook.Worksheets(1)

        Dim docNode As HtmlNode = htmlDoc.DocumentNode
        Dim table As IEnumerable(Of HtmlNode) = docNode.Descendants("table")
        Dim rows As IEnumerable(Of HtmlNode) = table(0).Descendants("tr")
        Dim cells As IEnumerable(Of HtmlNode) = rows(2).Descendants("td")
        Dim rowCount As Integer = rows.Count()
        Dim cellCount As Integer = 0
        For Each row As HtmlNode In rows
            If row.Descendants("td").Count > cellCount Then
                cellCount = row.Descendants("td").Count()
            End If
        Next
        cellCount = cellCount

        worksheet.Cells.Style.Font.Name = "Calibri"
        worksheet.Cells(1, 1, 1, cellCount).Merge = True
        worksheet.Cells(1, 1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center
        worksheet.Cells(1, 1).Style.Font.Size = 16

        worksheet.Cells(10, 1, 10, cellCount).Merge = True

        worksheet.Cells(10, 1).Value = reportName
        worksheet.Cells(10, 1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center
        worksheet.Cells(10, 1).Style.Font.Size = 16

        worksheet.Cells(3, 1, 3, cellCount).Merge = True
        worksheet.Cells(3, 1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Right
        worksheet.Row(3).Height = 30
        If reportName = "Report Card" Then
            worksheet.Column(3).Style.HorizontalAlignment = ExcelHorizontalAlignment.Right
        End If
        Dim epRow = 14
        If reportName = "Searchable Results" Then
            epRow = 13
        Else
            epRow = 14
        End If

        worksheet.Cells(1, 1, rowCount, cellCount).Style.Fill.PatternType = ExcelFillStyle.Solid
        worksheet.Cells(1, 1, rowCount, cellCount).Style.Fill.BackgroundColor.SetColor(Color.FromName("White"))

        rows = table(0).Descendants("tr")
        For Each row As HtmlNode In rows
            Dim headerRow As Boolean = False
            If Not row.Attributes("style") Is Nothing Then
                Dim styleAttr As String = row.Attributes("style").Value
                SetStyles(worksheet.Cells(epRow, 1, epRow, cellCount), styleAttr)
            End If
            If Not row.Attributes("class") Is Nothing Then
                Dim styleAttr As String = row.Attributes("class").Value
                SetStyles(worksheet.Cells(epRow, 1, epRow, cellCount), styleAttr)
            Else
                headerRow = True
            End If
            cells = row.Descendants("td")
            Dim epCell = 1
            For Each cell As HtmlNode In cells
                If Not cell.Attributes("style") Is Nothing Then
                    Dim styleAttr As String = cell.Attributes("style").Value
                    worksheet.Cells(epRow, epCell).Style.WrapText = True
                    SetStyles(worksheet.Cells(epRow, epCell), styleAttr)
                End If
                If Not cell.Attributes("class") Is Nothing Then
                    Dim styleAttr As String = cell.Attributes("class").Value
                    SetStyles(worksheet.Cells(epRow, epCell), styleAttr)
                End If
                If Not cell.Attributes("colspan") Is Nothing Then
                    Dim span = cell.Attributes("colspan").Value
                    Dim iSpan = Int32.Parse(span)
                    If iSpan > 1 Then
                        If epCell + iSpan > cellCount Then
                            iSpan = cellCount - epCell
                        End If
                        worksheet.Cells(epRow, epCell, epRow, epCell + iSpan).Merge = True
                        If cell.InnerHtml.Contains("<br/>") Then
                            worksheet.Cells(epRow, epCell, epRow, epCell + iSpan).Value = HtmlDecode(cell.InnerHtml.Replace("<br/>", Environment.NewLine))
                            worksheet.Cells(epRow, epCell, epRow, epCell + iSpan).Style.WrapText = True
                        ElseIf cell.InnerHtml.Contains("<br>") Or cell.InnerHtml.Contains("<br/>") Then
                            worksheet.Cells(epRow, epCell, epRow, epCell + iSpan).Value = HtmlDecode(cell.InnerHtml.Replace("<br>", Environment.NewLine).Replace("<br/>", Environment.NewLine))
                            worksheet.Cells(epRow, epCell, epRow, epCell + iSpan).Style.WrapText = True
                        Else
                            If cell.InnerText.Contains("$") Then

                            End If
                            worksheet.Cells(epRow, epCell, epRow, epCell + iSpan).Value = cell.InnerText
                        End If
                        epCell = epCell + iSpan
                    Else
                        If cell.InnerHtml.Contains("<br/>") Then
                            worksheet.Cells(epRow, epCell, epRow, epCell + iSpan).Value = HtmlDecode(cell.InnerHtml.Replace("<br/>", Environment.NewLine))
                            worksheet.Cells(epRow, epCell, epRow, epCell + iSpan).Style.WrapText = True
                        ElseIf cell.InnerHtml.Contains("<br>") Or cell.InnerHtml.Contains("<br/>") Then
                            worksheet.Cells(epRow, epCell, epRow, epCell + iSpan).Value = HtmlDecode(cell.InnerHtml.Replace("<br>", Environment.NewLine).Replace("<br/>", Environment.NewLine))
                            worksheet.Cells(epRow, epCell, epRow, epCell + iSpan).Style.WrapText = True
                        Else
                            If cell.InnerText.Contains("$") Then

                            End If
                            worksheet.Cells(epRow, epCell, epRow, epCell + iSpan).Value = cell.InnerText
                        End If
                        epCell = epCell + 1
                    End If
                Else
                    If cell.InnerHtml.Contains("<br/>") Then
                        worksheet.Cells(epRow, epCell).Value = HtmlDecode(cell.InnerHtml.Replace("<br/>", Environment.NewLine))
                        worksheet.Cells(epRow, epCell).Style.WrapText = True
                    ElseIf cell.InnerHtml.Contains("<br>") Or cell.InnerHtml.Contains("<br/>") Then
                        worksheet.Cells(epRow, epCell).Value = HtmlDecode(cell.InnerHtml.Replace("<br>", Environment.NewLine).Replace("<br/>", Environment.NewLine))
                        worksheet.Cells(epRow, epCell).Style.WrapText = True
                    Else
                        Dim outlier As Boolean = False
                        Dim tempValue As String = cell.InnerText
                        Dim decimalPlaces As Integer = 0
                        If cell.InnerText.Contains("||") Then
                            tempValue = tempValue.Replace("||", "")
                            outlier = True
                        End If
                        If cell.InnerText.Contains("$") Then
                            Dim value As Double
                            tempValue = tempValue.Replace("$", "").Trim()
                            decimalPlaces = CountDecimals(tempValue)
                            If Double.TryParse(tempValue, value) Then
                                worksheet.Cells(epRow, epCell).Value = value
                                If outlier Then
                                    If decimalPlaces > 0 Then
                                        worksheet.Cells(epRow, epCell).Style.Numberformat.Format = """|| ""$#,##0.00_)"" ||"";""|| ""[Red]($#,##0.00)"" ||"""
                                    Else
                                        worksheet.Cells(epRow, epCell).Style.Numberformat.Format = """|| ""$#,##0_)"" ||"";""|| ""[Red]($#,##0)"" ||"""
                                    End If

                                Else
                                    If decimalPlaces > 0 Then
                                        worksheet.Cells(epRow, epCell).Style.Numberformat.Format = "$#,##0.00_);[Red]($#,##0.00)"
                                    Else
                                        worksheet.Cells(epRow, epCell).Style.Numberformat.Format = "$#,##0_);[Red]($#,##0)"
                                    End If

                                End If
                            Else
                                worksheet.Cells(epRow, epCell).Value = cell.InnerText
                            End If

                        ElseIf cell.InnerText.Contains("%") Then
                            Dim value As Double
                            tempValue = tempValue.Replace("%", "").Trim()
                            decimalPlaces = CountDecimals(tempValue)
                            If Double.TryParse(tempValue, value) Then
                                worksheet.Cells(epRow, epCell).Value = value / 100
                                If outlier Then
                                    If decimalPlaces > 0 Then
                                        Dim format As String = ""
                                        format = """|| ""#,###."
                                        For dCount As Integer = 1 To decimalPlaces
                                            format = format + "0"
                                        Next
                                        format = format + "%"" ||"";""|| ""-#,###."
                                        For dCount As Integer = 1 To decimalPlaces
                                            format = format + "0"
                                        Next
                                        format = format + "%"" ||"";0%"
                                        worksheet.Cells(epRow, epCell).Style.Numberformat.Format = format
                                    Else
                                        worksheet.Cells(epRow, epCell).Style.Numberformat.Format = """|| ""#,###%"" ||"";""|| ""-#,###%"" ||"";0%"
                                    End If

                                Else
                                    If decimalPlaces > 0 Then
                                        Dim format As String = ""
                                        format = "#,##0."
                                        For dCount As Integer = 1 To decimalPlaces
                                            format = format + "0"
                                        Next
                                        format = format + "%;-#,##0."
                                        For dCount As Integer = 1 To decimalPlaces
                                            format = format + "0"
                                        Next
                                        format = format + "%;0%"
                                        worksheet.Cells(epRow, epCell).Style.Numberformat.Format = format
                                    Else
                                        worksheet.Cells(epRow, epCell).Style.Numberformat.Format = "#,###%;-#,###%;0%"
                                    End If
                                End If
                            Else
                                worksheet.Cells(epRow, epCell).Value = cell.InnerText
                            End If
                        Else
                            Dim value As Double
                            tempValue = tempValue.Trim()
                            decimalPlaces = CountDecimals(tempValue)
                            If Double.TryParse(tempValue, value) Then
                                worksheet.Cells(epRow, epCell).Value = value
                                If outlier Then
                                    If decimalPlaces > 0 Then
                                        Dim format As String = ""
                                        format = """|| ""#,###."
                                        For dCount As Integer = 1 To decimalPlaces
                                            format = format + "0"
                                        Next
                                        format = format + """ ||"";""|| ""-#,###."
                                        For dCount As Integer = 1 To decimalPlaces
                                            format = format + "0"
                                        Next
                                        format = format + """ ||"""
                                        worksheet.Cells(epRow, epCell).Style.Numberformat.Format = format
                                    Else
                                        worksheet.Cells(epRow, epCell).Style.Numberformat.Format = """|| ""#,###"" ||"";""|| ""-#,###"" ||"""

                                    End If
                                Else
                                    If headerRow Then
                                        worksheet.Row(epRow).Style.VerticalAlignment = ExcelVerticalAlignment.Center
                                        worksheet.Cells(epRow, epCell).Style.Numberformat.Format = "####;"
                                    Else
                                        If decimalPlaces > 0 Then
                                            Dim format As String = ""
                                            format = "#,##0."
                                            For dCount As Integer = 1 To decimalPlaces
                                                format = format + "0"
                                            Next
                                            format = format + ";-#,##0."
                                            For dCount As Integer = 1 To decimalPlaces
                                                format = format + "0"
                                            Next
                                            worksheet.Cells(epRow, epCell).Style.Numberformat.Format = format
                                        ElseIf decimalPlaces = -1 Then
                                            worksheet.Cells(epRow, epCell).Value = cell.InnerText
                                        Else
                                            worksheet.Cells(epRow, epCell).Style.Numberformat.Format = "#,###;-#,###"
                                        End If
                                    End If

                                End If
                            Else
                                worksheet.Cells(epRow, epCell).Value = cell.InnerText
                            End If
                        End If
                    End If
                    If reportName = "Report Card" Then
                        If epCell > 3 Then
                            worksheet.Cells(epRow, epCell).Style.HorizontalAlignment = ExcelHorizontalAlignment.Left

                        End If
                    Else
                        If epCell > 1 Then
                            worksheet.Cells(epRow, epCell).Style.HorizontalAlignment = ExcelHorizontalAlignment.Right
                        End If
                    End If
                    epCell = epCell + 1
                End If
            Next
            If epCell > epMaxCells Then
                epMaxCells = epCell
            End If
            epRow = epRow + 1
        Next
        If Not worksheet.Cells(13, 1).Value Is Nothing Then
            Dim lines As String() = worksheet.Cells(13, 1).Value.ToString().Split(Environment.NewLine)
            Dim lineCount As Integer = lines.Count()
            If reportName = "Interactive Peer Report" Then
                worksheet.Cells(13, 1).Value = String.Empty
            Else
                worksheet.Cells(13, 1, 13, cellCount).Value = String.Empty
            End If

            worksheet.Cells(13, 1, 13, cellCount).Merge = False
            Dim postRow = 12

            For line As Integer = 0 To lineCount - 1
                If line = 0 Then
                    epRow = epRow + 1
                End If
                If String.IsNullOrEmpty(lines(line).Trim()) Then
                    Continue For
                End If
                worksheet.Cells(postRow, 1, postRow, cellCount).Merge = True
                If lines(line).Contains("<b>") Then
                    worksheet.Cells(postRow, 1).Style.Font.Bold = True
                End If

                If lines(line).Contains("<u>") Then
                    worksheet.Cells(postRow, 1).Style.Font.UnderLine = True
                End If

                If lines(line).Contains("<em>") Then
                    worksheet.Cells(postRow, 1).Style.Font.Italic = True
                End If
                If Not lines(line).Contains("<b>") And Not lines(line).Contains("Sample Size") Then
                    worksheet.Row(postRow).Height = worksheet.Row(postRow).Height * 2
                    worksheet.Row(postRow).Style.VerticalAlignment = ExcelVerticalAlignment.Top
                End If
                If lines(line).Contains("Sample Size") Then
                    worksheet.InsertRow(postRow, 1)
                    worksheet.Cells(postRow, 1, postRow, cellCount).Merge = True
                    postRow = postRow + 1
                End If
                worksheet.Cells(postRow, 1).Value = Regex.Replace(lines(line), "<[^>]*(>|$)", "").Trim().Replace(Environment.NewLine, "")
                worksheet.Cells(postRow, 1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center
                worksheet.Cells(postRow, 1).Style.WrapText = True

                postRow = postRow + 1
                If line < lineCount - 2 Then
                    worksheet.InsertRow(postRow, 1)
                End If
                epRow = epRow + 1
            Next line
        End If

        worksheet.Column(1).Width = 60
        worksheet.Column(1).Style.Numberformat.Format = ""
        worksheet.Column(2).Width = 24
        worksheet.Column(3).Width = 24
        worksheet.Column(4).Width = 24
        worksheet.Column(2).Style.WrapText = True
        worksheet.Column(3).Style.WrapText = True
        worksheet.Column(4).Style.WrapText = True
        worksheet.Column(5).Style.WrapText = True
        worksheet.Column(6).Style.WrapText = True

        worksheet.Cells(epRow, 1, epRow, cellCount).Merge = True
        worksheet.Row(epRow).Height = 46
        worksheet.Cells(epRow, 1).Style.VerticalAlignment = ExcelVerticalAlignment.Bottom
        worksheet.Cells(epRow, 1).Style.Font.Size = 8
        worksheet.Cells(epRow, 1).Style.WrapText = True
        epRow = epRow + 1
        worksheet.Cells(epRow, 1, epRow, cellCount).Merge = True
        worksheet.Cells(epRow, 1).Value = String.Format("{0}{1} IICIC", Chr(169), (DateTime.Now).Year.ToString())
        worksheet.Cells(epRow, 1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center
        worksheet.Cells(epRow, 1).Style.Font.Size = 8

        Dim imagePath As String = HttpContext.Current.Server.MapPath("~/Content/Images/logo.png")
        Dim clientlogo As ExcelPicture
        Const maxHeight As Double = 0
        Dim logoTemp As New Bitmap(imagePath)
        Dim logo As String = String.Empty

        clientlogo = worksheet.Drawings.AddPicture("CLogo", logoTemp)
        clientlogo.SetPosition(1, 0, 0, 0)
        If reportName = "Report Card" Then
            clientlogo.SetSize(100)
        ElseIf reportName = "Searchable Results" Then
            clientlogo.SetSize(100)
        Else
            clientlogo.SetSize(100)
        End If


        worksheet.View.ShowGridLines = False
        worksheet.PrinterSettings.Orientation = eOrientation.Landscape
        worksheet.PrinterSettings.FitToPage = True
        worksheet.PrinterSettings.FitToWidth = 1
        worksheet.PrinterSettings.FitToHeight = 0
        worksheet.PrinterSettings.TopMargin = CType(0.25, Decimal)
        worksheet.PrinterSettings.LeftMargin = CType(0.25, Decimal)
        worksheet.PrinterSettings.RightMargin = CType(0.25, Decimal)
        worksheet.PrinterSettings.BottomMargin = CType(0.75, Decimal)

        worksheet.PrinterSettings.RepeatRows = New ExcelAddress("1:3")
        worksheet.HeaderFooter.OddFooter.RightAlignedText = ExcelHeaderFooter.PageNumber & " of " & ExcelHeaderFooter.NumberOfPages
        worksheet.Cells(6, 1, epRow, 26).Style.Locked = False
    End Sub
    Public Shared Sub SetStyles(source As Object, styleAttr As String)

        Dim styleAttrs As String() = styleAttr.Split(CType(";", Char))
        For Each styleString As String In styleAttrs
            Dim style As String() = styleString.Split(CType(":", Char))
            style(0) = style(0).Trim().ToLower()
            Select Case style(0)
                Case "background-color"
                    source.Style.Fill.PatternType = ExcelFillStyle.Solid
                    Dim backgroundColor As String = style(1).Trim()
                    If backgroundColor.Contains("#") Then
                        source.Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml(style(1)))
                    ElseIf backgroundColor.Contains("RGB(") Then
                        backgroundColor = backgroundColor.Remove(0, 4)
                        backgroundColor = backgroundColor.Remove(backgroundColor.Length - 1, 1)
                        Dim rgbs As String() = backgroundColor.Split(CType(",", Char))
                        source.Style.Fill.BackgroundColor.SetColor(Color.FromArgb(Int16.Parse(rgbs(0).Trim()), Int16.Parse(rgbs(1).Trim()), Int16.Parse(rgbs(2).Trim())))
                    Else
                        source.Style.Fill.BackgroundColor.SetColor(Color.FromName(backgroundColor))
                    End If

                Case "text-align"
                    Select Case style(1).Trim().ToLower()
                        Case "left"
                            source.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left
                        Case "center"
                            source.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center
                    End Select
                Case "color"
                    Dim colorString As String = style(1).Trim()
                    If colorString.Contains("#") Then
                        source.Style.Font.Color.SetColor(ColorTranslator.FromHtml(colorString))
                    Else
                        source.Style.Font.Color.SetColor(Color.FromName(colorString))
                    End If
                Case "font-weight"
                    Dim weight As String = style(1).Trim().ToLower()
                    If weight = "bold" Then
                        source.Style.Font.Bold = True
                    End If
                Case "gridalternaterow"
                    source.Style.Fill.PatternType = ExcelFillStyle.Solid
                    source.Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#E5E5E5"))
                Case "gridheader"
                    source.Style.Fill.PatternType = ExcelFillStyle.Solid
                    source.Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#0F2039"))
                    source.Style.Font.Color.SetColor(ColorTranslator.FromHtml("#FFFFFF"))
                    source.Style.Font.Bold = True
                Case "gridheadersub"
                    source.Style.Fill.PatternType = ExcelFillStyle.Solid
                    source.Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#1F4379"))
                    source.Style.Font.Color.SetColor(ColorTranslator.FromHtml("#FFFFFF"))
                    source.Style.Font.Bold = True
                Case "gridheadersub1"
                    source.Style.Fill.PatternType = ExcelFillStyle.Solid
                    source.Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#336DC5"))
                    source.Style.Font.Color.SetColor(ColorTranslator.FromHtml("#FFFFFF"))
                    source.Style.Font.Bold = True
                Case "gridheadersub2"
                    source.Style.Fill.PatternType = ExcelFillStyle.Solid
                    source.Style.Font.Color.SetColor(ColorTranslator.FromHtml("#000000"))
                    source.Style.Font.Bold = True
                Case "report-card-grade-green"
                    source.Style.Fill.PatternType = ExcelFillStyle.Solid
                    source.Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#C6EFCE"))
                    source.Style.Font.Color.SetColor(ColorTranslator.FromHtml("#000000"))
                    source.Style.Font.Bold = False
                Case "report-card-grade-yellow"
                    source.Style.Fill.PatternType = ExcelFillStyle.Solid
                    source.Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#FFFFCC"))
                    source.Style.Font.Color.SetColor(ColorTranslator.FromHtml("#000000"))
                    source.Style.Font.Bold = False
                Case "report-card-grade-red"
                    source.Style.Fill.PatternType = ExcelFillStyle.Solid
                    source.Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#FFC7CE"))
                    source.Style.Font.Color.SetColor(ColorTranslator.FromHtml("#000000"))
                    source.Style.Font.Bold = False
                Case "icr-main-header"
                    source.Style.Fill.PatternType = ExcelFillStyle.Solid
                    source.Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#358AC0"))
                    source.Style.Font.Color.SetColor(ColorTranslator.FromHtml("#FFFFFF"))
                    source.Style.Font.Bold = True
            End Select
        Next
    End Sub
    Public Shared Function CountDecimals(source As String) As Integer
        If source.Contains(".") Then
            Dim decimalIndex = source.IndexOf(".", StringComparison.Ordinal)
            If decimalIndex = source.Trim().Length - 1 Then
                Return -1
            End If
            Return source.Length - decimalIndex - 1
        Else
            Return 0
        End If
    End Function
End Class