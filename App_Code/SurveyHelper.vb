﻿Imports Microsoft.VisualBasic
Imports Models

Public Class SurveyHelper
    Public Shared Function ReturnDeadline(ByVal client As String) As String
        Dim deadline As String = String.Empty
        Using db As New IICICEntities
            Dim project As New Project
            project = (From p In db.Projects Where p.Name = client).FirstOrDefault
            If project IsNot Nothing Then
                Dim d As Date = project.SurveyDeadline
                deadline = String.Format("{0}, {1} {2}, {3}", d.DayOfWeek, MonthName(d.Month), d.Day, d.Year)
            End If
        End Using
        Return deadline
    End Function
    Public Shared Function ReturnLocationList(ByVal orgId As Integer) As List(Of ListItem)
        Dim locations As New List(Of ListItem)
        Using db As New IICICEntities
            Dim datasets = (From ds In db.DataSets Where ds.OrganizationId = orgId Order By ds.LocationID)

            For Each d In datasets
                If d IsNot Nothing Then
                    locations.Add(New ListItem(d.Name, d.LocationID))
                End If
            Next
        End Using
        Return locations
    End Function
    Public Shared Function ReturnDefaultLocation(ByVal orgId As Integer) As Integer
        Dim result As Integer = 0
        Using db As New IICICEntities
            Dim ds As New DataSet
            ds = (From d In db.DataSets Where d.OrganizationId = orgId Order By d.LocationID).FirstOrDefault
            result = ds.LocationID
        End Using
        Return result
    End Function
    Public Shared Function ReturnExcelFilePath(ByVal client As String) As String
        Dim filePath = "II Cross Industry Compensation Survey.xlsx"
        Using db As New IICICEntities
            Dim project As New Project
            project = (From p In db.Projects Where p.Name = client).FirstOrDefault
            If project IsNot Nothing Then
                If project.ExcelFile IsNot Nothing Then
                    filePath = project.ExcelFile
                End If
            End If
        End Using
        Return filePath
    End Function
End Class
