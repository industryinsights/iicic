﻿Imports Microsoft.VisualBasic

Namespace DataAccess.IIReporting

    Public Class ReportItem

        Private _id As Integer
        Private _Jobid As Integer
        Private _JobTitle As String
        Private _typeId As Integer
        Private _projectId As Double
        Private _columnId As Integer?
        Private _subValue As Integer?
        Private _formatId As Integer?
        Private _prefix As String
        Private _suffix As String
        Private _itemText As String
        Private _orderIndex As Integer


        Public Property Id As Integer
            Get
                Return _id
            End Get
            Set(ByVal value As Integer)
                _id = value
            End Set
        End Property
        Public Property JobId As Integer
            Get
                Return _Jobid
            End Get
            Set(ByVal value As Integer)
                _Jobid = value
            End Set
        End Property
        Public Property JobTitle As String
            Get
                Return _JobTitle
            End Get
            Set(ByVal value As String)
                _JobTitle = value
            End Set
        End Property

        Public Property TypeId As Integer
            Get
                Return _typeId
            End Get
            Set(ByVal value As Integer)
                _typeId = value
            End Set
        End Property

        Public Property ProjectId As Double
            Get
                Return _projectId
            End Get
            Set(ByVal value As Double)
                _projectId = value
            End Set
        End Property

        Public Property ColumnId As Integer?
            Get
                Return _columnId
            End Get
            Set(ByVal value As Integer?)
                _columnId = value
            End Set
        End Property

        Public Property SubValue As Integer?
            Get
                Return _subValue
            End Get
            Set(ByVal value As Integer?)
                _subValue = value
            End Set
        End Property

        Public Property FormatId As Integer?
            Get
                Return _formatId
            End Get
            Set(ByVal value As Integer?)
                _formatId = value
            End Set
        End Property

        Public Property Prefix As String
            Get
                Return _prefix
            End Get
            Set(ByVal value As String)
                _prefix = value
            End Set
        End Property

        Public Property Suffix As String
            Get
                Return _suffix
            End Get
            Set(ByVal value As String)
                _suffix = value
            End Set
        End Property

        Public Property ItemText As String
            Get
                Return _itemText
            End Get
            Set(ByVal value As String)
                _itemText = value
            End Set
        End Property

        Public Property OrderIndex As Integer
            Get
                Return _orderIndex
            End Get
            Set(ByVal value As Integer)
                _orderIndex = value
            End Set
        End Property

    End Class

End Namespace


