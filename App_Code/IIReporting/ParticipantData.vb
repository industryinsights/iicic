﻿Imports Microsoft.VisualBasic
Imports System.Collections.Generic
Imports System.Linq

Namespace DataAccess.IIReporting

    <Serializable()> Public Class ParticipantData
        Inherits ReportData

        Public Overrides Function DoesColumnMeetMinimumReportingRequirements(ByVal columnId As Integer, ByVal isPercentile As Boolean) As Boolean

            Dim calcs As Calculations = GetCalculationsByColumnId(columnId)
            Dim firstFreq As Frequency = GetFirstFrequencyByColumnIdOnly(columnId)
            Return (calcs IsNot Nothing AndAlso calcs.ResponseCount >= If(isPercentile, 1, 1)) _
                OrElse (firstFreq IsNot Nothing AndAlso firstFreq.ResponseCount >= If(isPercentile, 1, 1))

        End Function

    End Class

End Namespace


