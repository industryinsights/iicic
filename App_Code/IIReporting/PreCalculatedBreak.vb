﻿Imports Microsoft.VisualBasic
Imports System.Collections.Generic
Imports System.Linq

Namespace DataAccess.IIReporting

    <Serializable()> Public Class PreCalculatedBreak
        Inherits ReportData

        Private _id As Integer
        Private _projectId As Double
        Private _minimumSample As Integer
        Private _breakYear As Integer
        Private _filter As String

        Public Property Id As Integer
            Get
                Return _id
            End Get
            Set(ByVal value As Integer)
                _id = value
            End Set
        End Property

        Public Property ProjectId As Double
            Get
                Return _projectId
            End Get
            Set(ByVal value As Double)
                _projectId = value
            End Set
        End Property

        Public Property MinimumSample As Integer
            Get
                Return _minimumSample
            End Get
            Set(ByVal value As Integer)
                _minimumSample = value
            End Set
        End Property

        Public Property BreakYear As Integer
        Get 
                return _breakYear
        End Get
            Set(value As Integer)
                _breakYear = value
            End Set
        End Property

        Public Property Filter As String
            Get
                Return _filter
            End Get
            Set(ByVal value As String)
                _filter = value
            End Set
        End Property

        Public Overrides Function DoesColumnMeetMinimumReportingRequirements(ByVal columnId As Integer, ByVal isPercentile As Boolean) As Boolean

            Dim calcs As Calculations = GetCalculationsByColumnId(columnId)
            Dim firstFreq As Frequency = GetFirstFrequencyByColumnIdOnly(columnId)
            '12-10-14 - BF:  Disable 40 rule
            'Return (calcs IsNot Nothing AndAlso calcs.ParticipantCount >= If(isPercentile, 7, _minimumSample) AndAlso calcs.LargestParticipantResponseCountPercentage < 40) _
            '    OrElse (firstFreq IsNot Nothing AndAlso firstFreq.ParticipantCount >= If(isPercentile, 7, _minimumSample) AndAlso firstFreq.LargestParticipantResponseCountPercentage < 40)
            Return (calcs IsNot Nothing AndAlso calcs.ParticipantCount >= If(isPercentile, 7, _minimumSample)) _
                OrElse (firstFreq IsNot Nothing AndAlso firstFreq.ParticipantCount >= If(isPercentile, 7, _minimumSample))
        End Function

    End Class

End Namespace


