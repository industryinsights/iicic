﻿Imports Microsoft.VisualBasic
Imports System.Collections.Generic

Namespace DataAccess.IIReporting

    Public Class SearchableResultsCriteria

        Private _id As Integer
        Private _caption As String
        Private _searchableResultsCriteriaFilterList As List(Of SearchableResultsCriteriaFilter)

        Public Property Id As Integer
            Get
                Return _id
            End Get
            Set(ByVal value As Integer)
                _id = value
            End Set
        End Property

        Public Property Caption As String
            Get
                Return _caption
            End Get
            Set(ByVal value As String)
                _caption = value
            End Set
        End Property

        Public Property SearchableResultsCriteriaFilterList As List(Of SearchableResultsCriteriaFilter)
            Get
                Return _searchableResultsCriteriaFilterList
            End Get
            Set(ByVal value As List(Of SearchableResultsCriteriaFilter))
                _searchableResultsCriteriaFilterList = value
            End Set
        End Property

    End Class

End Namespace