﻿Imports Microsoft.VisualBasic

Namespace DataAccess.IIReporting

    Public Class SearchableResultsCriteriaFilter

        Private _id As Integer
        Private _subCaption As String
        Private _filter As String

        Public Property Id As Integer
            Get
                Return _id
            End Get
            Set(ByVal value As Integer)
                _id = value
            End Set
        End Property

        Public Property SubCaption As String
            Get
                Return _subCaption
            End Get
            Set(ByVal value As String)
                _subCaption = value
            End Set
        End Property

        Public Property Filter As String
            Get
                Return _filter
            End Get
            Set(ByVal value As String)
                _filter = value
            End Set
        End Property

    End Class

End Namespace