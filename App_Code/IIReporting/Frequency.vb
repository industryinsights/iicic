﻿Imports Microsoft.VisualBasic

Namespace DataAccess.IIReporting

    <Serializable()> Public Class Frequency

        Private _columnId As Integer
        Private _responseCount As Double
        Private _participantCount As Double
        Private _largestParticipantResponseCount As Double
        Private _largestParticipantResponseCountPercentage As Double
        Private _value As Integer
        Private _count As Double
        Private _percentage As Double

        Public Property ColumnId As Integer
            Get
                Return _columnId
            End Get
            Set(ByVal value As Integer)
                _columnId = value
            End Set
        End Property

        Public Property ResponseCount As Double
            Get
                Return _responseCount
            End Get
            Set(ByVal value As Double)
                _responseCount = value
            End Set
        End Property

        Public Property ParticipantCount As Double
            Get
                Return _participantCount
            End Get
            Set(ByVal value As Double)
                _participantCount = value
            End Set
        End Property

        Public Property LargestParticipantResponseCount As Double
            Get
                Return _largestParticipantResponseCount
            End Get
            Set(ByVal value As Double)
                _largestParticipantResponseCount = value
            End Set
        End Property

        Public Property LargestParticipantResponseCountPercentage As Double
            Get
                Return _largestParticipantResponseCountPercentage
            End Get
            Set(ByVal value As Double)
                _largestParticipantResponseCountPercentage = value
            End Set
        End Property

        Public Property Value As Integer
            Get
                Return _value
            End Get
            Set(ByVal value As Integer)
                _value = value
            End Set
        End Property

        Public Property Count As Double
            Get
                Return _count
            End Get
            Set(ByVal value As Double)
                _count = value
            End Set
        End Property

        Public Property Percentage As Double
            Get
                Return _percentage
            End Get
            Set(ByVal value As Double)
                _percentage = value
            End Set
        End Property

    End Class

End Namespace


