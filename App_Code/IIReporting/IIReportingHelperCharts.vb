﻿Imports DataAccess.IIReporting
Imports System.Collections.Generic
Imports System.Data.SqlClient
Imports System.Text.RegularExpressions

Public Class IIReportingHelperCharts
    Public Shared Function GetEquivBreakId(ByVal ProjectId As Double, ByVal BreakId As Integer) As Integer

        Dim result As Integer = 0

        Using connection As New SqlConnection(ConfigurationManager.ConnectionStrings("Website1DatabaseAuthentication").ConnectionString)

            connection.Open()

            Dim command As New SqlCommand
            command.Connection = connection
            command.CommandType = Data.CommandType.Text
            command.CommandText = "Select Id from IIreporting_PreCalculatedBreaks where ProjectId = @ProjectId And Caption = (Select Caption from IIreporting_Precalculatedbreaks where id = @BreakID)"
            command.Parameters.Add(New SqlParameter("@BreakID", BreakId))
            command.Parameters.Add(New SqlParameter("@ProjectID", ProjectId))
            Dim reader As SqlDataReader = command.ExecuteReader()

            While (reader.HasRows AndAlso reader.Read())
                result = reader("id")
            End While
            reader.Close()

            connection.Close()

        End Using

        Return result

    End Function
    Public Shared Function GetBreakIdString(ByVal BreakId As Integer) As String

        Dim result As String = "None"

        Using connection As New SqlConnection(ConfigurationManager.ConnectionStrings("Website1DatabaseAuthentication").ConnectionString)

            connection.Open()

            Dim command As New SqlCommand
            command.Connection = connection
            command.CommandType = Data.CommandType.Text
            command.CommandText = "Select Caption from IIreporting_PreCalculatedBreaks where id = @BreakID"
            command.Parameters.Add(New SqlParameter("@BreakID", BreakId))
            Dim reader As SqlDataReader = command.ExecuteReader()

            While (reader.HasRows AndAlso reader.Read())
                result = reader("Caption")
            End While
            reader.Close()

            connection.Close()

        End Using

        Return result

    End Function
    Public Shared Function GetPercentile(ByVal Break As Integer, ByVal Percentile As String, ByVal cvalue As Integer) As Double

        Dim result As Double = 0

        Using connection As New SqlConnection(ConfigurationManager.ConnectionStrings("Website1DatabaseAuthentication").ConnectionString)

            connection.Open()

            Dim command As New SqlCommand
            command.Connection = connection
            command.CommandType = Data.CommandType.Text
            command.CommandText = "Select LowerPercentile, UpperPercentile, Median from [IIReporting_PreCalculatedCalculations] where breakid = @BreakID and Columnid = @CValue"
            command.Parameters.Add(New SqlParameter("@BreakID", Break))
            command.Parameters.Add(New SqlParameter("@CValue", cvalue))
            Dim reader As SqlDataReader = command.ExecuteReader()

            While (reader.HasRows AndAlso reader.Read())
                If Percentile = "Upper" Then
                    result = Convert.ToDouble(reader("UpperPercentile"))
                ElseIf Percentile = "Lower" Then
                    result = Convert.ToDouble(reader("LowerPercentile"))
                ElseIf Percentile = "Middle" Then
                    result = Convert.ToDouble(reader("Median"))
                End If
            End While
            reader.Close()

            connection.Close()

        End Using

        Return result

    End Function

    Public Shared Function GetReportItemList_BySection(ByVal projectId As String, ByVal Sectionlist As String) As List(Of ChartItem)

        Dim result As New List(Of ChartItem)

        Using connection As New SqlConnection(ConfigurationManager.ConnectionStrings("Website1DatabaseAuthentication").ConnectionString)

            connection.Open()

            Dim command As New SqlCommand
            command.Connection = connection
            command.CommandType = Data.CommandType.Text
            command.CommandText = "Select * from graphguide where projectid = " + projectId + " "
            If Sectionlist IsNot Nothing And Sectionlist <> "" Then
                command.CommandText += " AND  ([Group] IN ( " + Sectionlist + ") OR [Group] Is NULL)"
            End If
            command.CommandText += " order by orderindex"
            Dim reader As SqlDataReader = command.ExecuteReader()

            While (reader.HasRows AndAlso reader.Read())
                Dim item As New ChartItem
                item.ID = reader("Id")
                If Not IsDBNull(reader("cvalue")) Then
                    item.CValue = Convert.ToInt32(reader("Cvalue"))
                End If
                If Not IsDBNull(reader("Heading")) Then
                    item.Heading = reader("Heading")
                End If

                item.ProjectId = Convert.ToDouble(reader("Projectid"))
                item.OrderIndex = reader("Orderindex")
                If Not IsDBNull(reader("Typeid")) Then
                    item.Type = Convert.ToInt32(reader("Typeid"))
                End If
                If Not IsDBNull(reader("Formatid")) Then
                    item.FormatType = Convert.ToInt32(reader("Formatid"))
                End If
                If Not IsDBNull(reader("Subvalue")) Then
                    item.Subvalue = Convert.ToInt32(reader("Subvalue"))
                End If
                If Not IsDBNull(reader("Suffix")) Then
                    item.Suffix = reader("Suffix")
                End If
                If Not IsDBNull(reader("Prefix")) Then
                    item.Prefix = reader("Prefix")
                End If
                If Not IsDBNull(reader("scalemin")) Then
                    item.minscale = reader("scalemin")
                End If
                If Not IsDBNull(reader("scalemax")) Then
                    item.maxscale = reader("scalemax")
                End If
                If Not IsDBNull(reader("unit")) Then
                    item.Unit = reader("unit")
                End If
                result.Add(item)
            End While
            reader.Close()

            connection.Close()

        End Using

        Return result

    End Function
    Public Shared Function GetReportItemList(ByVal projectid As String) As List(Of ChartItem)

        Dim result As New List(Of ChartItem)

        Using connection As New SqlConnection(ConfigurationManager.ConnectionStrings("Website1DatabaseAuthentication").ConnectionString)

            connection.Open()

            Dim command As New SqlCommand
            command.Connection = connection
            command.CommandType = Data.CommandType.Text
            command.CommandText = "Select * from graphguide where projectid = " + projectid + " order by orderindex"
            Dim reader As SqlDataReader = command.ExecuteReader()

            While (reader.HasRows AndAlso reader.Read())
                Dim item As New ChartItem
                item.ID = reader("Id")
                If Not IsDBNull(reader("cvalue")) Then
                    item.CValue = Convert.ToInt32(reader("Cvalue"))
                End If
                If Not IsDBNull(reader("Heading")) Then
                    item.Heading = reader("Heading")
                End If

                item.ProjectId = Convert.ToDouble(reader("Projectid"))
                item.OrderIndex = reader("Orderindex")
                If Not IsDBNull(reader("Typeid")) Then
                    item.Type = Convert.ToInt32(reader("Typeid"))
                End If
                If Not IsDBNull(reader("Formatid")) Then
                    item.FormatType = Convert.ToInt32(reader("Formatid"))
                End If
                If Not IsDBNull(reader("Subvalue")) Then
                    item.Subvalue = Convert.ToInt32(reader("Subvalue"))
                End If
                If Not IsDBNull(reader("Suffix")) Then
                    item.Suffix = reader("Suffix")
                End If
                If Not IsDBNull(reader("Prefix")) Then
                    item.Prefix = reader("Prefix")
                End If
                If Not IsDBNull(reader("scalemin")) Then
                    item.minscale = reader("scalemin")
                End If
                If Not IsDBNull(reader("scalemax")) Then
                    item.maxscale = reader("scalemax")
                End If
                If Not IsDBNull(reader("unit")) Then
                    item.Unit = reader("unit")
                End If
                result.Add(item)
            End While
            reader.Close()

            connection.Close()

        End Using

        Return result

    End Function



    Public Shared Function GetCalculationsList(ByVal projectId As Double, ByVal dataQueryId As Guid, ByVal lowerPercentileMultiple As Double, ByVal upperPercentileMultiple As Double) As List(Of Calculations)

        Return GetCalculationsList(False, projectId, dataQueryId, lowerPercentileMultiple, upperPercentileMultiple, Nothing)

    End Function


    Public Shared Function GetCalculationsList(ByVal breakId As Integer) As List(Of Calculations)

        Return GetCalculationsList(True, Nothing, Nothing, Nothing, Nothing, breakId)

    End Function
    Private Shared Function GetCalculationsList(ByVal getPreCalculatedBreak As Boolean, ByVal projectId As Double, ByVal dataQueryId As Guid, ByVal lowerPercentileMultiple As Double, ByVal upperPercentileMultiple As Double, ByVal breakId As Integer) As List(Of Calculations)

        Dim result As New List(Of Calculations)

        Using connection As New SqlConnection(ConfigurationManager.ConnectionStrings("Website1DatabaseAuthentication").ConnectionString)

            connection.Open()

            Dim command As New SqlCommand
            command.Connection = connection
            command.CommandType = Data.CommandType.StoredProcedure
            command.CommandTimeout = 0

            If getPreCalculatedBreak Then
                command.CommandText = "IIReporting_PreCalculatedCalculationsGet"
                command.Parameters.Add(New SqlParameter("@BreakId", breakId))
            Else
                command.CommandText = "IIReporting_GetCalculations"
                command.Parameters.Add(New SqlParameter("@ProjectId", projectId))
                command.Parameters.Add(New SqlParameter("@DataQueryId", dataQueryId))
                command.Parameters.Add(New SqlParameter("@LowerPercentileMultiple", lowerPercentileMultiple))
                command.Parameters.Add(New SqlParameter("@UpperPercentileMultiple", upperPercentileMultiple))
            End If

            Dim reader As SqlDataReader = command.ExecuteReader()

            While reader.HasRows AndAlso reader.Read()

                Dim calculations As New Calculations()
                calculations.ColumnId = Convert.ToInt32(reader("ColumnId"))
                If Not IsDBNull(reader("ResponseCount")) Then
                    calculations.ResponseCount = Convert.ToDouble(reader("ResponseCount"))
                End If
                If Not IsDBNull(reader("ParticipantCount")) Then
                    calculations.ParticipantCount = Convert.ToDouble(reader("ParticipantCount"))
                End If
                If Not IsDBNull(reader("LargestParticipantResponseCount")) Then
                    calculations.LargestParticipantResponseCount = Convert.ToDouble(reader("LargestParticipantResponseCount"))
                End If
                If Not IsDBNull(reader("LargestParticipantResponseCountPercentage")) Then
                    calculations.LargestParticipantResponseCountPercentage = Convert.ToDouble(reader("LargestParticipantResponseCountPercentage"))
                End If
                If Not IsDBNull(reader("Mean")) Then
                    calculations.Mean = Convert.ToDouble(reader("Mean"))
                End If
                If Not IsDBNull(reader("Median")) Then
                    calculations.Median = Convert.ToDouble(reader("Median"))
                End If
                If Not IsDBNull(reader("LowerPercentile")) Then
                    calculations.LowerPercentile = Convert.ToDouble(reader("LowerPercentile"))
                End If
                If Not IsDBNull(reader("UpperPercentile")) Then
                    calculations.UpperPercentile = Convert.ToDouble(reader("UpperPercentile"))
                End If
                result.Add(calculations)

            End While

            connection.Close()

        End Using

        Return result

    End Function

    Public Shared Function GetCalculationsListYF(ByVal projectid As Double, ByVal LocationID As Integer) As List(Of Calculations)

        Dim result As New List(Of Calculations)

        Using connection As New SqlConnection(ConfigurationManager.ConnectionStrings("Website1DatabaseAuthentication").ConnectionString)

            connection.Open()

            Dim command As New SqlCommand
            command.Connection = connection
            command.CommandType = Data.CommandType.StoredProcedure
            command.CommandTimeout = 0

            command.CommandType = Data.CommandType.Text
            command.CommandText = "Select ColumnId, 1 as ResponseCount, 1 as  ParticipantCount,1 as LargestParticipantResponseCount, 1 as LargestParticipantResponseCountPercentage, Value as Mean, Value as Median, Value as LowerPercentile, Value as UpperPercentile From IIReporting_Data "
            command.CommandText += " Where Projectid = " + projectid.ToString() + " AND LocationID = " + LocationID.ToString()


            Dim reader As SqlDataReader = command.ExecuteReader()

            While reader.HasRows AndAlso reader.Read()

                Dim calculations As New Calculations()
                calculations.ColumnId = Convert.ToInt32(reader("ColumnId"))
                If Not IsDBNull(reader("ResponseCount")) Then
                    calculations.ResponseCount = Convert.ToDouble(reader("ResponseCount"))
                End If
                If Not IsDBNull(reader("ParticipantCount")) Then
                    calculations.ParticipantCount = Convert.ToDouble(reader("ParticipantCount"))
                End If
                If Not IsDBNull(reader("LargestParticipantResponseCount")) Then
                    calculations.LargestParticipantResponseCount = Convert.ToDouble(reader("LargestParticipantResponseCount"))
                End If
                If Not IsDBNull(reader("LargestParticipantResponseCountPercentage")) Then
                    calculations.LargestParticipantResponseCountPercentage = Convert.ToDouble(reader("LargestParticipantResponseCountPercentage"))
                End If
                If Not IsDBNull(reader("Mean")) Then
                    calculations.Mean = Convert.ToDouble(reader("Mean"))
                End If
                If Not IsDBNull(reader("Median")) Then
                    calculations.Median = Convert.ToDouble(reader("Median"))
                End If
                If Not IsDBNull(reader("LowerPercentile")) Then
                    calculations.LowerPercentile = Convert.ToDouble(reader("LowerPercentile"))
                End If
                If Not IsDBNull(reader("UpperPercentile")) Then
                    calculations.UpperPercentile = Convert.ToDouble(reader("UpperPercentile"))
                End If
                result.Add(calculations)

            End While

            connection.Close()

        End Using

        Return result

    End Function
    Public Shared Function GetFrequencyList(ByVal projectId As Double, ByVal dataQueryId As Guid) As List(Of Frequency)

        Return GetFrequencyList(False, projectId, dataQueryId, Nothing)

    End Function

    Public Shared Function GetFrequencyList(ByVal breakId As Integer) As List(Of Frequency)

        Return GetFrequencyList(True, Nothing, Nothing, breakId)

    End Function

    Private Shared Function GetFrequencyList(ByVal getPreCalculatedBreak As Boolean, ByVal projectId As Double, ByVal dataQueryId As Guid, ByVal breakId As Integer) As List(Of Frequency)

        Dim result As New List(Of Frequency)

        Using connection As New SqlConnection(ConfigurationManager.ConnectionStrings("Website1DatabaseAuthentication").ConnectionString)

            connection.Open()

            Dim command As New SqlCommand
            command.Connection = connection
            command.CommandType = Data.CommandType.StoredProcedure
            command.CommandTimeout = 0
            If getPreCalculatedBreak Then
                command.CommandText = "IIReporting_PreCalculatedFrequenciesGet"
                command.Parameters.Add(New SqlParameter("@BreakId", breakId))
            Else
                command.CommandText = "IIReporting_GetFrequencies"
                command.Parameters.Add(New SqlParameter("@ProjectId", projectId))
                command.Parameters.Add(New SqlParameter("@DataQueryId", dataQueryId))
            End If



            Dim reader As SqlDataReader = command.ExecuteReader()

            While reader.HasRows AndAlso reader.Read()

                Dim frequency As New Frequency()
                frequency.ColumnId = Convert.ToInt32(reader("ColumnId"))
                If Not IsDBNull(reader("ResponseCount")) Then
                    frequency.ResponseCount = Convert.ToDouble(reader("ResponseCount"))
                End If
                If Not IsDBNull(reader("ParticipantCount")) Then
                    frequency.ParticipantCount = Convert.ToDouble(reader("ParticipantCount"))
                End If
                If Not IsDBNull(reader("LargestParticipantResponseCount")) Then
                    frequency.LargestParticipantResponseCount = Convert.ToDouble(reader("LargestParticipantResponseCount"))
                End If
                If Not IsDBNull(reader("LargestParticipantResponseCountPercentage")) Then
                    frequency.LargestParticipantResponseCountPercentage = Convert.ToDouble(reader("LargestParticipantResponseCountPercentage"))
                End If
                If Not IsDBNull(reader("Value")) Then
                    frequency.Value = Convert.ToInt32(reader("Value"))
                End If
                If Not IsDBNull(reader("Count")) Then
                    frequency.Count = Convert.ToDouble(reader("Count"))
                End If
                If Not IsDBNull(reader("Percentage")) Then
                    frequency.Percentage = Convert.ToDouble(reader("Percentage"))
                End If

                result.Add(frequency)

            End While

            connection.Close()

        End Using

        Return result

    End Function
    Public Shared Function GetFrequencyListYF(ByVal projectId As Double, ByVal LocationID As Double) As List(Of Frequency)


        Dim empty As New List(Of List(Of String))
        Dim dataQueryIdYF As Guid = IIReportingHelperSR.LoadQueryAndRetrieveDataQueryId(projectId, empty)

        Dim result As New List(Of Frequency)

        Using connection As New SqlConnection(ConfigurationManager.ConnectionStrings("Website1DatabaseAuthentication").ConnectionString)

            connection.Open()

            Dim command As New SqlCommand
            command.Connection = connection
            command.CommandType = Data.CommandType.StoredProcedure
            command.CommandTimeout = 0

            command.CommandText = "IIReporting_GetFrequencies"
            command.Parameters.Add(New SqlParameter("@ProjectId", projectId))
            command.Parameters.Add(New SqlParameter("@DataQueryId", dataQueryIdYF))
            command.Parameters.Add(New SqlParameter("@LocationID", LocationID))

            Dim reader As SqlDataReader = command.ExecuteReader()

            While reader.HasRows AndAlso reader.Read()

                Dim frequency As New Frequency()
                frequency.ColumnId = Convert.ToInt32(reader("ColumnId"))
                If Not IsDBNull(reader("ResponseCount")) Then
                    frequency.ResponseCount = Convert.ToDouble(reader("ResponseCount"))
                End If
                If Not IsDBNull(reader("ParticipantCount")) Then
                    frequency.ParticipantCount = Convert.ToDouble(reader("ParticipantCount"))
                End If
                If Not IsDBNull(reader("LargestParticipantResponseCount")) Then
                    frequency.LargestParticipantResponseCount = Convert.ToDouble(reader("LargestParticipantResponseCount"))
                End If
                If Not IsDBNull(reader("LargestParticipantResponseCountPercentage")) Then
                    frequency.LargestParticipantResponseCountPercentage = Convert.ToDouble(reader("LargestParticipantResponseCountPercentage"))
                End If
                If Not IsDBNull(reader("Value")) Then
                    frequency.Value = Convert.ToInt32(reader("Value"))
                End If
                If Not IsDBNull(reader("Count")) Then
                    frequency.Count = Convert.ToDouble(reader("Count"))
                End If
                If Not IsDBNull(reader("Percentage")) Then
                    frequency.Percentage = Convert.ToDouble(reader("Percentage"))
                End If

                result.Add(frequency)

            End While

            connection.Close()

        End Using

        Return result

    End Function

    Public Shared Function GetPreCalculatedBreakList(ByVal projectId As Double) As List(Of PreCalculatedBreak)

        Dim result As New List(Of PreCalculatedBreak)

        Using connection As New SqlConnection(ConfigurationManager.ConnectionStrings("Website1DatabaseAuthentication").ConnectionString)

            connection.Open()

            Dim command As New SqlCommand
            command.Connection = connection
            command.CommandType = Data.CommandType.StoredProcedure
            command.CommandText = "IIReporting_PreCalculatedBreaksGet"
            command.Parameters.Add(New SqlParameter("@ProjectId", projectId))

            Dim reader As SqlDataReader = command.ExecuteReader()

            While reader.HasRows AndAlso reader.Read()

                Dim break As New PreCalculatedBreak()
                break.Id = Convert.ToInt32(reader("Id"))
                break.ProjectId = Convert.ToDouble(reader("ProjectId"))
                break.Caption = reader("Caption").ToString()
                break.Filter = reader("Filter").ToString()

                result.Add(break)

            End While

            connection.Close()

        End Using

        Return result

    End Function



End Class
