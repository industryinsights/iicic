﻿Imports Microsoft.VisualBasic

Namespace DataAccess.IIReporting

    Public Class ChartItem
        Private _id As Integer
        Private _FormatType As Integer
        Private _projectid As Double
        Private _Type As Integer
        Private _cvalue As Integer
        Private _Heading As String
        Private _orderIndex As Integer
        Private _prefix As String
        Private _suffix As String
        Private _Subvalue As Integer
        Private _minscale As Integer
        Private _maxscale As Integer
        Private _unit As Double
        Public Property Unit As Double
            Get
                Return _unit
            End Get
            Set(ByVal value As Double)
                _unit = value
            End Set
        End Property

        Public Property CValue As Integer
            Get
                Return _cvalue
            End Get
            Set(ByVal value As Integer)
                _cvalue = value
            End Set
        End Property
        Public Property minscale As Integer
            Get
                Return _minscale
            End Get
            Set(ByVal value As Integer)
                _minscale = value
            End Set
        End Property
        Public Property maxscale As Integer
            Get
                Return _maxscale
            End Get
            Set(ByVal value As Integer)
                _maxscale = value
            End Set
        End Property
        Public Property Subvalue As Integer
            Get
                Return _Subvalue
            End Get
            Set(ByVal value As Integer)
                _Subvalue = value
            End Set
        End Property
        Public Property Heading As String
            Get
                Return _Heading
            End Get
            Set(ByVal value As String)
                _Heading = value
            End Set
        End Property


        Public Property ProjectId As Double
            Get
                Return _projectid
            End Get
            Set(ByVal value As Double)
                _projectid = value
            End Set
        End Property


        Public Property OrderIndex As Integer
            Get
                Return _orderIndex
            End Get
            Set(ByVal value As Integer)
                _orderIndex = value
            End Set
        End Property

        Public Property ID As Integer
            Get
                Return _id
            End Get
            Set(ByVal value As Integer)
                _id = value
            End Set
        End Property
        Public Property Type As Integer
            Get
                Return _Type
            End Get
            Set(ByVal value As Integer)
                _Type = value
            End Set
        End Property
        Public Property FormatType As Integer
            Get
                Return _FormatType
            End Get
            Set(ByVal value As Integer)
                _FormatType = value
            End Set
        End Property
        Public Property Prefix As String
            Get
                Return _prefix
            End Get
            Set(ByVal value As String)
                _prefix = value
            End Set
        End Property

        Public Property Suffix As String
            Get
                Return _suffix
            End Get
            Set(ByVal value As String)
                _suffix = value
            End Set
        End Property
    End Class


End Namespace


