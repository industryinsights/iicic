﻿Imports Microsoft.VisualBasic
Imports System.Collections.Generic
Imports System.Linq

Namespace DataAccess.IIReporting

    <Serializable()> Public Class SearchableResultsBreak
        Inherits ReportData

        Private _projectId As Double
        Private _dataQueryId As Guid
        Private _minimumSample As Integer
        Private _searchText As String

        Public Property ProjectId As Double
            Get
                Return _projectId
            End Get
            Set(ByVal value As Double)
                _projectId = value
            End Set
        End Property

        Public Property DataQueryId As Guid
            Get
                Return _dataQueryId
            End Get
            Set(ByVal value As Guid)
                _dataQueryId = value
            End Set
        End Property

        Public Property MinimumSample As Integer
            Get
                Return _minimumSample
            End Get
            Set(ByVal value As Integer)
                _minimumSample = value
            End Set
        End Property

        Public Property SearchText As String
            Get
                Return _searchText
            End Get
            Set(ByVal value As String)
                _searchText = value
            End Set
        End Property

        Public Overrides Function DoesColumnMeetMinimumReportingRequirements(ByVal columnId As Integer, ByVal isPercentile As Boolean) As Boolean

            Dim calcs As Calculations = GetCalculationsByColumnId(columnId)
            Dim firstFreq As Frequency = GetFirstFrequencyByColumnIdOnly(columnId)
            'Disable 40 rule
            'Return (calcs IsNot Nothing AndAlso calcs.ParticipantCount >= If(isPercentile, 7, _minimumSample) AndAlso calcs.LargestParticipantResponseCountPercentage < 40) _
            '    OrElse (firstFreq IsNot Nothing AndAlso firstFreq.ParticipantCount >= If(isPercentile, 7, _minimumSample) AndAlso firstFreq.LargestParticipantResponseCountPercentage < 40)
            Return (calcs IsNot Nothing AndAlso calcs.ParticipantCount >= If(isPercentile, 7, _minimumSample)) _
    OrElse (firstFreq IsNot Nothing AndAlso firstFreq.ParticipantCount >= If(isPercentile, 7, _minimumSample))

        End Function

    End Class

End Namespace


