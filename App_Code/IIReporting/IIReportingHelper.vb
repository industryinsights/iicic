﻿Imports DataAccess.IIReporting
Imports System.Collections.Generic
Imports System.Data.SqlClient
Imports System.Text.RegularExpressions

Public Class IIReportingHelper



    Public Shared Function hasparticipantdata(ByVal LocationID As Integer, ByVal ProjectId As Double) As Boolean

        Dim result As Boolean = False

        Using connection As New SqlConnection(ConfigurationManager.ConnectionStrings("Website1DatabaseAuthentication").ConnectionString)

            connection.Open()

            Dim command As New SqlCommand
            command.Connection = connection
            command.CommandText = " SELECT LocationID from iireporting_data WHERE LocationID = @LocationID AND  ProjectId = @ProjectId    "
            command.Parameters.Add(New SqlParameter("@LocationID", LocationID))
            command.Parameters.Add(New SqlParameter("@ProjectId", ProjectId))
            Dim reader As SqlDataReader = command.ExecuteReader()

            If reader.HasRows Then

                result = True

            End If

            connection.Close()

        End Using

        Return result

    End Function
    Public Shared Function getallResponsesBreakid(ByVal projectid As Double) As Integer

        Dim result As Integer = 0

        Using connection As New SqlConnection(ConfigurationManager.ConnectionStrings("Website1DatabaseAuthentication").ConnectionString)

            connection.Open()

            Dim command As New SqlCommand
            command.Connection = connection
            command.CommandText = " SELECT Id FROM IIReporting_PreCalculatedBreaks WHERE Caption = 'All Responses' AND  ProjectId = @ProjectId    "
            command.Parameters.Add(New SqlParameter("@ProjectId", projectid))

            Dim reader As SqlDataReader = command.ExecuteReader()

            While reader.HasRows AndAlso reader.Read()

                result = Convert.ToInt32(reader("Id"))

            End While

            connection.Close()

        End Using

        Return result

    End Function
    Public Shared Function GetCurrentFilterIdsForInstance(ByVal projectId As Double, ByVal instanceId As Guid) As List(Of Integer)

        Dim result As New List(Of Integer)

        Using connection As New SqlConnection(ConfigurationManager.ConnectionStrings("Website1DatabaseAuthentication").ConnectionString)

            connection.Open()

            Dim command As New SqlCommand
            command.Connection = connection
            command.CommandText = " SELECT FilterId FROM IIReporting_SearchableResultsInstance WHERE DataQueryId = ( SELECT TOP 1 DataQueryId FROM IIReporting_SearchableResultsInstance WHERE ProjectId = @ProjectId AND InstanceId = @InstanceId ORDER BY Id DESC ) AND FilterId IS NOT NULL "
            command.Parameters.Add(New SqlParameter("@ProjectId", projectId))
            command.Parameters.Add(New SqlParameter("@InstanceId", instanceId))
            Dim reader As SqlDataReader = command.ExecuteReader()

            Dim criteria As SearchableResultsCriteria = Nothing

            While reader.HasRows AndAlso reader.Read()

                result.Add(Convert.ToInt32(reader("FilterId")))

            End While

            connection.Close()

        End Using

        Return result

    End Function

    Public Shared Sub InsertSearchableResultsInstanceFilters(ByVal projectId As Double, ByVal instanceId As Guid, ByVal dataQueryId As Guid, ByVal searchText As String, ByVal filterIds As List(Of Integer))

        Using connection As New SqlConnection(ConfigurationManager.ConnectionStrings("Website1DatabaseAuthentication").ConnectionString)

            connection.Open()

            Dim command As New SqlCommand
            command.Connection = connection
            command.CommandText = String.Empty

            If filterIds IsNot Nothing AndAlso filterIds.Count > 0 Then
                For Each filterId As Integer In filterIds
                    command.CommandText += String.Format("INSERT INTO IIReporting_SearchableResultsInstance (InstanceId, ProjectId, DataQueryId, SearchText, FilterId ) VALUES ( @InstanceId, @Projectid, @DataQueryId, '{0}', {1} ); ", searchText, filterId.ToString())
                Next
            Else
                command.CommandText = "INSERT INTO IIReporting_SearchableResultsInstance (InstanceId, ProjectId, DataQueryId ) VALUES ( @InstanceId, @Projectid, @DataQueryId ) "
            End If

            command.Parameters.Add(New SqlParameter("@ProjectId", projectId))
            command.Parameters.Add(New SqlParameter("@InstanceId", instanceId))
            command.Parameters.Add(New SqlParameter("@DataQueryId", dataQueryId))

            command.ExecuteNonQuery()

            connection.Close()

        End Using

    End Sub

    Public Shared Function GetSearchableResultsInstanceBreaks(ByVal projectId As Double, ByVal instanceId As Guid, ByVal minimumSample As Integer) As List(Of SearchableResultsBreak)

        Dim result As New List(Of SearchableResultsBreak)

        Using connection As New SqlConnection(ConfigurationManager.ConnectionStrings("Website1DatabaseAuthentication").ConnectionString)

            connection.Open()

            Dim command As New SqlCommand
            command.Connection = connection
            command.CommandText = " SELECT DataQueryId, (SELECT TOP 1 SearchText FROM IIReporting_SearchableResultsInstance WHERE DataQueryId = DataQueryIds.DataQueryId) AS 'SearchText', (SELECT TOP 1 ID FROM IIReporting_SearchableResultsInstance WHERE DataQueryId = DataQueryIds.DataQueryId) FROM ( SELECT DISTINCT DataQueryId FROM IIReporting_SearchableResultsInstance WHERE InstanceId = @InstanceId AND ProjectId = @ProjectId ) DataQueryIds ORDER BY 3  "

            command.Parameters.Add(New SqlParameter("@ProjectId", projectId))
            command.Parameters.Add(New SqlParameter("@InstanceId", instanceId))

            Dim reader As SqlDataReader = command.ExecuteReader()
            Dim counter As Integer = 1

            While reader.HasRows AndAlso reader.Read()

                Dim break As New SearchableResultsBreak
                break.DataQueryId = Guid.Parse(reader("DataQueryId").ToString())
                If Not IsDBNull(reader("SearchText")) Then
                    break.SearchText = reader("SearchText").ToString()
                Else
                    break.SearchText = "All<br/><br/>"
                End If
                break.Caption = "Search " + counter.ToString()
                break.MinimumSample = minimumSample
                break.ProjectId = projectId
                result.Add(break)

                counter += 1

            End While

            connection.Close()

        End Using

        Return result

    End Function

    Public Shared Function GetSearchableResultsCriteriaList(ByVal projectId As Double) As List(Of SearchableResultsCriteria)

        Dim result As New List(Of SearchableResultsCriteria)

        Using connection As New SqlConnection(ConfigurationManager.ConnectionStrings("Website1DatabaseAuthentication").ConnectionString)

            connection.Open()

            Dim command As New SqlCommand
            command.Connection = connection
            command.CommandType = Data.CommandType.StoredProcedure
            command.CommandText = "IIReporting_GetSearchableResultsCriteriaFilters"
            command.Parameters.Add(New SqlParameter("@ProjectId", projectId))
            Dim reader As SqlDataReader = command.ExecuteReader()

            Dim criteria As SearchableResultsCriteria = Nothing

            While reader.HasRows AndAlso reader.Read()

                Dim currentId As Integer = Convert.ToInt32(reader("Id"))

                If criteria Is Nothing OrElse criteria.Id <> currentId Then
                    criteria = New SearchableResultsCriteria()
                    criteria.Id = Convert.ToInt32(reader("Id"))
                    criteria.Caption = reader("Caption").ToString()

                    criteria.SearchableResultsCriteriaFilterList = New List(Of SearchableResultsCriteriaFilter)
                    result.Add(criteria)
                End If

                Dim filter As New SearchableResultsCriteriaFilter()
                filter.Id = Convert.ToInt32(reader("Id2"))
                filter.SubCaption = reader("SubCaption").ToString()
                filter.Filter = reader("Filter").ToString()
                criteria.SearchableResultsCriteriaFilterList.Add(filter)


            End While

            connection.Close()

        End Using

        Return result

    End Function

    Public Shared Function GetReportItemList(ByVal projectId As Double) As List(Of ReportItem)

        Dim result As New List(Of ReportItem)

        Using connection As New SqlConnection(ConfigurationManager.ConnectionStrings("Website1DatabaseAuthentication").ConnectionString)

            connection.Open()

            Dim command As New SqlCommand
            command.Connection = connection
            command.CommandType = Data.CommandType.StoredProcedure
            command.CommandText = "IIReporting_GetReportItems"
            command.Parameters.Add(New SqlParameter("@ProjectId", projectId))
            Dim reader As SqlDataReader = command.ExecuteReader()

            While reader.HasRows AndAlso reader.Read()

                Dim item As New ReportItem()
                item.Id = Convert.ToInt32(reader("Id"))
                item.TypeId = Convert.ToInt32(reader("TypeId"))
                item.ProjectId = Convert.ToDouble(reader("ProjectId"))
                If Not IsDBNull(reader("ColumnId")) Then
                    item.ColumnId = Convert.ToInt32(reader("ColumnId"))
                End If
                If Not IsDBNull(reader("SubValue")) Then
                    item.SubValue = Convert.ToInt32(reader("SubValue"))
                End If
                If Not IsDBNull(reader("FormatId")) Then
                    item.FormatId = Convert.ToInt32(reader("FormatId"))
                End If
                If Not IsDBNull(reader("Prefix")) Then
                    item.Prefix = reader("Prefix").ToString()
                End If
                If Not IsDBNull(reader("Suffix")) Then
                    item.Suffix = reader("Suffix").ToString()
                End If
                If Not IsDBNull(reader("ItemText")) Then
                    item.ItemText = reader("ItemText").ToString()
                End If
                If Not IsDBNull(reader("OrderIndex")) Then
                    item.OrderIndex = Convert.ToInt32(reader("OrderIndex"))
                End If
                result.Add(item)

            End While

            connection.Close()

        End Using

        Return result

    End Function

    Public Shared Function GetQueryParticipantCount(ByVal projectId As Double, ByVal listOfFilterList As List(Of List(Of String))) As Integer

        Return GetQueryParticipantCount(projectId, listOfFilterList, Nothing)

    End Function

    Public Shared Function GetQueryParticipantCount(ByVal projectId As Double, ByVal listOfFilterList As List(Of List(Of String)), ByVal dedicatedTableName As String) As Integer

        Dim result As Integer

        Using connection As New SqlConnection(ConfigurationManager.ConnectionStrings("Website1DatabaseAuthentication").ConnectionString)

            connection.Open()

            Dim command As New SqlCommand
            command.Connection = connection
            command.CommandType = Data.CommandType.Text
            command.CommandText = " SELECT COUNT(DISTINCT SurveyId) " + GetCoreQueryPart(projectId, listOfFilterList, dedicatedTableName)
            result = Convert.ToInt32(command.ExecuteScalar())

            connection.Close()

        End Using

        Return result

    End Function

    Public Shared Function GetQueryRespondentCount(ByVal projectId As Double, ByVal listOfFilterList As List(Of List(Of String))) As Integer

        Return GetQueryRespondentCount(projectId, listOfFilterList, Nothing)

    End Function

    Public Shared Function GetQueryRespondentCount(ByVal projectId As Double, ByVal listOfFilterList As List(Of List(Of String)), ByVal dedicatedTableName As String) As Integer

        Dim result As Integer

        Using connection As New SqlConnection(ConfigurationManager.ConnectionStrings("Website1DatabaseAuthentication").ConnectionString)

            connection.Open()

            Dim command As New SqlCommand
            command.Connection = connection
            command.CommandType = Data.CommandType.Text
            command.CommandText = " SELECT COUNT(DISTINCT LocationID) " + GetCoreQueryPart(projectId, listOfFilterList, dedicatedTableName)
            result = Convert.ToInt32(command.ExecuteScalar())

            connection.Close()

        End Using

        Return result

    End Function

    Public Shared Function GetFiltersByUser(ByVal projectId As Double, ByVal User As String) As List(Of String)

        Dim result As List(Of String)
        Dim array As String()
        Dim list As String = ""
        Using connection As New SqlConnection(ConfigurationManager.ConnectionStrings("Website1DatabaseAuthentication").ConnectionString)

            connection.Open()

            Dim command As New SqlCommand
            command.Connection = connection
            command.CommandType = Data.CommandType.Text
            command.CommandText = " SELECT TOP 1 FiltersV From IIReporting_SaveFilters Where Projectid = '" & projectId & "' AND UserID = '" & User & "' ORDER BY Date_Searched DESC"
            list = command.ExecuteScalar()
            array = list.Split(",")
            connection.Close()

        End Using
        result = array.ToList
        Return result

    End Function
    Public Shared Function RecentSearch(ByVal projectId As Double, ByVal User As String) As String

        Dim result As String

        Using connection As New SqlConnection(ConfigurationManager.ConnectionStrings("Website1DatabaseAuthentication").ConnectionString)

            connection.Open()

            Dim command As New SqlCommand
            command.Connection = connection
            command.CommandType = Data.CommandType.Text
            command.CommandText = " SELECT TOP 1 CASE WHEN Filters = 'All Responses' THEN null else Date_Searched end as [DateSearched] From IIReporting_SaveFilters Where Projectid = '" & projectId & "' AND UserID = '" & User & "' ORDER BY Date_Searched DESC"
            Dim reader As SqlDataReader = command.ExecuteReader()

            result = ""
            While reader.HasRows AndAlso reader.Read()
                result = reader("DateSearched").ToString()
            End While

            connection.Close()

        End Using

        Return result

    End Function

    Public Shared Function LoadQueryAndRetrieveDataQueryId(ByVal projectId As Double, ByVal listOfFilterList As List(Of List(Of String))) As Guid

        Return LoadQueryAndRetrieveDataQueryId(projectId, listOfFilterList, Nothing)

    End Function
    Public Shared Function SaveFilters(ByVal UserID As String, ByVal projectId As String, ByVal listOfFilterList As List(Of String), ByVal Filters As String) As Guid
        Dim savefilter As String = ""
        Dim first As Boolean = True


        For Each filter As String In listOfFilterList
            If first Then
                savefilter += filter
                first = False
            Else
                savefilter += " ," + filter
            End If
        Next

        If savefilter = "" Then
            savefilter = "All Responses"
        End If

        Using connection As New SqlConnection(ConfigurationManager.ConnectionStrings("Website1DatabaseAuthentication").ConnectionString)

            connection.Open()

            Dim command As New SqlCommand
            command.Connection = connection
            command.CommandType = Data.CommandType.Text
            command.CommandText = String.Format("INSERT INTO IIReporting_SaveFilters VALUES ('" & projectId & "', '" & UserID & "', '" & savefilter & "','" + Filters + "',  getdate())")
            command.ExecuteNonQuery()

            connection.Close()

        End Using

    End Function


    Public Shared Function LoadQueryAndRetrieveDataQueryId(ByVal projectId As Double, ByVal listOfFilterList As List(Of List(Of String)), ByVal dedicatedTableName As String) As Guid

        Dim result As Guid = Guid.NewGuid

        Using connection As New SqlConnection(ConfigurationManager.ConnectionStrings("Website1DatabaseAuthentication").ConnectionString)

            connection.Open()

            Dim command As New SqlCommand
            command.Connection = connection
            command.CommandType = Data.CommandType.Text
            command.CommandText = String.Format("INSERT INTO IIReporting_DataQueries SELECT DISTINCT '{0}', LocationID" + GetCoreQueryPart(projectId, listOfFilterList, dedicatedTableName) + "; INSERT INTO IIReporting_DataQueries VALUES ('{0}', -1)", result.ToString())
            command.ExecuteNonQuery()

            connection.Close()

        End Using

        Return result

    End Function

    Private Shared Function GetCoreQueryPart(ByVal projectId As Double, ByVal listOfFilterList As List(Of List(Of String))) As String

        Return GetCoreQueryPart(projectId, listOfFilterList, Nothing)

    End Function

    Private Shared Function GetCoreQueryPart(ByVal projectId As Double, ByVal listOfFilterList As List(Of List(Of String)), ByVal dedicatedTableName As String) As String

        If dedicatedTableName Is Nothing Then

            Dim result As String = " FROM IIReporting_Data WHERE ProjectId = " + projectId.ToString()
            For Each filterList As List(Of String) In listOfFilterList
                result += " AND LocationID IN ( SELECT LocationID FROM IIReporting_Data WHERE ProjectId = " + projectId.ToString() + " AND ( 1 = 0 "
                For Each filter As String In filterList
                    result += " OR ( " + filter + " ) "
                Next
                result += " ) ) "
            Next

            Return result

        Else

            Dim result As String = " FROM " + dedicatedTableName + " WHERE 1 = 1 "
            For Each filterList As List(Of String) In listOfFilterList
                result += " AND ( 1 = 0 "
                For Each filter As String In filterList
                    Dim expression As New Regex("ColumnId = (\d+) AND Value (.+)")
                    Dim m As Match = expression.Match(filter)
                    result += " OR ( c" + m.Groups(1).Value + " " + m.Groups(2).Value + " ) "
                Next
                result += " ) "
            Next

            Return result

        End If

    End Function

    Public Shared Function GetCalculationsList(ByVal projectId As Double, ByVal dataQueryId As Guid, ByVal lowerPercentileMultiple As Double, ByVal upperPercentileMultiple As Double) As List(Of Calculations)

        Return GetCalculationsList(False, projectId, dataQueryId, lowerPercentileMultiple, upperPercentileMultiple, Nothing)

    End Function

    Public Shared Function GetCalculationsList(ByVal breakId As Integer) As List(Of Calculations)

        Return GetCalculationsList(True, Nothing, Nothing, Nothing, Nothing, breakId)

    End Function

    Private Shared Function GetCalculationsList(ByVal getPreCalculatedBreak As Boolean, ByVal projectId As Double, ByVal dataQueryId As Guid, ByVal lowerPercentileMultiple As Double, ByVal upperPercentileMultiple As Double, ByVal breakId As Integer) As List(Of Calculations)

        Dim result As New List(Of Calculations)

        Using connection As New SqlConnection(ConfigurationManager.ConnectionStrings("Website1DatabaseAuthentication").ConnectionString)

            connection.Open()

            Dim command As New SqlCommand
            command.Connection = connection
            command.CommandType = Data.CommandType.StoredProcedure
            command.CommandTimeout = 0

            If getPreCalculatedBreak Then
                command.CommandText = "IIReporting_PreCalculatedCalculationsGet"
                command.Parameters.Add(New SqlParameter("@BreakId", breakId))
            Else
                command.CommandText = "IIReporting_GetCalculations"
                command.Parameters.Add(New SqlParameter("@ProjectId", projectId))
                command.Parameters.Add(New SqlParameter("@DataQueryId", dataQueryId))
                command.Parameters.Add(New SqlParameter("@LowerPercentileMultiple", lowerPercentileMultiple))
                command.Parameters.Add(New SqlParameter("@UpperPercentileMultiple", upperPercentileMultiple))
            End If

            Dim reader As SqlDataReader = command.ExecuteReader()

            While reader.HasRows AndAlso reader.Read()

                Dim calculations As New Calculations()
                calculations.ColumnId = Convert.ToInt32(reader("ColumnId"))
                If Not IsDBNull(reader("ResponseCount")) Then
                    calculations.ResponseCount = Convert.ToDouble(reader("ResponseCount"))
                End If
                If Not IsDBNull(reader("ParticipantCount")) Then
                    calculations.ParticipantCount = Convert.ToDouble(reader("ParticipantCount"))
                End If
                If Not IsDBNull(reader("LargestParticipantResponseCount")) Then
                    calculations.LargestParticipantResponseCount = Convert.ToDouble(reader("LargestParticipantResponseCount"))
                End If
                If Not IsDBNull(reader("LargestParticipantResponseCountPercentage")) Then
                    calculations.LargestParticipantResponseCountPercentage = Convert.ToDouble(reader("LargestParticipantResponseCountPercentage"))
                End If
                If Not IsDBNull(reader("Mean")) Then
                    calculations.Mean = Convert.ToDouble(reader("Mean"))
                End If
                If Not IsDBNull(reader("Median")) Then
                    calculations.Median = Convert.ToDouble(reader("Median"))
                End If
                If Not IsDBNull(reader("LowerPercentile")) Then
                    calculations.LowerPercentile = Convert.ToDouble(reader("LowerPercentile"))
                End If
                If Not IsDBNull(reader("UpperPercentile")) Then
                    calculations.UpperPercentile = Convert.ToDouble(reader("UpperPercentile"))
                End If
                result.Add(calculations)

            End While

            connection.Close()

        End Using

        Return result

    End Function

    Public Shared Function GetFrequencyList(ByVal projectId As Double, ByVal dataQueryId As Guid) As List(Of Frequency)

        Return GetFrequencyList(False, projectId, dataQueryId, Nothing)

    End Function

    Public Shared Function GetFrequencyList(ByVal breakId As Integer) As List(Of Frequency)

        Return GetFrequencyList(True, Nothing, Nothing, breakId)

    End Function

    Private Shared Function GetFrequencyList(ByVal getPreCalculatedBreak As Boolean, ByVal projectId As Double, ByVal dataQueryId As Guid, ByVal breakId As Integer) As List(Of Frequency)

        Dim result As New List(Of Frequency)

        Using connection As New SqlConnection(ConfigurationManager.ConnectionStrings("Website1DatabaseAuthentication").ConnectionString)

            connection.Open()

            Dim command As New SqlCommand
            command.Connection = connection
            command.CommandType = Data.CommandType.StoredProcedure
            command.CommandTimeout = 0
            If getPreCalculatedBreak Then
                command.CommandText = "IIReporting_PreCalculatedFrequenciesGet"
                command.Parameters.Add(New SqlParameter("@BreakId", breakId))
            Else
                command.CommandText = "IIReporting_GetFrequencies"
                command.Parameters.Add(New SqlParameter("@ProjectId", projectId))
                command.Parameters.Add(New SqlParameter("@DataQueryId", dataQueryId))
            End If



            Dim reader As SqlDataReader = command.ExecuteReader()

            While reader.HasRows AndAlso reader.Read()

                Dim frequency As New Frequency()
                frequency.ColumnId = Convert.ToInt32(reader("ColumnId"))
                If Not IsDBNull(reader("ResponseCount")) Then
                    frequency.ResponseCount = Convert.ToDouble(reader("ResponseCount"))
                End If
                If Not IsDBNull(reader("ParticipantCount")) Then
                    frequency.ParticipantCount = Convert.ToDouble(reader("ParticipantCount"))
                End If
                If Not IsDBNull(reader("LargestParticipantResponseCount")) Then
                    frequency.LargestParticipantResponseCount = Convert.ToDouble(reader("LargestParticipantResponseCount"))
                End If
                If Not IsDBNull(reader("LargestParticipantResponseCountPercentage")) Then
                    frequency.LargestParticipantResponseCountPercentage = Convert.ToDouble(reader("LargestParticipantResponseCountPercentage"))
                End If
                If Not IsDBNull(reader("Value")) Then
                    frequency.Value = Convert.ToInt32(reader("Value"))
                End If
                If Not IsDBNull(reader("Count")) Then
                    frequency.Count = Convert.ToDouble(reader("Count"))
                End If
                If Not IsDBNull(reader("Percentage")) Then
                    frequency.Percentage = Convert.ToDouble(reader("Percentage"))
                End If

                result.Add(frequency)

            End While

            connection.Close()

        End Using

        Return result

    End Function

    Public Shared Function GetPreCalculatedBreakList(ByVal projectId As Double) As List(Of PreCalculatedBreak)

        Dim result As New List(Of PreCalculatedBreak)

        Using connection As New SqlConnection(ConfigurationManager.ConnectionStrings("Website1DatabaseAuthentication").ConnectionString)

            connection.Open()

            Dim command As New SqlCommand
            command.Connection = connection
            command.CommandType = Data.CommandType.StoredProcedure
            command.CommandText = "IIReporting_PreCalculatedBreaksGet"
            command.Parameters.Add(New SqlParameter("@ProjectId", projectId))

            Dim reader As SqlDataReader = command.ExecuteReader()

            While reader.HasRows AndAlso reader.Read()

                Dim break As New PreCalculatedBreak()
                break.Id = Convert.ToInt32(reader("Id"))
                break.ProjectId = Convert.ToDouble(reader("ProjectId"))
                break.Caption = reader("Caption").ToString()
                break.Filter = reader("Filter").ToString()

                result.Add(break)

            End While

            connection.Close()

        End Using

        Return result

    End Function

    Public Shared Sub InsertPreCalculatedBreak(ByVal break As PreCalculatedBreak)

        Using connection As New SqlConnection(ConfigurationManager.ConnectionStrings("Website1DatabaseAuthentication").ConnectionString)

            connection.Open()

            Dim command As New SqlCommand
            command.Connection = connection
            command.CommandType = Data.CommandType.StoredProcedure
            command.CommandText = "IIReporting_PreCalculatedBreaksInsert"

            Dim idParam As New SqlParameter() With {.ParameterName = "@Id", .Direction = Data.ParameterDirection.Output, .Size = 4}

            command.Parameters.Add(idParam)
            command.Parameters.Add(New SqlParameter("@ProjectId", break.ProjectId))
            command.Parameters.Add(New SqlParameter("@Caption", break.Caption))
            command.Parameters.Add(New SqlParameter("@Filter", break.Filter))
            command.ExecuteNonQuery()

            break.Id = Convert.ToInt32(idParam.Value)

            connection.Close()

        End Using

    End Sub

    Public Shared Sub InsertPreCalculatedCalculation(ByVal breakId As Integer, ByVal calculations As Calculations)

        Using connection As New SqlConnection(ConfigurationManager.ConnectionStrings("Website1DatabaseAuthentication").ConnectionString)

            connection.Open()

            Dim command As New SqlCommand
            command.Connection = connection
            command.CommandType = Data.CommandType.StoredProcedure
            command.CommandText = "IIReporting_PreCalculatedCalculationsInsert"

            command.Parameters.Add(New SqlParameter("@BreakId", breakId))
            command.Parameters.Add(New SqlParameter("@ColumnId", calculations.ColumnId))
            command.Parameters.Add(New SqlParameter("@ResponseCount", calculations.ResponseCount))
            command.Parameters.Add(New SqlParameter("@ParticipantCount", calculations.ParticipantCount))
            command.Parameters.Add(New SqlParameter("@LargestParticipantResponseCount", calculations.LargestParticipantResponseCount))
            command.Parameters.Add(New SqlParameter("@LargestParticipantResponseCountPercentage", calculations.LargestParticipantResponseCountPercentage))
            command.Parameters.Add(New SqlParameter("@Mean", calculations.Mean))
            command.Parameters.Add(New SqlParameter("@Median", calculations.Median))
            command.Parameters.Add(New SqlParameter("@LowerPercentile", calculations.LowerPercentile))
            command.Parameters.Add(New SqlParameter("@UpperPercentile", calculations.UpperPercentile))
            command.ExecuteNonQuery()

            connection.Close()

        End Using

    End Sub

    Public Shared Sub InsertPreCalculatedFrequency(ByVal breakId As Integer, ByVal frequency As Frequency)

        Using connection As New SqlConnection(ConfigurationManager.ConnectionStrings("Website1DatabaseAuthentication").ConnectionString)

            connection.Open()

            Dim command As New SqlCommand
            command.Connection = connection
            command.CommandType = Data.CommandType.StoredProcedure
            command.CommandText = "IIReporting_PreCalculatedFrequenciesInsert"

            command.Parameters.Add(New SqlParameter("@BreakId", breakId))
            command.Parameters.Add(New SqlParameter("@ColumnId", frequency.ColumnId))
            command.Parameters.Add(New SqlParameter("@ResponseCount", frequency.ResponseCount))
            command.Parameters.Add(New SqlParameter("@ParticipantCount", frequency.ParticipantCount))
            command.Parameters.Add(New SqlParameter("@LargestParticipantResponseCount", frequency.LargestParticipantResponseCount))
            command.Parameters.Add(New SqlParameter("@LargestParticipantResponseCountPercentage", frequency.LargestParticipantResponseCountPercentage))
            command.Parameters.Add(New SqlParameter("@Value", frequency.Value))
            command.Parameters.Add(New SqlParameter("@Count", frequency.Count))
            command.Parameters.Add(New SqlParameter("@Percentage", frequency.Percentage))
            command.ExecuteNonQuery()

            connection.Close()

        End Using

    End Sub

    Public Shared Sub DeletePreCalculatedData(ByVal projectId As Double)

        Using connection As New SqlConnection(ConfigurationManager.ConnectionStrings("Website1DatabaseAuthentication").ConnectionString)

            connection.Open()

            Dim command As New SqlCommand
            command.Connection = connection
            command.CommandType = Data.CommandType.StoredProcedure
            command.CommandText = "IIReporting_PreCalculatedDelete"

            command.Parameters.Add(New SqlParameter("@ProjectId", projectId))

            command.ExecuteNonQuery()

            connection.Close()

        End Using

    End Sub

End Class
