﻿Imports Microsoft.VisualBasic

Namespace DataAccess.IIReporting

    Public Class ReportCardItem
        Private _id As Integer
        Private _Type As Integer
        Private _projectid As Double
        Private _Direction As String
        Private _cvalue As Integer
        Private _itemText As String
        Private _orderIndex As Integer
        Private _FormatId As Integer
        Private _Prefix As String
        Private _Suffix As String

        Public Property CValue As Double
            Get
                Return _cvalue
            End Get
            Set(ByVal value As Double)
                _cvalue = value
            End Set
        End Property
        Public Property Direction As String
            Get
                Return _Direction
            End Get
            Set(ByVal value As String)
                _Direction = value
            End Set
        End Property


        Public Property ProjectId As Double
            Get
                Return _projectId
            End Get
            Set(ByVal value As Double)
                _projectId = value
            End Set
        End Property

        Public Property ItemText As String
            Get
                Return _itemText
            End Get
            Set(ByVal value As String)
                _itemText = value
            End Set
        End Property

        Public Property Prefix As String
            Get
                Return _Prefix
            End Get
            Set(ByVal value As String)
                _Prefix = value
            End Set
        End Property
        Public Property Suffix As String
            Get
                Return _Suffix
            End Get
            Set(ByVal value As String)
                _Suffix = value
            End Set
        End Property


        Public Property OrderIndex As Integer
            Get
                Return _orderIndex
            End Get
            Set(ByVal value As Integer)
                _orderIndex = value
            End Set
        End Property
        Public Property FormatId As Integer
            Get
                Return _FormatId
            End Get
            Set(ByVal value As Integer)
                _FormatId = value
            End Set
        End Property

        Public Property ID As Integer
            Get
                Return _id
            End Get
            Set(ByVal value As Integer)
                _id = value
            End Set
        End Property
        Public Property Type As Integer
            Get
                Return _Type
            End Get
            Set(ByVal value As Integer)
                _Type = value
            End Set
        End Property

    End Class

End Namespace


