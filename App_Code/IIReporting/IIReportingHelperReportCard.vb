﻿Imports DataAccess.IIReporting
Imports System.Collections.Generic
Imports System.Data.SqlClient
Imports System.Text.RegularExpressions

Public Class IIReportingHelperReportCard
    Public Shared Function GetPercentile(ByVal Break As Integer, ByVal Percentile As String, ByVal cvalue As Integer) As Double

        Dim result As Double = 0

        Using connection As New SqlConnection(ConfigurationManager.ConnectionStrings("Website1DatabaseAuthentication").ConnectionString)

            connection.Open()

            Dim command As New SqlCommand
            command.Connection = connection
            command.CommandType = Data.CommandType.Text
            command.CommandText = "Select LowerPercentile, UpperPercentile, Median, Mean from [IIReporting_PreCalculatedCalculations] where breakid = @BreakID and Columnid = @CValue"
            command.Parameters.Add(New SqlParameter("@BreakID", Break))
            command.Parameters.Add(New SqlParameter("@CValue", cvalue))
            Dim reader As SqlDataReader = command.ExecuteReader()

            While (reader.HasRows AndAlso reader.Read())
                If Percentile = "Upper" And Not IsDBNull(reader("UpperPercentile")) Then
                    result = Convert.ToDouble(reader("UpperPercentile"))
                ElseIf Percentile = "Lower" And Not IsDBNull(reader("LowerPercentile")) Then
                    result = Convert.ToDouble(reader("LowerPercentile"))
                ElseIf Percentile = "Middle" Then
                    result = Convert.ToDouble(reader("Median"))
                ElseIf Percentile = "Average" Then
                    result = Convert.ToDouble(reader("Mean"))
                End If
            End While
            reader.Close()

            connection.Close()

        End Using

        Return result

    End Function
    Public Shared Function GetReportItemList(ByVal projectId As Double) As List(Of ReportCardItem)

        Dim result As New List(Of ReportCardItem)

        Using connection As New SqlConnection(ConfigurationManager.ConnectionStrings("Website1DatabaseAuthentication").ConnectionString)

            connection.Open()

            Dim command As New SqlCommand
            command.Connection = connection
            command.CommandType = Data.CommandType.Text
            command.CommandText = "Select Id, Cvalue, Direction,Text,Projectid, Type, Orderindex, FormatId, Prefix, Suffix from ReportCardItems where projectid = @ProjectId order by orderindex"
            command.Parameters.Add(New SqlParameter("@ProjectId", projectId))

            Dim reader As SqlDataReader = command.ExecuteReader()

            While (reader.HasRows AndAlso reader.Read())
                Dim item As New ReportCardItem
                item.ID = reader("Id")
                If Not IsDBNull(reader("cvalue")) Then
                    item.CValue = Convert.ToInt32(reader("Cvalue"))
                End If
                If Not IsDBNull(reader("Direction")) Then
                    item.Direction = reader("Direction")
                End If
                item.ItemText = reader("Text")
                item.ProjectId = Convert.ToDouble(reader("Projectid"))
                item.OrderIndex = reader("Orderindex")
                If Not IsDBNull(reader("Type")) Then
                    item.Type = Convert.ToInt32(reader("Type"))
                End If
                If Not IsDBNull(reader("FormatId")) Then
                    item.FormatId = Convert.ToInt32(reader("FormatId"))
                End If
                If Not IsDBNull(reader("Prefix")) Then
                    item.Prefix = reader("Prefix")
                End If
                If Not IsDBNull(reader("Suffix")) Then
                    item.Suffix = reader("Suffix")
                End If
                result.Add(item)
            End While
            reader.Close()

            connection.Close()

        End Using

        Return result

    End Function

    Public Shared Function GetCalculationsListYF(ByVal projectid As Double, ByVal LocationID As Integer) As List(Of Calculations)

        Dim result As New List(Of Calculations)

        Using connection As New SqlConnection(ConfigurationManager.ConnectionStrings("Website1DatabaseAuthentication").ConnectionString)

            connection.Open()

            Dim command As New SqlCommand
            command.Connection = connection
            command.CommandType = Data.CommandType.StoredProcedure
            command.CommandTimeout = 0

            command.CommandType = Data.CommandType.Text
            command.CommandText = "Select ColumnId, 1 as ResponseCount, 1 as  ParticipantCount,1 as LargestParticipantResponseCount, 1 as LargestParticipantResponseCountPercentage, Value as Mean, Value as Median, Value as LowerPercentile, Value as UpperPercentile From IIReporting_Data "
            command.CommandText += " Where Projectid = " + projectid.ToString() + " AND LocationID = " + LocationID.ToString()


            Dim reader As SqlDataReader = command.ExecuteReader()

            While reader.HasRows AndAlso reader.Read()

                Dim calculations As New Calculations()
                calculations.ColumnId = Convert.ToInt32(reader("ColumnId"))
                If Not IsDBNull(reader("ResponseCount")) Then
                    calculations.ResponseCount = Convert.ToDouble(reader("ResponseCount"))
                End If
                If Not IsDBNull(reader("ParticipantCount")) Then
                    calculations.ParticipantCount = Convert.ToDouble(reader("ParticipantCount"))
                End If
                If Not IsDBNull(reader("LargestParticipantResponseCount")) Then
                    calculations.LargestParticipantResponseCount = Convert.ToDouble(reader("LargestParticipantResponseCount"))
                End If
                If Not IsDBNull(reader("LargestParticipantResponseCountPercentage")) Then
                    calculations.LargestParticipantResponseCountPercentage = Convert.ToDouble(reader("LargestParticipantResponseCountPercentage"))
                End If
                If Not IsDBNull(reader("Mean")) Then
                    calculations.Mean = Convert.ToDouble(reader("Mean"))
                End If
                If Not IsDBNull(reader("Median")) Then
                    calculations.Median = Convert.ToDouble(reader("Median"))
                End If
                If Not IsDBNull(reader("LowerPercentile")) Then
                    calculations.LowerPercentile = Convert.ToDouble(reader("LowerPercentile"))
                End If
                If Not IsDBNull(reader("UpperPercentile")) Then
                    calculations.UpperPercentile = Convert.ToDouble(reader("UpperPercentile"))
                End If
                result.Add(calculations)

            End While

            connection.Close()

        End Using

        Return result

    End Function

    Public Shared Function GetPreCalculatedBreakList(ByVal projectId As Double) As List(Of PreCalculatedBreak)

        Dim result As New List(Of PreCalculatedBreak)

        Using connection As New SqlConnection(ConfigurationManager.ConnectionStrings("Website1DatabaseAuthentication").ConnectionString)

            connection.Open()

            Dim command As New SqlCommand
            command.Connection = connection
            command.CommandType = Data.CommandType.StoredProcedure
            command.CommandText = "IIReporting_PreCalculatedBreaksGet"
            command.Parameters.Add(New SqlParameter("@ProjectId", projectId))

            Dim reader As SqlDataReader = command.ExecuteReader()

            While reader.HasRows AndAlso reader.Read()

                Dim break As New PreCalculatedBreak()
                break.Id = Convert.ToInt32(reader("Id"))
                break.ProjectId = Convert.ToDouble(reader("ProjectId"))
                break.Caption = reader("Caption").ToString()
                break.Filter = reader("Filter").ToString()

                result.Add(break)

            End While

            connection.Close()

        End Using

        Return result

    End Function




End Class
