﻿Imports Microsoft.VisualBasic

Namespace DataAccess.IIReporting

    <Serializable()> Public Class Calculations

        Private _columnId As Integer
        Private _JobID As Integer
        Private _JobTitle As String
        Private _responseCount As Double
        Private _participantCount As Double
        Private _largestParticipantResponseCount As Double
        Private _largestParticipantResponseCountPercentage As Double
        Private _mean As Double?
        Private _median As Double?
        Private _lowerPercentile As Double?
        Private _upperPercentile As Double?
        Private _tenPercentile As Double?
        Private _nintyPercentile As Double?
        Private _projectid As Double
        Private _max As Double
        Private _min As Double

        Public Property ColumnId As Integer
            Get
                Return _columnId
            End Get
            Set(ByVal value As Integer)
                _columnId = value
            End Set
        End Property
        Public Property Job_Title As String
            Get
                Return _JobTitle
            End Get
            Set(ByVal value As String)
                _JobTitle = value
            End Set
        End Property
        Public Property Job_ID As Integer
            Get
                Return _JobID
            End Get
            Set(ByVal value As Integer)
                _JobID = value
            End Set
        End Property

        Public Property ResponseCount As Double
            Get
                Return _responseCount
            End Get
            Set(ByVal value As Double)
                _responseCount = value
            End Set
        End Property
        Public Property ProjectId As Double
            Get
                Return _projectid
            End Get
            Set(ByVal value As Double)
                _projectid = value
            End Set
        End Property

        Public Property ParticipantCount As Double
            Get
                Return _participantCount
            End Get
            Set(ByVal value As Double)
                _participantCount = value
            End Set
        End Property

        Public Property LargestParticipantResponseCount As Double
            Get
                Return _largestParticipantResponseCount
            End Get
            Set(ByVal value As Double)
                _largestParticipantResponseCount = value
            End Set
        End Property

        Public Property LargestParticipantResponseCountPercentage As Double
            Get
                Return _largestParticipantResponseCountPercentage
            End Get
            Set(ByVal value As Double)
                _largestParticipantResponseCountPercentage = value
            End Set
        End Property

        Public Property Mean As Double?
            Get
                Return _mean
            End Get
            Set(ByVal value As Double?)
                _mean = value
            End Set
        End Property

        Public Property Median As Double?
            Get
                Return _median
            End Get
            Set(ByVal value As Double?)
                _median = value
            End Set
        End Property

        Public Property LowerPercentile As Double?
            Get
                Return _lowerPercentile
            End Get
            Set(ByVal value As Double?)
                _lowerPercentile = value
            End Set
        End Property

        Public Property UpperPercentile As Double?
            Get
                Return _upperPercentile
            End Get
            Set(ByVal value As Double?)
                _upperPercentile = value
            End Set
        End Property

        Public Property tenPercentile As Double?
            Get
                Return _tenPercentile
            End Get
            Set(ByVal value As Double?)
                _tenPercentile = value
            End Set
        End Property

        Public Property nintyPercentile As Double?
            Get
                Return _nintyPercentile
            End Get
            Set(ByVal value As Double?)
                _nintyPercentile = value
            End Set
        End Property
        Public Property Max As Double
            Get
                Return _max
            End Get
            Set(ByVal value As Double)
                _max = value
            End Set
        End Property
        Public Property Min As Double
            Get
                Return _min
            End Get
            Set(ByVal value As Double)
                _min = value
            End Set
        End Property
    End Class

End Namespace


