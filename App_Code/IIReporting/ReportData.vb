﻿Imports Microsoft.VisualBasic
Imports System.Collections.Generic
Imports System.Linq

Namespace DataAccess.IIReporting

    <Serializable()> Public MustInherit Class ReportData

        Private _caption As String
        Private _calculationsList As List(Of Calculations)
        Private _frequencyList As List(Of Frequency)

        Public Property Caption As String
            Get
                Return _caption
            End Get
            Set(ByVal value As String)
                _caption = value
            End Set
        End Property

        Public Property CalculationsList As List(Of Calculations)
            Get
                Return _calculationsList
            End Get
            Set(ByVal value As List(Of Calculations))
                _calculationsList = value
            End Set
        End Property

        Public Property FrequencyList As List(Of Frequency)
            Get
                Return _frequencyList
            End Get
            Set(ByVal value As List(Of Frequency))
                _frequencyList = value
            End Set
        End Property

        Public Function GetCalculationsByColumnId(ByVal columnId As Integer) As Calculations

            Return (From calcs In _calculationsList Where calcs.ColumnId = columnId Select calcs).FirstOrDefault()

        End Function

        Public Function GetFrequencyByColumnIdAndValue(ByVal columnId As Integer, ByVal value As Integer) As Frequency

            Return (From freq In _frequencyList Where freq.ColumnId = columnId AndAlso freq.Value = value Select freq).FirstOrDefault()

        End Function

        Public Function GetFirstFrequencyByColumnIdOnly(ByVal columnId As Integer) As Frequency

            Return (From freq In _frequencyList Where freq.ColumnId = columnId Select freq).FirstOrDefault()

        End Function

        Public Overridable Function DoesColumnMeetMinimumReportingRequirements(ByVal columnId As Integer, ByVal isPercentile As Boolean) As Boolean

            Dim calcs As Calculations = GetCalculationsByColumnId(columnId)
            Dim firstFreq As Frequency = GetFirstFrequencyByColumnIdOnly(columnId)
            Return (calcs IsNot Nothing AndAlso calcs.ParticipantCount >= If(isPercentile, 7, 5) AndAlso calcs.LargestParticipantResponseCountPercentage < 40) _
                OrElse (firstFreq IsNot Nothing AndAlso firstFreq.ParticipantCount >= If(isPercentile, 7, 5) AndAlso firstFreq.LargestParticipantResponseCountPercentage < 40)

        End Function

        Public Function FormatDataValue(ByVal value As Double?, ByVal formatId As Integer, ByVal prefixChar As String, ByVal suffixChar As String, ByVal columnId As Integer, ByVal isPercentile As Boolean) As String

            If (Not value.HasValue) OrElse (Not DoesColumnMeetMinimumReportingRequirements(columnId, isPercentile)) Then
                Return "*"
            End If

            Dim result As String

            If formatId = 0 Then
                result = value.Value.ToString("F0")
            ElseIf formatId = 1 Then
                result = value.Value.ToString("F1")
            ElseIf formatId = 2 Then
                result = value.Value.ToString("F")
            ElseIf formatId = 3 Then
                result = value.Value.ToString("N0")
            ElseIf formatId = 4 Then
                result = value.Value.ToString("N1")
            ElseIf formatId = 5 Then
                result = value.Value.ToString("N2")
            Else
                Throw New ApplicationException(String.Format("Format Id: {0} not handled.", formatId.ToString()))
            End If

            result = prefixChar + " " + result + " " + suffixChar
            result = result.Trim()

            Return result

        End Function


    End Class

End Namespace

