﻿
Imports Models

Partial Class Site
    Inherits System.Web.UI.MasterPage

    Protected Sub Page_Init(sender As Object, e As EventArgs) Handles Me.Load
        If Request.QueryString("c") IsNot Nothing Then
            Session("Client") = Request.QueryString("c")
        End If
        If Session("Client") IsNot Nothing Then
            If Session("Client").ToString.ToLower = "upload" Then
                Session("Client") = "ii"
            End If
            lnk_styleSheet.Href = String.Format("Content/{0}.css", Session("Client"))
            img_logo.Src = String.Format("Content/images/logo-{0}.png", Session("Client"))
        Else
            Session("Client") = "ii"
        End If
    End Sub
End Class

