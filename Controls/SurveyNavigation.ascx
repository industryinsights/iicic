﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="SurveyNavigation.ascx.vb" Inherits="Controls_SurveyNavigation" %>
<div class="row icon-menu ">
            <div class="col-md-3 text-center icon-border ">
                <div class="row">
                    <asp:ImageButton ID="imgbtn_Profile" runat="server" ImageUrl="~/Content/images/icon-operations.png" CssClass="survey-icon" NavigateUrl="~/Surveys/Comp/Profile.aspx" />
                </div>
                <p>
                    <asp:HyperLink ID="lb_Profile" runat="server" NavigateUrl="~/Surveys/Comp/Profile.aspx">Profile</asp:HyperLink>
                    <br />
                    <em>
                        <asp:Label ID="lbl_statusProfile" runat="server" Text="Not started"></asp:Label>
                    </em>
                </p>
            </div>
            <div class="col-md-3 text-center icon-border">
                <div class="row">
                    <asp:ImageButton ID="imgbtn_Recruiting" runat="server" ImageUrl="~/Content/images/icon-icr.png" CssClass="survey-icon" NavigateUrl="~/Surveys/Comp/Recruiting.aspx" />
                </div>
                <p>
                    <asp:HyperLink ID="lb_Recruiting" runat="server" NavigateUrl="~/Surveys/Comp/Recruiting.aspx">Recruiting</asp:HyperLink>
                    <br />
                    <em>
                        <asp:Label ID="lbl_statusRecruiting" runat="server" Text="Not started"></asp:Label>
                    </em>
                </p>
            </div>
            <div class="col-md-3 text-center icon-border">
                <div class="row">
                    <asp:ImageButton ID="imgbtn_Compensation" runat="server" ImageUrl="~/Content/images/icon-compensation.png" CssClass="survey-icon" NavigateUrl="~/Surveys/Comp/Compensation.aspx" />
                </div>
                <p>
                    <asp:HyperLink ID="lb_Compensation" runat="server" NavigateUrl="~/Surveys/Comp/Compensation.aspx">Compensation</asp:HyperLink>
                    <br />
                    <em>
                        <asp:Label ID="lbl_statusCompensation" runat="server" Text="Not started"></asp:Label>
                    </em>
                </p>
            </div>
            <div class="col-md-3 text-center icon-border">
                <div class="row">
                    <asp:ImageButton ID="imgbtn_CompByJob" runat="server" ImageUrl="~/Content/images/icon-compensation.png" CssClass="survey-icon" NavigateUrl="~/Surveys/Comp/CompByJob.aspx" />
                </div>
                <p>
                    <asp:HyperLink ID="lb_CompByJob" runat="server" NavigateUrl="~/Surveys/Comp/CompByJob.aspx">CompByJob</asp:HyperLink>
                    <br />
                    <br />
                    <em>
                        <asp:Label ID="lbl_statusCompByJob" runat="server" Text="Not started"></asp:Label>
                    </em>
                </p>
            </div>
        </div>
        <div class="row icon-menu ">
            <div class="col-md-3 text-center icon-border ">
                <div class="row">
                    <asp:ImageButton ID="imgbtn_Benefits" runat="server" ImageUrl="~/Content/images/icon-compensation.png" CssClass="survey-icon" NavigateUrl="~/Surveys/Comp/Benefits.aspx" />
                </div>
                <p>
                    <asp:HyperLink ID="lb_Benefits" runat="server" NavigateUrl="~/Surveys/Comp/Benefits.aspx">Benefits</asp:HyperLink>
                    <br />
                    <em>
                        <asp:Label ID="lbl_statusBenefits" runat="server" Text="Not started"></asp:Label>
                    </em>
                </p>
            </div>
            <div class="col-md-3 text-center icon-border">
                <div class="row">
                    <asp:ImageButton ID="imgbtn_Sales" runat="server" ImageUrl="~/Content/images/icon-incomestatement.png" CssClass="survey-icon" NavigateUrl="~/Surveys/Comp/Sales.aspx" />
                </div>
                <p>
                    <asp:HyperLink ID="lb_Sales" runat="server" NavigateUrl="~/Surveys/Comp/Sales.aspx">Sales</asp:HyperLink>
                    <br />
                    <em>
                        <asp:Label ID="lbl_statusSales" runat="server" Text="Not started"></asp:Label>
                    </em>
                </p>
            </div>
            <div class="col-md-3 text-center icon-border">
                <div class="row">
                    <asp:ImageButton ID="imgbtn_BranchCompBen" runat="server" ImageUrl="~/Content/images/icon-compensation.png" CssClass="survey-icon" NavigateUrl="~/Surveys/Comp/BranchCompBen.aspx" />
                </div>
                <p>
                    <asp:HyperLink ID="lb_BranchCompBen" runat="server" NavigateUrl="~/Surveys/Comp/BranchCompBen.aspx">BranchCompBen</asp:HyperLink>
                    <br />
                    <em class="small">(Complete for each location)</em>
                    <br />
                    <em>
                        <asp:Label ID="lbl_statusBranchCompBen" runat="server" Text="Not started"></asp:Label>
                    </em>
                </p>
            </div>
            <div class="col-md-3 text-center icon-border">
                <div class="row">
                    &nbsp;
                </div>
                <p>
                    &nbsp;
                </p>
            </div>
        </div>

        <div class="row">
            <div class="container">
                <asp:Label ID="lbl_auditSuccess" runat="server" Text="Your audit is complete.  No problems were found." Visible="False" CssClass="text-success" Font-Size="Large"></asp:Label>
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="text-danger" ValidationGroup="audit" HeaderText="&lt;h3&gt;Audit Results&lt;/h3&gt;" />
            </div>
        </div>
        <div class="jumbotron">
            <table class="table table-bordered table-hover centered">
                <tr>
                    <td></td>
                    <td class="text-bold text-center">Status</td>
                </tr>
                <tr>
                    <td class="text-center">
                        <asp:Button ID="btn_audit" runat="server" CssClass="btn btn-lg btn-primary disabled" Text="Audit Survey" ValidationGroup="audit" Enabled="False" Width="185px" />
                    </td>
                    <td class="text-center">
                        <em>
                            <asp:Label ID="lbl_audit" runat="server" CssClass="text-muted" Text="Not complete"></asp:Label></em>
                    </td>
                </tr>
                <tr>
                    <td class="text-center">
                        <asp:Button ID="btn_submit" runat="server" CssClass="btn btn-lg btn-primary disabled" Text="Submit Survey" Enabled="False" Width="185px" />
                    </td>
                    <td class="text-center">
                        <em>
                            <asp:Label ID="lbl_submit" runat="server" CssClass="text-muted" Text="Not complete"></asp:Label>
                            <br />
                            <asp:Label ID="lbl_deadline" runat="server" CssClass="text-muted small"></asp:Label></em>
                    </td>
                </tr>
            </table>
        </div>

    </div>
<hr />
