﻿
Imports Models

Partial Class Controls_SurveyNavigation
    Inherits System.Web.UI.UserControl
    Public Event OperationsClicked As ImageClickEventHandler

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        'If Not IsPostBack Then
        ResetLabels()
        Dim LocationID As Integer = Session("LocationID")
        Using db As New IICICEntities
            Dim survey As New CompSurvey
            survey = (From s In db.CompSurveys Where s.LocationID = LocationID And s.SurveyYear = Now.Year).FirstOrDefault
            If survey IsNot Nothing Then
                If survey.section1Complete IsNot Nothing Then
                    If survey.section1Complete = True Then
                        lbl_statusProfile.Text = "Complete"
                        lbl_statusProfile.CssClass = "text-success"
                    Else
                        lbl_statusProfile.Text = "In progress"
                        lbl_statusProfile.CssClass = "text-info"
                    End If
                End If
                If survey.section2Complete IsNot Nothing Then
                    If survey.section2Complete = True Then
                        lbl_statusRecruiting.Text = "Complete"
                        lbl_statusRecruiting.CssClass = "text-success"
                    Else
                        lbl_statusRecruiting.Text = "In progress"
                        lbl_statusRecruiting.CssClass = "text-info"
                    End If
                End If
                If survey.section3Complete IsNot Nothing Then
                    If survey.section3Complete = True Then
                        lbl_statusCompensation.Text = "Complete"
                        lbl_statusCompensation.CssClass = "text-success"
                    Else
                        lbl_statusCompensation.Text = "In progress"
                        lbl_statusCompensation.CssClass = "text-info"
                    End If
                End If
                If survey.section4Complete IsNot Nothing Then
                    If survey.section4Complete = True Then
                        lbl_statusCompByJob.Text = "Complete"
                        lbl_statusCompByJob.CssClass = "text-success"
                    Else
                        lbl_statusCompByJob.Text = "In progress"
                        lbl_statusCompByJob.CssClass = "text-info"
                    End If
                End If
                If survey.section5Complete IsNot Nothing Then
                    If survey.section5Complete = True Then
                        lbl_statusBenefits.Text = "Complete"
                        lbl_statusBenefits.CssClass = "text-success"
                    Else
                        lbl_statusBenefits.Text = "In progress"
                        lbl_statusBenefits.CssClass = "text-info"
                    End If
                End If
                If survey.section6Complete IsNot Nothing Then
                    If survey.section6Complete = True Then
                        lbl_statusSales.Text = "Complete"
                        lbl_statusSales.CssClass = "text-success"
                    Else
                        lbl_statusSales.Text = "In progress"
                        lbl_statusSales.CssClass = "text-info"
                    End If
                End If
            End If
        End Using
        'End If


    End Sub
    Sub ResetLabels()
        lbl_statusProfile.Text = "Not Started"
        lbl_statusProfile.CssClass = "text-muted"

        lbl_statusRecruiting.Text = "Not Started"
        lbl_statusRecruiting.CssClass = "text-muted"

        lbl_statusCompensation.Text = "Not Started"
        lbl_statusCompensation.CssClass = "text-muted"

        lbl_statusCompByJob.Text = "Not Started"
        lbl_statusCompByJob.CssClass = "text-muted"

        lbl_statusBenefits.Text = "Not Started"
        lbl_statusBenefits.CssClass = "text-muted"

        lbl_statusSales.Text = "Not Started"
        lbl_statusSales.CssClass = "text-muted"

        lbl_statusBranchCompBen.Text = "Not Started"
        lbl_statusBranchCompBen.CssClass = "text-muted"
    End Sub
End Class
